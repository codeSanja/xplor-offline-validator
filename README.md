##Install and dev

install node 0.12.7

run 
	npm install electron-prebuilt -g
	
run 
	npm install gulp -g

run 
	npm install gulp -bower
	

	
dev tips:
to run the app do 
	npm start

after some changes are made to the css run 
	gulp css
	
	
In order to create .exe file first install electron-packager
	npm i electron-packager -g
then run
	electron-packager ./ xplor-parser-validator --platform=win32 --arch=x64 --version=0.33.0
	
	
Links to some libraries used in project
https://github.com/maxogden/electron-packager
http://electron.atom.io/
http://gulpjs.com/

https://bitbucket.org/acambashmh/ebook-offline-validator/src