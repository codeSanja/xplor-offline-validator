/**
 * Created by DugganJ on 13/10/2015.
 */
var controllers = angular.module('ValidatorApp.Controllers', []);

controllers.controller('mainCtrl', function ($scope, $sce, packageContentService, CONFIG) {
    var self = this;
    self.loadEbook = false;
    self.data = null;
    self.loading = false;
    self.ebookPath = CONFIG.ebookPath;
    self.folderPath = CONFIG.folderPath;
    self.manifestPath = CONFIG.manifestPath;
    self.sections = [];
    self.errors = [];
    self.selectedTab = 'SECTIONS';

    self.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    };

    self.selectTab = function (tab) {
        self.selectedTab = tab;
    };

    self.listPackageDetails = function () {
        startProcessing();
        self.sections = [];

        packageContentService.getManifestDetails(self.manifestPath).then(function (response) {
            var manifestDetails = response.data;

            var rceTarLocation = manifestDetails[0].package_details.download_uri.server_url + manifestDetails[0].package_details.download_uri.relative_url;
            var xPlorTarLocation = manifestDetails[1].package_details.download_uri.server_url + manifestDetails[1].package_details.download_uri.relative_url;

            var rceTmp = rceTarLocation.split('/');
            var rceTarName = rceTmp[rceTmp.length - 1];

            var xPlotTmp = xPlorTarLocation.split('/');
            var xPlorTarName = xPlotTmp[xPlotTmp.length - 1];

            console.log("rceTarLocation, rceTarName: ");
            console.log(rceTarLocation, rceTarName);

            packageContentService.loadPackageContent(rceTarLocation, rceTarName).then(function () {
                packageContentService.loadPackageContent(xPlorTarLocation, xPlorTarName).then(function () {
                    console.log('RCE and Xplor are undacked.');
                    self.magazineUrl = manifestDetails[1].package_details.target_path.relative_url;
                    self.fullUrl = self.ebookPath + '/latest/index.html#/?book=' + self.magazineUrl;

                    self.loading = false;
                    self.loadEbook = true;
                }).catch(function (e) {
                    handleError(e);
                    self.loading = false;
                });
            }).catch(function (e) {
                handleError(e);
                self.loading = false;
            });
        }).catch(function (e) {
            handleError(e);
            self.loading = false;
        });
    };

    function handleError(e) {
        console.log(e.data);
        self.errors.push(e.data);
    };

    function startProcessing() {
        self.loading = true;
        self.loadEbook = false;
        self.errors = [];
    };

    self.loadTarContent = function (i) {
        startProcessing();

        packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(function () {
            self.loadEbook = true;
            self.sections[i].loaded = true;
            self.sections[i].failed = false;
        }).catch(function (e) {
            handleError(e);
            self.loading = false;
            self.sections[i].failed = true;
            self.loadEbook = false;
        }).finally(function () {
            self.loading = false;
        });
    };

    self.loadAllTarContent = function () {
        startProcessing();

        //reset all content to unloaded and with no error
        angular.forEach(self.sections, function (section) {
            section.loaded = false;
            section.failed = false;
        });

        i = 0;

        var success = function () {
            self.sections[i].loaded = true;
            self.sections[i].failed = false;
            loadNextTar();
        }

        var failure = function (e) {
            handleError(e);
            self.sections[i].failed = true;
            loadNextTar();
        }

        var loadNextTar = function () {
            i++;
            if (self.sections[i]) {
                packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(success).catch(failure);
            } else {
                self.loadEbook = true;
                self.loading = false;
            }
        };

        packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(success).catch(failure);
    };
});