//angular
var app = angular.module('ValidatorApp',
								['ValidatorApp.Controllers',
								'ValidatorApp.Services',
                                'ValidatorApp.Constants']);

app.filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);