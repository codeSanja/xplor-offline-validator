/**
 * Created by DugganJ on 13/10/2015.
 */
var appConstants = angular.module('ValidatorApp.Constants', []);

appConstants.constant('CONFIG', {
    ebookPath: "http://localhost:4567",
    folderPath: 'C:/Users/mandics/Desktop/rceTest/',
    manifestPath: 'C:/Users/mandics/Desktop/rceTest/mapping_manifest.json'
});