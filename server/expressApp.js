/**
 * Created by DugganJ on 13/10/2015.
 */
var express = require('express');
var q = require('q');
var path = require('path');
var contentManagerHelper = require('./contentManagerHelper');
var contentManagerService = require('./contentManagerService');

//express setup
var expressApp = express();
expressApp.use(express.static(path.join(__dirname, './untar')));
expressApp.use(express.static( path.join(__dirname, './ebookShell')));

expressApp.get('/listPackageDetails', function(req, res) {
     q.fcall(function () {
        var magazineUrl = contentManagerService.listPackageDetails(req.query.folderPath);
        res.send(magazineUrl);
    }).catch(function (err) {
         console.log(err);
         res.status(400).send(err);
    });
});

expressApp.get('/removeUntarContent', function(req, res) {
    q.fcall(function(){
        return contentManagerService.removeUntarContent();
    }).then(function () {
        console.log('sucessfully removed untar content');
        res.send();
    }).catch(function (err) {
        console.log(err);
        res.status(400).send(err);
    });
});


expressApp.get('/getManifestDetails', function(req, res) {
    q.fcall(function(){
        return contentManagerService.getManifestDetails(req.query.manifestPath);
    }).then(function (manifestDetails) {
        console.log('manifest details are sent');
        console.log(manifestDetails);
        res.send(manifestDetails);
    }).catch(function (err) {
        console.log(err);
        res.status(400).send(err);
    });
});


expressApp.get('/loadPackageContent', function(req, res) {
    var fileName = req.query.fileName;
    var tarPath = req.query.folderPath;
    q.fcall(function() {
        var folderName = fileName.substring(0, fileName.length-4);
        return contentManagerHelper.untar(tarPath, folderName);
    }).then(function () {
            console.log('sucessfully untarred file');
            res.send();
    }).catch(function (err) {
        console.log(err);
        res.status(400).send("Error loading tar: " + tarPath);
    });
});

var server = expressApp.listen(4567, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('app listening at http://%s:%s', host, port);
});