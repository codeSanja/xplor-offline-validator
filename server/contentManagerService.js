/**
 * Created by DugganJ on 14/10/2015.
 */
var fs = require('fs-extra');
var q = require('q');
var exec = require('child_process').exec;
var path = require('path');
var download = require('url-download');
var untarFolderPath = path.join(__dirname, './untar');

function listPackageDetails(folderPath) {
    var filePath = path.resolve(path.join(folderPath, 'mapping_manifest.json'));
    var sectionManifest = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
    var magazineUrl = sectionManifest.download_packages.downloads[1].package_details.target_path.book.relative_url;

    return magazineUrl;
}


function getManifestDetails(manifestPath) {
    var filePath = path.resolve(manifestPath);
    var mappingManifest = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
    return mappingManifest.download_packages.downloads;
}

function removeUntarContent() {
    var defer = q.defer();
    var untarFolderExists = fs.existsSync(untarFolderPath);
    if (!untarFolderExists) {
        fs.mkdirSync(untarFolderPath);
        return true;
    } else {
        exec('RMDIR ' + untarFolderPath + '  /S /Q', function (err, stdout, stderr) {
            if (err) {
                return defer.reject(err);
            }
            fs.mkdir(untarFolderPath, function () {
                defer.resolve(true);
            });

        });
    }

    return defer.promise;
}

module.exports.listPackageDetails = listPackageDetails;
module.exports.removeUntarContent = removeUntarContent;
module.exports.getManifestDetails = getManifestDetails;