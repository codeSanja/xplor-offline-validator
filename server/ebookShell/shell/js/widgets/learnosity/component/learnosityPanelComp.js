(function($, undefined) {
    $.widget("magic.learnosityPanelComp", $.magic.magicwidget, {
        options : {
            
        },

        member : {
           
        },

        _init : function() {
           
        },

        _create : function() {
        	var objThis = this;
        	
        	 $( document ).bind( 'qaPanelDataLoadComplete', function ()
        	 {
        	 	  $( "#learnosityPanel #learnosityPanelCloseBtn" ).trigger( "click" );
        	 });
        	 
        	 $( document ).bind( 'repositionLearnosityPanelUp', function ()
        	 {
    	 		  $( "#learnosityPanel" ).css( "bottom", "40px" );
        	 });
        	 
        	 $( document ).bind( 'repositionLearnosityPanelDown', function ()
        	 {
    	 		  $( "#learnosityPanel" ).css( "bottom", "0px" );
        	 });
        	 
        	 $( document ).bind( 'updateLearnosityViewSize', function ()
        	 {
        	 	 objThis.updateViewSize();
        	 });
        },
        
        updateViewSize : function ()
		{
			$( "#learnosityPanel" ).css( "height", "auto" );
			var _height = parseInt( EPubConfig.Learnosity_PanelHeight );
	        if( isNaN( _height ) || _height < 35 || _height > 100 )
	        {
	          	_height = 50;
	        }
			$( "#learnosityPanelScrollableDiv" )
				.height( (window.height * ( _height / 100)) - 
													$( "#lnsQuestionFooter" ).height() - $( "#learnosityPanelHeader" ).height() );
			
			if (navigator.userAgent.match(/(ipad)/i)) 
            {
                $( "#learnosityPanelScrollableDiv iframe" ).height($( "#learnosityPanelScrollableDiv iframe" ).contents().height() + 50 ); 
            }
            else
            {
               $( "#learnosityPanelScrollableDiv iframe" ).height($( "#learnosityPanelScrollableDiv" ).height() - 50 );
            }
			
			$( "#learnosityPanelScrollableDiv iframe" ).width( $( "#learnosityPanel" ).width() );
			$( "#learnosityPanelScrollableDiv iframe" ).css( "max-width", $( "#learnosityPanel" ).width() );
			
			if( navigator.userAgent.match(/(ipad)/i) )
			{
				$("#learnosityPanelScrollableDiv").css("overflow", "auto");
				$("#learnosityPanelScrollableDiv").css("-webkit-overflow-scrolling", "touch");
				$("#learnosityPanelScrollableDiv").css("width", "100%");
			}
		}
		
    });
})(jQuery);
