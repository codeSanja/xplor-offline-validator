/**
 *
 * This class displays students list based on classes.
 * Name of students can be displayed either by First or Last name
 * User can search, sort the list based on requirement.
 * Clicking on a student name will load its data( note, highlight etc ) on the book.
 *
 */
var isMyClassClicked = false;
function StudentList() {
	
	var _self = this;
	var _sharedStudentsArr = [];
    var _studentListArr = []; // holds full JSON
    var _searchStudentListArr = []; // holds only students name
    var _studentPanel = $("#studentPanel");
    var _dropDownOptions = $(_studentPanel.find("#studentPanel-dd-options")[0]);
    var _expandedItemId = null;
    var _scrollViewRef = null;
    var _searchedStudentsArr = null;
    var _sortBy = "firstname";
    var _strLoader = '<div id="tempLoaderRightPanel" style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';
	var _studentPanelScroll = null;
	var _studentSearchPanelScroll = null;
	var selectedClassElementId = "";
	var _tempSelectedStudents = [];
	var _tempChanges = false;
    // Set event name on the basis of browser
    if (!($.browser.msie && $.browser.version == 10) && !isWinRT && $.browser.version != 11) {
        var eventName = "input propertychange";
    } else {
        var eventName = "keyup";
    }

    // Bind window resize event
    $(window).bind("resize", function () {
        _self.updateViewSize(500);
    });

    _scrollViewRef = $("#bookContentContainer").data('scrollviewcomp');
    
    $(_scrollViewRef).bind("closeOpenedPanel", function () {
        if (_studentPanel.css("display") == "block") {
            $(".teacherStudentArrow").removeClass("teacherStudentArrowExpanded");
            $(".teacherStudentArrow").removeClass("teacherArrowUp");
            if (_dropDownOptions.css("display") == "block") {
                _dropDownOptions.css("display", "none");
            }
            _studentPanel.stop(true, true).slideUp(500);
        }

        if (_dropDownOptions.css("display") == "block") {
            _dropDownOptions.css("display", "none");
        }
    });

    $(_studentPanel).bind('click', function (e) {
        e.preventDefault();
        if (_dropDownOptions.css("display") == "block") {
            _dropDownOptions.css("display", "none");
        }
    });
    $("#studentListDoneBtn").bind("click", function (){
    	_tempChanges = false;
    	_tempSelectedStudents = [];
    	HotkeyManager.setPopupFocus($( "#stickyNoteContainer" ));
    	var studentPanel = ( _studentPanel.find("#searchStudentList").css("display") == "block" ) ?
    	 _studentPanel.find("#searchStudentList") : _studentPanel.find("#studentList");
    	 //console.log( _tempSelectedStudents, " Student List Done btn click");
    	if( GlobalModel.currentAnnotationObject )
    	{
			GlobalModel.currentAnnotationObject.tempTeachersSharedWith = {};
			var selectedItem = studentPanel.find(".studentNameCheckBoxSelected").next("div.studentName");
			GlobalModel.currentAnnotationObject.tempTeachersSharedWith.results = [];
			for( var i = 0; i < selectedItem.length; i++ )
			{
				var obj = {};
				obj.platformid = $( selectedItem[ i ] ).attr( "platformid" );
				GlobalModel.currentAnnotationObject.tempTeachersSharedWith.results.push( obj );
			}
    	}
    	else // new note
    	{
    		GlobalModel.NewNoteSelectedStudents = [];
    		var selectedItem = studentPanel.find(".studentNameCheckBoxSelected").next("div.studentName");
			for( var i = 0; i < selectedItem.length; i++ )
			{
				var obj = {};
				obj.platformid = $( selectedItem[ i ] ).attr( "platformid" );
				GlobalModel.NewNoteSelectedStudents.push( obj );
			}
    	}
    	_tempSelectedStudents = [];
    	_self.disableEnableSingleComponent( $("#stickyNoteContainer #stickynoteSaveBtn"), "enable" );
    	_self.showHidePanel();
    });
    
     $("#studentListCancelBtn").bind("click", function (){
     	_tempChanges = false;
     	_tempSelectedStudents = [];
     	HotkeyManager.setPopupFocus($( "#stickyNoteContainer" ));
     	_self.showHidePanel();
     } );
     
     $("#studentPanelSelectAll .studentPanelSelectAllcheckbox").unbind("click").bind("click", function (){
     	_self.selectAllCheckboxClickHandler();
     });
     
     $("#studentPanelSelectAll span").unbind("click").bind("click", function (){
     	_self.selectAllCheckboxClickHandler();
     });

    $("#studentSearchInputCancelBtn").bind('click', function (e) {
        e.preventDefault();
        $(this).hide();
        _studentPanel.find('#searchInputBox').val("");
        // Show class wise student list and hide searched student list
        _studentPanel.find('#studentList').show();
        _studentPanel.find('#searchStudentList').hide();
        _self.populateClassStudent(_studentListArr);
        _self.selectUnselectSelectAllCheckbox();
        _self.enableDisableDoneBtn();
    });
    
    if( EPubConfig.TE_Path == undefined || EPubConfig.TE_Path.trim() == "" )
    {
    	// disable _studentPanel.find(".studentPanelTEBtn")
    	_studentPanel.find(".studentPanelTEBtn").attr( "disabled", true );
        _studentPanel.find(".studentPanelTEBtn").addClass( "ui-disabled" ).attr( "aria-disabled", true );
    }
        
    this.selectAllCheckboxClickHandler = function()
    {
    	if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
	    {
    		_tempSelectedStudents = [];
    		_tempChanges = true;
    		$(".studentPanelSelectAllcheckbox").toggleClass( "studentPanelSelectAllcheckboxSelected" );
			var studentPanel = ( _studentPanel.find("#searchStudentList").css("display") == "block" ) ?
	    	 _studentPanel.find("#searchStudentList") : _studentPanel.find("#studentList");
	    	if( $(".studentPanelSelectAllcheckbox").hasClass( "studentPanelSelectAllcheckboxSelected" ) )
    		{
    			studentPanel.find( ".studentNameCheckBox" ).addClass( "studentNameCheckBoxSelected" );
    			studentPanel.find( ".classNameCheckBox" ).addClass( "classNameCheckBoxSelected" );
    		}
    		else
    		{
    			studentPanel.find( ".studentNameCheckBox" ).removeClass( "studentNameCheckBoxSelected" );
    			studentPanel.find( ".classNameCheckBox" ).removeClass( "classNameCheckBoxSelected" );
    		}
			var selectedItem = studentPanel.find(".studentNameCheckBoxSelected").next("div.studentName");
			for( var i = 0; i < selectedItem.length; i++ )
			{
				var obj = {};
				obj.platformid = $( selectedItem[ i ] ).attr( "platformid" );
				_tempSelectedStudents.push( obj );
			}
			_self.enableDisableDoneBtn();
			//_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "enable" );
	    }
    };

    
    /*
     * Load data and call method to show hide the student panel
     */
    this.showPanel = function () {
        var self = this;        
        self.updateHeightOfPanel();
        if (_studentListArr.length === 0) {
            setTimeout(function () {
                self.loadJSON();
            }, 500);
            // Show preloader till the time not loaded and parsed 
            _studentPanel.find('#addedStudentList').html(_strLoader);
        }
		isMyClassClicked = true;
        $(_scrollViewRef).trigger('closeOpenedPanel');
        self.showHidePanel();
        if (_studentListArr.length != 0) {
        	self.showHideIcons();
        }
    };

	this.showHideIcons = function (){
    	var self = this;
    	if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" &&
    		$("#stickyNoteContainer").css("display") == "block" )
    	{
    		$(".studentNameCheckBox").css("display", "block");
    		$(".classNameCheckBox").css("display", "block");
    		$("#studentListDoneCancelPanel").css("display", "block");
    		//$("#studentPanelSelectAll").css("display", "block");
    		self.setStateofIcons();
    	}
    	else
    	{
    		$(".studentNameCheckBox").css("display", "none");
    		$(".classNameCheckBox").css("display", "none");
    		$("#studentListDoneCancelPanel").css("display", "none");
    		//$("#studentPanelSelectAll").css("display", "none");
    	}
    	self.showHideSelectAllButton();
    };
    
    this.showHideSelectAllButton = function ()
    {
    	if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" &&
    		$("#stickyNoteContainer").css("display") == "block" )
    	{
    		if( $( "#searchStudentList" ).css( "display" ) == "block" )
    		{
    			if( $( "#searchStudentList .depth2-studentList" ).length > 0 )
    			{
    				$("#studentPanelSelectAll").css("display", "block");
    			}
    			else
    			{
    				$("#studentPanelSelectAll").css("display", "none");
    			}
    		}
    		else
    		{
    			if( $( "#studentList #addedStudentList .depth2-studentList" ).length > 0 )
    			{
    				$("#studentPanelSelectAll").css("display", "block");
    			}
    			else
    			{
    				$("#studentPanelSelectAll").css("display", "none");
    			}
    		}
    	}
    	else
    	{
    		$("#studentPanelSelectAll").css("display", "none");
    	}
    };

    /*
     * Show hide the student panel
     */
    this.showHidePanel = function () {
    	if( $( "#searchInputBox").val().length > 0 )
    	{
    		$( "#studentSearchInputCancelBtn" ).trigger( "click" );
    	}
        if (_studentPanel.css("display") == "block") {
            if (_dropDownOptions.css("display") == "block") {
                _dropDownOptions.css("display", "none");
            }
            if ($(".teacherStudentArrow").hasClass("teacherArrowDown")) {
                $(".teacherStudentArrow").removeClass("teacherArrowUp");
            } else {
                $(".teacherStudentArrow").removeClass("teacherStudentArrowExpanded");
            }
			_studentPanel.removeClass("forcefullyIncreaseZIndexofStudentPanel");
			$("#divTomakeOnlyStudentPanelInteractive").remove();
            _studentPanel.stop(true, true).slideUp(500);
        } else {

            if ($(".teacherStudentArrow").hasClass("teacherArrowDown")) {
                $(".teacherStudentArrow").addClass("teacherArrowUp");
            } else {
                $(".teacherStudentArrow").addClass("teacherStudentArrowExpanded");
            }
            
            _studentPanel.stop(true, true).slideDown(500,function (){
            	_self.updateViewSize(0);
            });
        }
    };

    // TODO this code will move to ServiceManager
    /*
     * Load student list JSON and call method to populate student panel
     */
    this.loadJSON = function () {

        //$(".studentSearchInputBoxContainer").hide();
        //$("#studentPanelSortBy").hide();
		var self = this;
        var obj = {
            success: function (objData) {

                if (objData.value) {
                	GlobalModel.StudentListArr = objData.value;
                    _studentListArr = objData.value;
                    _self.studentListSort("firstname");
                    _self.populateClassStudent(_studentListArr);
                    $(".studentSearchInputBoxContainer").show();
                    $("#studentPanelSortBy").show();
                }
            },
            error: function (strMsg) {
                if (strMsg == undefined)
                {
                    _studentPanel.find('#addedStudentList').html('<div style="padding-top: 50px;">' +
                    GlobalModel.localizationData.ERROR_IN_FETCHING_DATA + '</div>');
                }
                else
                {
                    _studentPanel.find('#addedStudentList').html('<div style="padding-top: 50px;">' + strMsg + '</div>');
                }
            }
        };
        ServiceManager.getStudentList(obj);
        
    };

    // Bind search box event
    _studentPanel.find('#searchInputBox').unbind(eventName).bind(eventName, function (event) {
    	
		if(_studentSearchPanelScroll != null)
        {
        	_studentSearchPanelScroll.destroy();
        	_studentSearchPanelScroll = null;
        }
        var searchText = $(this).val().trim();  // Text to search
        var searchedstudentList = "<div id='searchList' style='float:left;'>";           // Searched student html string
        _searchedStudentsArr = null;         // Searched student array

        var studentDivHeight = _studentPanel.find('.depth2-studentList').height() + 2; //2 for border height
        var studentListHeight = _studentPanel.find('#studentList').height();       
		var studentPanel = ( _studentPanel.find("#searchStudentList").css("display") == "block" ) ?
		    	 _studentPanel.find("#searchStudentList") : _studentPanel.find("#studentList");
		    	 
	    var selectedItem = studentPanel.find(".studentNameCheckBoxSelected").next("div.studentName");
	   
	    if( studentPanel.find( ".depth2-studentList" ).length > 0 )
    	{
	    	_tempSelectedStudents = [];
	    	for( var i = 0; i < selectedItem.length; i++ )
			{
				var obj = {};
				obj.platformid = $( selectedItem[ i ] ).attr( "platformid" );
				_tempSelectedStudents.push( obj );
			}
    	}
	   // console.log( _tempSelectedStudents, "Search Input box" );
        if (searchText.length > 0) {
			_studentPanel.find('#studentSearchInputCancelBtn').show();
            _searchedStudentsArr = _searchStudentListArr.filter(function (obj) {
                if ((obj.FullNameFL.toLowerCase().indexOf(searchText.toLowerCase()) > -1) ||
                	(obj.FullNameLF.toLowerCase().indexOf(searchText.toLowerCase()) > -1)) {
                    return obj;
                }

            });


            // Get total height for student list
            var totalHeight = _searchedStudentsArr.length * studentDivHeight;

            _self.studentListSort(_sortBy, "search");            

            searchedstudentList += _self.getStudentsListHtml(_searchedStudentsArr);
			searchedstudentList += '</div>';
			
			// Check data found or not
			if (_searchedStudentsArr.length > 0) {
                _studentPanel.find('#searchStudentList').html(searchedstudentList);
                if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" &&
                	$("#stickyNoteContainer").css("display") == "block" )
                {
					_studentPanel.find('#studentPanelSelectAll').css("display","block");                	
                }
            }
            else {
 				_studentPanel.find('#studentPanelSelectAll').css("display","none");
                _studentPanel.find('#searchStudentList').html(GlobalModel.localizationData["SEARCH_NO_RESULTS"]);
            }
            
            if (totalHeight > studentListHeight) {
				
            	_studentSearchPanelScroll = new iScroll( 'searchStudentList', {
                            scrollbarClass: 'myScrollbar'
                  } );
            } else {
				
                _studentPanel.find('#searchStudentList').css("overflow-y", "hidden");
            }

            // Hide class wise student list and show searched student list
            _studentPanel.find('#studentList').hide();
            _studentPanel.find('#searchStudentList').show();


            if (_dropDownOptions.css("display") == "block") {
                _dropDownOptions.css("display", "none");
            }


            // Bind click event for student name
            $("#searchStudentList").find('.depth2-studentList').unbind("click").bind("click", function (e) {
				if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" && $("#stickyNoteContainer").css("display") == "block" )
	        	{
	        		//var target = $( e.currentTarget ).find(".studentNameCheckBox");
			    	//target.toggleClass( "studentNameCheckBoxSelected" );
	        		return;
	        	}
            	if (ServiceManager.DPLive && ServiceManager.isServiceAuthenticated) {
        		$("#mainShowroom").css("opacity", "0.2");
            	$("#appPreloaderContainer").css("display","block");
        	}
                _self.studentNameClick($.trim($(this).text()), $(this).children("div.studentName").attr("platformId"));
                _studentPanel.stop(true, true).slideUp(500, function () {
                    $(".teacherStudentArrow").removeClass("teacherArrowUp");
                });
            });
			
			if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" &&
				$("#stickyNoteContainer").css("display") == "block" )
	    	{
	    		 _studentPanel.find('#searchStudentList .classNameCheckBox').css("display", "block");
	    		 _studentPanel.find('#searchStudentList .studentNameCheckBox').css("display", "block");
	    	}
	    	else
	    	{
	    		 _studentPanel.find('#searchStudentList .classNameCheckBox').css("display", "none");
	    		 _studentPanel.find('#searchStudentList .studentNameCheckBox').css("display", "none");
	    	}
            _self.setStateofIcons();
            _self.addEventOnIcons();
	
			_self.updateViewSize(100);

        } else {
			_studentPanel.find('#studentSearchInputCancelBtn').hide();
            // Show class wise student list and hide searched student list
            _studentPanel.find('#studentList').show();
            _studentPanel.find('#searchStudentList').hide();

            _self.populateClassStudent(_studentListArr);

        }
        _self.selectUnselectSelectAllCheckbox();
        _self.enableDisableDoneBtn();
    });
    
    this.enableDisableDoneBtn = function ()
    {
    	var studentPanel = ( _studentPanel.find("#searchStudentList").css("display") == "block" ) ?
		    	 			 _studentPanel.find("#searchStudentList") : _studentPanel.find("#studentList");
    	if( _tempChanges )
    	{
    		if( studentPanel.find( ".depth2-studentList" ).length > 0 )
    		{
    			_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "enable" );
    		}
    		else
    		{
    			_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "disable" );
    		}
    	}
    	else
    	{
    		_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "disable" );
    	}
    }


    this.studentListSort = function (option, strSearchType) {
        var arrSort = [];
        var i;
        var strNameFilter;
        switch ($.trim(option.toLowerCase())) {
            case "firstname":
                strNameFilter = "firstName";
                break;

            case "lastname":
                strNameFilter = "lastName";
                break;
        }
        if (strSearchType == "search") {
            arrSort = _searchedStudentsArr;
            _searchedStudentsArr = [];
            arrSort.sort(function (a, b) {
                var nameA = a[strNameFilter] != undefined ? a[strNameFilter].toLowerCase() : "",
                    nameB = b[strNameFilter] != undefined ? b[strNameFilter].toLowerCase() : "";
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;
            });
            _searchedStudentsArr = arrSort;
        } else {
            for (i = 0; i < _studentListArr.length; i++) {
                if (_studentListArr[i].students) {
                    if (_studentListArr[i].students.length > 1) {
                        arrSort = _studentListArr[i].students;
                        _studentListArr[i].students = [];
                        arrSort.sort(function (a, b) {
                            var nameA = a[strNameFilter] != undefined ? a[strNameFilter].toLowerCase() : "",
                                nameB = b[strNameFilter] != undefined ? b[strNameFilter].toLowerCase() : "";
                            if (nameA < nameB) //sort string ascending
                                return -1;
                            if (nameA > nameB)
                                return 1;
                            return 0;
                        });
                        _studentListArr[i].students = arrSort;
                    }
                } else {
                    arrSort = [_studentListArr[i].students];
                    _studentListArr[i].students = arrSort;
                }
            }
        }
        
    };

    /*
     * Handles Click on Drop down button
     */
    $(_studentPanel.find("#studentPanel-dd-clickable")).unbind("click").bind("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        /*
         * If dropdown list is not visible then display it
         */
        if (_dropDownOptions.css("display") == "none") {

            _dropDownOptions.css("display", "block");

            _expandedItemId = _studentPanel.find(".classListExpanded").attr("id");

            /*
             * handles click of an item on dropdown list.
             * sorts the bookmark list accordingly and then hide itself.
             */
            $(_studentPanel.find("#studentPanel-dd-options li")).unbind("click").bind("click", function (e) {

                var oldSortBy=_sortBy; 

                $(_studentPanel.find("#studentPanel-dd-selected")).html(e.currentTarget.innerText);

                _sortBy = $($(this).find('a')[0]).attr('value');
                var strSortByString = $.trim($($(this).find('a')[0]).text());
                $(_studentPanel.find("#studentPanel-dd-selected")).html(strSortByString);

                if (oldSortBy != _sortBy)
                {
                    _studentPanel.find('#addedStudentList').html(_strLoader);

                    if ($("#searchStudentList").css("display") == "none") {
                      
                        if (typeof (_sortBy) != "undefined") {
                            
                                _self.studentListSort(_sortBy);

                                _self.populateClassStudent(_studentListArr);

                                if (_expandedItemId != undefined) {
                                    _studentPanel.find('#' + _expandedItemId).trigger("click");
                                }

                            }
                     
                    } else {
                        var studentDivHeight = _studentPanel.find('.depth2-studentList').height() + 2; //2 for border height
                        var studentListHeight = _studentPanel.find('#studentList').height();

                        if (_searchedStudentsArr.length > 0) {
                            _self.studentListSort(_sortBy, "search");
                            var searchedstudentList = "";
                            // Get total height for student list
                            var totalHeight = _searchedStudentsArr.length * studentDivHeight;

                            searchedstudentList = _self.getStudentsListHtml(_searchedStudentsArr);                            

                            if (totalHeight > studentListHeight) {

                                _studentPanel.find('#searchStudentList').css("overflow-y", "scroll");
                            } else {

                                _studentPanel.find('#searchStudentList').css("overflow-y", "hidden");
                            }

                            // Hide class wise student list and show searched student list
                            _studentPanel.find('#studentList').hide();
                            _studentPanel.find('#searchStudentList').show();


                            if (_dropDownOptions.css("display") == "block") {
                                _dropDownOptions.css("display", "none");
                            }

                            // Check data found or not
                            if (_searchedStudentsArr.length > 0) {
                                _studentPanel.find('#searchStudentList').html(searchedstudentList);
                            }

                            // Bind click event for student name
                            $("#searchStudentList").find('.depth2-studentList').unbind("click").bind("click", function (e) {                                
							if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" && $("#stickyNoteContainer").css("display") == "block" )
					        	{
					        		_self.setStateofIcons();
					        		//var target = $( e.currentTarget ).find(".studentNameCheckBox");
							    	//target.toggleClass( "studentNameCheckBoxSelected" );
					        		return;
					        	} 
                                _self.studentNameClick($.trim($(this).text()), $(this).children("div.studentName").attr("platformId"));
                                _studentPanel.stop(true, true).slideUp(500, function () {
                                    $(".teacherStudentArrow").removeClass("teacherStudentArrowExpanded");

                                });
                            });
                            _self.showHideIcons();
							_self.addEventOnIcons();
							
                        }
                    }
                }

                _self.selectUnselectSelectAllCheckbox();
                _dropDownOptions.css("display", "none");

            });


        } else // Hide the drop down list
        {
            _dropDownOptions.css("display", "none");
        }
    });


    /*
     * Populates student panel class and student list
     */
    this.populateClassStudent = function (data) {       
        var studentList = "";
        _searchStudentListArr = [];
        var firstName = "";
        var lastName = "";
        for (var i = 0; i < data.length; i++) {
            studentList += '<div class="classList" id ="' + data[i].id + '"><div class="classNameCheckBox"></div><div class="className">' + data[i].classname + '</div><div class="classArrow"></div></div><div class="depth2-expandable">';
            if (data[i].students) {
                studentList += _self.getStudentsListHtml(data[i].students);

                for (var j = 0; j < data[i].students.length; j++) {
                    if (data[i].students[j]) {

                        firstName = data[i].students[j].firstName || "";
                        lastName = data[i].students[j].lastName || "";
                        if( data[i].students[j].middleName && data[i].students[j].middleName.length > 0 )
		                {
		                	firstName += " " + data[i].students[j].middleName + ".";
		                }

                        data[i].students[j].FullNameFL =$.trim(firstName+" "+lastName);
                        data[i].students[j].FullNameLF = $.trim(lastName + " " + firstName);

                        _searchStudentListArr.push(data[i].students[j]);
						if(sessionStorage.selectedStudentData)
						{
							var objSelectedStData = JSON.parse(sessionStorage.selectedStudentData);
							if(objSelectedStData.refId == data[i].students[j].refId)
							{
								selectedClassElementId = data[i].id;
							} 
							//selectedClassElementId = 
						}
                    }

                }
            }
            studentList += '</div>';

        }

        _studentPanel.find('#addedStudentList').html(studentList);
        this.showHideIcons();
        _studentPanel.find('#addedStudentList').css("float", "left");;
     
        
        var classDivHeight = _studentPanel.find('.classList').height() + 2; //2 for border height
        var studentDivHeight = _studentPanel.find('.depth2-studentList').height() + 2; //2 for border height
       
       if(_studentPanelScroll == null)
       {
	       	_studentPanelScroll = new iScroll('studentList', {
	            scrollbarClass: 'myScrollbar'
	        });
       }
       this.updateViewSize(100);


        // Bind click event for class name
        _studentPanel.find('.classList').unbind("click").bind("click", function (e) {

            var displayState = $(this).next('div').css("display");

            var totalHeight = data.length * classDivHeight + $(this).next('div').find('.depth2-studentList').length * studentDivHeight;
           

            var previousExpandedDivHeight = $(".classListExpanded").next('div').find('.depth2-studentList').length * studentDivHeight;            
                
            //$(this).next('div').height(_studentPanel.find('#studentList').height() - data.length * classDivHeight);

            if ($(this).hasClass("classListExpanded")) {
                //_studentPanel.find('#addedStudentList').height(totalHeight - previousExpandedDivHeight)
            }
            else {
                //_studentPanel.find('#addedStudentList').height(totalHeight);
            }            

            _studentPanel.find(".classArrow").removeClass("classArrowExpanded");

            _studentPanel.find('.depth2-expandable').stop(true, true).slideUp(500, function () {

            });

            $(".classList").removeClass("classListExpanded");

            if (displayState != "block") {

                $(this).find(".classArrow").addClass("classArrowExpanded");

                $(this).next('div').stop(true, true).slideDown(500, function () {
                    _self.updateViewSize(100);
                });
                $(this).addClass("classListExpanded");


            } else {

                $(this).find(".classArrow").removeClass("classArrowExpanded");
                $(this).next('div').stop(true, true).slideUp(500, function () {
                    _self.updateViewSize(100);
                });

            }

            if (_dropDownOptions.css("display") == "block") {
                _dropDownOptions.css("display", "none");
            }
			if( _studentPanelScroll != null )
			{
				_studentPanelScroll.refresh();				
			}
			
        });

        // Bind click event for student name
        _studentPanel.find('.depth2-studentList').bind("click", function (e) {
        	if( !GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" && $("#stickyNoteContainer").css("display") == "block" )
        	{
        		return;
        	}
        	if (ServiceManager.DPLive && ServiceManager.isServiceAuthenticated) {
        		$("#mainShowroom").css("opacity", "0.2");
            	$("#appPreloaderContainer").css("display","block");
        	}
            _self.studentNameClick($.trim($(this).text()), $(this).children("div.studentName").attr("platformid"));
        });


        this.updateViewSize(100);
		

		if(selectedClassElementId != "")
		{
			setTimeout(function(){
				_studentPanel.find('#' + selectedClassElementId).trigger('click');
			}, 1);
		}
		
		this.addEventOnIcons();
		
    };
    
    this.addEventOnIcons = function ()
    {
	    _studentPanel.find( ".depth2-studentList" ).bind( "click", function ( event )
	    {
	    	if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
	    	{
	    		//_tempSelectedStudents = [];
	    		var target = $( event.currentTarget ).find("div.studentNameCheckBox");
		    	target.toggleClass( "studentNameCheckBoxSelected" );
		    	var platformId = $( event.currentTarget ).find("div.studentName").attr("platformid");
		    	if( target.hasClass( "studentNameCheckBoxSelected" ) )
		    	{
		    		var obj = {};
					obj.platformid = platformId;
					_tempSelectedStudents.push( obj );
		    	}
		    	else
		    	{
		    		for( var i = 0; i < _tempSelectedStudents.length; i++ )
		    		{
		    			if( _tempSelectedStudents[ i ].platformid == platformId )
		    			{
		    				_tempSelectedStudents.splice( i, 1 );
		    			}
		    		}
		    	}

		    	if( _studentPanel.find("#searchStudentList").css("display") != "block" )
		    	{
		    		var classListParent = $( event.currentTarget ).parent().prev("div.classList");
					var currentTargetParent = $( event.currentTarget ).parent();
					if( currentTargetParent.find( "div.studentNameCheckBox" ).length == currentTargetParent.find( "div.studentNameCheckBoxSelected" ).length )
					{
						classListParent.find( ".classNameCheckBox" ).addClass("classNameCheckBoxSelected");
					}
					else
					{
						classListParent.find( ".classNameCheckBox" ).removeClass("classNameCheckBoxSelected");
					}
		    	}
		    	
				_tempChanges = true;
				_self.selectUnselectSelectAllCheckbox();
				_self.enableDisableDoneBtn();
				//_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "enable" );
	    	}
	    } );
    
	    _studentPanel.find( ".classNameCheckBox" ).unbind( "click" ).bind( "click", function ( event )
	    {
	    	_tempSelectedStudents = [];
	    	_tempChanges = true;
	    	var targetClass = $( event.target );
	    	targetClass.toggleClass( "classNameCheckBoxSelected" );
	    	event.stopPropagation();
	    	if( targetClass.hasClass( "classNameCheckBoxSelected" ) )
	    	{
	    		targetClass.parent().next(".depth2-expandable").find(".depth2-studentList .studentNameCheckBox").addClass( "studentNameCheckBoxSelected" );
	    	}
	    	else
	    	{
	    		targetClass.parent().next(".depth2-expandable").find(".depth2-studentList .studentNameCheckBox").removeClass( "studentNameCheckBoxSelected" );
	    	}
	    	
	    	var studentPanel = ( _studentPanel.find("#searchStudentList").css("display") == "block" ) ?
		    	 _studentPanel.find("#searchStudentList") : _studentPanel.find("#studentList");
		    	 
		    _self.selectUnselectSelectAllCheckbox();
		    
		    var selectedItem = studentPanel.find(".studentNameCheckBoxSelected").next("div.studentName");
			for( var i = 0; i < selectedItem.length; i++ )
			{
				var obj = {};
				obj.platformid = $( selectedItem[ i ] ).attr( "platformid" );
				_tempSelectedStudents.push( obj );
			}
			_self.enableDisableDoneBtn();
	    	//_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "enable" );
	    } );
    };
    
    this.selectUnselectSelectAllCheckbox = function ()
    {
    	if( _studentPanel.find("#searchStudentList").css("display") != "block" )
	    {
	    	if( _studentPanel.find("#studentList").find( ".classNameCheckBox").length == _studentPanel.find("#studentList").find( ".classNameCheckBoxSelected").length )
			{
				$(".studentPanelSelectAllcheckbox").addClass("studentPanelSelectAllcheckboxSelected");
			}
			else
			{
				$(".studentPanelSelectAllcheckbox").removeClass("studentPanelSelectAllcheckboxSelected");
			}
	    }
	    else
    	{
	    	
    		if( _studentPanel.find("#searchStudentList").find( ".studentNameCheckBox").length == _studentPanel.find("#searchStudentList").find( ".studentNameCheckBoxSelected").length )
    		{
    			$(".studentPanelSelectAllcheckbox").addClass("studentPanelSelectAllcheckboxSelected");
    		}
    		else
			{
				$(".studentPanelSelectAllcheckbox").removeClass("studentPanelSelectAllcheckboxSelected");
			}
    	}
    };
    
    this.disableEnableSingleComponent = function ( component, operation )
	{
	    if( operation == "disable" )
        {
            component.attr( "disabled", true );
            component.addClass( "ui-disabled" ).attr( "aria-disabled", true );
        }
        else
        {
            component.attr( "disabled", false );
            component.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
        }
	};
    
    this.setStateofIcons = function()
    {
    	if( $("#stickyNoteContainer").css("display") == "none" )
    	{
    		return;
    	}
    	$(".classNameCheckBoxSelected").removeClass("classNameCheckBoxSelected");
    	$(".studentNameCheckBoxSelected").removeClass("studentNameCheckBoxSelected");
    	$(".studentPanelSelectAllcheckbox").removeClass("studentPanelSelectAllcheckboxSelected");
    	_sharedStudentsArr = [];
    	var arr = [];
    	if( _tempSelectedStudents.length == 0 && !_tempChanges )
    	{
    		if( GlobalModel.currentAnnotationObject )
	    	{
	    		if( GlobalModel.currentAnnotationObject.tempTeachersSharedWith && GlobalModel.currentAnnotationObject.tempTeachersSharedWith.results )
	    		{
	    			arr = GlobalModel.currentAnnotationObject.tempTeachersSharedWith.results;
	    		}
	    		else if( GlobalModel.currentAnnotationObject.teachersSharedWith && GlobalModel.currentAnnotationObject.teachersSharedWith.results )
	    		{
	    			arr = GlobalModel.currentAnnotationObject.teachersSharedWith.results;
	    		}
	    	}
	    	else
	    	{
	    		arr = GlobalModel.NewNoteSelectedStudents;
	    	}
    	}
    	else
    	{
    		arr = _tempSelectedStudents;
    	}
    	
    	//if( GlobalModel.currentAnnotationObject )
    	//{
    		if( arr.length == undefined )
    		{
    			var tempArr = [];
    			tempArr.push( arr );
    		}
    		else
    		{
    			var tempArr = arr;
    		}
    		if( tempArr.length > 0 )
    		{
    			if( tempArr[ 0 ].platformid )
	    		{
	    			var refList = null;
	    			if( _studentPanel.find("#searchStudentList").css("display") == "block" )
	    			{
	    				refList = _studentPanel.find("#searchStudentList");
	    			}
	    			else
	    			{
	    				refList = _studentPanel.find("#addedStudentList");
	    			}
	    			for( var i = 0; i < tempArr.length; i++ )
	    			{
	    				var item = refList.find("[platformid=" + tempArr[ i ].platformid  + "]");
	    				item.prev("div").addClass( "studentNameCheckBoxSelected" );
						var studentNameParent = item.parent().parent();
						if( studentNameParent.find(".studentNameCheckBox").length == studentNameParent.find(".studentNameCheckBoxSelected").length )
						{
							studentNameParent.prev("div.classList").find(".classNameCheckBox").addClass("classNameCheckBoxSelected");
						}
	    			}
	    		}
	    		else
	    		{
	    			if( _studentListArr.length == 0 )
	    			{
	    				// do nothing
	    			}
	    			else
	    			{
	    				var isSearchModeVisible = false;
	    				$("#searchInputBox").hide();
	                    $("#studentPanelSortBy").hide();
	                    if( $("#searchInputBox").val().length > 0 )
	                    {
	                    	isSearchModeVisible = true;
	                    }
	                    else
	                    {
	                    	isSearchModeVisible = false;
	                    }
	                    $("#searchStudentList").hide();
	                    $("#studentList").hide();
	                    $("#tempLoaderRightPanel").remove();
	                    _studentPanel.append( _strLoader );
	    				ServiceManager.getSharedStudents( GlobalModel.currentAnnotationObject.annotationID, function ( data ){
		    				for( var i = 0; i < tempArr.length; i++ )
		    				{
		    					for( var j = 0; j < data.length; j++ )
		    					{
		    						if( tempArr[ i ].annotator_id == data[ j ].annotator_id )
		    						{
		    							tempArr[ i ].platformid = data[ j ].platform_user_id;
		    							var item = $(".depth2-studentList").find("[platformid=" + tempArr[ i ].platformid  + "]");
		    							var studentNameParent = item.parent().parent();
		    							item.prev("div").addClass( "studentNameCheckBoxSelected" );
		    							if( studentNameParent.find(".studentNameCheckBox").length == studentNameParent.find(".studentNameCheckBoxSelected").length )
		    							{
		    								studentNameParent.prev("div.classList").find(".classNameCheckBox").addClass("classNameCheckBoxSelected");
		    							}
		    							break;
		    						}
		    					}
		    				}
		    				$("#searchInputBox").show();
	                    	$("#studentPanelSortBy").show();
		    				if( isSearchModeVisible )
		    				{
		    					$("#searchStudentList").show();
		    				}
		    				else
		    				{
		    					$("#studentList").show();
		    				}
		    				$("#tempLoaderRightPanel").remove();
		    				_self.selectUnselectSelectAllCheckbox();
	    				},
	    				function ( error )
	    				{
	    					$("#searchInputBox").show();
	                    	$("#studentPanelSortBy").show();
		    				if( isSearchModeVisible )
		    				{
		    					$("#searchStudentList").show();
		    				}
		    				else
		    				{
		    					$("#studentList").show();
		    				}
		    				$("#tempLoaderRightPanel").remove();
	    				} );
	    			}
	    			
	    		}
    		}
    		_self.selectUnselectSelectAllCheckbox();
    	//}
    		_self.enableDisableDoneBtn();
		//_self.disableEnableSingleComponent( _studentPanel.find("#studentListDoneBtn"), "disable" );
    };


    this.updateViewSize = function (timer) {
        var self = this;
        setTimeout(function () {
            self.updateHeightOfPanel();
        }, timer);

    };

    this.updateHeightOfPanel = function () {
        var topheight = _studentPanel.find('#studentListTopbar').height() + $("#navigationBar").height() + 2 +
        				_studentPanel.find(".studentPanelNavigationBtn").height()
        				+ ( ( _studentPanel.find("#studentPanelSelectAll").css("display") == "block" ) ? 
        				_studentPanel.find("#studentPanelSelectAll").height() : 0 )
						+ ( ( _studentPanel.find("#studentListDoneCancelPanel").css("display") == "block" ) ? 
						_studentPanel.find("#studentListDoneCancelPanel").height() : 0 );
        var classDivHeight = _studentPanel.find('.classList').height() + 2; //2 for border height
        
        if(_studentPanel.find('#searchStudentList').css("display") == "block")
        {
        	_studentPanel.height(window.height - 50);
        	_studentPanel.find('#searchStudentList').height(window.height - topheight);
			if (_studentSearchPanelScroll != null) {
				_studentSearchPanelScroll.refresh();
			}
	        /*
	         * adjust the width of the bookmarks list if scrollbar is visible
	         */ 
	       if ( ismobile == null ) 
	        {
	            if (_studentPanel.find("[ class = myScrollbarV ]").html())
	            {
	            	$( _studentPanel.find( "[ id = searchStudentList ]" )[ 0 ]).width( 283 );
	                	
	                $( _studentPanel.find( ".depth2-studentList" ) ).width( 283 );
	            } 
	            else 
	            {
	            	$( _studentPanel.find( "[ id = searchStudentList ]" )[ 0 ]).width( 285 );
	                	
	                $( _studentPanel.find( ".depth2-studentList" ) ).width( 285 );
	            }
	           
	            //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
	            iScrollClickHandler(_studentPanel.find("[ class = myScrollbarV ]"), _studentPanelScroll, "searchStudentList");	           
	        }
        }
        else
        {
        	_studentPanel.height(window.height - 50);
        	_studentPanel.find('#studentList').height(window.height - topheight);
            if (_studentPanelScroll != null) {
                _studentPanelScroll.refresh();
            }
            //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
        	iScrollClickHandler(_studentPanel.find("[ class = myScrollbarV ]"), _studentPanelScroll, "addedStudentList");
        }
       
		
    };


    this.getStudentsListHtml = function (studentsArr) {      

        var studentListHtml = "";
        var firstName = "";
        var lastName = "";
        var nameStr = "";
        for (var j = 0; j < studentsArr.length; j++) {
            if (studentsArr[j]) {

                firstName = studentsArr[j].firstName || "";
                lastName = studentsArr[j].lastName || "";
                nameStr = "";

                if (firstName.length > 0 || lastName.length > 0) {
                    switch ($.trim(_sortBy)) {
                        case "firstname":
                        	
                        	if( studentsArr[j].middleName && studentsArr[j].middleName.length > 0 )
			                {
			                	firstName += " " + studentsArr[j].middleName + ".";
			                }
			                
                            if (firstName.length > 0)
                            {
                                nameStr = firstName;

                            }
                            
                            if (lastName.length > 0)
                            {
                                if (nameStr.length > 0)
                                {
                                    nameStr =nameStr + " " + lastName;
                                }
                                else
                                {
                                    nameStr = lastName;
                                }
                            }
                            studentListHtml += '<div class="depth2-studentList" ><div class="studentNameCheckBox"></div><div platformid="' + 
                            					studentsArr[j].platformId + '" refid="' + studentsArr[j].refId + '" class="studentName">' +
                            					nameStr + '</div></div>';
                            break;

                        case "lastname":
                            if (lastName.length > 0) {
                                nameStr = lastName;

                            }
                            
                            if( studentsArr[j].middleName && studentsArr[j].middleName.length > 0 )
			                {
			                	firstName += " " + studentsArr[j].middleName + ".";
			                }
                            
                            if (firstName.length > 0) {
                                if (nameStr.length > 0) {
                                    nameStr =nameStr + ", " + firstName;
                                }
                                else {
                                    nameStr = firstName;
                                }
                            }
                            studentListHtml += '<div class="depth2-studentList" ><div class="studentNameCheckBox"></div><div platformid="' + 
                            					studentsArr[j].platformId + '" refid="' + studentsArr[j].refId + '" class="studentName">' +
                            					nameStr + '</div></div>';
                            break;
                    }

                    studentListHtml = $.trim(studentListHtml);

                }

                
            }
        }

        return studentListHtml;
    };

    this.studentNameClick = function (studentName,platformId) { 
		var studentNameStr = "<div class='studentPanelStudentName'>" + studentName + "</div>";
		var closeBtnStr = "<div class=studentPanelReturnToTSEBtn></div>";
        $("#currentPage").trigger("click"); //Trigger event in printPanelComp to initialize the panel. 
		$(_scrollViewRef).trigger('hideBookmarkStrip');
        $(".teacherStudentBtn").addClass("teacherStudentBrownBtn");
        $(".teacherStudentArrow").removeClass("teacherArrowUp");
        $(".teacherStudentArrow").addClass("teacherArrowDown");
        $(".mystudentEdition").html(studentNameStr + closeBtnStr);
        $(".mystudentEdition").attr("title", studentName);        
        GlobalModel.StudentContentLoaded = true;       

		$(".studentPanelReturnToTSEBtn").unbind("click").bind("click", function ()
		{
			delete GlobalModel.selectedStudentData;
			delete sessionStorage.selectedStudentData;
	    	$("#teacherStudentPanelBtn").removeClass("teacherStudentBrownBtn");
	    	$("#questionAnswerPanel").css( "display", "none" );
	    	$("#teacherStudentPanelBtn").trigger("click");
	    	$(".mystudentEdition").html( '<span>' + GlobalModel.localizationData["MY_CLASSES"] + "</span>" );
	    	$("#mainShowroom").css("opacity", "0.2");
	        $("#appPreloaderContainer").css("display","block");
	        $(_scrollViewRef).trigger('showBookmarkStrip');
	        $(document).trigger( "qaCommentsLoaded" );
	    	ServiceManager.Annotations.fetchAllAnnotations();
		} );
        var selectedStudentData = _searchStudentListArr.filter(function (obj) {
            if (obj.platformId == platformId) {
                return obj;
            }

        });

        $(_scrollViewRef).trigger('closeOpenedPanel');
        $("#questionAnswerPanel").css( "display", "none" );
        ServiceManager.setStudentData(selectedStudentData[0]);
        

    };
    
	this.setSelectedStudentDataInStudentList = function(objData)
	{
		var studentName = objData.FullNameFL;
		var studentNameStr = "<div class='studentPanelStudentName'>" + studentName + "</div>";
		var closeBtnStr = "<div class=studentPanelReturnToTSEBtn></div>";
		$(".teacherStudentBtn").addClass("teacherStudentBrownBtn");
		$(".teacherStudentArrow").removeClass("teacherArrowUp");
        $(".teacherStudentArrow").addClass("teacherArrowDown");
        $(".mystudentEdition").html(studentNameStr + closeBtnStr);
        $(".mystudentEdition").attr("title", studentName);
        $(".studentPanelReturnToTSEBtn").unbind("click").bind("click", function ()
		{
			delete GlobalModel.selectedStudentData;
			delete sessionStorage.selectedStudentData;
	    	$("#teacherStudentPanelBtn").removeClass("teacherStudentBrownBtn");
	    	$("#questionAnswerPanel").css( "display", "none" );
	    	$("#teacherStudentPanelBtn").trigger("click");
	    	$(".mystudentEdition").html( '<span>' + GlobalModel.localizationData["MY_CLASSES"] + "</span>" );
	    	$("#mainShowroom").css("opacity", "0.2");
	        $("#appPreloaderContainer").css("display","block");
	        $(_scrollViewRef).trigger('showBookmarkStrip');
	        $(document).trigger( "qaCommentsLoaded" );
	    	ServiceManager.Annotations.fetchAllAnnotations();
		} );
	}
}