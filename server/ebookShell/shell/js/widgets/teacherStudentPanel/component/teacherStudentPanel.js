

$.widget( "magic.teacherStudentPanel", $.magic.tabViewComp, 
{
	events:
	{
		
	},
	
	options: 
	{
		
	},
	
	member:
	{
		
	},
	
	_create: function()
	{
		var objThis = this;
		$.extend(this.options, this.element.jqmData('options'));
		
	},
	
	_init: function()
	{
	    var self = this;
		$.magic.tabViewComp.prototype._init.call(this);	
		
		if ( EPubConfig.teacherStudentPanel_isAvailable )
		{
		    $('#teacherStudentPanelBtn').css("display", "block");
		    $('.mystudentEdition').text("Mrs. Bellows");
		}		
		
		$( "#teacherStudentPanelBtn" ).unbind( "click" ).bind( "click" , function ( e ) {
			if ( $( self.element ).css( "display" ) == "none" )
			{
				$( this ).find( ".teacherStudentArrow" ).addClass( "teacherStudentArrowExpanded" )
				$( self.element ).stop(true, true).slideDown(500, function()
				{
					
				});
			}
			else
			{
				$( this ).find( ".teacherStudentArrow" ).removeClass( "teacherStudentArrowExpanded" )
				$( self.element ).stop(true, true).slideUp(500, function()
				{
					
				});
				
			}
			self.updateViewSize();
		} );
		
		$( window ).resize( function ( )
		{
			console.log($( self.element ).find( '.depth2-expandable[display="block"] ' ))
			
			self.updateViewSize();
		} );
		
		self.loadStudentList();
		
	},
	
	loadStudentList: function ( )
	{
		var url = "content/en/ePubPOC/OPS/data/studentlist.json";
		var self = this;
		alert(url);
		
		$.getJSON ( url , function ( objData ) {
			if ( objData.value ) 
			{
				objData = objData.value;
			}
			self.createStudentList( objData );
		});
	},
	
	createStudentList: function ( data )
	{
		var self = this;
		var studentList = "";
		
		for ( var i = 0; i < data.length; i++ )
		{
			studentList += '<div class="classList" ><div class="className">' + data[i].class + '</div><div class="classArrow"></div></div><div class="depth2-expandable">';
			for ( var j = 0; j < data[i].studentList.length; j++ )
			{
				studentList += '<div class="depth2-studentList" ><div class="studentName">' + data[i].studentList[j].name + '</div></div>';
			}
			studentList += '</div>';
		}

		$( self.element ).find( '#addedStudentList' ).html( studentList );
		
		var classDivHeight = $( self.element ).find( '.classList' ).height() + 2;//2 for border height
		var studentDivHeight = $( self.element ).find( '.depth2-studentList' ).height() + 2;//2 for border height
		console.log(classDivHeight,studentDivHeight)
		$( self.element ).find( '.classList' ).unbind( "click" ).bind( "click" , function ( e ) {
			
			var displayState = $( this ).next( 'div' ).css( "display" );
				
			var totalHeight = 	data.length * classDivHeight + $( this ).next( 'div' ).find('.depth2-studentList').length * studentDivHeight;
			
			$( this ).next( 'div' ).height( $( self.element ).find( '#studentList' ).height() - data.length * classDivHeight );
			if( totalHeight > $( self.element ).find( '#studentList' ).height() )
			{
				
				$( this ).next( 'div' ).css( "overflow-y" , "scroll" );
			}
			else
			{
				//$( this ).next( 'div' ).css( "height" , "auto" );
				$( this ).next( 'div' ).css( "overflow-y" , "hidden" );
			}
			
			$( self.element ).find( ".classArrow" ).removeClass( "classArrowExpanded" );
			$( self.element ).find( '.depth2-expandable' ).stop(true, true).slideUp(500, function()
			{
				
			});
			
			if(displayState != "block" )
			{
				$( this ).find( ".classArrow" ).addClass( "classArrowExpanded" );
				$( this ).next( 'div' ).stop(true, true).slideDown(500, function()
				{
					
				});
			}
				
		} );
		
		self.updateViewSize();
	},
	
	updateViewSize: function (  )
	{
		var self = this;
		setTimeout ( function ()
		{
			var topheight = $( self.element ).find( '#studentListTopbar' ).height() + $( "#navigationBar" ).height() + 2;
			$( self.element ).find( '#studentList' ).height( window.height - topheight );
		
		} , 500 );
	}
		
     
});
