/**
 * 
 * This class creates instance of the panel to show Student's list.
 * This class is just a triggering point and is expected to be instantiated at the start of the application.
 * All the functionality of the panel will be handled in the StudentList class
 * 
 * @params component JQuery object of component whose click will display the student panel
 */

function TEController ( component )
{
    var studentList = null;
    
    this.setTELauncherButtonSettings = function()
    {
    	var strUserRole = ServiceManager.getUserRole();
    	if (strUserRole == AppConst.USERTYPE_TEACHER && EPubConfig.Component_type == AppConst.TEACHER_EDITION && EPubConfig.TSE_Path != "" && EPubConfig.TSE_Path != undefined)
        {
            $(".teacherBtn_navSrh").show();
            $(".teacherBtn").unbind('click').bind('click', function (e) {
                window.open(EPubConfig.TSE_Path);
            });
        }
        else if(strUserRole == AppConst.USERTYPE_TEACHER && EPubConfig.Component_type == AppConst.STUDENT_EDITION)
        {
            $(".teacherBtn_navSrh").hide();
            $(".teacherStudentBtn_navSrh").show();
            component.bind( "click", function ( event )
            {
                event.preventDefault();
                event.stopPropagation();
                
                if( studentList === null )
                {
                    studentList = new StudentList();
                }
                studentList.showPanel();
            } );
			//console.log("sessionStorage.selectedStudentData: sessionStorage.selectedStudentData: ", sessionStorage.selectedStudentData);
            if(sessionStorage.selectedStudentData != undefined && EPubConfig.AnnotationSharing_isAvailable != false)
			{
				if( studentList === null )
                {
                    studentList = new StudentList();
                }
				
				studentList.setSelectedStudentDataInStudentList(JSON.parse(sessionStorage.selectedStudentData));
			}
        }
        else if(strUserRole == AppConst.USERTYPE_STUDENT && EPubConfig.Component_type == AppConst.STUDENT_EDITION)
        {
            $(".teacherStudentBtn_navSrh").hide();
        }
    };
    
    if( EPubConfig.AnnotationSharing_isAvailable )
    {
    	this.setTELauncherButtonSettings();
    }
    
};
