/**
 * @author Harsh Chawla
 */

MG.SearchOperator = function() {
	this.searchTextBox = null;
	this.strSearchedText = "";
	this.totalSearchResults = 0;
}

MG.SearchOperator.prototype = new MG.BaseOperator();


MG.SearchOperator.prototype.attachComponent = function(objComp) {
	if (objComp == null) {
		return;
	}

	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
	var objThis = this;

	switch (strType) {

		case AppConst.SEARCH_PANEL_COMP:
			var searchBtnRef = $(objComp.element).find('[type=searchBtn]')[0];
			
			$(document).bind("searchresultscrollcomplete", function(e){
				//console.log("callingscrollcompleteevent");
				objThis.loadNextSearchItems();
			});
			var searchTextBoxRef = $(objComp.element).find('[type=text]')[0];
			objThis.searchTextBox = searchTextBoxRef;
			if (ismobile) {
				$(searchBtnRef).addClass('searchBtnMobile');
				if (navigator.userAgent.match(/(android)/i)) {// To handle styling for android tablet
					$(searchTextBoxRef).addClass('searchInputBoxMobileAndroid');
				} else {
					$(searchTextBoxRef).addClass('searchInputBoxMobile');
				}
			}

			var objPathManager = getPathManager();
			var searchDataFileName = EPubConfig.searchDataUrl;
			var searchDataUrl = objPathManager.getStandardsPath(searchDataFileName);
			if(EPubConfig.LocalSearch_isAvailable) {
				$.getJSON(searchDataUrl, function(objData) {
					objThis.onURLDataFileLoad(objData);
				})
			}
			
			$(searchBtnRef).addClass('ui-disabled');
			$(searchBtnRef).data('buttonComp').disable();
			
			$(searchTextBoxRef).bind( "input propertychange", function ( event )
			{
			    $(searchTextBoxRef).trigger( "keyup" );
			} );

			$(searchTextBoxRef).unbind('keyup').bind('keyup', function(e) {
			   //$(searchTextBoxRef).val(objThis.removeScriptTag($(searchTextBoxRef).val()));
				if ( ($(searchTextBoxRef).val().trim() != "" && EPubConfig.LocalSearch_isAvailable ) || ($(searchTextBoxRef).val().trim() != "" && $(searchTextBoxRef).val().trim().length > 2) ) {
					$(searchBtnRef).removeClass('ui-disabled');
					$(searchBtnRef).data('buttonComp').enable();
					if (e.which == 13) {
						$('body').removeHighlight();
						if($(this).val() != "") {
							objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showSearchResults');
							objThis.searchedData = $(this).val();	
						}						
						$(searchBtnRef).trigger('click');
						
					}
				} else {
					$(searchBtnRef).addClass('ui-disabled');
					$(searchBtnRef).data('buttonComp').disable();
				}
			});
			
			$(searchTextBoxRef).unbind('paste').bind('paste', function (e) {

			    setTimeout(
                    function () {
                        if (($(searchTextBoxRef).val().trim() != "" && EPubConfig.LocalSearch_isAvailable) || ($(searchTextBoxRef).val().trim() != "" && $(searchTextBoxRef).val().trim().length > 2)) {
                            $(searchBtnRef).removeClass('ui-disabled');
                            $(searchBtnRef).data('buttonComp').enable();
                        }
                    }, 100
                    );
			});
			
			// This will define all the functionality of search button in Search Panel.
			var searchBtnRole = $(searchBtnRef).data('buttonComp');
			$(searchBtnRole).unbind(searchBtnRole.events.CLICK).bind(searchBtnRole.events.CLICK, function() {
				objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'resetVisibleCount');
				$('#resultCountSet').css("display", "none");
				if ($(searchBtnRef).hasClass('ui-disabled')) {
					return;
				}
				var searchString = $(searchTextBoxRef)[0].value;
				objThis.strSearchedText = searchString;
				objThis.searchedKey = searchString;
				if(EPubConfig.LocalSearch_isAvailable) {
				    if (searchString.match('^\s*([0-9a-zA-Z]+)\s*$')) {
						objThis.search(escape((searchString).toLowerCase()));
					} else {
						$('.searchContainer').empty();
						$('.searchContainer').append("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_NO_RESULTS'] + "</div>");
							$("#resultCountSet").show();
							$('#resultCount').empty();
							$('#resultCount').append("0");
					}
				} else {
					objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showSearchResults');
					$('.searchContainer').empty();
					//$('.searchContainer').append("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_SEARCHING'] + "</div>");
					$('.searchContainer').html('<div class="searchResultNO"><div style="width: 100%;" align="center"><div class="pagePreloader"></div></div></div>');
					//
					ServiceManager.getSearchResults({
						searchText: $(searchTextBoxRef)[0].value,
						success: "searchSuccessHandler",
						error: "searchErrorHandler",
						caller: objThis
					});
				}
				
				$("#searchTree").css("display", "block");
				$("#searchPanel").css("height", "100%");
				
				var _objSearch = {};
				_objSearch.type = AppConst.SEARCH_TEXT;
				_objSearch.term = searchString;
				$(document).trigger( AppConst.SEND_TRACKING_STATEMENT, [_objSearch] );
			});

			break;
			
		default:
			break;
	}
	return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.SearchOperator.prototype.searchSuccessHandler = function(objData, calltype){
	
	var objThis = this;
	var results = objData.results;
	//console.log(objData);
	objThis.totalSearchResults = objData.total;
	if (results) {
		
		var resultSize = 0;//results.length;
		if(results.length)
		{
			resultSize = results.length;
		}
		var objScrollViewComp = $.find("[data-role=scrollviewcomp]")[0];
		if (calltype != "shownextrows") {
			$('.searchContainer').empty();
			if (resultSize == 0) {
				$('.searchContainer').html("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_NO_RESULTS'] + "</div>");
				$('#resultCount').empty();
				$('#resultCount').append(resultSize);
				
			}
			else {
				var searchResultCount = 0;
				for (var resultArr = 0; resultArr < resultSize; resultArr++) {
					if (results[resultArr].content.p) {
						if (results[resultArr].content.p.value && results[resultArr].content.p.page_no && results[resultArr].content.p.objectid) {
							var searchText = results[resultArr].content.p.value;
							var pageNumber = -1;//results[resultArr].content.p.page_no;
							var objectID = results[resultArr].content.p.objectid;
							
							var strPageNum = results[resultArr].content.p.page_no;
							//console.log("strPageNum: ", strPageNum);
							/*var strPageName = "";
							objScrollViewComp = $.find("[data-role=scrollviewcomp]");
							var objPageElement = $(objScrollViewComp).find('[pagebreakid=' + strPageNum + ']')[0];
							//var objSearchObject;
							if (objPageElement) {
								pageNumber = $(objPageElement).attr('pagebreakvalue');
								strPageName = $(objPageElement).attr('pagename');
							}
							else {
								objPageElement = $(objScrollViewComp).find('[pagename=' + strPageNum + ']')[0];
								if (objPageElement) {
									pageNumber = $(objPageElement).attr('pagebreakvalue');
									strPageName = $(objPageElement).attr('pagename');
								}
							}
							
							var displayText = searchText.replace(/\<span class\=\"hit\"\>/gi, "<span class=\'searchHighlight\'>");
							var displayPage = "Page <span class='searchPageNumber' pagename=" + strPageName + " pageid=" + pageNumber + " objnumber=" + objectID + ">" + pageNumber + "</span>";
							$('.searchContainer').append("<div class='searchResult'><div class='panelPageNumber'>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");*/
							var strPageName = "";
							//objScrollViewComp = $.find("[data-role=scrollviewcomp]");
							var objPageElement = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + strPageNum];//$(objScrollViewComp).find('[pagebreakid=' + strPageNum + ']')[0];
							//var objSearchObject;
							if (objPageElement) {
								pageNumber = objPageElement.pageBreakValue;//$(objPageElement).attr('pagebreakvalue');
								strPageName = objPageElement.pagebreakname;//$(objPageElement).attr('pagename');
							}
							else {
								objPageElement = GlobalModel.pageDataByPageName[strPageNum];//$(objScrollViewComp).find('[pagename=' + strPageNum + ']')[0];
								if (objPageElement) {
									pageNumber = objPageElement.pageBreakValue;//$(objPageElement).attr('pagebreakvalue');
									strPageName = objPageElement.pagebreakname;//$(objPageElement).attr('pagename');
									//pageNumber = $(objPageElement).attr('pagebreakvalue');
									//strPageName = $(objPageElement).attr('pagename');
								}
							}
							//if (pageNumber != -1) {
								var displayText = searchText.replace(/\<span class\=\"hit\"\>/gi, "<span class=\'searchHighlight\'>");
								var displayPage = GlobalModel.localizationData["PAGE_TEXT_FREE"] + " <span class='searchPageNumber' pagename=" + strPageName + " pageid=" + pageNumber + " objnumber=" + objectID + ">" + pageNumber + "</span>";
								$('.searchContainer').append("<div class='searchResult'><div class='panelPageNumber'>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");
							//}
						}
					}
				}
				//$('#resultCount').empty();
				//$('#resultCount').append(results.length);
			}
			objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showSearchResults');
			
			//objThis.totalSearchResults = objData.total;
		}
		else
		{
			//console.log("search success handler: ", objData);
			for (var resultArr = 0; resultArr < resultSize; resultArr++) {
				if (results[resultArr].content.p) {
					if (results[resultArr].content.p.value && results[resultArr].content.p.page_no && results[resultArr].content.p.objectid) {
						/*var searchText = results[resultArr].content.p.value;
						var objectID = results[resultArr].content.p.objectid;
						var pageNumber = results[resultArr].content.p.page_no;
						var displayText = searchText.replace(/\<span class\=\"hit\"\>/gi, "<span class=\'searchHighlight\'>");
						var displayPage = "Page <span class='searchPageNumber' pageid=" + pageNumber + " objnumber=" + objectID + ">" + pageNumber.replace('page', '') + "</span>";
						$('.searchContainer').append("<div class='searchResult'><div class='panelPageNumber'>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");
						*/
						var searchText = results[resultArr].content.p.value;
						var pageNumber = -1;//results[resultArr].content.p.page_no;
						var objectID = results[resultArr].content.p.objectid;
						
						var strPageNum = results[resultArr].content.p.page_no;
						var strPageName = "";
						objScrollViewComp = $.find("[data-role=scrollviewcomp]");
						var objPageElement = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + strPageNum];//$(objScrollViewComp).find('[pagebreakid=' + strPageNum + ']')[0];
						//var objSearchObject;
						if (objPageElement) {
							pageNumber = objPageElement.pageBreakValue;//$(objPageElement).attr('pagebreakvalue');
							strPageName = objPageElement.pagebreakname;//$(objPageElement).attr('pagename');
						}
						else {
							objPageElement = GlobalModel.pageDataByPageName[strPageNum];//$(objScrollViewComp).find('[pagename=' + strPageNum + ']')[0];
							if (objPageElement) {
								pageNumber = objPageElement.pageBreakValue;//$(objPageElement).attr('pagebreakvalue');
								strPageName = objPageElement.pagebreakname;//$(objPageElement).attr('pagename');
								//pageNumber = $(objPageElement).attr('pagebreakvalue');
								//strPageName = $(objPageElement).attr('pagename');
							}
						}
						//if (pageNumber != -1) {
							var displayText = searchText.replace(/\<span class\=\"hit\"\>/gi, "<span class=\'searchHighlight\'>");
							var displayPage = GlobalModel.localizationData["PAGE_TEXT_FREE"] + "<span class='searchPageNumber' pagename=" + strPageName + " pageid=" + pageNumber + " objnumber=" + objectID + ">" + pageNumber + "</span>";
							$('.searchContainer').append("<div class='searchResult'><div class='panelPageNumber'>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");
						//}
					}
				}
			}
			
			//$('#resultCount').empty();
			//$('#resultCount').append(results.length);
			
			
		}
		
		$('#resultCount').html($(".searchContainer .searchResult").length +" of " + objThis.totalSearchResults);
	}
	else
	{
		$('.searchContainer').html("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_NO_RESULTS'] + "</div>");
		$('#resultCount').empty();
		//$('#resultCount').append(resultSize);
	}
	/*if($('.searchResult').length == 0)
	{
		$('.searchContainer').html("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_NO_RESULTS'] + "</div>");
		$('#resultCount').empty();
	}*/
	
	$('.searchResult').unbind("click").bind('click', function() {
        var pageNumber = $(this).find('.searchPageNumber').attr('pagename');
        //console.log(this);
        var objectNumber = $(this).find('.searchPageNumber').attr('objnumber');
        var pageLink;
        /*for (var pages = 0; pages < GlobalModel.ePubGlobalSettings.PageBreaks.length; pages++) {
            if (GlobalModel.ePubGlobalSettings.PageBreaks[pages].pageBreakId == pageNumber) {
                pageLink = pages;
            }
        }

        if (!pageLink) {
            for (var pages = 0; pages < GlobalModel.ePubGlobalSettings.PageBreaks.length; pages++) {
                if (GlobalModel.ePubGlobalSettings.PageBreaks[pages].pageName.split(".")[0] == pageNumber) {
                    pageLink = pages;
                }
            }
        }*/
        
        var objScrollViewComp = $.find("[data-role=scrollviewcomp]");
        //var objPageElement = $(objScrollViewComp).find('[pagename='+pageNumber+']')[0];
        
        //console.log("------", $(objScrollViewComp).find('[pagebreakid=-1]')[0])
        pageLink = parseInt(GlobalModel.pageDataByPageName[pageNumber].pagesequence);//$($(objScrollViewComp).find('[pagename='+pageNumber+']')[0]).attr('pagenum');
        var pageBrkValue;
        try {
            if (EPubConfig.pageView == 'doublePage')
                pageLink = parseInt(pageLink) + 1;
                
            pageBrkValue =  GlobalModel.pageBrkValueArr[parseInt(pageLink)];
            objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageLink);
        } catch(e) {
            console.log(e);
        }
        
        $('body').removeHighlight();
        setTimeout(function() {
            var objSrchPageElement = $(objScrollViewComp).find('[pagebreakvalue='+pageBrkValue+']')[0];
            $(objSrchPageElement).data('pagecomp').setSearchHighlight(objectNumber, objThis.strSearchedText);
        }, 500);
        
		// Hide Audio Player on search result click
		if ( $("#audioPopUp").css("display") == "block" ) 
		{
			$("#audioPlayerCloseBtn").trigger('click');
		}
        //alert(objectNumber + " " + objThis.strSearchedText);
        //setTimeout(function() {
            //console.log($("#innerScrollContainer").find('[objectid=' + objectNumber + ']'), objThis.strSearchedText);
            //$("#innerScrollContainer").find('[objectid=' + objectNumber + ']').highlight(objThis.strSearchedText);
        //}, 500);

    });
		
	$('#resultCountSet').css("display", "block");
	
	if(calltype != "shownextrows")
	{
		objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showSearchResults');
	}
	else
	{
		objThis.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showNextRows', objThis.totalSearchResults);
	}

}

MG.SearchOperator.prototype.searchErrorHandler = function(objData){
	var strListPart_2 = '<div style="margin-top:5px;">' + GlobalModel.localizationData["ERROR_IN_FETCHING_SEARCH_DATA"] + '</div>';
	//$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
	$('.searchContainer').html(strListPart_2);
	this.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'onServiceCallResponseFailed');
	//this.searchSuccessHandler(GlobalModel.ePubGlobalSettings.searchdata);
}


MG.SearchOperator.prototype.loadNextSearchItems = function(objData){
	//alert("failed");
	//console.log("this.strSearchedText: ", this.strSearchedText);
	//console.log("calling load next rows search itesm", this.strSearchedText, " ", this.totalSearchResults)
	ServiceManager.getSearchResults({
			searchText: this.strSearchedText,
			success: "searchSuccessHandler",
			error: "searchErrorHandler",
			caller: this,
			start: $(".searchContainer .searchResult").length + 1,
			calltype: "shownextrows"
		});
}

MG.SearchOperator.prototype.onURLDataFileLoad = function (objData)
{
	this.searchUrlData = objData;
	var objPathManager = getPathManager();
	var indexDataFileName = objData.indexfile;
	if(EPubConfig.LocalSearch_isAvailable) {
		var idxFileUrl = objPathManager.getStandardsPath(indexDataFileName);
		this.loadData(idxFileUrl, this.onLoad);
	}
}

MG.SearchOperator.prototype.loadData = function(strUri, callBack, callBackData)
{
	
	var objData = {};
	var objThis = this;
	if(EPubConfig.LocalSearch_isAvailable) {
		$.ajax({
	        url: strUri,
			type: "GET",
			dataType: "text",
			complete: function( jqXHR, status ) {
			    objData.status = status;
				if(status == "success") {
	                if ( jqXHR.isResolved() ) {
	                    // #4825: Get the actual response in case
	                    // a dataFilter is present in ajaxSettings
	                    jqXHR.done(function( responseText ) {
	                        objData.responseText = responseText;
							
	                    });
	                }
			  } else if(status == "error") {
			  		$("#alertTxt").html(GlobalModel.localizationData["SEARCH_DATA_FILES_UNAVAILABLE"]);
			  		 PopupManager.addPopup($("#alertPopUpPanel"), null, {
	                    isModal : true,
	                    isCentered : true,
	                    popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
	                    isTapLayoutDisabled : true
	                });
			  }
	            callBack.call(objThis, objData, callBackData);
	        }
		});
	}
}

MG.SearchOperator.prototype.onLoad = function (objData)
{
	if(objData.status != "success")
	{
		console.error("Failed to load/parse search data file. Failure status: " , objData.status);
		return;
	}
	this.arrSplitterWords = objData.responseText.split("\n");
	this.arrSplitterWords.sort();
	if(this.arrSplitterWords.length > 1)
	{
		//this.arrSplitterWords.splice(0,1);
	}
	this.nDataFiles = this.arrSplitterWords.length;
	this.dataDictionary = {};
}

/*****************/
MG.SearchOperator.prototype.search = function (strSearchString)
{
	this.globalArr = new Array();
	this.arrPages = new Array();
	this.descriptivetext = new Array();
	this.searchFor(strSearchString);
	this.searchedKey = "";
}

MG.SearchOperator.prototype.searchFor = function(strSearchString)
{
	var arrWords = unescape(strSearchString).split(" ");
	if(arrWords ==  0)
	{
		this.updateView(this.globalArr, "", true);
		return;
	}
	this.arrWords = arrWords;
	this.findEntries(arrWords.shift());
}

MG.SearchOperator.prototype.findEntries = function(strWord, bDontCheckDataFileLoad)
	{
		var idDataFile = this.findDatafile(strWord);
		
		if(bDontCheckDataFileLoad) // if its is already know that the data file is present, e.g. in case of call from onDatafileLoaded function 
		{
			this.findPages(strWord, idDataFile);
			this.searchFor(this.arrWords.join(" "));
		}
		else
		{
			if(idDataFile != null && idDataFile != undefined)
			{
				if(this.isDataFileLoaded(idDataFile))
				{
					this.findPages(strWord, idDataFile);
					this.searchFor(this.arrWords.join(" "));
				}
				else
				{
					this.loadDataFile(idDataFile, strWord, this.onDataFileLoaded);
				}
				
			}
			
		}
	}

MG.SearchOperator.prototype.isDataFileLoaded = function(iDataLocation)
	{
		var key = "index_" + iDataLocation;
		if(this.dataDictionary[key] == undefined )
		return false;
		return true;
		
	}

MG.SearchOperator.prototype.updateView = function(arrData, strNoDataFoundText, refresh, startupNote)
	{
		var objThis = this;
		var strSearchText = "";
		if(refresh == undefined)
		{
			$(this.element).find("input[name= 'search']").attr("value", "");
			
			
			var element=  document.getElementById('searc-basic');
		   
//			setTimeout(function() {
//		            element.focus();
//		        }, 1000);
	
		}
		if(arrData.length == 0 && refresh != undefined)
		{
			strSearchText = "No Search Item Found."
		}
		if(startupNote){
			
			strSearchText = "Enter the text for search.";
		}
        $("#resultCountSet").show();
        $("#resultCount").html(arrData.length);
        
        /*var str = '';
        for(var i=0; i<arrData.length; i++) {
        	str += "<div class='rows'>"+this.descriptivetext[i]+"</div>";
        }
        $("#searchTree div").html(str);*/
        $("#searchTree").css("height","100%")
        
        var resultSize = arrData.length;
        if (resultSize == 0) {
        	$('.searchContainer').empty();
			$('.searchContainer').append("<div class='searchResultNO'>" + GlobalModel.localizationData['SEARCH_NO_RESULTS'] + "</div>");
			$('#resultCount').empty();
			$('#resultCount').append(resultSize);

		} else {
			var searchResultCount = 0;
			$('.searchContainer').empty();
			for (var resultArr = 0; resultArr < resultSize; resultArr++) {
			var searchText = arrData[resultArr].descriptivetext[0];
			var pageNumber = arrData[resultArr].arrPages[0];
			var objectID = 1;
			var searchedData = $("#searchInputBox").val();
			if (searchedData.trim() != "") {
				searchedData = searchedData.trim().toLowerCase();
			}
			try {
				console(searchText)
			} catch(e) {
			}
			var styled = "";
			if (resultArr > (EPubConfig.paginationThreshold - 1)) {
				styled = "display:none;";
			}
			var re = new RegExp('(' + searchedData + ')', 'gi');
			if (searchText) {
				searchResultCount++;
				var displayText = searchText.replace(re, "<span class=\'searchHighlight\'>" + searchedData + "</span>");
				if (EPubConfig.pageView == 'doublePage') {
					var displayPage = GlobalModel.localizationData["PAGE_TEXT_FREE"] + "<span class='searchPageNumber' pageid=" + (pageNumber) + " objnumber=" + objectID + ">" + GlobalModel.pageBrkValueArr[pageNumber] + "</span>";
				} else {
					var displayPage = GlobalModel.localizationData["PAGE_TEXT_FREE"] + "<span class='searchPageNumber' pageid=" + (pageNumber - 1) + " objnumber=" + objectID + ">" + GlobalModel.pageBrkValueArr[pageNumber - 1] + "</span>";
				}
				$('.searchContainer').append("<div class='searchResult' style='" + styled + "'><div class='panelPageNumber' style='float:left''>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");
			}
		}
			/*for (var resultArr = 0; resultArr < resultSize; resultArr++) {
				var searchText = this.descriptivetext[resultArr];
				var pageNumber = arrData[resultArr];
				var objectID = 1;//results[resultArr].content.p.objectid;
				var searchedData = $("#searchInputBox").val();
				if(searchedData.trim() != ""){
					searchedData = searchedData.trim().toLowerCase();
				} 
				var styled = "";
				if(resultArr > (EPubConfig.paginationThreshold -1)) {
					styled = "display:none;";
				}
				var re= new RegExp('('+searchedData+')', 'gi');
				if (searchText) {
					searchResultCount++;
					var displayText = searchText.replace(re, "<span class=\'searchHighlight\'>"+searchedData+"</span>");
					if (EPubConfig.pageView == 'doublePage') {
						var displayPage = "Page <span class='searchPageNumber' pageid=" + (pageNumber) + " objnumber=" + objectID + ">" + GlobalModel.pageBrkValueArr[pageNumber] + "</span>";
					} else {
						var displayPage = "Page <span class='searchPageNumber' pageid=" + (pageNumber) + " objnumber=" + objectID + ">" + GlobalModel.pageBrkValueArr[pageNumber - 1] + "</span>";
					}
					$('.searchContainer').append("<div class='searchResult' style='"+styled+"'><div class='panelPageNumber' style='float:left''>" + displayPage + "</div><div class='searchResultText'>" + displayText + "</div></div>");
				}
			}*/
			$('#resultCount').empty();
			if(EPubConfig.paginationThreshold < searchResultCount) {
				$('#resultCount').append(EPubConfig.paginationThreshold + " of "+arrData.length);
			} else {
				$('#resultCount').append(arrData.length + " of "+arrData.length);
			}
		}
        
        
        $('.searchResult').bind('click', function() {
			var pageNumber = $(this).find('.searchPageNumber').attr('pageid');
			var objectNumber = $(this).find('.searchPageNumber').attr('objnumber');
			var pageLink;
			
			for (var pages = 0; pages < GlobalModel.ePubGlobalSettings.PageBreaks.length; pages++) {
				if (GlobalModel.ePubGlobalSettings.PageBreaks[pages].pageBreakValue == GlobalModel.pageBrkValueArr[pageNumber]) {
					pageLink = pages;
				}
			}
			
			if (!pageLink) {
				for (var pages = 0; pages < GlobalModel.ePubGlobalSettings.PageBreaks.length; pages++) {
					if (GlobalModel.ePubGlobalSettings.PageBreaks[pages].pageBreakValue.split(".")[0] == GlobalModel.pageBrkValueArr[pageNumber]) {
						pageLink = pages;
					}
				}
			}
			
            var pageSequenceNumber = GlobalModel.pageBrkValueArr[pageNumber];
			try {
				if (EPubConfig.pageView == 'doublePage')
					pageLink = pageLink + 1;
				objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageLink);
			} catch(e) {
				console.log(e);
			}
			$('body').removeHighlight();
			setTimeout(function() {
				$("#innerScrollContainer").find('[id=PageContainer_' + pageSequenceNumber + ']').highlight(searchedData);
			}, 800);
		});

		$('#resultCountSet').css("display", "block");
		this.doFunctionCall(AppConst.SEARCH_PANEL_COMP, 'showSearchResults');
	}
	
MG.SearchOperator.prototype.findPages = function(strWord, iDataLocation) {
	var key = "index_" + iDataLocation;
	var strText = this.dataDictionary[key];
	//var re = //;
	var arrMatch = null;
	/**
	 *  'm' for search multiline, 'g' all occurences, 'i' case insensitive
	 * ^  word should begin from
	 * strWord - word to find
	 * (\\S)* - to obtain the complete line of that word till you get \S - break or white space
	 *
	 **/
	if (strWord == '*' || strWord == '+' || strWord == '.') {
		strWord = strWord.replace('*', '_____');
		strWord = strWord.replace('+', '_____');
		strWord = strWord.replace('.', '_____');
	}
	var strPattern = "^" + strWord + "(\\S)*";
	var rePattern = new RegExp(strPattern, "mgi");

	var strPattern1 = "^" + strWord + "[^\n]*";
	var rePattern1 = new RegExp(strPattern1, "mgi");

	// TO FIND ALL ENTERIES MATCHING THE WORD OR PART (IN BEGINING) OF WORD
	while ( arrMatch = rePattern.exec(strText)) {
		var strWordAndPageString1 = (rePattern1.exec(strText)).toString();
		var splittedStr = strWordAndPageString1.split(",");
		
		for (var cnt = 1; cnt < splittedStr.length; cnt++) {
			/*if(cnt%3 == 1) {
			 var strPage = splittedStr[cnt];
			 if(strPage.length === 1) {
			 strPage = strPage;
			 }
			 newObj.arrPages.push(strPage);
			 //this.arrPages.push(strPage);
			 }*/

			if (cnt % 3 == 0 ) {
				if ((cnt - 2) % 3 == 1) {
					var newObj = new Object();
					newObj.arrPages = new Array();
					newObj.descriptivetext = new Array();
					var strPage = splittedStr[cnt - 2];
					if (strPage.length === 1) {
						strPage = parseInt(strPage);
					}
					newObj.arrPages.push(strPage);
					//this.arrPages.push(strPage);
					newObj.descriptivetext.push(splittedStr[cnt]);
					//this.descriptivetext.push(splittedStr[cnt])
					this.globalArr.push(newObj)
				}
				
			}
			
		}
	}
	this.globalArr.sort(function(a, b) {
		return a.arrPages - b.arrPages
	});
	console.log(this.globalArr)
	/**********Sort Array************/
	return this.globalArr;

}
	
MG.SearchOperator.prototype.findDatafile = function(strWord)
	{
		var iDataFileIndex;
		for(var i = 0; i < this.nDataFiles; i++)
		{
			if(strWord <= this.arrSplitterWords[i])
			{
				iDataFileIndex = i;
				break;
			} 
			
		}
		return iDataFileIndex;
	}

MG.SearchOperator.prototype.loadDataFile = function(idDataFile, strWord, callback)
	{
		var objPathManager = getPathManager();
		var searchDataFileName = this.searchUrlData.wordfile[idDataFile].file;
		var strFilename = objPathManager.getStandardsPath(searchDataFileName);
		var objData = {};
		objData.idDataFile = idDataFile;
		objData.word = strWord;
		this.loadData(strFilename, callback, objData);
	}

MG.SearchOperator.prototype.onDataFileLoaded = function(objData, callBackData)
	{
		if(objData.status != "success")
		{
			console.error("Failed to load/parse search data file. Failure status: " , objData.status);
			return;
		}
		var key = "index_" + callBackData.idDataFile;
		this.dataDictionary[key] = objData.responseText;//.split("\n");
		
		this.findEntries(callBackData.word, true);
        
	}

/**
 * This function will remove script tag from the text to be appended in HTML element. 
 */

MG.SearchOperator.prototype.removeScriptTag = function(textContent) {
    textContent = textContent.replace(/<script(.*?)>/gim, "");
    textContent = textContent.replace(/<\/script(.*?)>/gim, "");
    return textContent;
    
}
