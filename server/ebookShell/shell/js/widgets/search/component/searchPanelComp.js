/**
 * Search is used to search content within
 * contents of the ebook
 */
(function($, undefined){
    $.widget("magic.searchPanelComp", $.magic.tabViewComp, {

	isCallSentToLoadMore: false,
	
	member:
	{
			searchPanelScroll : null,
	
	},
	
	events: 
	{
		SCROLL_COMPLETE: "scrollcomplete"
	},
	
	options:
	{
		visibleResCnt : 10,
		nextItemShow:true
	},

	_create: function()
	{
		$.extend(this.options, this.element.jqmData('options'));

		var searchbarRef = ($(this.element[0]).find('[id=searchTopbar]')[0]);
		var searchInputBoxRef = $($(searchbarRef)[0]).find('[id=searchInputBox]')[0];

		$(searchInputBoxRef).attr("placeholder", GlobalModel.localizationData["SEARCH_PLACEHOLDER_TEXT"]);

	},
	_init: function()
	{
		var self = this;
		$.magic.tabViewComp.prototype._init.call(this);	
		var searchTextBoxRef = $(self.element).find('[type=text]');
		$(searchTextBoxRef).bind('keydown', function(e) {
			if (e.which == 13) {
				self.options.visibleResCnt = EPubConfig.paginationThreshold;
			}
		})
		
		
			self.member.searchPanelScroll = new iScroll('searchTree', {
				scrollbarClass : 'myScrollbar',
				onScrollEnd: function() {
					try {
						 - $(".myScrollbarV div").height();
						 //console.log("------", $(".myScrollbarV").length);
						 var objScrollVElement = $(self.element).find("[class=myScrollbarV]");
						 var objScrollVThumbElement = $(objScrollVElement).find('div')[0];
						 var objSerachTree = $(self.element).find("[id=searchTree]");
						 var a = parseInt($(objScrollVThumbElement).offset().top);
						 var b =  parseInt($(objScrollVElement).offset().top);
						 var c = parseInt($(objSerachTree).height());
						 var d = parseInt($(objScrollVThumbElement).height());
						 
						if((a-b) > (c-d-15)) {
							//console.log("888888888 ", EPubConfig.LocalSearch_isAvailable);
							if (EPubConfig.LocalSearch_isAvailable == true) {
								self.showNextRows();
							}
							else
							{
								if (self.isCallSentToLoadMore == false) {
									self.isCallSentToLoadMore = true;
									
									$(document).trigger("searchresultscrollcomplete");
								}
							}
						}
					} catch(e) {}
				}
			});

			$(window).resize(function() {
				self.updateViewSize();
			});
			if (EPubConfig.LocalSearch_isAvailable) {
				$(self.element).bind("panelOpened", function(){
					self.showSearchResults();
					// var searchBtnRef = $("#searchBtn");
					// var searchBtnRole = $(searchBtnRef).data('buttonComp');
					// $(searchBtnRole).trigger(searchBtnRole.events.CLICK);
					// self.options.visibleResCnt = EPubConfig.paginationThreshold;
				})
			}
			else
			{
				$(self.element).bind("panelOpened", function(){
					self.updateViewSize();
				});
			}

	},
	destroy: function()
	{
		$.widget.prototype.destroy.call( this );		
	},

	showSearchResults: function()
	{
				this.updateViewSize();
	},

			updateViewSize: function(Index){
			var objThis = this;
			setTimeout(function(){
				$("#searchTree").height($($.find("[id=tocMainPanel]")).height() - 150);

				if(objThis.member.searchPanelScroll)
				{
					objThis.member.searchPanelScroll.scrollTo(0,0,0);
					objThis.member.searchPanelScroll.refresh();
				}

                if (ismobile == null && objThis.member.searchPanelScroll) {
                    //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                    iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), objThis.member.searchPanelScroll, "searchTree");
                }


			},200);
		},
		
		showNextRows: function(totalResults) {
			var objThis = this;
			objThis.options.nextItemShow = false;
			objThis.isCallSentToLoadMore = false;
			var totalResultCnt;// = (totalResults)?
			if (totalResults) {
				totalResultCnt = totalResults;//$(".searchContainer .searchResult").length;
			}
			else
			{
				totalResultCnt = $(".searchContainer .searchResult").length;
			}
			var visibleResCnt = this.options.visibleResCnt;
			if($($(".searchContainer").children(".searchResult")).length >= EPubConfig.paginationThreshold) {
				for(var cnt = visibleResCnt; cnt < (visibleResCnt+EPubConfig.paginationThreshold);   cnt++) {
					if($($(".searchContainer").children(".searchResult")[cnt]).length > 0) {
						$($(".searchContainer").children(".searchResult")[cnt]).show();
						this.options.visibleResCnt++;	
						this.member.searchPanelScroll.refresh();
					}				
				}
				$('#resultCount').html(objThis.options.visibleResCnt + " of "+totalResultCnt);
			}
		},
		
		onServiceCallResponseFailed: function()
		{
			this.isCallSentToLoadMore = false;
		},
		
		resetVisibleCount: function()
		{
			this.visibleResCnt = EPubConfig.paginationThreshold;
		}



});

})(jQuery);
