/**
 * @author magic s/w pvt ltd
 */
(function($, undefined) {
    $.widget("magic.imagegallerycomp", $.magic.magicwidget, {
        options : {
            openAsPopup : false
        },

        member : {
            imageData : null,
            curZoomLev : 1,
            curViewBtnClicked : null,
            imgGalleryDataArr : [],
            curImageIndex : 0,
            curImageSelected : null,
            currentComponent : null,
            hasPanned : false,
            nGapFromTop : 0,
            imageGalleryExpanded : null
        },

        _init : function() 
        {
            var objThis = this;
            var lastTap = 0;
            var noSwiping = false;
            $.magic.magicwidget.prototype._init.call( this );

            $(objThis.element).css( 'width', EPubConfig.pageWidth );
           	 
           	 var imageGalleryLoader = 'data:image/gif;base64,R0lGODlhaABlAOZ/ADBddi1adC5cdiRWczNgeiRlhSldeiVaeDhogjZmfyRhgR5OZjRifC1mgjBeeEhziy1kgDdogVmCly5kgHGVpyRefTJeeC1geyxohDZkfi9mgipgfSVpiStVbCZYdiNYdixifiVceiJOZh5XcSNdfDRheypefCRYdmKJnTBhfGyRpCNigSVVbSBRaiRgfi5eeS1jgCxgfCJceixeeS1kfilediJaeChZcS5rh0t5kVN9ky9ifTZmfmeNoB9SbC5deC5kgiladChbeDVkfStgfiVjgitifC9lgi1eei1YcS5gfCFSbEFshCRkhC5ifC5ohC5mhSFUbyVSaSpgejRjfTpmfixdeCZYcDBfeSZadyRXdCVnhiRTaj1uiC1ngzlngC1mgC9mgCtlgCRQZz9xiy9bdSZXdC1feilQZiBYczBkfydacyNjgiRSbStkfzZkfTZtiCRZd094jihWbyxohzJfeS9ohiZigShmhStffCdbdS1phS9nhHaZqhpLY////yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ1dWlkOjY1RTYzOTA2ODZDRjExREJBNkUyRDg4N0NFQUNCNDA3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkY5NTJDQ0Y0MTdDQjExRTM4N0MzQzE3OTgwRUQ1RTE0IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkY5NTJDQ0YzMTdDQjExRTM4N0MzQzE3OTgwRUQ1RTE0IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDNjM4RDlBMUU0NDE2ODExODA4M0U3Nzk5ODlEQ0RDRiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGQjdGMTE3NDA3MjA2ODExODA4M0RFOURGMUZCMTNCMyIvPiA8ZGM6dGl0bGU+IDxyZGY6QWx0PiA8cmRmOmxpIHhtbDpsYW5nPSJ4LWRlZmF1bHQiPldlYjwvcmRmOmxpPiA8L3JkZjpBbHQ+IDwvZGM6dGl0bGU+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEBQoAfwAsAAAAAGgAZQAAB/+ACQiDhIWGEYiJiouMjF+Ph4oJk5SVlBmYmRlDnJxUVAyhogwlpSUEqKkEFgEdgoawjbKzjY+Pspa5CZqbnUOfoKOiparFBA6tr7AItM3OiJPQurm8mL6/wMHCpMaqdXUASa6xz+WN0+iXmtfZwNum3ajfFhbh44Pm+ZLp6eu+7dlGmTpl7FsdegjtJdD3jJI0fv16dQK4jdTAbvMQalTIsBnEj5MkegKobdhAgqkMZtRIr0wSNAtl4csHMiI7dwxwhjp5UZ7Kg/QcCHUAoKhLNBGWRXJWU12GSzcDvuN58adKhESLaj2aVCmhW7iaVtKU4B/JklRP+rQKtJ7Wt0f/lXlFADaRWF0isZ01mZaYt58J3woGEHduoVt3p5k9C6wvvIJXLQgdPLgVTMOEHiaeFJXxzr7x1kqeTNlomdNlOlyeu2hz2Wt6qQRgwWKOA22gQ8sbPTSrYNTALcstFLYp7JGf5vhZ4Kf5Ai510upOebC379/AUQtfOgvk8YnAWDBvTn45lp66D161Xjq7dtU8ZpZL930xlTnjy5NvgTI9W97XweXeaZbxoI9i7ARwxRxBMABeKPnp59wcxgAwB21JYJHReqQNJoAAA54mDhoGNoSXNb4oN94CC7CAkwARSugHF6pgwQWLzvnBwmhBSVYaAB+CGKJLqmXgET8ociKe/4QLtJATKDfEyGQqDki5HAu9/VhUkEKGOKKR54AkEn4yXvkJA1GWSd4CBHzTgpotaqkVl0OKqNobrYklERVqOucAKQFYWV4L3ywJp5xbdhlcGQGgNiKeEWwmUhCCkseCRX1e+Q0XmS6QhJaKDhnAqI9KmsEbeimX6RKmkFnmAvTUUemE7dVJ4Ki4ltoUL29kw0KmfvBnyo1lAhDrrMvNMZitjOLqbAC6QlQNJr0C46qaLPhFgKHOtWAsQm/2ucAN2IX47LnQqjYEktP20g4Wsy4QQIUscLEECzewZAG3xQo4ILrojkjFNO3mdaYov6rZAkYG6UsPAJXGSRizAD8rsP8lBR9X0k7hMukARvoK5gCMMj63FcUV56rawLu0q/F9LLTAxRwlDEPsmi18XIzDbi2L45pcmFYGF1y0IHOjwaWsMhpUuAybtcut6cccFpUgQMwtsDDvX20FhegcRWcdwFac5sccF0krnS7TvLxsrZUtqlWMVUENhShlIN6s33NppzwiA0naBwwBEQeg7Vo/WRcgolwGCifa2in999OMJawwSmzVgcXmit8NJJ1DZ9oC0s36rZqDyDHGZ6cAZP7N5liwxziXH6IGrKe3Sn666jgR3mkAmcPOud2g0h6kdsguMIfazqpWgurCwPv768ILT/yPxhuPfKfLo/uv8yRVVEr/8q1rXn3si+OdPe3AIevHuXVa9rxOU5XAr4TQUT98h3evfzxqnOrTEp5lK/ltTHynqIO4yrc5AHnuLf6rnXbElQQCDihIAZgDGghQEWEUwwGvYsE3FJe+2fkvOAFkEhdwxagQNU6DHOzgY1ThgBaYrUVY6dwD5xTByI3Khmt6DgtdyKUfQGuD9ZthMWaDIRLKboefW1/fMki0mVkQOLT7gRaN+JIYVo0n8egaCaGoPilOEWBYzOIWtSiAJEiBAI453G5YUkIyCuaE/6pYGYy3xj62UQrRcYwqrsacFgUAK3bEXgRDlbaisUhm2evjFl/wx0CCplAxwiH/EsnDRYoq/1ARYlEAiijJF5iykrnBZMm4sElOLvJ//+pTAKwgSS2a8pRutKQSv+G4ENYRiq9k5GkEEICOyagFtbSlKc9ghQAAUolrSWGZvMXJO3oyO1y6VsluUMtlnoGZNwCkPCCjwE4J4Jd2NOMejSfNMrGgjy/4pjyZ6UwLqOQvWCCZuAKATs/9oJPB/JAxy8QFZc5znla4QQfsaRB8/iB5/Oznj/qYqIB+aAnA4gISNoqEg8rTCkFYqFV8sjn3Xe+ByayoRe+3tyhwtKMeZWYQ5sDQzf0EdiwtD5ZOqqVkbhEAPyjiK40ILCu89KAXSOoMMggA4anEerMCQJZ66lM/CmCNAf/V295YcNR5JlUJSjjDbJoKu6cK76Hu1KFgqsrWWkbwB1p1jkY5Ks+k2hWsYlXW+fQHOweIh0Us4sI5x1iUtsIzqGyN5A+swNi/ArZFG62rXSeLV6ae77Kw+8GCbiAA9DnRsJN8wQ9E29arBtWUjE3tDFa7BhYM4AZ0naxsLwDWsM6UrJjNbV+d6IAXOICttzTlaEFrytUa1wrGTS5dzzBbyoLguTGogV51m1sH3GAOnHXiLX+7xuB6N7HFTa54rXCFAVzBqJJt7gViEIPnPvcM0sUtda33q8cWlITezW9+fRpe8SbXCo5l0RJmoN71stcI7oVufOdbPbTKaKe9DS7/Fs6g3++WEgn+Ve5qrSAoAk+WvSBGcIJBcAEhTJfBmxuofkZ7y+rF85sV7i5HM/xSjs6KuSDO8YjdG90TMzin5OHCCzA7zwqbssZIRvI3gdwcFoRYxDt2rxFqcAX5ztd9SshtkYMrzyR7+aDuY2+UE0yDMk+5yijGwgtuF4TaZrl68vTmQb283IMaIHk1iDINYFDmMkMAAiCgspWxkIIUXNbBcGqzmy+b1JjOebnqvXOnhDBiGvz50pgONJqrV2hDn+92VnAzWGEn6gs42qMFTmoMbjcF92L61ZfW9A843WlPC6+dTBI1WFOgayU0OqaptmuOMSouEPgZ1rCe8g0c/2BrQtfa0J5Wgj5LxgI3F7rXtf31N4Ot6hyztwYRG8Cxkf1nMZjbCFNY9rPX3Wln5/Q5u+Y1tkvNXG57OwZGyPdz380FPpMbAuYOuBjckG5ms/vghb5fi2oNViHooQbzBmuw7z0FPeih1QlWOAtg8O9yi6EBDTA3wW/wAoSb/AVLeo4VGF5fwLKAtr1W772ju4THZs29NJhCyllggI5DAORABzoI0l1yk5t8B0hHegqQfgEpLWAJF3CC1N0s25nH4AJRcDoLxv1vMVw66GAH+dBv4GyjPzvpaE+6Eiq1BKk7odRWB/EABLWAK7ih617/edjD7gUxTGENZTd72gePdP/LlewKUwcribNwhShcwQAhdi+yOD6BCWA64Hvnuxc273fAC57woAdWC5Du3r86ZwFRwPhz9RCvAfwZ5F/PfNA3T3vO/73o7Aa97ncAbmA9lwYgeHcenmuEAQArCq+XPdhrz/y+337du48+6ztlBOBfoVItKL3o9a78BjT/+53HffR3oIby634G7iszCFpvbONnigXd/74XnkD/J9QeDEbQg/ijX3417B5ZS7BnvSdAMAADkgYnWpB531d/DEh/GIABe+AF+XcGSlBooNd/GHiBMHB9cGIABbgGyVOAMKBi5bEAmid/DfgED7gHLBiBE1iBS0d4GNh/aVd+IjgBxMb/JBv3gSHoBm5gBHByBUAnf7Rnf/W3gi3YghKoBxTYaTI4g/5Hfmpwg5U3ATRQc1vFcX82fX3SAj74Z0AoJQnYfAw4fwyIhEmYhEt4BrX2hFBIhVUYh1fwJsyxBDUAa50yAHdXbm4wd1FzBWKwgCmoghCYhobogkzohG5YfhMwhXH4iJAIAnH4ahz4KpcncD7ofWSYgmh4iIYogTVwBk6we4xYeWFwiqh4ikewikcQBpA4ia+GhUziBgKHebPHfA3YiZ54iBjgBjUQdU8IA6aYiqjIisaoAa5YhaxYeZcoBvzCIrQocAqIi4S4i7uIA9iIA734i6OYdDZYgBNAjKpo/4yrqAHmaI5hQI5H0AAeV4sbMABGEwVaII0n2Hy6aI1JmI3ZuAe+CIxIB4fDmI7qeI4EaY6sGHSXVosKaW7TKH/3iI8sqI/76ItKkHTC+IoCeYwFuZHdt5AfJ3tE+IAPaY0SKZH8+Is7UGavWIUCuZEcCXKbp3y1qHwOKZIiCZElmZPYeJLGtpKPGAYueY5DyHwy2X2aWHs2mZQkqZNMyZM++ZMuOZREyHdGCZObl5RK6YlMuZU76YuS+JSVV45CaZVESHtS6QXx531YOZJpyJVc6ZQ+SY4acJZlWYTN131reZMQuQduuZUn2V59FpiCiXOW5nOXBgZABwa16IOMyaaYHjmTdcmWafgEhwiKNZBhxjUFmrmZm5lvnultnhmansmZpLkBpnmaqJmaG+CZIIBgrtmYsBmbsumYP1gD13UDuJmbuqmba9CbaxAEwBmcQVADw1mcwimcvpmcFreczNmczsmcNRCd0jmd1Fmd1imdN8AFY7Cd3Nmd3tmdUhCe4llF5FmeVRSe5pme6lmetNGe7vmetHEFLCCf8jmf9kmf9kk0IhAIACH5BAUKAH8ALCwADQAQAA4AAAdlgH+CglRfX29Ug4qCVToqfZAoTCWLf1+PkJl9cpSDbz2aoQ+KcqGhKgSCDKCmmkyqFK2acnV/DLGykHKCJay5TAQWBDq5fRSpgm+YraODBEy4oToWlVUSmj0P1JV/dSVMTFW1i4EAIfkEBQoAfwAsOwARAA8ADgAAB2GAf4KCBARVDHV1g4sEXygUfX0qcgSLfwQPkZqRPW8WhEyboj2Kf3UqoqIPn6GpmygAf3KumxQAFrO0kbZ/rbo9sXWQunKDua4qn4IAEq4UVZYAD6iaEm+WgwBfTEwEsYOBACH5BAUKAH8ALEcAHQAPAA4AAAdegH+CggBVTFUWZYOLAHIUfZAUOg6LfwAokJmQPQR/AX9lEpqjKJ9/VaOpTIpyqaMSZaGumqWys5Clf623fTqCJY+3VYJlu64SpqA6rigClX9MmJsPSc+CAQAMAMmCgQAh+QQFCgB/ACxKAC0AEAAOAAAHZIB/goICTA9yTAGDi4IPFH2QfSoPAoxlOpGZfXICP4RMmppMgwI9oZkonn8lp5opgl+tmV+wspEZgj+PshQzMwICmLI6SIMEKq0qr4sZyJo9CYyCZzkoFBQoD1jSglZ/vkjFjIEAIfkEBQoAfwAsRwA8AA8ADwAAB2qAf4KCM4UzSIOJfzMJEj0UPRIIF4pID32YmX06lIOXmpo5SoI7FKCaFBGCn6eZOkZnEq2aKEYXsrOYKDR/OrmYEiB/Xb99DzB/Rii5KmqDCSqtFHBOiWrLtAiKgjQROTo5cBDbg25u5H+BACH5BAUKAH8ALDsASAAQAA8AAAdrgH+Cg0ZGICCDiYMxXRI9PRIPiIoxahJ9mJk9EYo0KJmgfSoaEIJGOaGhOqV/ID2poBRHrWGwoV2CEbagZGAwE7uZcIIwn7sqXoNkwTmJbpewKMmDbm46FKESfIqCYnY5EhI5cNPcg2IN5oEAIfkEBQoAfwAsLABMAA8ADgAAB2OAf4J/ICBOakYxg4t/Ozk9fX0UEgggjAmQkZp9D5aCNJmbmmRGfzE5oqIqlqCpol1/MBSumzl/ELO0kTluEKG0ZII6upJHEBN8Krq2g125ohINjHAomyo5YoyCYnZkD3DSi4EAIfkEBQoAfwAsHABIAA8ADwAAB2eAf4KDSoWDh4IpETooPSg5aimIOzkUfZeXFF1Og5SYn5ddgwiWoJgqO4I6pqBkf2oorJ86fzs9spgSIDuxuH20fzm+faJqGqWsKDCDZLIUEYhkKqAo0Ih/Rw86EjpdE9eCEBB/39eBACH5BAUKAH8ALBAAPAAPAA4AAAdfgH+CgliFDg6DiX9YXzo9FD0STIqLD32XmH06iIOWmZlyWIIEFJ+fX4Keppg6ghKrmSiFr7CXPUovOrWXKEp/TLt9D4tYKLUqKYMJKqsUCMnKtJgoqJQpXw86cghqiYEAIfkEBQoAfwAsDAAtAA8ADgAAB2CAf4J/dVUPcg9VFoOMbxJ9kJAob4x/byqRmRRVg3UomaA9i39MoKYPgjqmoBIOWI+rkSgWdbCxfSgOfw+3kHJ1dW8UvZx/WLyxOoxYcqs6ustfEioUKhJM0JXGWNxYlYEAIfkEBQoAfwAsEAAdABAADgAAB2WAf4KCb0wPTG8Mg4t/bzoUfZEUEm+MfxkokZqRKhmLbxKboj0lg0yiqA8Efww6qKISqwyhr5o9FgQltLV9KHWCcryREhaCVZC8TIMEwbUSv4N1u5sopYwETJmRPaqW0SVVJcWMgQAh+QQFCgB/ACwcABEADwAOAAAHYIB/gn9DQ18IhYOKf18SKn19FBJMQ4tMj5CZFA+DGQiYmZpMgkMSoad9KFR/DBSop1+Mr6cPBF+zoZxUrrh9sW+muD0Egky8r0wlgw+zcsSDDEw9oT3Ji4IlX0wPVc+DgQAh+QQFCgB/ACwsAA0AEAAOAAAHXIB/goIBLCxzDoOKgnN+C36QC1x1i38sj5CZjliKc5iamS2Kn6CRc4ICpKV+XII3qqULBH+vq5oLhLCgLXV1JbaZCyyDnsC4gyVcun4AlZe3Lc2VfwAsXEssN5WBACH5BAUKAH8ALDsAEQAPAA4AAAdbgH+CggBzLCxJWIOLWFwLC35+kCyLfw6QkZmSLA6CdS2aoQuUfyyYoZkLAH9cqKJJf6euknN1sq4Lc3+gs5ELN6W3qIPCqaR/AsULXJVlj5rLlYNzXC0tLAGLgQAh+QQFCgB/ACxHAB0ADwAOAAAHUoB/goJcXC0tXAGDi1x+C36Qj1yLf1yPkJiOk2V/AZeZmAuTlaClLYqlpQtJnqmZC3OtrpFzf5+zhLOQS4IBuquDjamilH8tnwvExX9zhVy1i4EAIfkEBQoAfwAsAAAAAGgAZQAAB/+AO0cahIUafHyEiImKi46HjoiGg4aFkZIaDZoNRxOen6AQoqIwpaYgqKkxRKytrTFTQjcsgpWWi5WXjIe2vb6VR8HBoMQQMKPGp6mprs1EedAGsiwglL6YjZEawtzBv4Sbmt3cxKGmpcvLzq3Q7dAmBkGzNA3fuN/j3JXhnPnkxaRgpBuIqpm7gyYSwgtihgWMer+wjSvkLxi/f57ylVNGkCC7g+8UKjQQb0Cbh7u+betGqGInf+W6eTonsGPHVyDbJSTJU9qaAVLoYVO5smK5CeOOzqTJ0Wa6GKuegdzZs6eQNXO41CI6SabST0k/MR3rFEQMVGfRQo0RUmTVqlf/50jZqnLTsK9HuQEBMrZvTWZpVald23YnvLckhSgWYkYuDGu+xN3Fu3ECEFB+aQ5cexaq2pxuEUtbvLjhSUiXKHrFy9QTsr4gBP59ypktq5x5Rh5+S3pxFjMDxqAc2nX1xrHIkp2LXbYgbtxUefcm7aHhmGpEk9L8pJxmwKY2Pz53F9rqdMVZ0qevHhw7vmB7xZ5L7temhz9RohhwNj63yJFwnYeeellUx8J1kAEjU3IMdtccCAP8IeGES0wBFXT/KTRDdDwJKMR6HoQYYkMiuFeUURMwyNSDqEzo4oQGkJchgCRxmJiAQYAoInBtlIiUSwtmFttsHUX44oszblie/2ijnReEiFB6oIVJPgJ5xF5YZrYcUh4RceSRUfinkBD5RWEGkx32RmCUUE7JRYkQ5INlfJjVBwMfOrwYQTpCfPniEm4tASaTvhGoI5seDEAlCBC0NOdXZMkGAhx+kuGKFn6+2FOmSwRImqEFsmnGqMBRGcNjk0G64jIaZPoHAq0Y6eofPM0ahYcfHlodqbxOaSoMPx4FW0d5uroBK1nMKiFJUSjroXpR9qrFtNMq2uOpxGgJHghEgKDCrGpIpax+Qgg6qxbThSoqtexSa5oIp2pLVjpEqKFsF+006+piyuL3aY6IStkutYoOwMK1Q8qrGbesgGDvrPj6Z+6XWfA7bv+h6kZLcMEcm4Swwuio40oeyiYgpgH6vlixxbMOAGq0pHYss0lLLBADDVqG54yyu/GURRRLtLDEANNNnOnLI/I66swdt9HGAnkkvK3OrkTgahdoNjndrEvouqvSpTKtaBRPR00ki2a5AhUZFHyZQ8+eqpleHJwGDPbGYps0BtTdoq0WAmR0IdU7CKDwItaifQoqtF8uEaUPLUg49MDV5t1GzXn07XcOL+qwQ1tqdNFFAmjKvbh6QEuuBZQ+fNlCu3kX3OMCJmjuVCsg+NnDEWLaKB3Gp0OLqNF/xi4z0H7UfhN/rqKQddy5Br8mm5i62oLxHLfRggiZi7wOK1ZfDXf/4qYvHqLwURL/Jfay12xC9wx//wznsxL6rPTnZxzitP2yb20LtMuc/G6TG2U5L024GhD+DjWqavUrCv4D2gJitI6p5KFYrtIarqTnNSiZgWAP9J/2JijA8SiEfpnSgQbPE7yAIeqD1Opf7AhGtgm+z4QKmcCsEJBABaLPhdRjV+SsJ7Z2JcppNuxPdFD4pQQiDYguZJeiWjYzylWrZga44VR8h0FN3W96UAQi3oboOplZUQsnmBIWtTgj0XThRWQQwgFY+MQwRrFyBSsjx854gj6qcQFCyOKMfNcTE6hBgxzUnx2DiMeCRaEFkISgGSnXRz8OYAkiCOQgCXkj4CWS/00/hCLs2GfFSlZySlHIpCAH+TvFJdJQUepgwEaJvYGZ0pSoVOUm4ZICMuSADFTw5CtlaceBDcBXBYPkAvzQAh8cs123rGSiUqlJt4xPCUeSwA6G+Uko4tKKBVvAMv1ATnE+c1rRPIGIBkDNGqFpB5naJjdbyKZ0pvGMiiKnPvfphwHYU51xiMM0Afm8EMxqnviLwz/vecZptYCf/GzBPz0Q0BOwswWBRFMIEACx0+XvdAENaUX/2dBpRWGcEC1nFEz5gUqKVAuPVAyTQmBQZXn0o1kQqU5DGiKBRrOU1PJBSvnpgw8YtaVxOIBSlRqHDzzSAxl9SwgMUNODenSnWP/FqgfSaUuGPnSo+mzBB0S61KXGAaYtgOpMDdBFPxkqq3CFK0mt+FWw+sEHZC0rU52aViFMdao9mWqfZpUDOTI1rojV6Vwp5wOUpnQBeA2pXpfKV6j+laok+asQSJYpCmyzrIkN7VbtacUPaMGu5NSpXml6gA8MwAeWxexlBTtYPxV2sqGN62hZelSjaqG3R23sUCGb16XSlKZxeK1aZ0tV1h4gBA9w22QPm9udjha42M2uUYXLT+JK9rnHDa8NLhnb4zb3uH49gBq6GIGyZkGv1VUscEWq3ew2Vpz49YENJhve8J4Vtlnor4DDO92lpge38e1jXOuL3TiUaQD7XS3/ePsrAxs41QMBHvCAC9zfAh8grmZdcG+b2uCA2uDEJ+ZveGUgAxK4uMIXnrCGOyzjDXv4wx4W8YhNfOI4oPjHBWaxkF1MZBhHwQM1nnF4YSAhFXwrAQS+cYETa1SR/hjFIOjiEZYq5C6zmMhFtvCRk6xkg7btRTnQcFlDkGO4XvnNKP7DmV2UAy97GcwkqEAFSCCDC2e4zMd9o20B7VwE8xjOiLaBoP10Zzy7WM8uqECfx0xomprgW5lSA6E9nOhO26ACmP5SH2DwZTDr+dQuSLWkY9zf8wqYDISttHEL7WkUexnWrsrBo08N6VT7OtKTjoNfK93WL1VawM+d7I/t/9zlYh+J17+OtqpJEIcRxOHYFTBgBfqrZ0CX1dbMZna/8txraft6zx+wtoArYIBt8zoEFWDilyQAb16fWsnhzveQW9yvCpg71Qr49Z6rLWN2n7rd9dYzR2dl74Yft9T6brSjSUApV5Eh4NEOuAI2znEXkMAGIwiwu9ttb4R3e1aRbvied+3ofE/c0Xue1R0yzvGaryDgHx8BeA1e8pHrmcl+soPKh47nLvO5xS8nt73dkCk7ALzmG1+B1Ke+Ao+D/AAkH3qv/e0CCHxJA6gWuL9VnvSXq/zXYpDAkewAdaq7neqSTsNzof1vabcqBw9werQb8KKZp5zsZSc3pQz3B/8yZBwKD+Ac223+9sYrIO7wrrvkny5tP8FB2oA3tZ7v4KcGQP3zUW/81ItA+iKsAPJcn7zqfe2qy4vd3kqHdqZ6gAcXKED0jS9CE3bfhNKTvuoySEPkVz/5jfNhVhhXda9nfmoS+FoB0XXV7XEvdd3z/vq8L73H5Z564j9/43fg+L2Sn+q1l9sFympC49nAfutj//1NKEATrD781SvgDuEH/Qr69XwxVCr1nDcrcLACpJd9RcAGBQh/8VcADMiA6vdxMtB90sZx+FeBoLdxyqIDGKcAredrfKcsCPh+Cch7DViCW9CATaAAECiB4FeBLmiB+qcsORB10ac7tnd7yoL/A+73fiXIgFvwg0AYhAVwejYQgU9Xcy/4ghT4gsoCBxxHeJnihDg4K713fT1ogkGYhT9YAEXgAjaQZ0e4hElIekmIfyuAB8gXdcryAFOnLEVwhXCohXK4BV34hakHdRXoe6VXhhW4aC+iAnRQc/1Cdc4mIXG4hQ04h3I4fy3mb3iIf3roe2NYek1QcS8SiIIogFPHfvL2BzpwglioiKIIhEWgAC12gy0IiZG4iqzYBLiWA3DAfmxAdZ34IvIni+yHhsWCAwUAij44iorIAcJ4gitwikiYh6yohyLYBGxwfaTHfpuIhq6Ci7jIgM24gIkIjHIojNxIjMYohqpIiQpo/4XyZ4DPCI1TZ4lHQo3st3vXeIXauI3cOIxD+I3g53vjSI5XiH2lB424SAdfwo67B4fZKIocAITzmJDcOIQuNn13UH3iyIMEWYL5yI5sgIZkwIu4OJATWZBBqJAgGZIMWQHTN3r4iI0dmZIV2YwOGH9skJI9qIUhOZMJuQWnR5ImOYIw6ZGISJHwt5Md+ZE0OZQ12QR6dnsjiH0TqY37CJRLKY9EGZUHyQZ6RoBJqZS/GI9A6JQEGY9SKZVbYJQVQIDjyJNa2ZNcmZUG+ZVgSZUusIMoGYpnKYRpqZYIyZZ4OYxG6QJkqXt++ZMxGY/7SIJcOYp5KZIrAIY3d4GMZ46VV+mMqyiL1LeJFvmOcXmIcyl/LlBhSDdxDfdvGmd7oiltVad8/uZ8qOlx/1Z1oleZK9CMfxmbVRibUldhaZAGJ4abNqCbuploERdub4abwrlsERd4LqaaqZmcyKmaLAZyPuADUTAC0Tmd0lmd1DkC2Imdt7md3Mmd2fmd4Bme4ime3Vme5nme5omdLRAIACH5BAUKAH8ALEYAPAAQAA4AAAdSgH+Cg39Rhh+EiXE+C42NS3GJggt+lZaVNomUl5yEjJycPoOboJeCNqSlfguZqKqWrCR/r7Aygj60qy6nqZwLUYRRvZULS5LCvsCSfyTIv7aDgQAh+QQFCgB/ACw4AEgAEwAQAAAHbIB/goODMjY2MoSKhDItC48LLVGLgiR/UQt+mpsLk5SYm6F+nQp/FYMumaKiLoppqquaCyOKUbGiPrW3oS1FhLa7mp6FsLELaYQKCi3BC5Quu8eUK9DFmsiUgisjmY9RbAXZg01NbCvilE3igQAh+QQFCgB/ACwrAEwAEAAPAAAHX4B/goNpUVEjMoOKglELC35+jj4kiy6NkJiQC5SDI4+ZmS2DCp+gmAsjgjalppA+gmmsrQuwsqYLCiurraGDvKcjbH8rnr+0ij62kSuLf5enC8zNfxUjPi2HTdOL2s2BACH5BAUKAH8ALBwASAAPAA8AAAdbgH+CgzaFg4eCFVF+C41+UYh/JAt+lZYLLRWDFZadlj6Di56dCyOCo6MtLjKUqJ1/Nq2ujCSTs5aCsq6gdyO6ngsygy2uC5CDKz6/xpF/I8SUPsKRRdV/CkWRgQAh+QQFCgB/ACwPADwAEAAPAAAHWoB/goMHA1EDcYOKggdLfguQCz4Hi38HC36Zmpk+iy2boH6UglGYoZpLg6anmoOsmwsflqussRV/r5oLNoI+uY+DcbSgC1GKpacLqYvIsMuVIcjKvJWLLi6VgQAh+QQFCgB/ACwKAC0AEQAOAAAHWIB/goMeSwt+C0seg4x/QlGHfpKIS42ChpOZiY0ekZmTCz6MS5+lCwchBn+lpot/B56siAOCsLKgWoOxslmDPruflYNZwJkHjbasUZaXC87OwsyDUdRxjYEAIfkEBQoAfwAsEAAdAA8ADgAAB1KAf4KCUVwtS1Emg4sDfgt+kI9Ri39Rj5CYjpOCBpeZmAsehJ+kLQZ/pKQLQp2pmaEGrq+inrJClbWkS4OyjnGDQq4Lm8AtngvDt5R/HlGIA5SBADs=';
             $(objThis.element).find('[id=imageGalleryLoader]').attr('src', imageGalleryLoader);
             
           	 objThis.addListeners();
           	 objThis.hideZoomButtons();
           	 
            var startDiff = 0;
            var minDist = 0;
            var atEnd = 0;
            var stopZoom = false;
            $(objThis.element).find("#expandedImageContainer").on("touchmove", function(event) {
                //only run code if the user has two fingers touching
                if (event.originalEvent.touches.length === 2) {
                    noSwiping = true;
                    event.preventDefault();
                    var YDiff = (event.originalEvent.touches[1].pageY - event.originalEvent.touches[0].pageY);
                    var XDiff = (event.originalEvent.touches[1].pageX - event.originalEvent.touches[0].pageX);
                    var _temp = Math.sqrt(YDiff * YDiff + XDiff * XDiff) - startDiff + atEnd;
                    objThis.member.curZoomLev = (_temp) / 150;
                    //+ objThis.member.curZoomLev;
                    if (objThis.member.curZoomLev <= 2.5 && objThis.member.curZoomLev >= .5) {
                        minDist = _temp;
                    }
                    //   objThis.member.curZoomLev = (minDist) / 200 ;//+ objThis.member.curZoomLev;
                    if (objThis.member.curZoomLev > 2) {
                        objThis.member.curZoomLev = 2;
                    }
                    if (objThis.member.curZoomLev < 1) {
                        objThis.member.curZoomLev = 1;
                    }
                    var scaleFunc = "scale(" + objThis.member.curZoomLev + "," + objThis.member.curZoomLev + ")";

                    var cssObj = {};
                    cssObj["-ms-transform"] = scaleFunc;
                    cssObj["-moz-transform"] = scaleFunc;
                    cssObj["-webkit-transform"] = scaleFunc;
                    cssObj["-o-transform"] = scaleFunc;
                    cssObj["-ms-transform-origin"] = "50% 50%";
                    cssObj["-moz-transform-origin"] = "50% 50%";
                    cssObj["-webkit-transform-origin"] = "50% 50%";
                    cssObj["-o-transform-origin"] = "50% 50%";
                    objThis.element.find("#imageGalleryExpanded").css(cssObj);
                    objThis.repositionImage();
                } else if (event.originalEvent.touches.length === 1) {
                    noSwiping = false;
                }
            }).on("touchstart", function(e) {
                if (e.originalEvent.touches.length === 2) {
                    //start-over
                    var YDiff = (e.originalEvent.touches[1].pageY - e.originalEvent.touches[0].pageY)
                    var XDiff = (e.originalEvent.touches[1].pageX - e.originalEvent.touches[0].pageX)
                    startDiff = Math.sqrt(YDiff * YDiff + XDiff * XDiff);
                    //objThis.handleMouseDown(e);
                }
            }).on("touchend", function() {
                atEnd = minDist;
            });

            $(objThis.element).find("#expandedImageContainer").on('tap', function(e) {
                var now = (new Date()).valueOf();
                var diff = (now - lastTap);
                lastTap = now;
                if (diff < 250 && ismobile) {
                    objThis.zoomInOutInnerPanelImage(null, 1);
                    objThis.repositionImage();
                    atEnd = 0;
                }
            });
        },

        _create : function() 
        {
            $.extend( this.options, this.element.jqmData( 'options' ) );
        },

        addListeners : function() {
        	var objThis = this;
        	
            $(objThis.element).find("#imageGalleryExpanded").bind("mousedown", function(e) {
                objThis.handleMouseDown(e);
            });
            
            $(objThis.element).find("#imageGalleryExpanded").bind("mouseup", function(e) {
                objThis.handleMouseUp(e);
            });

            $(objThis.element).find("#imageGalleryExpanded").bind("touchstart", function(e) {
                objThis.handleMouseDown(e);
            });
            
            $(objThis.element).find("#imageGalleryExpanded").bind("touchend", function(e) {
                objThis.handleMouseUp(e);
            });
            
            $(objThis.element).find("#imageGalleryExpanded").bind("mouseleave", function(e) {
                objThis.handleMouseUp(e);
            });
            
            $( objThis.element ).find( "#expandedImageContainer" ).bind( 'swipeleft', function( e ) 
            {
                if ( noSwiping ) 
                {
                    return;
                }
                if ( $( objThis.element ).find('[type="nextExpandedImg"]' ).css( 'visibility' ) != "hidden" ) 
                {
                    if ( ( objThis.member.curZoomLev == 1 ) && ismobile ) 
                    {
                        objThis.showExpandedImage( 1 );
                    }
                }
            });
            
            $( objThis.element ).find( "#expandedImageContainer" ).bind( 'swiperight', function() 
            {
                if ( noSwiping ) 
                {
                    return;
                }
                if ( $( objThis.element ).find( '[type="prevExpandedImg"]' ).css( 'visibility' ) != "hidden" ) 
                {
                    if ( ( objThis.member.curZoomLev == 1 ) && ismobile ) 
                    {
                        objThis.showExpandedImage( -1 );
                    }
                }
            });
        },

		hideZoomButtons: function()
		{
			if ( ismobile ) {
	           $( this.element ).find( '[id = iGZoomInBtn]' ).css( 'display', 'none' );
	           $( this.element ).find( '[id = iGZoomOutBtn]' ).css( 'display', 'none' );
	        }
		},
		
        handleMouseDown : function(e) {
            if ( e.originalEvent.touches && e.originalEvent.touches.length == 2 ) 
            {
                return;
            }
            var objThis = this;
            e.preventDefault();
            if ( objThis.member.curZoomLev <= 1 ) 
            {
                return;
            }

            var scaleVal = objThis.member.curZoomLev;
            var oldPosX;
            var oldPosY;
            var _imageObj = $( objThis.element ).find( "#imageGalleryExpanded" );
            var _imageParentDivObj = $( objThis.element ).find( "#expandedImageContainer" );
            var _ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);

            if (e.offsetX == undefined) {
                if (_ismobile != null) {
                    var _pX = e.originalEvent.touches[0].pageX;
                    var _pY = e.originalEvent.touches[0].pageY;
                } else {
                    var _pX = e.pageX;
                    var _pY = e.pageY;
                }
                oldPosX = _pX - _imageObj.offset().left;
                oldPosY = _pY - _imageObj.offset().top;
            } else {
                oldPosX = e.offsetX;
                oldPosY = e.offsetY;
            }

            var imageParentdivHeight = _imageParentDivObj.height();
            var imageParentdivWidth = _imageParentDivObj.width();
            var imageHeight = _imageObj.height();
            var imageWidth = _imageObj.width();
            var scaleImageWidth = _imageObj.width() * scaleVal;
            var scaledImageHeight = _imageObj.height() * scaleVal;

            objThis.member.nGapFromTop = Math.abs((_imageObj.height() - imageParentdivHeight) / 2);

            var _diffWidth = imageParentdivWidth - imageWidth;
            var _diffHeight = imageParentdivHeight - imageHeight;
            //_diff = _diff / 2;
            var diffLeft = scaleImageWidth - imageWidth - _diffWidth;
            var diffTop = scaledImageHeight - imageHeight;

            _imageObj.bind("mousemove touchmove", function(event) {
                if (event.originalEvent.touches && event.originalEvent.touches.length == 2) {
                    return;
                }
                objThis.member.hasPanned = true;
                var newPosX;
                var newPosY;
                if (event.offsetX == undefined) {
                    if (_ismobile != null) {
                        var _pNewX = event.originalEvent.touches[0].pageX;
                        var _pNewY = event.originalEvent.touches[0].pageY;
                    } else {
                        var _pNewX = event.pageX;
                        var _pNewY = event.pageY;
                    }
                    newPosX = _pNewX - _imageObj.offset().left;
                    newPosY = _pNewY - _imageObj.offset().top;
                } else {
                    newPosX = event.offsetX;
                    newPosY = event.offsetY;
                }
                var lft = _imageObj.css("left");
                lft = parseInt(lft);

                var tp = _imageObj.css("top");
                tp = parseInt(tp);
                var leftMoveVal = lft + newPosX - oldPosX;
                var topMoveVal = tp + newPosY - oldPosY;
                if (scaleImageWidth > imageParentdivWidth) {
                    if ((leftMoveVal <= diffLeft / 2 ) && (leftMoveVal * -1 < diffLeft / 2 )) {
                        _imageObj.css("left", leftMoveVal);
                    } else {
                        if (leftMoveVal < 0) {
                            _imageObj.css("left", -diffLeft / 2);
                        } else {
                            _imageObj.css("left", diffLeft / 2);
                        }
                    }
                }

                if (scaledImageHeight > imageParentdivHeight) {
                    if (objThis.member.nGapFromTop > 0) {
                        //...fix the position problem while panning
                        if ((topMoveVal <= diffTop / 2 ) && ((topMoveVal * -1 < diffTop / 2 - (objThis.member.nGapFromTop * 2) ) )) {
                            _imageObj.css("top", topMoveVal);
                        }
                    } else {
                        // run normally
                        if ((topMoveVal <= diffTop / 2 ) && ((topMoveVal * -1 < diffTop / 2 ) )) {
                            _imageObj.css("top", topMoveVal);
                        } else {
                            if (topMoveVal < 0) {
                                _imageObj.css("top", -diffTop / 2);
                            } else {
                                _imageObj.css("top", diffTop / 2);
                            }
                        }
                    }
                }

            });
            //e.preventDefault();
        },

        handleMouseUp : function() 
        {
        	var objThis = this;
            $(objThis.element).find("#imageGalleryExpanded").unbind( "mousemove touchmove" );
        },

        showImageGalleryPanel : function( viewBtnId, objComp ) 
        {
            var objThis = this;
            var _audioLauncherBtn = $("#audioLauncherBtn");
            objThis.member.curViewBtnClicked = viewBtnId;
            objThis.member.currentComponent = objComp;

            // If audio is open, close it
            //var audioBtnLauncherDataRole = _audioLauncherBtn.attr( 'data-role' );
            if ( $("#audioPopUp").css("display") == "block" ) 
			{
				$("#audioPlayerCloseBtn").trigger('click');
			}
			
            // Hide expanded image container div on opening
            $( objThis.element ).find( '[id = imageGalleryImageContainer]' ).hide();
            $( objThis.element ).stop( true, true ).slideDown( 500, "easeOutBounce" );
            // If openAsPopup is true, open component as a popup.
            if ( objThis.options.openAsPopup ) 
            {
                PopupManager.addPopup($( objThis.element ), null, {
                    isModal : true,
                    isFloatable : false,
                    popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                    callbackFunc : "hideImageGalleryPanel",
                    caller : this,
                    isTapLayoutDisabled : true,
                    isToCallbackOnTapOutside : true
                });
            }

            // If array is already populated, no need to load XML again.
            if ( GlobalModel.imageGalleryDataArr.length > 1 ) 
            {
                objThis.populateImageGalleryPanel();
            } 
            else 
            {
                objThis.loadRequiredXML();
            }
        },

        hideImageGalleryPanel : function() {
            var objThis = this;
            $(objThis.element).slideUp(500, "easeInOutExpo");

            // Reset image thumbnail div, and hide collapse/contract button after hiding image gallery.
            setTimeout(function() {
                $(objThis.element).find('[id="imageGalleryContractBtn"]').hide();
                $(objThis.element).find('[id=imageOuterContainer]').css('left', 0);
                // Remove overlay div if component opened as a popup
                if (objThis.options.openAsPopup) {
                    PopupManager.removePopupmanagerOverlay();
                }
            }, 500);
        },

        populateImageGalleryPanel : function() {
            var objThis = this;
            var curBtnId = objThis.member.curViewBtnClicked;		//$(objThis.member.curViewBtnClicked).attr('data-galleryid');
            var imgGalleryDataArr = GlobalModel.imageGalleryDataArr;
            var url = "";
            
            if( imgGalleryDataArr[curBtnId].item && imgGalleryDataArr[curBtnId].item.length == undefined )
            {
            	var totalImages = 1;
            }
            else
            {
            	var totalImages = imgGalleryDataArr[curBtnId].item.length;
            }
            
            var imgFolderPath = getPathManager().getEPubImagePath();
            // remove the already existing imageInnerContainer divs, as these will be created again.
            $(objThis.element).find('[id=imageOuterContainer]').find('[type="imageInnerContainer"]').remove();
            //$(objThis.element).find('[id=imageOuterContainer]').css('left', 0)
            for (var i = 0; i < totalImages; i++) {
            	if( totalImages == 1 )
            	{
            		url = imgGalleryDataArr[curBtnId].item.thumbnail
            	}
            	else
            	{
            		url = imgGalleryDataArr[curBtnId].item[i].thumbnail
            	}
            	
                $(objThis.element).find('[id=imageOuterContainer]').append('<div type="imageInnerContainer" class="imageInnerContainer"><img class="imageGalleryThumbnailImage" src="' + imgFolderPath + url + '"></div>')
            }

            // If any image is selected, remove the selection class as new divs are created.
            if (objThis.member.curImageSelected != null) {
                objThis.member.curImageSelected.removeClass('imageInnerContainerSelected');
            }

            $(objThis.element).find('[type="imageInnerContainer"]').each(function() {
                $(this).unbind('click').bind('click', function() {
                    if (!$(this).hasClass('imageInnerContainerSelected')) {// check added for MREAD-551
                        var imgIndex = $(this).parent().children().index($(this));
                        objThis.member.curImageIndex = imgIndex;
                        if( totalImages > 1 )
                        {
                        	var imgSrcPath = imgFolderPath + imgGalleryDataArr[curBtnId].item[imgIndex].image;	
                        }
                        else
                        {
                        	var imgSrcPath = imgFolderPath + imgGalleryDataArr[curBtnId].item.image;
                        }
                        objThis.expandImageBox(this, imgSrcPath);
                    }
                });
            });

            // Calculating the length of imageOutercontainer (Thumbnail container)
            var imageContainerW = (parseInt($('.imageInnerContainer').outerWidth()) + parseInt($('.imageInnerContainer').css('margin-left')) + parseInt($('.imageInnerContainer').css('margin-right'))) * totalImages;
            $(objThis.element).find('[id=imageOuterContainer]').css('width', imageContainerW);
            var imgPrevComp = $(objThis.element).find('[type="imageGalleryPrev"]').data('buttonComp');
            var imgNextComp = $(objThis.element).find('[type="imageGalleryNext"]').data('buttonComp');
            // Disable the prev arrow by default
            imgPrevComp.disable();
            imgNextComp.enable();
            var outerContainerparentWidth = $(objThis.element).find('[id=outerContainerParent]').width();
            // Total number of images' width is less than container width, disable next arrow.
            if (outerContainerparentWidth > imageContainerW) {
                imgNextComp.disable();
            }
            var ref = $("#bookContentContainer").data('scrollviewcomp');
            ref.updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent);

            $(objThis.element).find("[id=imageOuterContainer]").css({
                'transition' : 'left 0.5s',
                '-webkit-transition' : 'left 0.5s',
                '-moz-transition' : 'left 0.5s',
                '-o-transition' : 'left 0.5s',
                'left' : 0 + 'px'
            });
        },

        loadRequiredXML : function() {
            var objThis = this;
            var objPathManager = getPathManager();
            var strUri = objPathManager.getImageGalleryXMLPath(EPubConfig.imageGalleryXMLPath);
            var returnVal = null;
            // If file type is JSON
            if (strUri.indexOf('.json') != -1) {
                $.getJSON(strUri, function(objData) {
                    if (objData.galleries) {
                        objData = objData.galleries;
                    }
                    //objThis.member.glossaryData = objData;
                    objThis.imageGalleryXMLLoadComplete(objData);
                });
            }
            // If file type is xml
            else if (strUri.indexOf('.xml') != -1) {
                $.ajax({
                    type : "GET",
                    url : strUri,
                    processData : false,
                    contentType : "plain/text",
                    success : function(objData) {
                        var strXML;
                        if (window.ActiveXObject) {
                            var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
                            xmlObject.async = false;
                            strXML = objData;
                        } else {
                            strXML = new XMLSerializer().serializeToString(objData);
                        }
                        var imageGalleryData = $.xml2json(strXML);
                        objThis.imageGalleryXMLLoadComplete(imageGalleryData);
                    },
                    error : function(e) {
                        console.log(e.error);
                    }
                });
            }
            return returnVal;
        },

        imageGalleryXMLLoadComplete : function(objData) {
            var objThis = this;
            var imageGalleryData = objData;
            objThis.member.imageData = objData;
            var imageGalleryDataLength;
            var galleryId;
            if (imageGalleryData.gallery.length) {
                imageGalleryDataLength = imageGalleryData.gallery.length;
                for (var i = 0; i < imageGalleryDataLength; i++) {
                    galleryId = imageGalleryData.gallery[i].id;
                    GlobalModel.imageGalleryDataArr[galleryId] = imageGalleryData.gallery[i];
                }
            } else {
                galleryId = imageGalleryData.gallery.id;
                GlobalModel.imageGalleryDataArr[galleryId] = imageGalleryData.gallery;
            }
            objThis.populateImageGalleryPanel();
        },

        expandImageBox : function(imgRef, imgSrcPath) {
            var objThis = this;
            var imgSrc = imgSrcPath;
            $(objThis.element).find('[id=iGZoomInBtn]').css('visibility', 'visible');
            $(objThis.element).find('[id=iGZoomOutBtn]').css('visibility', 'visible');
            // If imageGalleryImageContainer is not displayed, calculate its height.
            if ($(objThis.element).find('[id=imageGalleryImageContainer]').css('display') == 'none') {
                var componentBottom = parseInt($($(objThis.member.currentComponent.element)).css('bottom'));
                var imgGalleryHeight = $(objThis.member.currentComponent.element).height();
                var imgGalleryContainer = $(objThis.element).find('[id=imageGalleryImageContainer]');
                var imgGalleryContainerHeight = window.height - imgGalleryHeight - parseInt($(imgGalleryContainer).css('padding-top')) - 
                								parseInt($(imgGalleryContainer).css('padding-bottom')) - componentBottom;
                $(objThis.element).find('[id=imageGalleryImageContainer]').height(imgGalleryContainerHeight - 10);
                // resetImagePos will position the loading image.
                objThis.resetImagePos();
            }
            // Hide the image by default.
            $(objThis.element).find("#imageGalleryExpanded").css('visibility', 'hidden');
            $(objThis.element).find('[id=imageGalleryImageContainer]').slideDown(300);
            $(objThis.element).find('[id=imageGalleryContractBtn]').css('display', 'block');
            $(document).trigger("imageBoxExpanded");
            objThis.showExpandedImage();
        },

        /** This function displays the expanded image.
         * bVal is true if next image is called, it is false if prev image is called.
         * @param {Object} bVal
         */
        showExpandedImage : function(bVal) {
            var objThis = this;
            // resetImagePos will position the loading image.
            //objThis.resetImagePos();
            // Disable the prev and next navigation buttons.
            $(objThis.element).find("#prevExpandedImg").data('buttonComp').disable();
            $(objThis.element).find("#nextExpandedImg").data('buttonComp').disable();
            $(objThis.member.currentComponent.element).find('[id=questionPanelPrintIcon]').css('display', 'none');
            $(objThis.member.currentComponent.element).find('[id=questionPanelMinimizeIcon]').css('display', 'none');
            // hide the image and show the loading image
            $(objThis.element).find("#imageGalleryExpanded").css('visibility', 'hidden');
            $(objThis.element).find("#imageGalleryWaitText").show();
            // set zoom level to 1.
            objThis.zoomInOutInnerPanelImage(null, 1);
            //disable zoom in zoom out buttons
            $(objThis.element).find('[id=iGZoomOutBtn]').data('buttonComp').disable();
            $(objThis.element).find('[id=iGZoomInBtn]').data('buttonComp').disable();
            var objPathManager = getPathManager();
            var imgFolderPath = objPathManager.getEPubImagePath();
            var imgGalleryDataArr = GlobalModel.imageGalleryDataArr;
            var curBtnId = objThis.member.curViewBtnClicked;		//$(objThis.member.curViewBtnClicked).attr('data-galleryid');
            var url = "";
            var singleItem = "";
            var caption = "";
            var credits = "";
            
            if( imgGalleryDataArr[curBtnId].item && imgGalleryDataArr[curBtnId].item.length == undefined )
            {
            	var totalImages = 1;
            	singleItem = true;
            }
            else
            {
            	var totalImages = imgGalleryDataArr[curBtnId].item.length;
            	singleItem = false;
            }
            var index;
            if (bVal) {
                if (bVal == 1) {
                    //next
                    index = objThis.member.curImageIndex + 1;
                } else {
                    //prev
                    index = objThis.member.curImageIndex - 1;
                }
            } else {
                index = objThis.member.curImageIndex;
            }
            objThis.member.curImageIndex = index;

            // set the source of the image
            if( !singleItem )
	        {
	        	console.log( imgFolderPath ,imgGalleryDataArr[curBtnId].item[index].image );
	        	var imgSrcPath = imgFolderPath + imgGalleryDataArr[curBtnId].item[index].image;
	        	caption = imgGalleryDataArr[curBtnId].item[index].imagecaption;
	        	credits = imgGalleryDataArr[curBtnId].item[index].imagecredits;	
	        }
	        else
	        {
	        	var imgSrcPath = imgFolderPath + imgGalleryDataArr[curBtnId].item.image;
	        	caption = imgGalleryDataArr[curBtnId].item.imagecaption;
	        	credits = imgGalleryDataArr[curBtnId].item.imagecredits;
	        }
            $(objThis.element).find("#imageGalleryExpanded").attr('src', imgSrcPath);
            $(objThis.element).find('[class=iGCaptionArea]').html(caption);
            $(objThis.element).find('[class=iGCreditArea]').html(credits);
            $(objThis.element).find('[id=iGDescriptionArea]').scrollTop(0);
            // Set the current selected image
            if (objThis.member.curImageSelected == null) {
                objThis.member.curImageSelected = $($(objThis.element).find('[id=imageOuterContainer]').find('[type=imageInnerContainer]')[index]);
                objThis.member.curImageSelected.addClass('imageInnerContainerSelected');
            } else {
                objThis.member.curImageSelected.removeClass('imageInnerContainerSelected');
                objThis.member.curImageSelected = $($(objThis.element).find('[id=imageOuterContainer]').find('[type=imageInnerContainer]')[index]);
                objThis.member.curImageSelected.addClass('imageInnerContainerSelected');
            }

            // Navigation buttons are visible by default.
            $(objThis.element).find('[type="prevExpandedImg"]').css('visibility', 'visible');
            $(objThis.element).find('[type="nextExpandedImg"]').css('visibility', 'visible');

            if (index == 0) {
                $(objThis.element).find('[type="prevExpandedImg"]').data('buttonComp').disable();
                $(objThis.element).find('[type="prevExpandedImg"]').css('visibility', 'hidden');
            } else if (index == (totalImages - 1)) {
                $(objThis.element).find('[type="nextExpandedImg"]').data('buttonComp').disable();
                $(objThis.element).find('[type="nextExpandedImg"]').css('visibility', 'hidden');
            }
            
            if( singleItem )
            {
            	$(objThis.element).find('[type="prevExpandedImg"]').data('buttonComp').disable();
                $(objThis.element).find('[type="prevExpandedImg"]').css('visibility', 'hidden');
                $(objThis.element).find('[type="nextExpandedImg"]').data('buttonComp').disable();
                $(objThis.element).find('[type="nextExpandedImg"]').css('visibility', 'hidden');
            }

            $(objThis.element).find("#imageGalleryExpanded").each(function() {
                // If image is not already loaded, load event will be binded
                if (!this.complete) {
                    $(objThis.element).find("#imageGalleryExpanded").unbind('load').bind('load', function() {
                        objThis.member.nGapFromTop = 0;
                        var divHeight = $(objThis.element).find("#expandedImageContainer").height();
                        objThis.member.nGapFromTop = Math.abs(($(objThis.element).find("#imageGalleryExpanded").height() - divHeight) / 2);
                        setTimeout(function() {
                            // calcultae the position of the image. setTimeout of 500 is used because, it will execute only when the animation has finished (first time)
                            objThis.resetImagePos();
                        }, 500);
                        // Slide the thubnails below the expanded image.
                        objThis.slideThumbnailPanel(index);
                    });
                } else {
                    // same thing (as above) is happening in the else part, because when the image is already loaded, load event is not triggered, and we need to execute the upepr events.
                    objThis.member.nGapFromTop = 0;
                    var divHeight = $(objThis.element).find("#expandedImageContainer").height();
                    objThis.member.nGapFromTop = Math.abs(($(objThis.element).find("#imageGalleryExpanded").height() - divHeight) / 2);
                    setTimeout(function() {
                        objThis.resetImagePos();
                    }, 500);
                    $(objThis.element).find("#imageGalleryExpanded").css('opacity', 1);
                    objThis.slideThumbnailPanel(index);
                }
            });
        },

        /**
         * Slide the thumbnails div on selection of image through the expanded portion.
         * @param {Object} index: index of the current image selected
         */

        slideThumbnailPanel : function(index) {
            var objThis = this;
            if (!(index)) {
                index = objThis.member.curImageIndex;
            }
            var curImage = objThis.member.curImageSelected;
            var curImageParentOffset = $(objThis.element).find('[id=outerContainerParent]').offset().left;
            var curImageOffset = $($(objThis.element).find('.imageInnerContainer')[index]).offset().left;
            var curImageParentMaxOffset = $(objThis.element).find('[id=outerContainerParent]').offset().left + $(objThis.element).find('[id=outerContainerParent]').width();
            var curImgwidth = $($(objThis.element).find('.imageInnerContainer')[index]).outerWidth();
            // If offset of the current image selected is less than the offset of the thumbnail container.
            if ((curImageOffset) < curImageParentOffset) {
                var updatedLeft = $(objThis.element).find('#imageOuterContainer').offset().left - curImageOffset;
                updatedLeft = objThis.enableDisableNavBtns(updatedLeft);
                $(objThis.element).find('#imageOuterContainer').css('left', updatedLeft);
            } else if ((curImageOffset + curImgwidth) > curImageParentMaxOffset) {
                var updatedLeft = $(objThis.element).find('#imageOuterContainer').offset().left - (curImageOffset - curImageParentMaxOffset) - 3 * curImgwidth;
                updatedLeft = objThis.enableDisableNavBtns(updatedLeft);
                $(objThis.element).find('#imageOuterContainer').css('left', updatedLeft);
            }
        },

        collapseImageBox : function(collapseBtnRef) {
            var objThis = this;
            $(objThis.element).find('[id=imageGalleryImageContainer]').slideUp(300);
            $(objThis.element).find('[id=iGZoomInBtn]').css('visibility', 'hidden');
            $(objThis.element).find('[id=iGZoomOutBtn]').css('visibility', 'hidden');
            setTimeout(function() {
                $(collapseBtnRef).hide();
                $(objThis.member.currentComponent.element).find('[id=questionPanelPrintIcon]').css('display', 'block');
                $(objThis.member.currentComponent.element).find('[id=questionPanelMinimizeIcon]').css('display', 'block');
            }, 200);
            if (objThis.member.curImageSelected != null) {
                objThis.member.curImageSelected.removeClass('imageInnerContainerSelected');
            }
        },

        /**
         *Navigates to previous thumbnail images
         */
        navigateToPrevImages : function() {
            var objThis = this;
            var totalImages = $(objThis.element).find('.imageInnerContainer').length;
            var imageContainerW = ($(objThis.element).find('.imageInnerContainer').outerWidth() + parseInt($(objThis.element).find('.imageInnerContainer').css('margin-left')) + parseInt($(objThis.element).find('.imageInnerContainer').css('margin-right'))) * totalImages;
            //$(objThis.element).find("#imageOuterContainer").width(imageContainerW);
            var moveLeftBy = (imageContainerW / totalImages) * 3;

            $(objThis.element).find('[type="imageGalleryNext"]').removeClass('ui-disabled');
            var nLeftPos = parseInt($(objThis.element).find("#imageOuterContainer").css('left')) + moveLeftBy;
            nLeftPos = objThis.enableDisableNavBtns(nLeftPos);
            $(objThis.element).find("[id=imageOuterContainer]").css({
                'transition' : 'left 0.5s',
                '-webkit-transition' : 'left 0.5s',
                '-moz-transition' : 'left 0.5s',
                '-o-transition' : 'left 0.5s',
                'left' : nLeftPos + 'px'
            });
        },
        /**
         *
         * Navigates to next thumbnail images
         */
        navigateToNextImages : function() {
            var objThis = this;
            var totalImages = $(objThis.element).find('.imageInnerContainer').length;
            var imageContainerW = ($(objThis.element).find('.imageInnerContainer').outerWidth() + parseInt($(objThis.element).find('.imageInnerContainer').css('margin-left')) + parseInt($(objThis.element).find('.imageInnerContainer').css('margin-right'))) * totalImages;
            // $(objThis.element).find("#imageOuterContainer").width(imageContainerW);
            var moveLeftBy = (imageContainerW / totalImages) * 3;
            var nLeftPos = parseInt($(objThis.element).find("#imageOuterContainer").css('left')) - moveLeftBy;
            nLeftPos = objThis.enableDisableNavBtns(nLeftPos);
            $(objThis.element).find("[id=imageOuterContainer]").css({
                'transition' : 'left 0.5s',
                '-webkit-transition' : 'left 0.5s',
                '-moz-transition' : 'left 0.5s',
                '-o-transition' : 'left 0.5s',
                'left' : nLeftPos + 'px'
            });
        },

        /**
         * This function will enable or disable the navigation buttons.
         * @param {Object} nLeftPos: nLeftPos is the current left position of the thumbnails container
         */
        enableDisableNavBtns : function(nLeftPos) {
            var objThis = this;
            var imgPrevComp = $(objThis.element).find('[type="imageGalleryPrev"]').data('buttonComp');
            var imgNextComp = $(objThis.element).find('[type="imageGalleryNext"]').data('buttonComp');
            imgNextComp.enable();
            imgPrevComp.enable();
            if (nLeftPos > 0 || nLeftPos == 0) {
                nLeftPos = 0;
                imgPrevComp.disable();
            }

            var outerContainerParentWidth = parseInt($(objThis.element).find("#imageOuterContainer").parent().outerWidth());
            var maxLeft = Math.abs(nLeftPos - outerContainerParentWidth);
            var imageOuterContainerW = parseInt($(objThis.element).find("#imageOuterContainer").outerWidth());

            if ((maxLeft > imageOuterContainerW || maxLeft == imageOuterContainerW) && (imageOuterContainerW > outerContainerParentWidth)) {
                nLeftPos = -1 * (imageOuterContainerW - outerContainerParentWidth);
                imgNextComp.disable();
            } else if (imageOuterContainerW < outerContainerParentWidth) {
                nLeftPos = 0;
                imgNextComp.disable();
            }
            return nLeftPos;
        },

        currentPageNumber : function(curRef) {
            var objThis = this;
            var containerParent = curRef;
            if ($(containerParent).attr('type') == "PageContainer") {
                containerParent = $(containerParent);
            } else {
                while ($(containerParent).parent().attr('id') != 'bookContentContainer' && containerParent != null) {
                    if ($(containerParent).parent().attr('type') == "PageContainer") {
                        containerParent = $(containerParent).parent();
                        break;
                    } else {
                        containerParent = $(containerParent).parent();
                    }
                }
            }

            var curPageNumber = $(containerParent).attr('pagebreakvalue');
            var pageindex = GlobalModel.pageBrkValueArr.indexOf(curPageNumber);
            $(objThis.element).find('[id=imageGalleryPageNumber]').html('<span class="imageGalleryPageSpan">' + GlobalModel.localizationData["IMAGE_GALLERY_PAGE_PREFIX"] + '</span> ' + curPageNumber);
            return pageindex;
        },

        /**
         *Calculates and sets the position of image and loader image.
         */
        resetImagePos : function() {
            var objThis = this;
            // set zoom level to 1.
            objThis.zoomInOutInnerPanelImage(null, 1);
            // Set the position of expanded image
            
            var divHeight = $(objThis.element).find("#expandedImageContainer").height();
            objThis.member.nGapFromTop = Math.abs(($(objThis.element).find("#imageGalleryExpanded").height() - divHeight) / 2);
            $(objThis.element).find("#imageGalleryExpanded").css("left", "0px");
           $(objThis.element).find("#imageGalleryExpanded").css("top", objThis.member.nGapFromTop + "px");

            // displays image after its position has been set and hides the preloader
            $(objThis.element).find("#imageGalleryExpanded").css('visibility', 'visible');
            $(objThis.element).find("#imageGalleryWaitText").hide();
            //enables the navigation buttons
            $(objThis.element).find('[type="prevExpandedImg"]').data('buttonComp').enable();
            $(objThis.element).find('[type="nextExpandedImg"]').data('buttonComp').enable();
        },

        resetLoaderPos : function() {
            
        },

        getPositiveInteger : function(val) {
            if (val < 0) {
                return val * -1;
            }
            return val;
        },

        zoomInOutInnerPanelImage : function(bVal, zoomLevel) {
            var objThis = this;
            var _isZoomingOut = false;
            if (!zoomLevel) {
                zoomLevel = objThis.member.curZoomLev;
            }
            if (bVal) {
                if (bVal == 1) {
                    zoomLevel = zoomLevel + 0.25;
                    if (zoomLevel > 2) {
                        zoomLevel = 2;
                    }
                } else {
                    if (zoomLevel > 1) {
                        zoomLevel = zoomLevel - 0.25;
                        _isZoomingOut = true;
                        // objThis.resetImagePos();
                        if (zoomLevel < 1) {
                            zoomLevel = 1;
                        }
                    }
                }
            }
            if (zoomLevel == 1) {
                //objThis.resetImagePos();
                $(objThis.element).find('[id=iGZoomOutBtn]').data('buttonComp').disable();
            } else {
                $(objThis.element).find('[id=iGZoomOutBtn]').data('buttonComp').enable();
            }

            if (zoomLevel == 2) {
                $(objThis.element).find('[id=iGZoomInBtn]').data('buttonComp').disable();
            } else {
                $(objThis.element).find('[id=iGZoomInBtn]').data('buttonComp').enable();
            }
            $(objThis.element).find("#imageGalleryExpanded").css("cursor", "default");
            objThis.member.curZoomLev = zoomLevel;
            objThis.member.curZoomLev = zoomLevel;
            var scaleFunc = "scale(" + zoomLevel + "," + zoomLevel + ")";
            var cssObj = {};
            cssObj["-ms-transform"] = scaleFunc;
            cssObj["-moz-transform"] = scaleFunc;
            cssObj["-webkit-transform"] = scaleFunc;
            cssObj["-o-transform"] = scaleFunc;
            cssObj["-ms-transform-origin"] = "50% 50%";
            cssObj["-moz-transform-origin"] = "50% 50%";
            cssObj["-webkit-transform-origin"] = "50% 50%";
            cssObj["-o-transform-origin"] = "50% 50%";
            $(objThis.element).find("#imageGalleryExpanded").css(cssObj);

            if (_isZoomingOut == true) {
                if (objThis.member.curZoomLev <= 1) {
                    //objThis.resetImagePos();
                    //return;
                }

                objThis.repositionImage();
            }

            if (objThis.member.curZoomLev > 1) {
                $(objThis.element).find("#imageGalleryExpanded").css("cursor", "pointer");
            } else {
                $(objThis.element).find("#imageGalleryExpanded").css("cursor", "default");
            }

        },

        repositionImage : function() {
            var objThis = this;
            var nImageWidth = $(objThis.element).find("#imageGalleryExpanded").width() * this.member.curZoomLev;
            var _diffWidth = $(objThis.element).find("#expandedImageContainer").width() - $(objThis.element).find("#imageGalleryExpanded").width();
            var diffLeft = objThis.member.curZoomLev * $(objThis.element).find("#imageGalleryExpanded").width() - $(objThis.element).find("#imageGalleryExpanded").width() - _diffWidth;
            var diffTop = objThis.member.curZoomLev * $(objThis.element).find("#imageGalleryExpanded").height() - $(objThis.element).find("#imageGalleryExpanded").height();

            var lft = $(objThis.element).find("#imageGalleryExpanded").css("left");
            lft = parseInt(lft.substr(0, lft.length - 2));
            var tp = $(objThis.element).find("#imageGalleryExpanded").css("top");
            tp = parseInt(tp.substr(0, tp.length - 2));

            if ($(objThis.element).find("#expandedImageContainer").width() < nImageWidth) {
                if (lft >= diffLeft / 2) {
                    $(objThis.element).find("#imageGalleryExpanded").css("left", diffLeft / 2 + "px");
                }

                if (lft * -1 > diffLeft / 2) {
                    $(objThis.element).find("#imageGalleryExpanded").css("left", -1 * diffLeft / 2 + "px");
                }
            } else {
                $(objThis.element).find("#imageGalleryExpanded").css("left", "0px");
            }
            if ($(objThis.element).find("#expandedImageContainer").height() < $(objThis.element).find("#imageGalleryExpanded").height() * this.member.curZoomLev) {
                //...fix the position problem while panning
                if (tp >= diffTop / 2) {
                    $(objThis.element).find("#imageGalleryExpanded").css("top", diffTop / 2 + "px");
                }

                if (tp * -1 > diffTop / 2 - (objThis.member.nGapFromTop * 2)) {
                    $(objThis.element).find("#imageGalleryExpanded").css("top", -1 * (diffTop / 2 - (objThis.member.nGapFromTop * 2)) + "px");
                }
            } else {
                $(objThis.element).find("#imageGalleryExpanded").css("top", objThis.member.nGapFromTop + "px");
            }
        }
    });
})(jQuery);
