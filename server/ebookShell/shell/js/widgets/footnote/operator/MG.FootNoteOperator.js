/**
 * @author Magic s/w pvt ltd
 */

MG.FootNoteOperator = function(){
    this.superClass = MG.BaseOperator.prototype;
}

/*----------------------------------------------------------------------------------------------------------------------------------------------
 *
 * 														Prototype functions starts
 *  
 *---------------------------------------------------------------------------------------------------------------------------------------------*/

MG.FootNoteOperator.prototype = new MG.BaseOperator();

/*
 * This function will handle all the events related to Footnote components.
 */
MG.FootNoteOperator.prototype.attachComponent = function(objComp){
    if(objComp == null)
        return;
    
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var objThis = this;
    switch (strType) {
    
        case AppConst.SCROLL_VIEW_CONTAINER:
        	/* 
             * PAGE_RENDER_COMPLETE ( pagerendercomplete ) is triggerd from pagecomp when the page rendering completes.
             */
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function(){
                objThis.doFunctionCall(AppConst.FOOT_NOTE_PANEL_COMP,'loadFootNoteContent');
                objThis.scrollViewRenderCompleteHandler(objComp);
            });
            break;
            
        case AppConst.FOOT_NOTE_PANEL_COMP:
            
            break;
            
        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            break;
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.FootNoteOperator.prototype.scrollViewRenderCompleteHandler = function(objComp)
{
    var objThis = this;
    var arrPageComps =$(objComp.element).find('[data-role="pagecomp"]');
    var objPageRole = null;
    for(var i = 0; i < arrPageComps.length; i++)
    {
        objPageRole = $(arrPageComps[i]).data("pagecomp");
        /* 
         * PAGE_CONTENT_LOAD_COMPLETE ( pagecontentloadcomplete ) is triggerd from pagecomp when the page completely loaded.
         */
        $(objPageRole).bind(objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function(e){
            objThis.initializeFootNoteItemClick(e.currentTarget);
        });
    }

}

/* 
 * initializeFootNoteItemClick attach the click event on footnote/note.
 */
MG.FootNoteOperator.prototype.initializeFootNoteItemClick = function(objComp)
{
    if(EPubConfig.RequiredApplicationComponents.indexOf(AppConst.FOOT_NOTE) > -1)
    {
    	var objThis = this;
        var arrPageFootnote = $(objComp.element).find('[type="footnote"]');
        
        for (var i = 0; i < arrPageFootnote.length; i++) 
        {
            var strMethod = $(arrPageFootnote[i]).attr("data-role");
            try {
                $(arrPageFootnote[i])[strMethod]();
            } 
            catch (err) {
            //console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);
            }
                       
            /* 
			 * Click event on footnote.
			 */
            $(arrPageFootnote[i]).unbind("click").bind('click', function(e)
            {
            	e.preventDefault();
            	var strId;
            	if(objThis.doFunctionCall(AppConst.FOOT_NOTE_PANEL_COMP,"isFootnoteXHTML"))
            		strId = $(this).attr('id');
            	else
            		strId = $(this).attr('data-id');
            	if(strId != undefined)	
                	var bNoErrorEncountered = objThis.doFunctionCall(AppConst.FOOT_NOTE_PANEL_COMP, "setFootNoteText", strId , "footnote");
                
                if(bNoErrorEncountered)
                {
                    PopupManager.removePopup();
                    if ( $(arrPageFootnote[i]).offset().top > $(window).height() - 20 * GlobalModel.currentScalePercent )
    	            {
	                    var scrollBy =  $(arrPageFootnote[i]).offset().top + $("#bookContentContainer2").scrollTop();
		            	$("#bookContentContainer2").scrollTop( scrollBy - $(window).height()/2 );
	            	}
                    PopupManager.addPopup($("#footNotePanel"), $(this).data("buttonComp"), {
                        isModal: true,
                        hasPointer: true,
                        offsetScale: GlobalModel.currentScalePercent
                    });
                    $("#footNoteTextDiv").parent().scrollTop(0);
                    $("#footNoteTextDiv").parent().scrollLeft(0);
                    
                }
            });
        }
            
            
        var arrPageNote = $(objComp.element).find('[type="note"]');

        for (var i = 0; i < arrPageNote.length; i++) 
        {
            var strMethod = $(arrPageNote[i]).attr("data-role");
            try {
                $(arrPageNote[i])[strMethod]();
            } 
            catch (err) {
            //console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);
            }
            
            /* 
			 * Click event on note.
			 */
            $(arrPageNote[i]).unbind("click").bind('click', function(e){
            	e.preventDefault();
                var strId = $(this).attr('data-id');
                var bNoErrorEncountered = objThis.doFunctionCall(AppConst.FOOT_NOTE_PANEL_COMP, "setFootNoteText", strId , "note");
                var scrollBy =  $(arrPageFootnote[i]).offset().top + $("#bookContentContainer2").scrollTop();
                if ( $("#bookContentContainer2").scrollTop() + $("#footNotePanel").height() + $('[type="note"]').height() > scrollBy )
            		$("#bookContentContainer2").scrollTop( scrollBy - $(window).height()/2 );
                if(bNoErrorEncountered)
                {
                    
                    PopupManager.removePopup();
                    var calculateMargin = $(this).offset().left - $("#bookContentContainer2").offset().left ;
                    if (ismobile && calculateMargin < 0) {
                     PopupManager.addPopup($("#footNotePanel"), $(this).data("buttonComp"), {
                        isModal: true,
                        hasPointer: true,
                        isCentered : true
                    });
                    } else {
                    PopupManager.addPopup($("#footNotePanel"), $(this).data("buttonComp"), {
                        isModal: true,
                        hasPointer: true,
                        offsetScale: GlobalModel.currentScalePercent
                    });
                    }
                    $("#footNoteTextDiv").parent().scrollTop(0);
                    $("#footNoteTextDiv").parent().scrollLeft(0);
                    
                }
            });
        }
     }
}