/**
 * @author lakshay.ahuja
 */

var interactivityTypeTosave = null;
var g_printXMLLoaded = 0;
(function($, undefined) {
    $.widget("magic.questionmanager", $.magic.magicwidget, {

        options : {},

        member : {
            questionArrData : null,
            currInteractionType : null,
            groupXMLData : null,
            currQuesClicked : null,
            prevQuesClicked : null,
            lastGrpXMLLoaded : null,
            prevInteractionType : null,
            objectClicked : null,
            numXmlLoadedForPrint : 0,
            currentGrpQuestions : [],
            questionXMLData : [],
            interactionTypeForQues : [],
            curQuestionIndexForPrint : 0,
            isprintCalled : false,
            currentObj:null
        },

        _init : function() {
            var objThis = this;
            $.magic.magicwidget.prototype._init.call(this);
        },

        _create : function() {
        	this.addListeners();
        },
        
        addListeners : function ()
        {
        	var objThis = this;
        	if ( !( $.browser.msie && $.browser.version == 10 ) && !isWinRT && $.browser.version != 11 )
            {
            	var eventName = "input propertychange";
            }
            else
            {
            	var eventName = "keyup";
            }
        	
        	$("#qaPanelAddCommentBtn").bind( "click", function ()
			{
				if( $("#qaCommentTxt").css("display") == "block" )
				{
					objThis.hideCommentSection( true );
				}
				else
				{
					objThis.hideCommentSection( false );
				}
			} );
        	
        	$( "#qaCommentTxt .close-container" ).bind( "click", function ()
			{
        		if( GlobalModel.BookEdition == "TSE" )
        		{
        			$( "#qaCommentTxtArea" ).val( "" );
        			objThis.saveBtnClickHandler();
        		}
        		objThis.hideCommentSection( true );
			} );
        	
        	$( "#qaCommentSaveBtn" ).bind( "click", function()
			{
        		objThis.saveBtnClickHandler();
			})
        	
        	$( "#qaCommentTxtArea" ).bind( eventName, function () { objThis.commentTxtAreaTextChangeHandler(); } );
        	$( "#qaCommentTxtArea" ).bind( "paste", function () { objThis.commentTxtAreaTextChangeHandler(); } );
        },
        
        saveBtnClickHandler : function ()
        {
        	var objThis = this;
        	var tempObj = GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ][ objThis.member.currentObj.childClicked ];
        	var objToPost = {};
	    	objToPost.title = $( "#qaCommentTxtArea" ).val();
	    	objToPost.qaType = true;
	    	if( !tempObj )
	    	{
	    		objThis.disableEnableSingleComponent( $( "#qaCommentTxt #qaCommentSaveBtn" ), "disable" );
	    		return;
	    	}
	    	// update and delete
	    	tempObj.isNotified = true;
	    	if( tempObj.comments )
	    	{
	    		tempObj.comments.body_text = objToPost.title;
	    		objToPost.annotation_id = tempObj.comments.annotation_id;
	    		if( $.trim( $( "#qaCommentTxtArea" ).val() ) == "" )
	    		{
	    			ServiceManager.Annotations.remove( tempObj.comments.annotation_id );
	    			delete tempObj.comments;
	    			
					var obj = GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ];
		    		var hasQAComment = false;
		    		for( var tempObj in obj )
		    		{
		    			if(obj[tempObj].comments)
		    			{
		    				hasQAComment = true;
		    			}
		    		}
		    		if( !hasQAComment )
		    		{
		    			$('[interactiongroupid="'+ objThis.member.currentObj.id  +'"]' ).find('[type="widget:quizStatus"]').removeClass( "quizStatusQACommentAdded" );
		    		}
	    		}
	    		else
	    		{
	    			ServiceManager.Annotations.update( 7, objToPost );
	    		}
	    	}
	    	else
	    	{
	    		//create
	    		objToPost.annotationId = tempObj.annotation_id;
	    		objToPost.pageNumber = tempObj.pageNumber;
	    		tempObj.comments = {};
	    		tempObj.comments.body_text = objToPost.title;
	    		tempObj.comments.parent_id = tempObj.annotation_id;
	    		$( "#" + objThis.member.currentObj.id ).find('[type="widget:quizStatus"]').addClass( "quizStatusQACommentAdded" );
	    		ServiceManager.Annotations.create( objToPost, 7, function ( objData )
			    {
	    			var _tempObj = objThis.getQuestionObject( objData.parent_id );
	    			_tempObj.comments = objData;
			    } );	    		
	    	}
	    	objThis.disableEnableSingleComponent( $( "#qaCommentTxt #qaCommentSaveBtn" ), "disable" );
	    	objThis.showHideCommentNotificationIcon();
	    	GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
        },
        
        
        
        commentTxtAreaTextChangeHandler : function ()
        {
        	var objThis = this;
        	var isNew = true;
        	var tempObj = GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ][ objThis.member.currentObj.childClicked ];
        	var commentTextArea = $( "#qaCommentTxtArea" );
        	
        	if( tempObj && tempObj.comments )
        	{
        		isNew = false;
        	}
        	
        	if( commentTextArea.hasClass("ui-disabled") )
    	    {
    	        return;
    	    }
        	if( ( $.trim(commentTextArea.val()) == "" && isNew ) ||
        		( tempObj && tempObj.comments && tempObj.comments.body_text == commentTextArea.val() )  )
        	{
        		objThis.disableEnableSingleComponent( $( "#qaCommentSaveBtn" ), "disable" );
        	}
        	else
    		{
        		console.log("TESTING")
        		objThis.disableEnableSingleComponent( $( "#qaCommentSaveBtn" ), "enable" );
    		}
        },
        
        disableEnableSingleComponent : function ( component, operation )
    	{
    		if( operation == "disable" )
            {
                component.attr( "disabled", true );
                component.addClass( "ui-disabled" ).attr( "aria-disabled", true );
            }
            else
            {
                component.attr( "disabled", false );
                component.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
            }
    	},
        
        resetCommentSection : function ()
        {
    		this.disableEnableSingleComponent( $( "#qaCommentTxt #qaCommentSaveBtn" ), "disable" );
    		$( "#qaCommentTxtArea" ).val( "" );
        },
        
        hideCommentSection : function ( boolToHide )
        {

        	if( boolToHide )
        	{
        		$("#qaCommentTxt").css("display", "none");
				$( "#qaPanelAddCommentBtn" ).find( ".addCommentBtnDownArrow" ).removeClass( "rotateAddCommentBtnDownArrow" );
        	}
        	else
        	{
        		$("#qaCommentTxt").css("display", "block");
				$( "#qaPanelAddCommentBtn" ).find( ".addCommentBtnDownArrow" ).addClass( "rotateAddCommentBtnDownArrow" );
        	}
        	
    		var obj = GlobalModel.questionPanelAnswers[ this.member.currentObj.id ];
    		var hasQAComment = false;
    		for( var tempObj in obj )
    		{
    			if(obj[tempObj].comments)
    			{
    				if(obj[tempObj].flag_id)
    					hasQAComment = true;
    			}
    		}
    		
    		if( !hasQAComment )
    		{
    			$( "#" + this.member.currentObj.id ).find('[type="widget:quizStatus"]').removeClass( "quizStatusQACommentAdded" );
    		}
        },
        
        showHideCommentNotificationIcon : function ()
        {
        	if( $.trim( $( "#qaCommentTxtArea" ).val() ) == "" || $("#qaPanelAddCommentBtn").css("display") == "none" )
    		{
        		$( ".qaPanelCommentAddedNotificationIcon" ).css( "display", "none" );
    		}
        	else
        	{
        		$( ".qaPanelCommentAddedNotificationIcon" ).css( "display", "block" );
        	}
        },
        
        addCommentInCommentBox : function ()
        {
        	$( "#qaCommentTxtArea" ).val( "" );
        	var objThis = this;
        	var flag = false;
        	if( objThis.member.currentObj != null && GlobalModel.questionPanelAnswers
        		&& GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ] 
        		&& GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ][ objThis.member.currentObj.childClicked ]
        		&& GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ][ objThis.member.currentObj.childClicked ].comments )
        	{
        		var tempObj = GlobalModel.questionPanelAnswers[ objThis.member.currentObj.id ][ objThis.member.currentObj.childClicked ];
        		$( "#qaCommentTxtArea" ).val( tempObj.comments.body_text );
        		flag = tempObj.flag_id;
        	}
        	
        	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        	{
        		//objThis.hideCommentSection( false );
        	}
        	else if( GlobalModel.BookEdition == "SE" )
        	{
        		if( !flag )
            	{
            		objThis.hideCommentSection( true );
            	}
        		else
        		{
        			objThis.hideCommentSection( false );
        		}
        	}
        	else
        	{
        		objThis.hideCommentSection( true );
        	}

        	/*if( !flag && GlobalModel.BookEdition == "SE" )
        	{
        		objThis.hideCommentSection( true );
        	}
        	else
        	{
        		objThis.hideCommentSection( false );
        	}*/
        	objThis.showHideCommentNotificationIcon();
        },
        
        addCommentToQA : function ( obj, comment, str )
        {
        	var objThis = this;
        	var flag = false;
        	//if( $( "#qaCommentTxt #qaCommentSaveBtn" ).hasClass( "ui-disabled" ) )
    		//{
        //		flag = true;
    	//	}
        	//var objToPost = {};
        	//objToPost.title = $( "#qaCommentTxtArea" ).val();
        	//objToPost.qaType = true;
        	//objThis.saveBtnClickHandler();
        	objThis.resetCommentSection();
        	objThis.addCommentInCommentBox();
        	
        	if( comment != undefined )
        	{
        		//obj.comments = comment;
        		//if( objToPost.title == "" || flag )
        		//{
        			//flag = false;
        			//return;
        		//}
        		//obj.comments.body_text = objToPost.title;
        		//objToPost.annotation_id = obj.comments.annotation_id;
        		//ServiceManager.Annotations.update( 7, objToPost );
        	}
        	else
        	{
        		//if( objToPost.title == "" )
        		//{
        		//	return;
        		//}
        		//objToPost.annotationId = obj.annotation_id;
        		//objToPost.pageNumber = str;
        	//	obj.comments = {};
        	////	obj.comments.body_text = objToPost.title;
        	//	obj.comments.parent_id = obj.annotation_id;
        	//	$( "#" + objThis.member.currentObj.id + "_indicator" ).addClass( "quizStatusQACommentAdded" );
        	//	ServiceManager.Annotations.create( objToPost, 7, function ( objData )
			 //   {
        	//		var _tempObj = objThis.getQuestionObject( objData.parent_id );
        		//	_tempObj.comments = objData;
			  //  } );
        	}
        	//objThis.showHideCommentNotificationIcon();
        	//console.log( obj, comment ,  " This is addCVoment to QA")
        },
        
        getQuestionObject : function ( annoID )
        {
        	for( var object in GlobalModel.questionPanelAnswers )
    		{
        		var obj = GlobalModel.questionPanelAnswers[ object ];
        		for( var tempObj in obj )
        		{
        			if( obj[tempObj].annotation_id == annoID )
	        		{
	        			return obj[tempObj];
	        		}
        		}
    		}
        },

        loadGroupXML : function(arrObj) {
            if ($("#questionAnswerPanel").css('display') == "none")
                $("#questionAnswerPanel").slideDown(500, "easeInOutExpo");
            $("#questionanswerInnerContent").css("visibility", "hidden");
            $("#checkAnswer").css('display', 'none');
            $("#quesWaitText").show();
            if( !EPubConfig.AnnotationSharing_isAvailable )
        	{
            	$( "#questionAnswerPanel #qaPanelAddCommentBtn" ).remove();
            	$( "#qaCommentTxt" ).remove();
        	}
            
            if( GlobalModel.BookEdition == "SE" )
        	{
            	$( "#qaCommentSaveBtn" ).remove();
            	$("#qaPanelAddCommentBtn").css('visibility','hidden');
            	$("#qaCommentTxt .close-container").remove();
        	}
            
            var objThis = this;
            objThis.member.currentObj = arrObj;
            if( ( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) || ( GlobalModel.BookEdition == "SE" ) )
        	{
        		var _tempObject;
        		if( GlobalModel.questionPanelAnswers[objThis.member.currentObj.id] )
        		{
        			if( GlobalModel.questionPanelAnswers[objThis.member.currentObj.id][objThis.member.currentObj.childClicked] )
        				_tempObject = GlobalModel.questionPanelAnswers[objThis.member.currentObj.id][objThis.member.currentObj.childClicked].flag_id;
        		} 
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		if( _tempObject )
            		{
            			objThis.hideCommentSection( false );
            			$( "#qaPanelAddCommentBtn" ).css( "display", "block" );
            		}
            		else
            		{
            			objThis.hideCommentSection( true );
            			$( "#qaPanelAddCommentBtn" ).css( "display", "none" );
            		}
            		$('.savetonotebook').css('display','none');
        		}
            	else
        		{
        			if( _tempObject )
            		{
	            		objThis.disableEnableSingleComponent( $( "#qaCommentTxtArea" ), "disable" );
	            		$( "#qaPanelAddCommentBtn" ).css( "display", "block" );
	            	}
	            	else
	            	{
	            		objThis.hideCommentSection( true );
            			$( "#qaPanelAddCommentBtn" ).css( "display", "none" );
	            	}
	            	$('.savetonotebook').css('display','block');
        		}
        	}
            else
        	{
            	$( "#qaPanelAddCommentBtn" ).css( "display", "none" );
            	$('.savetonotebook').css('display','block');
        		objThis.hideCommentSection( true );
        	}
            objThis.member.isprintCalled = false;
            var groupXMLFileName = arrObj.id;
            objThis.member.prevQuesClicked = objThis.member.currQuesClicked;
            objThis.member.currQuesClicked = arrObj.childClicked;
            // For now the current interaction type has not been updated. So initialize previousinteractiontype as currentinteractiontype
            objThis.member.prevInteractionType = objThis.member.currInteractionType;

            /**
             * If the current group is not loaded, then only the group XML will load, else previous question response will be saved.
             */
            if (objThis.member.lastGrpXMLLoaded != groupXMLFileName) {

                objThis.member.questionXMLData = [];

                objThis.member.lastGrpXMLLoaded = groupXMLFileName.replace('.xml', '');

                objThis.loadRequiredXML(groupXMLFileName, true);

                objThis.member.prevQuesClicked = arrObj.childClicked;

                // This will return an array of questions contained in the group.
                return objThis.member.currentGrpQuestions;

            } else {
                //saveCurrentResponse
                objThis.savePreviousQuesResponse(true);
                // objThis.setQuestionContent();
            }
        },

        /**
         * This function sets the current Dot property and current question xml and properties.
         */

        setQuestionContent : function() {
            var objThis = this;

            var grpData = objThis.member.groupXMLData;

            var grpInteractionLength = grpData.interaction.interaction.length;

            if (!grpInteractionLength) {
                grpInteractionLength = 1;
            }

            var currQuesId = null;

            var currQuesType = null;

            var currQuesURL = null;

            $('.questionPanelquesDots').removeClass('quesCurrentCircle');

            var indexElem = objThis.member.currentGrpQuestions.indexOf(objThis.member.currQuesClicked) + 1;
            $("#questionPanelques" + indexElem).addClass('quesCurrentCircle');

            for (var i = 0; i < grpInteractionLength; i++) {
                if (grpInteractionLength == 1) {
                    if (grpData.interaction.interaction.id == objThis.member.currQuesClicked) {
                        currQuesId = objThis.member.currQuesClicked;
                        currQuesURL = grpData.interaction.interaction.url;
                        currQuesType = grpData.interaction.interaction.type;
                        break;
                    }
                } else {
                    if (grpData.interaction.interaction[i].id == objThis.member.currQuesClicked) {
                        currQuesId = objThis.member.currQuesClicked;
                        currQuesURL = grpData.interaction.interaction[i].url;
                        currQuesType = grpData.interaction.interaction[i].type;
                        break;
                    }
                }
            }
            var arrObj = new Object();
            arrObj.id = currQuesId;
            arrObj.currQuesURL = currQuesURL;
            arrObj.currQuesType = currQuesType;

            objThis.setCurrentQuesType(arrObj, true);

        },

        /**
         * This fucntion will load questionGroup XML, and questions XML
         * @param {Object} fileName: It is the name of the path to be loaded.
         * @param {Object} bVal: true/false: true when group XML is to be loaded, false when Question XML is to be loaded.
         * @param {Object} setDataForPopulateContent
         */
        loadRequiredXML : function(fileName, bVal) {
            var objThis = this;
            var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.curQuestionIndexForPrint];
            // objThis.questionXMLLoadComplete(objThis.member.questionXMLData[currentQuestion], bVal, setDataForPopulateContent);
            var objPathManager = getPathManager();
            var strUri = objPathManager.getIntractivitiesXMLPath(fileName);
            $.ajax({
                type : "GET",
                url : strUri,
                processData : false,
                contentType : "plain/text",
                success : function(objData) {
                    /**
                     * This function is called on success.
                     */
                    //console.log(fileName);
                    objThis.questionXMLLoadComplete(objData, bVal,fileName);
                    //console.log("objData in loadRequiredXML ",fileName,objData);
                },
                error : function(e) {
                    console.log("Onerror: ", strUri);
                    console.log(e.error);

                    // objThis.displayErrorMessage();
                }
            });
        },

        /**
         * This function initializes the current question component and calls for loading the current question XML.
         */
        setCurrentQuesType : function(arrObj) {
            var objThis = this;
            var currQuesId = arrObj.id;
            var currQuesURL = arrObj.currQuesURL;
            var currQuesType = arrObj.currQuesType;
            var currentQuestionIndex = 0;
            /**
             * If : print is not called, then the currQuesId will be the current Question clicked (passed by QAOperator).
             * Else: print is called, and index is passed as a parameter in object.
             */
            if (!objThis.member.isprintCalled) {
                currentQuestionIndex = objThis.member.currentGrpQuestions.indexOf(currQuesId);
            } else {
                currentQuestionIndex = arrObj.index;
            }

            var currQuesDotReference = $("#questionPanelques" + (currentQuestionIndex + 1));
            objThis.member.curQuestionIndexForPrint = currentQuestionIndex;

            /**
             * Every component needs to be initialized with the current Question(Dot is referenced by Dot) Reference so as to be able to be accessed or to implement their functions.
             */
            switch (currQuesType) {
                case 'singleChoice':
                    currQuesDotReference['singleChoiceInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('singleChoiceInteractivity');
                    break;

                case 'freeText':
                    currQuesDotReference['freeTextInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('freeTextInteractivity');
                    break;
                case 'freeTextWithImage':
                    currQuesDotReference['freeTextWithImageInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('freeTextWithImageInteractivity');
                    break;
                case 'fillUps':
                    currQuesDotReference['fillUpsInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('fillUpsInteractivity');
                    break;
                case 'multipleChoice':
                    currQuesDotReference['multipleChoiceInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('multipleChoiceInteractivity');
                    break;
                case 'tables':
                    currQuesDotReference['tableInteractivity']();
                    objThis.member.currInteractionType = currQuesDotReference.data('tableInteractivity');
                    break;

            }

            // This array sets the interactionType for every rendered question in the group.
            var curentQuestion = objThis.member.currentGrpQuestions[currentQuestionIndex];
            objThis.member.interactionTypeForQues[curentQuestion] = objThis.member.currInteractionType;

            if (!objThis.member.isprintCalled) {
                if (objThis.member.prevInteractionType == null) {
                    objThis.member.prevInteractionType = objThis.member.currInteractionType;
                }
                interactivityTypeTosave = objThis.member.currInteractionType;
            }

            currQuesURL = currQuesURL.replace('.xml', '');
            //console.log("currQuesURL: ",currQuesURL);
            objThis.loadRequiredXML(currQuesURL);
        },

        /**
         * This fucntion is called on XML load success
         * @param {Object} objData: the data contained in the XML
         * @param {Object} bVal: true/false, true when Group XML is loaded, false when question XML is loaded
         * @param {Object} setDataForPopulateContent
         */

        questionXMLLoadComplete : function(objData, bVal,fileName) {
            var objThis = this;
            var questionArrData = "";
            var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.curQuestionIndexForPrint];
            // if (true) {

            var strXML;
            if (window.ActiveXObject) {
                var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
                xmlObject.async = false;
                strXML = objData;
            } else {
                strXML = new XMLSerializer().serializeToString(objData);
            }
            var questionArrData = $.xml2json(strXML);

            /*} else {
             var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.curQuestionIndexForPrint];
             var questionArrData = objThis.member.questionXMLData[currentQuestion];
             }*/
            if (bVal) {
                objThis.member.groupXMLData = questionArrData;
                var interactivityLength = objThis.member.groupXMLData.interaction.interaction.length;
                if (interactivityLength == undefined && typeof objThis.member.groupXMLData.interaction.interaction == "object") {
                    interactivityLength = 1;
                }
                //Calculating the width of navigator
                //Left and right margin of dots set to 5.
                var dotsMargin = 5;
                //(dotsParentWidth - 25) / (4 * interactivityLength);
                // total width of navigator will be width + margin of navigation arrows and width + margin of dots. width of dots is 12px.
                var totalNavigatorWidth = 2 * $("#prevQuestionArrow").outerWidth() + 2 * (parseInt($("#prevQuestionArrow").css('margin-left')) + parseInt($("#prevQuestionArrow").css('margin-right'))) + interactivityLength * (12 + 2 * dotsMargin);
                // + 20 is set to give some extra space to the navigator.
                $("#questionPanelQuestionNavigator").width(totalNavigatorWidth + 20);
                $(objThis.element).parent().parent().find('[id=numberOfQuestionDots]').find('[type="questionPanelquesDots"]').remove();

                if (interactivityLength == 1) {
                    $("#nextQuestionArrow").css('visibility', 'hidden');
                    $("#prevQuestionArrow").css('visibility', 'hidden');
                } else {
                    $("#nextQuestionArrow").css('visibility', 'visible');
                    $("#prevQuestionArrow").css('visibility', 'visible');
                }
				objThis.member.currentGrpQuestions = [];
                for (var i = 0; i < interactivityLength; i++) {

                    /**
                     * Here all the questions IDs are saved in an array.
                     */
                    if (interactivityLength == 1) {
                        objThis.member.currentGrpQuestions[i] = objThis.member.groupXMLData.interaction.interaction.id;
                    } else {
                        objThis.member.currentGrpQuestions[i] = objThis.member.groupXMLData.interaction.interaction[i].id;
                    }

                    $(objThis.element).parent().parent().find('[id=numberOfQuestionDots]').append('<div data-role="buttonComp" id="questionPanelques' + (i + 1) + '" type="questionPanelquesDots" class="questionPanelquesDots quesPendingCircle" style="margin-left:' + dotsMargin + 'px;margin-right:' + dotsMargin + 'px "><div class="questionBulletInnerShadow"></div></div>');
                    $("#questionPanelques"+(i+1))['buttonComp']();
                    var totalNavigatorWidth = 2 * $("#prevQuestionArrow").outerWidth() + 2 * (parseInt($("#prevQuestionArrow").css('margin-left')) + parseInt($("#prevQuestionArrow").css('margin-right'))) + interactivityLength * ($("#questionPanelques" + (i + 1)).outerWidth() + 2 * dotsMargin);
                    // + 20 is set to give some extra space to the navigator.
                    $("#questionPanelQuestionNavigator").width(totalNavigatorWidth + 10);
                    if (GlobalModel.isAnnotationDataLoaded != true) {
                        $('#questionPanelQuestionNavigator .questionPanelquesDots').each(function() {
                            $(this).data('buttonComp').disable();
                            $(this).addClass('ui-disabled');
                            $(this).addClass('navigatorQAPanelDisable');
                            $("#prevQuestionArrow").addClass('navigatorQAPanelDisable');
                            $("#nextQuestionArrow").addClass('navigatorQAPanelDisable');
                        });
                    }

                    $(objThis.element).parent().parent().find('[id=numberOfQuestionDots]').find('[type="questionPanelquesDots"]').unbind('click').bind('click', function() {
                        var thisRef = this;
                        if ($(thisRef).data('buttonComp').options.isEnabled == true) {
                            $($(thisRef).parent()).trigger('questiondotClicked', thisRef);
                        }

                    });
                    //type="questionPanelquesDots"
                }
                if (GlobalModel.isAnnotationDataLoaded == true) {
                    $(objThis).trigger('questionDotsLoaded', objThis.member.groupXMLData);
                } else {
                    $(document).bind('qaPanelDataLoadComplete', function() {
                        $(objThis).trigger('questionDotsLoaded', objThis.member.groupXMLData);
                    });
                }

                objThis.setQuestionContent();

            } else {
                /**
                 * This else part will be processed after loading of current QUESTION XML.
                 */
                objThis.member.questionArrData = questionArrData;
                var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.curQuestionIndexForPrint];
                if (!(objThis.member.questionXMLData[currentQuestion]) && (!objThis.member.isprintCalled)) {
                    objThis.member.questionXMLData[currentQuestion] = questionArrData;
                }
                if (objThis.member.isprintCalled){
                    var currentQuestion = objThis.member.currentGrpQuestions[g_printXMLLoaded];
                    //console.log("currentQuestion: ",currentQuestion);
                     objThis.member.questionXMLData[fileName] = questionArrData;
                     //console.log("objThis.member.questionXMLData[currentQuestion] ",objThis.member.questionXMLData[currentQuestion]);
                     //console.log("questionArrData: ",questionArrData);
                     g_printXMLLoaded++;
                }
                

                if (objThis.member.isprintCalled) {
                    var interactivityLength = objThis.member.groupXMLData.interaction.interaction.length;
                    if (interactivityLength == undefined && typeof objThis.member.groupXMLData.interaction.interaction == "object") {
                        interactivityLength = 1;
                    }
                    var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.numXmlLoadedForPrint];
                    //console.log(currentQuestion,fileName);
                    objThis.member.questionXMLData[fileName] = questionArrData;
                    //console.log(fileName,questionArrData);
                    objThis.member.numXmlLoadedForPrint++;
                    if (objThis.member.numXmlLoadedForPrint == interactivityLength) {
                         objThis.addPrintPopup();
                        objThis.member.numXmlLoadedForPrint = 0;
                    }
                    return;
                }

                try {
                    var strQAContent = objThis.member.questionArrData;
					$(document).bind('qaPanelDataLoadComplete', function() {
                        objThis.callPopupateContentOfInteractivities(strQAContent);
                    });
                    if (GlobalModel.isAnnotationDataLoaded == true || !ServiceManager.DPLive) {
                    	$(document).trigger("qaPanelDataLoadComplete");
                    } 
                } catch (e) {
                    console.log(e.message)
                    // objThis.displayErrorMessage();
                }
                $("#questionanswerInnerContent").css("display", "block");
                $("#questionanswerInnerContent").css("visibility", "visible");
                $("#quesWaitText").hide();
            }

            /*
             * This event is handled in QAOpertaor
             * TRIGGER BY DOCUMENT NEEDS TO BE CHANGED
             */
            if (bVal) {
                $(document).trigger('grpXMLLoadComplete');
            } else {
                $(document).trigger('XMLLoadComplete');
            }

        },

        callPopupateContentOfInteractivities : function(strQAContent) {
            var objThis = this;
            //console.log( "callPopupateContentOfInteractivities ", strQAContent.responseDeclaration)
            if ( strQAContent.responseDeclaration && strQAContent.responseDeclaration.correctResponse && strQAContent.responseDeclaration.correctResponse.value) {
                $("#checkAnswer").css('display', 'block');
                if ($("#checkAnswer").hasClass('hideQAAnswers')) {
                	$("#checkAnswer").removeClass('hideQAAnswers').addClass('showQAAnswers');
                    $("#checkAnswer").html('Clear Answer');
                }
                else
                {
                	$("#checkAnswer").addClass('showQAAnswers').removeClass('hideQAAnswers');
                    $("#checkAnswer").html('Check Answer');
                }
            } else {
                $("#checkAnswer").css('display', 'none');
            }
            objThis.member.currInteractionType.populateQAPanelInnerContent(strQAContent, objThis);
        },

        saveCurrentResponse : function() {
            var objThis = this;
            if (interactivityTypeTosave != null) {
                interactivityTypeTosave.saveCurrentResponseToServices();
                objThis.updateQAHotspotStatus();
            }
        },

        savePreviousQuesResponse : function() {
            var objThis = this;

            if (objThis.member.currInteractionType != null) {
                objThis.saveCurrentResponseToServices();
            } else {
                objThis.saveCurrentResponseToServices();
                $("#questionAnswerPanel").trigger('recentResponseSaved');
            }

        },

        saveCurrentResponseToServices : function() {
            var objThis = this;
            objThis.member.prevInteractionType.saveCurrentResponse();
            objThis.member.prevInteractionType.saveCurrentResponseToServices();
            objThis.updateQAHotspotStatus();
        },

        /**
         * This function updates the status of the Question Indicator present on the page.
         * Also, this function updates GlobalModel.questionPanelAnswersForAnnoPanel which is needed to display the questions in the left side  question panel.
         *
         */

        updateQAHotspotStatus : function() {
            var objThis = this;
            var completedQuesLength = $($(objThis.element).parent().parent().find('.quesCompleteCircle')).length;
            var totalQues = $($(objThis.element).parent().parent().find('[type="questionPanelquesDots"]')).length;
            if (completedQuesLength == totalQues) {
              // $("#"+objThis.member.lastGrpXMLLoaded).find('[type="widget:quizStatus"]').removeClass('incompleteQuestionStatus').addClass('completedQuestionStatus');
                var arrObj = new Object();
                arrObj.pageNumber = $(objThis.element).parent().parent().find('[id="questionPanelPageNumber"]').html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
                arrObj.creationDate = getTodaysDate();
                arrObj.parentGrpId = objThis.member.lastGrpXMLLoaded;
                var pushItem = true;
                for (var n = 0; n < GlobalModel.questionPanelAnswersForAnnoPanel.length; n++) {
                    if (GlobalModel.questionPanelAnswersForAnnoPanel[n].parentGrpId == arrObj.parentGrpId) {
                        pushItem = false;
                    }
                }
                if (pushItem == true) {
                    GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj);
                }
            } else if (completedQuesLength > 0) {
               // $("#"+objThis.member.lastGrpXMLLoaded).find('[type="widget:quizStatus"]').addClass('incompleteQuestionStatus').removeClass('completedQuestionStatus');
                var arrObj = new Object();
                arrObj.pageNumber = $(objThis.element).parent().parent().find('[id="questionPanelPageNumber"]').html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
                arrObj.creationDate = getTodaysDate();
                arrObj.parentGrpId = objThis.member.lastGrpXMLLoaded;
                var pushItem = true;
                for (var n = 0; n < GlobalModel.questionPanelAnswersForAnnoPanel.length; n++) {
                    if (GlobalModel.questionPanelAnswersForAnnoPanel[n].parentGrpId == arrObj.parentGrpId) {
                        pushItem = false;
                    }
                }
                if (pushItem == true) {
                    GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj);
                }
            } else {
              // $("#"+objThis.member.lastGrpXMLLoaded).find('[type="widget:quizStatus"]').removeClass('incompleteQuestionStatus').removeClass('completedQuestionStatus');
                var parentGrpId = objThis.member.lastGrpXMLLoaded;
                var indexAt = -1;
                for (var n = 0; n < GlobalModel.questionPanelAnswersForAnnoPanel.length; n++) {
                    if (GlobalModel.questionPanelAnswersForAnnoPanel[n].parentGrpId == parentGrpId) {
                        indexAt = n;
                    }
                }
                if (indexAt > -1) {
                    GlobalModel.questionPanelAnswersForAnnoPanel.splice(indexAt, 1)
                }
            }
        },

        displayHideCorrectResponse : function(bVal) {
            this.member.currInteractionType.displayHideCorrectResponse(bVal);
        },

        handleQAPanelPrintClick : function() {
            var objThis = this;
            objThis.member.isprintCalled = true;
            $(".print-footer").remove();
            $(".print-wrapper").remove();
            
            $("#questionAnswerPrintPanel").find('[class=questionanswerInnerContent]').remove();
            var annotationPanelWidth = $("#bookContentContainer").offset().left + ($("#bookContentContainer").width() - $("#questionAnswerPrintPanel").width()) / 2;
            $("#questionAnswerPrintPanel").css({
                'position' : 'absolute',
                'bottom' : "0px",
                'top' : "auto",
                'left' : annotationPanelWidth + "px",
                'visibility' : "hidden"
            });
            $("#printPanelWaitImgContainer").css('display','block');
            var quesAnsPrintPanelHeight = $("#questionAnswerPrintPanel").height();
            var quesAnsPrintPanelWidth = $("#questionAnswerPrintPanel").width();
            var quesAnsPrintImgHeight = $("#printPanelWaitImg").height();
            var quesAnsPrintImgWidth = $("#printPanelWaitImg").width();
            $("#printPanelWaitImg").css({
                'position':'absolute',
                'top': (quesAnsPrintPanelHeight - quesAnsPrintImgHeight) /2,
                'left':(quesAnsPrintPanelWidth - quesAnsPrintImgWidth) /2
            });
            PopupManager.addPopup($("#questionAnswerPrintPanel"), $(objThis), {
                isModal : true,
                isTapLayoutDisabled : true,
                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                isCentered : true,
                callbackFunc : objThis.positionPrintPopup()

            });
            //$("#printBtn").remove();
            //questionanswerInnerContent
            var numberOfQuestions = $(objThis.element).parent().parent().find('[id=numberOfQuestionDots]').find('[type="questionPanelquesDots"]').length;
            var grpXMLName = "";
            for( var grpName in GlobalModel.questionPanelAnswers )
            {
            	if( grpName == objThis.member.lastGrpXMLLoaded )
            	{
            		grpXMLName = grpName;
            		break;
            	}
            }
            var strList = "<div class='print-wrapper'>";
            var tempArr = [];
            var ques = GlobalModel.questionPanelAnswers[ grpXMLName ];
            for( var tempQues in ques )
            {
            	tempArr.push(  ques[ tempQues ] );
            }
            for (var i = 0; i < numberOfQuestions; i++) {
            	var temp = 0;
            	var matchFound = false;
                // var imgSrc = getPathManager().getQAPanelQuestionImagePath("page0003.gif");
                strList += '<div id="questionanswerInnerContent_' + i + '" class="questionanswerInnerContent" data-role="questionmanager" type="questionanswerInnerContent"><div id="questionPanelSectionName"><div id="questionPanelSectionTitle"></div><div id="questionPanelSectionContent"></div></div><div id="questionPanelQuestion"><div id="questionPanelQuestionContent" class="questionPanelQuestionContent"><div id="theQuestionAsked"></div></div><div id="questionPanelOptions" class="questionPanelOptions"></div><div id="questionImage" class="questionImage"><img id="qaInnerImage" src="" /></div></div></div>';                
                for( var j = 0; j < objThis.member.groupXMLData.interaction.interaction.length; j++ )
                {
                	if( numberOfQuestions == 1 )
                	{
                		if( tempArr[ 0 ].comments != undefined && tempArr[ 0 ].comments.body_text != "" )
	        			{
	        				strList += '<div id="printTeacherComment_'+ i + 
	        				'" class="printPanelTeacherCommentContainer"><div class="printPanelTeacherComment">' + 
	        				tempArr[ 0 ].comments.body_text +
	        				'</div><div class="printTeacherCommentIcon">' + '</div></div>';
	        			}
                	}
                	else
                	{
                		var id = objThis.member.groupXMLData.interaction.interaction[ j ].id;
                		for( var k = 0; k < tempArr.length; k++ )
                		{
                			
                			if( tempArr[ k ].id == id )
                			{
                				if( tempArr[ k ].comments != undefined && tempArr[ k ].comments.body_text != "" )
			        			{
			        				strList += '<div id="printTeacherComment_'+ i + 
			        				'" class="printPanelTeacherCommentContainer"><div class="printPanelTeacherComment">' + 
			        				tempArr[ k ].comments.body_text +
			        				'</div><div class="printTeacherCommentIcon">' + '</div></div>';
			        			}
                				tempArr.splice( k, 1 );
                				matchFound = true;
                				break;
                			}
                		}
                		if( matchFound )
                		{
                			break;
                		}
                	}
                }
                //for( var j =  )
                //{
                	//if( temp != i )
                	//{
                	//	temp++;
                	//	continue;
                //	}
                	//else
                //	{
               
                		//console.log( tempArr[ i ], tempArr, currentQuestion, parentGrpIdforPrint, objThis.member.groupXMLData );
                		
                	//}                	
        		//}
            }         
            
            strList += '</div>';
            $("#questionAnswerPrintPanel").append($(strList));
            $(".questionanswerInnerContent").width($("#questionAnswerPrintPanel").width());
            $(".questionanswerHeaderPrint").width($("#questionAnswerPrintPanel").width());
            $("#questionAnswerPrintPanel .print-wrapper .questionImage").css("max-width", $("#questionAnswerPrintPanel").width() - 88 );
            $("#questionAnswerPrintPanel .print-wrapper .questionImage img").css("max-width", $("#questionAnswerPrintPanel").width() - 88 );
            objThis.populatePrintPanelInnerContent();
            $("#questionAnswerPrintPanel").append('<div class="print-footer">&nbsp;</div>');

        },

        populatePrintPanelInnerContent : function() {
            var objThis = this;
            //console.log("***************************************************************************: ", objThis.member.numXmlLoadedForPrint);
            var numberOfQuestions = objThis.member.groupXMLData.interaction.interaction.length;
            if (numberOfQuestions == undefined) {
                numberOfQuestions = 1;
            }

            for (var l = 0; l < numberOfQuestions; l++) {
                if (numberOfQuestions == 1) {
                    currQuesId = "questionanswerInnerContent_" + l;
                    var parentGrpIdforPrint = objThis.member.lastGrpXMLLoaded;
                    $($("#questionAnswerPrintPanel").find('[id="' + currQuesId +'"]')[0]).attr("interid", objThis.member.groupXMLData.interaction.interaction.id);
                    
                    $($("#questionAnswerPrintPanel").find('[id="' + currQuesId +'"]')[0]).attr("interParentid", parentGrpIdforPrint);

                    currQuesURL = objThis.member.groupXMLData.interaction.interaction.url;
                    currQuesType = objThis.member.groupXMLData.interaction.interaction.type;
                } else {
                    currQuesId = "questionanswerInnerContent_" + l;
                    var currentQuestion = objThis.member.currentGrpQuestions[objThis.member.curQuestionIndexForPrint];
                    var parentGrpIdforPrint = $("#" + currentQuestion).parent().attr('interactiongroupid');
                    //console.log("currQuesId: ",currQuesId);
                    $($("#questionAnswerPrintPanel").find('[id="' + currQuesId +'"]')[0]).attr("interid", objThis.member.groupXMLData.interaction.interaction[l].id);
                    $($("#questionAnswerPrintPanel").find('[id="' + currQuesId +'"]')[0]).attr("interParentid", objThis.member.lastGrpXMLLoaded);

                    currQuesURL = objThis.member.groupXMLData.interaction.interaction[l].url;
                    currQuesType = objThis.member.groupXMLData.interaction.interaction[l].type;
                }

                var arrObj = new Object();
                arrObj.id = currQuesId;
                arrObj.index = l;
                arrObj.currQuesURL = currQuesURL;
                arrObj.currQuesType = currQuesType;
                objThis.setCurrentQuesType(arrObj);
                
            }
        },

        addPrintPopup : function() {
            var objThis = this;
            var numberOfQuestions = objThis.member.groupXMLData.interaction.interaction.length;
            if (numberOfQuestions == undefined) {
                numberOfQuestions = 1;
            }
            for (var i = 0; i < numberOfQuestions; i++) {
                var arrObj = new Array();
                if(numberOfQuestions == 1){
                    arrObj.interid = objThis.member.groupXMLData.interaction.interaction.id;
                }
                else{
                    arrObj.interid = objThis.member.groupXMLData.interaction.interaction[i].id;
                }
                arrObj.interParentid =objThis.member.lastGrpXMLLoaded;
                
                var currentQuestionToBePrinted = objThis.member.currentGrpQuestions[i];
                var strQAContent = objThis.member.questionXMLData[currentQuestionToBePrinted];
               // console.log("strQAContent:strQAContent: ",strQAContent);
                var printableInnerContent = $("#questionAnswerPrintPanel").find('[class="questionanswerInnerContent"]')[i];
                var currentInteractiontype = objThis.member.interactionTypeForQues[currentQuestionToBePrinted];
                //console.log("currentInteractiontype: ",$(currentInteractiontype.element).attr('id'), i);
                    try {
                        currentInteractiontype.populatePrintPanelForInteractivity(strQAContent, printableInnerContent, arrObj);
                    } catch(e) {
                            console.log("Error: ",$(currentInteractiontype.element).attr('id'));
                    }
                
               // console.log("************************   ",i,$(printableInnerContent)[0],objThis.member.interactionTypeForQues[currentQuestionToBePrinted]);
            }

           /* var annotationPanelWidth = $("#bookContentContainer").offset().left + ($("#bookContentContainer").width() - $("#questionAnswerPrintPanel").width()) / 2;
            $("#questionAnswerPrintPanel").css({
                'position' : 'absolute',
                'bottom' : "0px",
                'top' : "auto",
                'left' : annotationPanelWidth + "px",
                'visibility' : "hidden"
            });
            PopupManager.addPopup($("#questionAnswerPrintPanel"), $(objThis), {
                isModal : true,
                isTapLayoutDisabled : true,
                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                isCentered : true,
                callbackFunc : objThis.positionPrintPopup()

            });*/
           $("#printPanelWaitImgContainer").css('display','none');
            objThis.createPrintHtml();
            $("#questionAnswerPrintPanel").find(".questionPanelOptionsText").css({
                "height" : "auto"
            })
            $("#questionAnswerPrintPanel").find('[id="chkBoxBtn"]').css("cursor", "auto");
            $("#questionAnswerPrintPanel").find('[id="chkBoxBtn"]').css("cursor", "auto");
        },

        createPrintHtml : function() {
            $("#questionAnswerPrintPanel").find('.print-wrapper').append('<div class="qadivFooter"><span id="qadivFooterText"></span></div>');
            $("#questionAnswerPrintPanel").find('.print-wrapper').find('#qadivFooterText').html(EPubConfig.Rights);
            var objThis= this;
            $(".questionPaneIconPrint").unbind('click').bind('click', function() {
            	var tempDiv = "<div id='tempQAPrint' ><div class='QAPrint' >"+ $("#questionAnswerPrintPanel").html() +"</div></div>"
            	$("#questionAnswerPrintPanel").append(tempDiv);
            	objThis.detectBrowserAgent();
            	
            	$("#tempQAPrint .questionanswerInnerContent").css("width", "780px");
                $($("#tempQAPrint")[0]).printArea({
                    mode : "iframe",
                    popClose : false
                });
                $("#tempQAPrint").remove();	
            });
            $('.printPanelClsBtn').unbind('click').bind('click', function() {
            	$(".questionanswerInnerContent").css("width" , "auto");
           		$(".questionanswerHeaderPrint").css("width" , "auto");
                objThis.member.numXmlLoadedForPrint = 0;
                objThis.member.isprintCalled = false;
                objThis.member.currInteractionType = interactivityTypeTosave;
                PopupManager.removePopup();
            });
        },
		
		detectBrowserAgent : function() {
			var a4SheetWidth;
			var a4SheetHeight;
            var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
            // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
            if (isOpera) {
                // return "opera";
            }
            var isFirefox = typeof InstallTrigger !== 'undefined';
            // Firefox 1.0+
            if (isFirefox) {
                a4SheetWidth = "780px";
                a4SheetHeight = "1000px";
                // return "firefox";
            }
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            // At least Safari 3+: "[object HTMLElementConstructor]"
            if (isSafari) {

                a4SheetWidth = "840px";
                a4SheetHeight = "1000px";
                // return "safari";
            }
            var isChrome = !!window.chrome && !isOpera;
            // Chrome 1+
            if (isChrome) {

                a4SheetWidth = "840px";
                a4SheetHeight = "1070px";
                /* if(EPubConfig.pageView == "doublePage"){
                objThis.member.a4SheetWidth = 780;
                }*/

                //return "chrome";
            }
            var isIE = /*@cc_on!@*/false || document.documentMode;
            if (isIE) {
                //objThis.member.a4SheetWidth = 1300;
                //objThis.member.a4SheetHeight = 1650;

                a4SheetWidth = "780px";
                a4SheetHeight = "1000px";
                // return "IE";
            }
			$("#tempQAPrint").css("width",a4SheetWidth);
            $("#tempQAPrint").css("height",a4SheetHeight);
            currentPageWidth = objThis.member.a4SheetWidth;
			currentPageHeight = objThis.member.a4SheetHeight;
            frameWidth = objThis.member.a4SheetWidth;
            frameHeight = objThis.member.a4SheetHeight;
        },
        
        positionPrintPopup : function() {
            //var annotationPanelWidth = $("#annotationPanel").width() + 5;
            var annotationPanelWidth = $("#bookContentContainer").offset().left + ($("#bookContentContainer").width() - $("#questionAnswerPrintPanel").width()) / 2;
            setTimeout(function() {
                $("#questionAnswerPrintPanel").css({
                    'position' : 'absolute',
                    'bottom' : "0px",
                    'top' : "auto",
                    'left' : annotationPanelWidth + "px",
                    'visibility' : "visible"
                });
            }, 200);
        },
        
        /**
         * This group returns the array of questions in the current group. 
         */

        getCurrentGroupQuestionsArray : function() {
            return objThis.member.currentGrpQuestions;
        }
    });
})(jQuery);
