/**
 * @author lakshay.ahuja
 */
(function($, undefined) {
    $.widget("magic.singleChoiceInteractivity", $.magic.magicwidget, {

        options : {
            userResponse : -1,
            interactivityType : "singleChoiceInteractivity",
            xmlContent : null,
            parentgrpId : null

        },

        member : {
            questionArray : [],
            questionArrData : null,
            questionInnercontent : null,
            correctResponse : null,
        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            this.options.parentgrpId = questionManagerRef.member.lastGrpXMLLoaded;
            if (GlobalModel.questionPanelAnswers[this.options.parentgrpId] == null)
                GlobalModel.questionPanelAnswers[this.options.parentgrpId] = {};
        },

        _create : function() {
        },

        setQAPanelInnercontentLayout : function(strQAContent, questionInnerContent) {
            var objThis = this;
            var questionPanelOptions = $(questionInnerContent).find('[id=questionPanelOptions]');
            questionPanelOptions.removeClass("questionPanelMultipleChoiceOptions");
            var numberOfHeading;
            if (strQAContent.itemBody.div == undefined || strQAContent.itemBody.div.p == undefined) {
                numberOfHeading = 0;
                $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                $(questionInnerContent).find("#questionPanelSectionContent").html('');
            } else if (strQAContent.itemBody.div.p.length == undefined && typeof strQAContent.itemBody.div.p == "object") {
                numberOfHeading = 1;
            } else {
                numberOfHeading = strQAContent.itemBody.div.p.length;
            }
            for (var k = 0; k < numberOfHeading; k++) {
                if (numberOfHeading == 1) {
                    switch(strQAContent.itemBody.div.p.label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div.p.span);
                            $(questionInnerContent).find("#questionPanelSectionContent").html('');

                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div.p.span);
                            $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                            break;
                    }
                } else {
                    switch(strQAContent.itemBody.div.p[k].label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div.p[k].span);
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div.p[k].span);
                            break;
                    }
                }
            }
            if (strQAContent.itemBody.choiceInteraction.prompt) {
                $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.choiceInteraction.prompt);
            } else {
                $(questionInnerContent).find('[id=theQuestionAsked]').html('');
            }
            $(questionInnerContent).find('[id=QAfreeTextImage]').remove();
            questionPanelOptions.removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
            $(questionInnerContent).find('[id=questionImage]').css("display", "none");
            $(questionInnerContent).find("#questionPanelQuestionContent").css('width', 'auto');
            var numberOfChoices = strQAContent.itemBody.choiceInteraction.simpleChoice.length;
            if (numberOfChoices == undefined && typeof strQAContent.itemBody.choiceInteraction.simpleChoice == "Object") {
                numberOfChoices = 1;
            }
            var optionList = "";
            for (var i = 0; i < numberOfChoices; i++) {
                if (numberOfChoices == 1) {
                    optionList += "<div class='leftFloat marginB15' style='padding-left:5px;' identifier='" + strQAContent.itemBody.choiceInteraction.simpleChoice.identifier + "'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice + "</div><div type='showHideAnswer' class='incorrectAnswer'><div id='IncorrectIcon'></div></div></div>";
                } else {
                    optionList += "<div class='leftFloat marginB15' style='padding-left:5px;' identifier='" + strQAContent.itemBody.choiceInteraction.simpleChoice[i].identifier + "'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice[i] + "</div><div type='showHideAnswer' class='incorrectAnswer'><div id='IncorrectIcon'></div></div></div>";
                }
            }
            questionPanelOptions.html(optionList);
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	questionPanelOptions.attr( "disabled", true );
            	questionPanelOptions.addClass( "ui-disabled" ).attr( "aria-disabled", true );
            }
        },

        populatePrintPanelForInteractivity : function(strQAContent, printableInnerContent, arrObj) {
            var objThis = this;
            objThis.setQAPanelInnercontentLayout(strQAContent, printableInnerContent); 
            var elemId = arrObj.interid;
            var parentGrpIdforPrint = arrObj.interParentid;
            //console.log(elemId + "    " + parentGrpIdforPrint);
            var objData = GlobalModel.questionPanelAnswers[parentGrpIdforPrint][elemId];
            //console.log("objThis.options.userResponse:   ",objThis,objThis.options.userResponse);
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = parseInt($(objData.response).find("response").html());
                //console.log("objThis.options.userResponse:  ",objThis.options.userResponse);
                //$($(printableInnerContent).find('[id=chkBoxBtn]')[1]).removeClass("choiceUnselected").addClass("choiceSelected");
                //$($(printableInnerContent).find('[id=option]')[1]).removeClass("choiceCopy").addClass("choiceCopySelected");
                if (objThis.options.userResponse > -1) {
                    $($(printableInnerContent).find('[id=chkBoxBtn]')[objThis.options.userResponse]).removeClass("choiceUnselected").addClass("choiceSelected");
                    $($(printableInnerContent).find('[id=option]')[objThis.options.userResponse]).removeClass("choiceCopy").addClass("choiceCopySelected");
                }
            }
        },

        populateQAPanelInnerContent : function(strQAContent, questionInnerContent) {
            var objThis = this;
            try {
                objThis.member.questionInnercontent = questionInnerContent;
                objThis.options.xmlContent = strQAContent;
                objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                objThis.setSavedUserResponse();
                $(questionInnerContent.element).find('[id=chkBoxBtn]').unbind('click').bind('click', function() {
                    objThis.choiceClickHandler(this);
                });
            } catch(e) {

            }

        },
        choiceClickHandler : function(selectedRef) {
            var objThis = this;
            if ($(selectedRef).hasClass("choiceUnselected")) {
                $(objThis.member.questionInnercontent.element).find('#option').removeClass("choiceCopySelected").addClass("choiceCopy");
                $(objThis.member.questionInnercontent.element).find('#chkBoxBtn').removeClass("choiceSelected").addClass("choiceUnselected");
                $(selectedRef).removeClass("choiceUnselected").addClass("choiceSelected");
                $($(selectedRef).parent()).find('#option').removeClass("choiceCopy").addClass("choiceCopySelected");
                var indexElem = ($(selectedRef).parent().parent().children().index($(selectedRef).parent()));
                objThis.options.userResponse = indexElem;
            } else {
                $(objThis.member.questionInnercontent.element).find('#option').removeClass("choiceCopySelected").addClass("choiceCopy");
                $(objThis.member.questionInnercontent.element).find('#chkBoxBtn').removeClass("choiceSelected").addClass("choiceUnselected");
                $(selectedRef).removeClass("choiceSelected").addClass("choiceUnselected");
                $($(selectedRef).parent()).find('#option').removeClass("choiceCopySelected").addClass("choiceCopy");
                objThis.options.userResponse = -1;
            }
        },

        saveCurrentResponse : function() {
            this.setUserResponse();
        },

        setUserResponse : function() {
            var objThis = this;
            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');
            //objThis.options.userResponse = $(objThis.member.questionInnercontent.element).find('.choiceCopySelected');

        },

        setSavedUserResponse : function() {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];
            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];
            //console.log("objData: ",objData);
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = parseInt($(objData.response).find("response").html());

                if (objThis.options.userResponse > -1) {
                    $($(objThis.member.questionInnercontent.element).find('[id=chkBoxBtn]')[objThis.options.userResponse]).removeClass("choiceUnselected").addClass("choiceSelected");
                    $($(objThis.member.questionInnercontent.element).find('[id=option]')[objThis.options.userResponse]).removeClass("choiceCopy").addClass("choiceCopySelected");
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (objThis.options.userResponse != null && objThis.options.userResponse != -1) {

                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                } else {
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
            }
        },

        saveCurrentResponseToServices : function() {
        	var objThis = this;
        	var strFormattedQuestion = $(objThis.member.questionInnercontent.element).find('[id=questionPanelQuestion]').html();
            strFormattedQuestion = strFormattedQuestion.replace("choiceSelected", "choiceUnselected");
            strFormattedQuestion = strFormattedQuestion.replace("choiceCopySelected", "choiceCopy");
            var strBodyText = "<qa><question>" + strFormattedQuestion + "</question><response>" + objThis.options.userResponse + "</response></qa>";
            var arrObj = new Object();
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques', '')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            arrObj.id = questionsGrp[indexofQues];
            arrObj.response = strBodyText;
            arrObj.interactivityType = objThis.options.interactivityType;
            //console.log( $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]'), " This is Interactivity");
            arrObj.pageNumber = $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]').find("span").html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
            arrObj.creationDate = getTodaysDate();
            arrObj.parentGrpId = questionManagerRef.member.lastGrpXMLLoaded;

            if ($("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk')) {
                arrObj.savetonote = 1;
            } else {
                arrObj.savetonote = 0;
            }
            //var curParentId = ($("#"+arrObj.id).parent().attr('interactiongroupid'));
            var flag = -1;
            //GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj)
            for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                var gModelIdParent = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                if (gModelIdParent == arrObj.parentGrpId) {
                    flag = 1;
                    break;
                }
            }
            var indexElem = $(objThis.element).parent().children().index($(objThis.element));
            if (objThis.options.userResponse != null && objThis.options.userResponse != -1) {
                arrObj.flag_id = 1;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                /* if (flag == -1) {
                 var pushItem = true;
                 for(var n=0; n<GlobalModel.questionPanelAnswersForAnnoPanel.length ; n++){
                 if(GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId == arrObj.parentGrpId){
                 pushItem = false;
                 }
                 }
                 if(pushItem == true){
                 GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj);
                 }

                 }*/
            } else {
                arrObj.flag_id = 0;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
            }
            if (GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id] == null) {
                
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.create(arrObj, 9, function(data){
            			GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].annotation_id = data.annotation_id;
            		});
            	}
                
            } else {
                
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.update(9, arrObj);
            	}
                arrObj.annotation_id = GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id].annotation_id;
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] &&
                	GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments )
            {
            	arrObj.comments = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments;
            	arrObj.isNotified = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].isNotified;
            }
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
        		{
            		$("#questionanswerInnerContent").data('questionmanager').addCommentToQA( arrObj
        				,GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments
        				,GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber);
        		}    
        		else
        		{
        			$("#questionanswerInnerContent").data('questionmanager').addCommentToQA();
        		}    		
            }
            else if( GlobalModel.BookEdition == "SE" )
            {
            	$("#questionanswerInnerContent").data('questionmanager').resetCommentSection();
            	$("#questionanswerInnerContent").data('questionmanager').addCommentInCommentBox();
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
            {
            	arrObj.pageNumber = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber;
            }
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    		{
        		// do nothing
    		}
        	else
        	{
        		GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] = arrObj;
        	}
        },

        displayHideCorrectResponse : function(bVal) {
            var objThis = this;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = objThis.options.xmlContent.responseDeclaration.correctResponse.value;
            //console.log($(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]'));
            if (bVal == 1) {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'visible')
                $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse + ']').find('[type=showHideAnswer]').removeClass('incorrectAnswer').addClass('correctAnswer');
                $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse + ']').find('[type=showHideAnswer]').html('<div id="correctIcon"></div>');

            } else {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'hidden')
                $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse + ']').find('[type=showHideAnswer]').addClass('incorrectAnswer').removeClass('correctAnswer');

            }
        }
    });
})(jQuery);
