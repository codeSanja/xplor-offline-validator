/**
 * @author: sachin.manchanda
 */
(function($, undefined) {
    $.widget("magic.fillUpsInteractivity", $.magic.magicwidget, {

        options : {
            userResponse : [],
            interactivityType : "fillUpsInteractivity",
            parentgrpId : null,
            xmlContent : null,
            currentResponse : []
        },

        member : {
            questionArray : [],
            questionArrData : null,
            questionInnercontent : null
        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            this.options.parentgrpId = questionManagerRef.member.lastGrpXMLLoaded;
            if (GlobalModel.questionPanelAnswers[this.options.parentgrpId] == null)
                GlobalModel.questionPanelAnswers[this.options.parentgrpId] = {};
        },

        _create : function() {
        },

        setQAPanelInnercontentLayout : function(strQAContent, questionInnerContent) {
            var objThis = this;
            var questionPanelOptions = $(questionInnerContent).find('[id=questionPanelOptions]');
            questionPanelOptions.removeClass("questionPanelMultipleChoiceOptions");
            var numberOfHeading;
            var itemBodyFirstDivRef;
            $(questionInnerContent).find('[id=questionImage]').css("display", "none");
			
            if (strQAContent.itemBody.div.length) {
				
                itemBodyFirstDivRef = strQAContent.itemBody.div[0];
                $(questionInnerContent).find('[id=questionImage]').css("display", "block");
                questionPanelOptions.removeClass("questionPanelOptionsText").addClass("questionPanelOptions").addClass("questionPanelFillupOptions");
                var isImgAvailable = false;
				if(strQAContent.itemBody.div[1])
				{
					if(strQAContent.itemBody.div[1].div)
					{
						if(strQAContent.itemBody.div[1].div.object)
						{
							var imgSrc = getPathManager().getQAPanelQuestionImagePath(strQAContent.itemBody.div[1].div.object.data);
	
	            			$($(questionInnerContent).find('[id=questionImage]')[0]).find('[id=qaInnerImage]').attr("src", imgSrc);
							isImgAvailable = true;
						}
					}
				}
	            if(isImgAvailable == false)
				{
					$($(questionInnerContent).find('[id=questionImage]')[0]).css('display', 'none');
					questionPanelOptions.removeClass("questionPanelFillupOptions")
				}
				
				
            } else {
                itemBodyFirstDivRef = strQAContent.itemBody.div;
                questionPanelOptions.removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
				questionPanelOptions.removeClass("questionPanelFillupOptions")
            }

            if (itemBodyFirstDivRef == undefined || itemBodyFirstDivRef.p == undefined) {
                numberOfHeading = 0;
                $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                $(questionInnerContent).find("#questionPanelSectionContent").html('');
            } else if (itemBodyFirstDivRef.p.length == undefined && typeof itemBodyFirstDivRef.p == "object") {
                numberOfHeading = 1;
            } else {
                numberOfHeading = itemBodyFirstDivRef.p.length;
            }
            for (var k = 0; k < numberOfHeading; k++) {
                if (numberOfHeading == 1) {
                    switch(itemBodyFirstDivRef.p.label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(itemBodyFirstDivRef.p.span);
                            $(questionInnerContent).find("#questionPanelSectionContent").html('');
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(itemBodyFirstDivRef.p.span);
                            $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                            break;
                    }
                } else {
                    switch(itemBodyFirstDivRef.p[k].label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(itemBodyFirstDivRef.p[k].span);
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(itemBodyFirstDivRef.p[k].span);
                            break;
                    }
                }
            }
            if (strQAContent.itemBody.p) {
                $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.p);
            } else {
                $(questionInnerContent).find('[id=theQuestionAsked]').html('');
            }
            $(questionInnerContent).find("#questionPanelQuestionContent").css('width', 'auto');
            var numberOffillUps = strQAContent.itemBody.blockquote.length;

            
            if (numberOffillUps == undefined && typeof strQAContent.itemBody.blockquote == "object") {
                numberOffillUps = 1;
            }
            var fillUpList = "";
            if (strQAContent.itemBody.blockquote && typeof strQAContent.itemBody.blockquote == "string") {
                numberOffillUps = 1;
            }
            for (var i = 0; i < numberOffillUps; i++) {
                fillUpList += "<div class='questionPanelFillup'>"
                if (numberOffillUps == 1) {
                    fillUpList += strQAContent.itemBody.blockquote.p;
                } else {
                    fillUpList += strQAContent.itemBody.blockquote[i].p;
                }
                fillUpList += "</div>";
            }
            //alert(imgSrc);
            questionPanelOptions.html(fillUpList);
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	questionPanelOptions.attr( "disabled", true );
            	questionPanelOptions.addClass( "ui-disabled" ).attr( "aria-disabled", true );
            }
            $(questionInnerContent).find('textentryinteraction').replaceWith("<input class='qAPanelFillinTheBlanks' type='text' style='margin:0px 5px; outline:none;'   maxlength='25'/>");

        },

        populatePrintPanelForInteractivity : function(strQAContent, printableInnerContent,arrObj) {
            var objThis = this;
            objThis.setQAPanelInnercontentLayout(strQAContent, printableInnerContent);
            $(printableInnerContent).find('input').attr('disabled', 'disabled');
            $(printableInnerContent).find('input').css('width', '220px');
            var elemId = arrObj.interid;
            var parentGrpIdforPrint = arrObj.interParentid;
            var objData = GlobalModel.questionPanelAnswers[parentGrpIdforPrint][elemId];
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                	$($(printableInnerContent).find('.qAPanelFillinTheBlanks')[0]).replaceWith("<div id='qaPanelFillInTheBlankPrintDiv'>" + objThis.options.userResponse[i] + "</div>");
                }
                var flag = -1;
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    if (objThis.options.userResponse[i] != "") {
                        flag = 0;
                        break;
                    }
                }
            }

        },

        populateQAPanelInnerContent : function(strQAContent, questionInnerContent) {
            var objThis = this;
            try{
                objThis.member.questionInnercontent = questionInnerContent;
                objThis.options.xmlContent = strQAContent;
                objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                objThis.setSavedUserResponse();
            }
            catch(e){
                
            }
            
        },

        saveCurrentResponse : function() {
            this.setUserResponse();
        },

        setUserResponse : function() {
            var objThis = this;
            objThis.options.userResponse = [];
            var numberOfQuestions = ($(objThis.member.questionInnercontent.element).find('.questionPanelFillup').length);
            for (var i = 0; i < numberOfQuestions; i++) {
                var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qAPanelFillinTheBlanks')[i]).val());
                objThis.options.userResponse.push(fillUpResponse);
            }
            $(objThis.member.questionInnercontent.element).find('[id=questionPanelOptions]').removeClass("questionPanelFillupOptions")
            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');

        },

        setSavedUserResponse : function() {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];
            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];

            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");

                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qAPanelFillinTheBlanks')[i]).val($.trim(objThis.options.userResponse[i])));
                }
                var flag = -1;
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    if ($.trim(objThis.options.userResponse[i]) != "") {
                        flag = 0;
                        break;
                    }
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (flag != -1) {

                    //var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                } else {

                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
            }
        },

        saveCurrentResponseToServices : function() {
        	var objThis = this;
            var arrObj = new Object();
            objThis.options.userResponse = [];
            var numberOfQuestions = ($(objThis.member.questionInnercontent.element).find('.qAPanelFillinTheBlanks').length);
            for (var i = 0; i < numberOfQuestions; i++) {
                var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qAPanelFillinTheBlanks')[i]).val());
                objThis.options.userResponse.push(fillUpResponse);
            }
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            arrObj.id = questionsGrp[indexofQues];
            var reponseString = "";
            for (var i = 0; i < objThis.options.userResponse.length; i++) {
                reponseString = reponseString + objThis.options.userResponse[i];
                if (i != objThis.options.userResponse.length - 1) {
                    reponseString = reponseString + ",";
                }
            }

            var strFormattedQuestion = $(objThis.member.questionInnercontent.element).find('[id=questionPanelQuestion]').html();
            var strBodyText = "<qa><question>" + strFormattedQuestion + "</question><response>" + reponseString + "</response></qa>";

            arrObj.response = strBodyText;
            arrObj.interactivityType = objThis.options.interactivityType;
            //console.log( $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]'), " This is Interactivity");
            arrObj.pageNumber = $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]').find("span").html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
            arrObj.creationDate = getTodaysDate();
            arrObj.parentGrpId = questionManagerRef.member.lastGrpXMLLoaded;
           // arrObj.pageNumber = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber;
            if ($("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk')) {
                arrObj.savetonote = 1;
            } else {
                arrObj.savetonote = 0;
            }

            //var curParentId = ($("#"+arrObj.id).parent().attr('interactiongroupid'));
            var tempflag = -1;
            //GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj)
            for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                var gModelIdParent = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                if (gModelIdParent == arrObj.parentGrpId) {
                    tempflag = 1;
                    break;
                }
            }
            var flag = -1;
            for (var i = 0; i < objThis.options.userResponse.length; i++) {
                if ($.trim(objThis.options.userResponse[i]) != "") {
                    flag = 0;
                    break;
                }
            }
            var indexElem = $(objThis.element).parent().children().index($(objThis.element));

            if (flag != -1) {
                arrObj.flag_id = 1;
                //var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
               /* var pushItem = true;
                for (var n = 0; n < GlobalModel.questionPanelAnswersForAnnoPanel.length; n++) {
                    if (GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId == arrObj.parentGrpId) {
                        pushItem = false;
                    }
                }
                if (pushItem == true) {
                    GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj);
                }*/
            } else {
                arrObj.flag_id = 0;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');

            }
            if (GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id] == null) {
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.create(arrObj, 9, function(data){
            			GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].annotation_id = data.annotation_id;
            		});
            	}
                //save data
                
            } else {

            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.update(9, arrObj);
            	}
                arrObj.annotation_id = GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id].annotation_id;
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] &&
                	GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments )
            {
            	arrObj.comments = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments;
            	arrObj.isNotified = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].isNotified;
            }
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
        		{
            		$("#questionanswerInnerContent").data('questionmanager').addCommentToQA( arrObj,
        				GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments
        				,GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber);
        		}
        		else
        		{
        			$("#questionanswerInnerContent").data('questionmanager').addCommentToQA();
        		}
            }
            else if( GlobalModel.BookEdition == "SE" )
            {
            	$("#questionanswerInnerContent").data('questionmanager').resetCommentSection();
            	$("#questionanswerInnerContent").data('questionmanager').addCommentInCommentBox();
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
            {
            	arrObj.pageNumber = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber;
            }
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    		{
        		// do nothing
    		}
        	else
        	{
        		GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] = arrObj;
        	}
        },

        displayHideCorrectResponse : function(bVal) {
            var objThis = this;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = [];
            var correctResponseLength = objThis.options.xmlContent.responseDeclaration.correctResponse.value.length;
            if (bVal == 1) {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'visible')
                for (var i = 0; i < correctResponseLength; i++) {
                    objThis.options.currentResponse[i] = $($(objThis.member.questionInnercontent.element).find('input')[i]).val();
                    correctResponse[i] = objThis.options.xmlContent.responseDeclaration.correctResponse.value[i];
                    $($(objThis.member.questionInnercontent.element).find('input')[i]).val(correctResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('input')[i]).attr('disabled', 'disabled');
                }
            } else {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'hidden')
                for (var i = 0; i < correctResponseLength; i++) {
                    $($(objThis.member.questionInnercontent.element).find('input')[i]).val(objThis.options.currentResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('input')[i]).removeAttr('disabled')
                }
            }
        }
    });
})(jQuery);
