/**
 * @author lakshay.ahuja
 */
var qaScrollHigher;
(function($, undefined) {
    $.widget("magic.questionanswerpanelcomp", $.magic.magicwidget, {
        options : {
            questionPanelList : []
        },

        member : {
            currentQuestion : null,
            isSAveToNoteBookChecked: [],
            currentGrp: null,
            currentGrpArray: [],
        },

        _init : function() {
            var objThis = this;
            $.magic.magicwidget.prototype._init.call(this);
            if (ismobile != null) {
                objThis.addSwipeListner(objThis);
            }
            
            $(window).resize(function(){
                    objThis.updateViewSize();
                });
                
            $( document ).bind( "learnosityPanelOpening", function(){
            	objThis.closeDisplayQuesAnsPanel( false );
            } );

            $(objThis.element).css('width', EPubConfig.pageWidth);
        },

        _create : function() {
        },


        setCurrentQuestionGrpAndArray: function(curGrp,curGrpArray){
          var objThis = this;
          objThis.member.currentGrp = curGrp;
          objThis.member.currentGrpArray = curGrpArray;
        },
        /**
         * This function will update the status of questions attempeted on the QAPanel header.
         * @param {Object} questionClicked This is the reference of the question has to be displayed in the panel
         */
        updateQuestionPanelDotsStatus : function(questionClicked) {
            var objThis = this;
            $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel');
            objThis.saveCurrentResponse();
            var questionNumberClicked = objThis.member.currentGrpArray.indexOf(questionClicked) + 1;
            var questionClickedId = questionClicked;
            objThis.member.currentQuestion = questionClicked;
            objThis.updateQuestionPanelList(questionClicked);
            objThis.updatePanelPositionWidth();
            objThis.setSaveToNoteBook();
        },
        
        setSaveToNoteBook: function(){
        	var objThis = this;
        	if(GlobalModel.questionPanelAnswers[objThis.member.currentGrp] && GlobalModel.questionPanelAnswers[objThis.member.currentGrp][objThis.member.currentQuestion] &&
        		GlobalModel.questionPanelAnswers[objThis.member.currentGrp][objThis.member.currentQuestion].savetonote){
				$("#questionAnswerPanel .savetoNoteBookBtn").addClass('savetoNoteBookBtnChk');	
			}
			else{
				$("#questionAnswerPanel .savetoNoteBookBtn").removeClass('savetoNoteBookBtnChk');
			}
        },
        
        
        handleSaveToNotebookClick : function()
        {
        	var objThis = this;
        	if( $("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk') )
        	{
        		$("#questionAnswerPanel .savetoNoteBookBtn").removeClass('savetoNoteBookBtnChk');
        		objThis.member.isSAveToNoteBookChecked[objThis.member.currentGrp] = false;
        		
        	}
        	else
        	{
        		$("#questionAnswerPanel .savetoNoteBookBtn").addClass('savetoNoteBookBtnChk');
        		objThis.member.isSAveToNoteBookChecked[objThis.member.currentGrp] = true;
        	}
        	
        },

        /**
         * This function will remove or display the question answer panel on the screen
         * @param {Object} bVal If bVal is true, display panel, else remove panel.
         */
        closeDisplayQuesAnsPanel : function(bVal) {
            var objThis = this;
            if (bVal) {
            	 $("#questionAnswerPanel").css("height", "auto");
            	 $("#qaPanelScrollableDiv").css("height", "auto");
                $(objThis.element).find('[id=questionPanelPrintIcon]').css('display','block');
                $(objThis.element).find('[id=questionPanelMinimizeIcon]').css('display','block');

                //var audioBtnLauncherDataRole = $("#audioLauncherBtn").attr('data-role');
                /*if ($(".players").css('display') == "block") {
                 $(".players").find('.audioPlayerCloseBtn').trigger('click');
                 }*/
                //var audioBtnLauncherDataRole = $("#audioLauncherBtn").attr('data-role');
				if ( $("#audioPopUp").css("display") == "block" ) 
				{
					$("#audioPlayerCloseBtn").trigger('click');
				}
                    
                if($("#imageGalleryPanel").css('display') == "block"){
                    $("#imageGalleryPanel").find('[id="imageGalleryClsBtn"]').trigger('click');
                }

                // $('#jp-stop-parent').trigger('click');
                //$(this.element).css('display','block');
                var minimizeBtn = $(this.element).find('[id=questionPanelMinimizeIcon]');
                if ($(minimizeBtn).hasClass('maximizeQAPanel')) {
                    objThis.minimizeMaximizeQAPanel();
                }
            } else {
                $("#questionAnswerPanel").slideUp(500, "easeInOutExpo");
                //this.saveCurrentResponse();
                $("#questionanswerInnerContent").css("display", "none");
                $("#checkAnswer").css('display', 'none');
                $(this.element).trigger('PanelClosed');
                //this.resetQuestionInnerContent();
            }
            var ref = $("#bookContentContainer").data('scrollviewcomp');
            ref.updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent)
        },
        /**
         * This function will minimize and maximize the question Answer Panel
         */
        minimizeMaximizeQAPanel : function() {
            var minimizeBtn = $(this.element).find('[id=questionPanelMinimizeIcon]');
            var isPageGoingOutOfWindow = false;
            if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
                if (($("#scrollMainContainer").width()) * GlobalModel.currentScalePercent > $("#bookContentContainer").width()) {
                    isPageGoingOutOfWindow = true;
                }
            } else {
                if (($("[type='PageContainer']").width()) * GlobalModel.currentScalePercent > $("#bookContentContainer2").width()) {
                    isPageGoingOutOfWindow = true;
                }

            }
            if (isPageGoingOutOfWindow) {
                if (!ismobile) {
                    $("#questionAnswerPanel").css({
                        'bottom' : '16px'
                    });
                }
            } else {
                if (!ismobile) {
                    $("#questionAnswerPanel").css({
                        'bottom' : '0px'
                    });
                }
            }

            if ($(minimizeBtn).hasClass('minimizeQAPanel')) {
                $(minimizeBtn).removeClass('minimizeQAPanel').addClass('maximizeQAPanel');
                
                	$(this.element).find('[id=questionanswerInnerContent]').css('display', 'none');
	                $(this.element).find('[id=questionanswerFooter]').css('display', 'none');
	                //$(this.element).find('[id=questionPanelSaveButton]').css('display','none');
	                $(this.element).find('[id=prevQuestionArrow]').css('display', 'none');
	                $(this.element).find('[id=nextQuestionArrow]').css('display', 'none');
	                $(this.element).find('[id=questionPanelPrintIcon]').css('display', 'none');
	                $(this.element).find('[type=imageGalleryPanel]').css('display', 'none');
	                $(this.element).find('[id=imageGalleryContractBtn]').css( "display", "none" );
                
                $(this.element).css('width', '625px');
                $(this.element).trigger('PanelMinimized');
            } else if ($(minimizeBtn).hasClass('maximizeQAPanel')) {
                $(minimizeBtn).removeClass('maximizeQAPanel').addClass('minimizeQAPanel');
               
                	$(this.element).find('[id=questionanswerInnerContent]').css('display', 'block');
	                $(this.element).find('[id=questionanswerFooter]').css('display', 'block');
	                // $(this.element).find('[id=questionPanelSaveButton]').css('display','block');
	                $(this.element).find('[id=prevQuestionArrow]').css('display', 'block');
	                $(this.element).find('[id=nextQuestionArrow]').css('display', 'block');
	                $(this.element).find('[id=questionPanelPrintIcon]').css('display', 'block');
	                $(this.element).find('[type=imageGalleryPanel]').css('display', 'block');
	                if ( $(this.element).find('[id=imageGalleryImageContainer]').css( "display" ) == "block" )
                	{
                		$(this.element).find('[id=imageGalleryContractBtn]').css( "display", "block" );
                	}
                
                $(this.element).css('width', '820px');
            }
            this.updatePanelPositionWidth();
        },

        updatePanelPositionWidth : function() {
            var minimizeBtn = $("#questionPanelMinimizeIcon");
            var isPageGoingOutOfWindow = false;
            var nVal = GlobalModel.currentScalePercent;
            if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
                if (($("#scrollMainContainer").width()) * nVal > $("#bookContentContainer").width()) {
                    isPageGoingOutOfWindow = true;
                }
            } else {
                if (($("[type='PageContainer']").width()) * nVal > $("#bookContentContainer2").width()) {
                    isPageGoingOutOfWindow = true;
                }
            }
            $("#bookContentContainer").data('scrollviewcomp').updateDesignForPageView("panelPosition",GlobalModel.currentScalePercent);
        },

        /**
         * This function will handle the clicks on the dots of QA Panel header
         * @param {Object} curDotclicked the current Dot clicked
         */
        handleQuesDotsClick : function(curDotclicked) {
            var objThis = this;
            var IdDotElementClickedNumber = $(curDotclicked).attr('id').replace('questionPanelques', '');
            IdDotElementClickedNumber = parseInt(IdDotElementClickedNumber);
            var arrObj = new Object();
            arrObj.childClicked = objThis.member.currentGrpArray[IdDotElementClickedNumber - 1];
            arrObj.id = objThis.member.currentGrp;
            $(document).trigger('newQuestionClicked',arrObj);
            //$($(objThis.member.currentQuestion).parent().find('[type="widget:quiz"]')[IdDotElementClickedNumber - 1]).trigger('click');
            //$(questionClicked).parent().find('[type="widget:quiz"]').index(questionClicked) + 1
        },
        /**
         * This function will handle the navigation in question panel on click of next prev questions.
         * @param {Object} bVal If 1, navigate to next, if -1, navigate to previous ques.
         */
        handleQAPanelNavigationArrows : function(bVal, questionArray) {
            var objThis = this;
            var currQuestionClicked = objThis.member.currentQuestion;
            var currQuestionNumberClicked = objThis.member.currentGrpArray.indexOf(currQuestionClicked) + 1;
            var arrObj = new Object();
            if (bVal == 1) {
                var totalQues = objThis.member.currentGrpArray.length;
                arrObj.childClicked = objThis.member.currentGrpArray[currQuestionNumberClicked];
                arrObj.id = objThis.member.currentGrp;
                if (totalQues > currQuestionNumberClicked) {
                } else {
                    arrObj.childClicked = objThis.member.currentGrpArray[0];
                    arrObj.id = objThis.member.currentGrp;
                }
                $(document).trigger('newQuestionClicked',arrObj);
            } else if (bVal == -1) {
                var totalQues = objThis.member.currentGrpArray.length;
                arrObj.childClicked = objThis.member.currentGrpArray[currQuestionNumberClicked - 2];
                arrObj.id = objThis.member.currentGrp;
                if (currQuestionNumberClicked > 1) {
                } else if (currQuestionNumberClicked == 1) {
                    arrObj.childClicked = objThis.member.currentGrpArray[totalQues - 1];
                    arrObj.id = objThis.member.currentGrp;
                }
                $(document).trigger('newQuestionClicked',arrObj);
            }
        },

        saveCurrentResponse : function() {
            $(this.element).trigger('saveCurrentResponse');
        },

        handleQAPanelprint : function() {
            if (EPubConfig.printExternally == "true") {
                window.open(strReaderPath + "print.html", "_blank");
            } else {
                $("#questionAnswerPrintPanel").css({
                    'position' : 'absolute',
                    'top' : 0,
                    'left' : 0,
                    'background' : 'white',
                    'height' : '85%',
                    'width' : '85%'
                });

                PopupManager.addPopup($("#questionAnswerPrintPanel"), $(objThis), {
                    isModal : true,
                    isTapLayoutDisabled : true,
                    popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                    isCentered : true

                });
                $(".popupMgrMaskStyleSemiTransparent").css("opacity", "0.6");
                $("#questionAnswerPrintPanel").find(".printPanelClsBtn").unbind('click').bind('click', function() {
                    $(".popupMgrMaskStyleSemiTransparent").css("opacity", "0");
                    PopupManager.removePopup();
                });
                setTimeout(function() {
                    $("#contentarea").attr("data", strReaderPath + "print.html");
                }, 100);
            }
        },

        currentQuestionPageNumber : function(curRef) {
            var objThis = this;
            var containerParent = curRef;
            if ($(containerParent).attr('type') == "PageContainer") {
                containerParent = $(containerParent);
            } else {
                while ($(containerParent).parent().attr('id') != 'bookContentContainer' && containerParent != null) {
                    if ($(containerParent).parent().attr('type') == "PageContainer") {
                        containerParent = $(containerParent).parent();
                        break;
                    } else {
                        containerParent = $(containerParent).parent();
                    }
                }
            }

            var curPageNumber = $(containerParent).attr('pagebreakvalue');
            var pageindex = GlobalModel.pageBrkValueArr.indexOf(curPageNumber);
            $(objThis.element).find('[id=questionPanelPageNumber]').html('<span class="questionPanelPageSpan">' + GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"] + curPageNumber+ '</span> ' );
            $(".questionanswerHeaderPrint #header-txt").html(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"] + " " + curPageNumber);
            return pageindex;

        },

        addSwipeListner : function(objSwipeElement) {
            $(objSwipeElement.element).find('[id=questionanswerInnerContent]').unbind("swipeleft").bind("swipeleft", function() {
                $("#nextQuestionArrow").trigger('click', 1);
            });
            $(objSwipeElement.element).find('[id=questionanswerInnerContent]').unbind("swiperight").bind("swiperight", function() {
                $("#prevQuestionArrow").trigger('click', -1);
            });
        },

        resetQuestionInnerContent : function() {
            $(this.element).find('[id=theQuestionAsked]').html('');
            $(this.element).find('[id=QAfreeTextImage]').remove();
            $(this.element).find('[id=questionPanelOptions]').html('');
            $(this.element).find('[id=questionImage]').css("display", "none");
        },

        openQuestionPanelSection: function (annotationPanelComp) {
            var objThis = this;
            $(annotationPanelComp.element).find('[id=annotationAllTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
            $(annotationPanelComp.element).find('[id=annotationMyNotesTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
            $(annotationPanelComp.element).find('[id=annotationTeacherNotesTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
            $(annotationPanelComp.element).find('[id=annotationMyQuestionsTab]').addClass('leftPanelTabActive').removeClass('leftPanelTab');            
            
            $(annotationPanelComp.element).find('[id=addedStickyNotesList]').css('display', 'none');
            $(annotationPanelComp.element).find('[id=annotationSortBy]').css('display', 'none');
            $(annotationPanelComp.element).find('[id=externalURLLink]').css('display', 'none');
            $(annotationPanelComp.element).find('[id=addedTeacherNotesList]').css('display', 'none');
            $(annotationPanelComp.element).find('[id=teacherAnnotationSortBy]').css('display', 'none');
            $(annotationPanelComp.element).find('[id=addedQuestionsList]').css('display', 'block');
            if(isQAPanelDataLoadComplete == false)
            {
            	$(annotationPanelComp.element).find('[id=addedQuestions]').html('<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>');
            }
            objThis.createQuestionPanelList(annotationPanelComp);
        },
        
        getQuestionPanelVisibility: function(annotationPanelComp)
        {
        	return ($(annotationPanelComp.element).find('[id=addedQuestionsList]').css('display') == "block") ? true : false;
        },

        updateQuestionPanelList : function(questionClicked) {
            var objThis = this;
            var arrObj = new Object();
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
            arrObj.curDate = dateOfCreation;
            arrObj.curQuesClicked = $(questionClicked).attr('id');
            arrObj.id = $(questionClicked).attr('id');
            arrObj.curPageNumber = $(objThis.element).find('[id="questionPanelPageNumber"]').html();
            var questionListLength = objThis.options.questionPanelList.length;
            //arrObj.pageNumber = "Page "+$(objThis.element).find('[id=questionPanelPageNumber]');
            var flag = false;
            for (var i = 0; i < questionListLength; i++) {
                if (objThis.options.questionPanelList) {
                    if (objThis.options.questionPanelList[i].id == arrObj.id) {
                        objThis.options.questionPanelList[i].curDate = arrObj.curDate;
                        flag = true;
                    }
                }
            }

            if (flag == false) {
                objThis.options.questionPanelList.push(arrObj);
            }
        },

        createQuestionPanelList : function(annotationPanelComp, isDataLoadComplete) {
            var objThis = this;
            todaysDate = getTodaysDate();
            var questionListData = "";
            var arrData = new Array();
            for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                if (GlobalModel.questionPanelAnswersForAnnoPanel[i]) {
                    var dayData = "";
                    var questionListId = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                    if (GlobalModel.questionPanelAnswersForAnnoPanel[i].creationDate == todaysDate) {
                        dayData = GlobalModel.localizationData["DATE_TODAY"];
                    } else {
                        dayData = GlobalModel.questionPanelAnswersForAnnoPanel[i].creationDate;
                    }
                    if(arrData.indexOf(questionListId) == -1) {
                              arrData.push(questionListId);
                              questionListData += "<div questionRef='" + questionListId + "' id='bookmarkData_" + questionListId + "' class='panelData stickynoteData' type='stickynoteData' style='text-shadow: none;'><div class='annotationData'><span id='stickynoteDataDayData' class='dayData'>" + dayData + "</span><div class='questionListIcon'></div><span id='stickynoteDataTitle' class='dataTitle'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + GlobalModel.questionPanelAnswersForAnnoPanel[i].pageNumber + "</span></div></div>"
                    }
                }
            }
            //console.log("isDataLoadComplete ",isDataLoadComplete);
			if(questionListData != ""){
				$(annotationPanelComp.element).find('[id=addedQuestions]').html(questionListData);
				$($(annotationPanelComp.element).find('[id=blankQuestionAnswerPanelText]')[0]).css("display", "none");
			}
			else if(isDataLoadComplete == true || isQAPanelDataLoadComplete == true){
				$($(annotationPanelComp.element).find('[id=blankQuestionAnswerPanelText]')[0]).css("display", "block");
				$(annotationPanelComp.element).find('[id=addedQuestions]').html("");
			}
            
            $(annotationPanelComp).trigger('questionPanelOpened');
            if(qaScrollHigher == null) {
                    setTimeout(function() {
                                qaScrollHigher = new iScroll('addedQuestionsList', {
                                        scrollbarClass: 'myScrollbar'
                                    });
                                    objThis.updateViewSize();
                      },500);
             } else {
                       objThis.updateViewSize();
             }
            
        },

        updateQuestionAttemptedIndicator : function(grpRef) {
        	var objThis = this;
            var numberOfgroups = grpRef.length;
            for (var k = 0; k < numberOfgroups; k++) {
                var currentGrp = $(grpRef)[k];
                var noOfQuestions = $(currentGrp).find('[type="widget:quiz"]').length;
                var grpId = $(currentGrp).attr('interactiongroupid');
                var tempQuesAttempted = 0;
                if (noOfQuestions > 0) {
                    if (GlobalModel.questionPanelAnswers[grpId] != undefined) {
                        for (var i = 0; i < noOfQuestions; i++) {
                            var curQuesId = $($(currentGrp).find('[type="widget:quiz"]')[i]).attr('interactionid');
                            if ( GlobalModel.questionPanelAnswers[grpId][curQuesId] && GlobalModel.questionPanelAnswers[grpId][objThis.member.currentQuestion])
                            {
                            	if(GlobalModel.questionPanelAnswers[grpId][objThis.member.currentQuestion].savetonote == 1){
                            	objThis.member.isSAveToNoteBookChecked[grpId] = true;
	                            }
	                            
	                            if (GlobalModel.questionPanelAnswers[grpId][curQuesId].flag_id == 1) {
	                                tempQuesAttempted = tempQuesAttempted + 1;
	                            }
                            }
                            
                        }
                    }
                }
                
                if (tempQuesAttempted < noOfQuestions) {
                    if (tempQuesAttempted != 0) {
                        $(currentGrp).find('[type="widget:quizStatus"]').addClass('incompleteQuestionStatus').removeClass('completedQuestionStatus');
                    } else {
                        $(currentGrp).find('[type="widget:quizStatus"]').removeClass('incompleteQuestionStatus').removeClass('completedQuestionStatus');
                    }
                } else {
                    $(currentGrp).find('[type="widget:quizStatus"]').removeClass('incompleteQuestionStatus').addClass('completedQuestionStatus');
                }
            }
            setTimeout( function (){
            	objThis.setSaveToNoteBook();
            } , 0)
        },

        updateQAPanelDotsOnPageLoad : function(grpData) {
            var objThis = this;
            //console.log("grpData: ",grpData);
            var grpInteractionLength = grpData.interaction.interaction.length;
            //console.log(grpInteractionLength);
            var currQuesID = null;
            var currQuesType = null;
            var currQuesURL = null;
            for (var i = 0; i < grpInteractionLength; i++) {
                //console.log($("[type=PageContainer]").find('[interactionid = "'+ currQuesId +'"]').parent(),$("#" + currQuesId).parent());
                currQuesID = grpData.interaction.interaction[i].id;
                currQuesURL = grpData.interaction.interaction[i].url;
                currQuesType = grpData.interaction.interaction[i].type;
                var interactivityType = null;
                var curElem = $("[type=PageContainer]").find('[interactionid = "' + currQuesID + '"]');
                var indexElem = $(curElem).parent().children().index($(curElem));
                var grpId = $(curElem).parent().attr('interactiongroupid');
                if (GlobalModel.questionPanelAnswers[grpId] != undefined) {
                    switch (currQuesType) {
                        //
                        case 'singleChoice':
                            interactivityType = 'singleChoiceInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                //alert(objData.response);
                                if (objData) {
                                    var userResponse = parseInt($(objData.response).find("response").html());
                                    if (userResponse != null && userResponse != -1) {($($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle'));
                                    }
                                }
                            }
                            //
                            break;
                        case 'freeText':
                            interactivityType = 'freeTextInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                if (objData) {
                                    var userResponse = $(objData.response).find("response").html();
                                    if (userResponse != null && userResponse != "") {
                                        $($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                                    }
                                }
                            }
                            break;
                        case 'freeTextWithImage':
                            interactivityType = 'freeTextWithImageInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                if (objData) {
                                    var userResponse = $(objData.response).find("response").html();
                                    if (userResponse != null && userResponse != "") {
                                        $($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                                    }
                                }
                            }
                            break;
                        case 'fillUps':
                            interactivityType = 'fillUpsInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                if (objData) {
                                    var userResponse = $(objData.response).find("response").html().split(",");
                                    var flag = -1;
                                    for (var j = 0; j < userResponse.length; j++) {
                                        if (userResponse[j] != "") {
                                            flag = 0;
                                            break;
                                        }
                                    }
                                    if (flag != -1) {
                                        $($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                                    }
                                }
                            }
                            break;
                        case 'multipleChoice':
                            interactivityType = 'multipleChoiceInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                if (objData) {
                                    var userResponse = $(objData.response).find("response").html().split(",");

                                    var validResponse = false;
                                    for (var j = 0; j < userResponse.length; j++) {
                                        if (parseInt(userResponse[j])) {
                                            validResponse = true;
                                            break;
                                        }
                                    }
                                    // If any option is selected, userresponse should not be null, -1, or NAN.
                                    if (userResponse != null && userResponse != -1 && userResponse != '' && userResponse && validResponse) {
                                        $($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                                    }
                                }
                            }
                            break;

                        case 'tables':
                            interactivityType = 'tableInteractivity'
                            if (GlobalModel.questionPanelAnswers[grpId]) {
                                var objData = GlobalModel.questionPanelAnswers[grpId][currQuesID];
                                if (objData) {
                                    var userResponse = $(objData.response).find("response").html().split(",");
                                    var flag = -1;
                                    for (var j = 0; j < userResponse.length; j++) {
                                        if (userResponse[j] != "") {
                                            flag = 0;
                                            break;
                                        }
                                    }
                                    if (flag != -1) {
                                        $($(objThis.element).find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                                    }
                                }
                            }
                            //
                            break;
                    }
                }
            }
        },
        
        updateViewSize: function(){
            var objThis = this;
            setTimeout(function(){
                var headerHeight = ($("#header").css('display') == 'block') ? $("#header").height() : 1;
                var bookmarkSectionPanelTop = parseInt($("#bookmarkSectionPanelTop").height());
               $("#addedQuestionsList").height(window.innerHeight - headerHeight - $("#StickyNotesSectionPanelTop").height());
               //$("#addedBookmarkslist").height($($.find("[id=BookMarkSectionPanel]")).height() - (bookmarkSectionPanelTop + headerHeight));
               if(qaScrollHigher != null) {
                    qaScrollHigher.refresh();
                }
                //adjust the width of the bookmarks list if scrollbar is visible
                if (ismobile == null) {
                    //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                    iScrollClickHandler($("#addedQuestionsList").find("[class=myScrollbarV]"), qaScrollHigher, "addedQuestionsList");
                    if ($(objThis.element).find("[class=myScrollbarV]").html()) 
                        $($(objThis.element).find("[id=addedQuestions]")[0]).width($($(objThis.element).find("[id=addedQuestionsList]")[0]).width() - 14);
                    else 
                        $($(objThis.element).find("[id=addedQuestions]")[0]).width($($(objThis.element).find("[id=addedQuestionsList]")[0]).width());
                }
            }, 500);
            
        }
    });
})(jQuery);
