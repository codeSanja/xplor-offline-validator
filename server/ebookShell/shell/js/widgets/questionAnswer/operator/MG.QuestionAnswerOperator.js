/**
 * @author Lakshay Ahuja
 */
var isQAPanelDataLoadComplete = false;
MG.QuestionAnswerOperator = function() {
    //this.objOperatorManager = null;
    this.superClass = MG.BaseOperator.prototype;
    this.qaBtnRefrence = null;
    this.groupXMLName = null;
    this.curQuesClicked = null;
    this.grpRefrence = [];
    this.questionPanelRef = null;
    this.openQuestionPanelData = null;
    this.imageBoxContracted = true;
    this.stickynoteSectionPanel = null;
    this.currentGroupQuestionsArray = null;
    this.dataGalleryArray = null;
}

MG.QuestionAnswerOperator.prototype = new MG.BaseOperator();

MG.QuestionAnswerOperator.prototype.attachComponent = function(objComp) {
    if (objComp == null)
        return;

    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var objThis = this;
    switch (strType) {

        case AppConst.NOTE_SECTION_PANEL:
            objThis.stickynoteSectionPanel = objComp;
            // Selecting the Question Tab
            $($(objComp.element).find('[id="annotationMyQuestionsTab"]')).unbind('click').bind('click', function() {
                //annotationAllTab
                if ($("#annotationMyQuestionsTab span").css("visibility") == 'hidden') {
                    return;
                }
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'openQuestionPanelSection', objComp);
                $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "none");
                $($(objComp.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");
                if ($.trim($(objComp.element).find('[id=addedQuestions]').html()) == "")
                    $($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "block");
                else
                    $($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");
                
                objThis.removeAllNotificationsForQuestionTab();
                GlobalModel.updateTeacherNotificationIcon( 'teacherNoteAlert' );

            });

            $(objComp).unbind("removePreviouslySetListeners").bind("removePreviouslySetListeners", function() {
                $(objComp.element).find('[id=addedQuestions]').find('[class="panelData stickynoteData"]').unbind('click');
            });

            $(objComp).unbind('questionPanelOpened').bind('questionPanelOpened', function() {
                $(objComp.element).find('[id=addedQuestions]').find('[class="panelData stickynoteData"]').unbind('click').bind('click', function() {
                    var questionId = $(this).attr('questionRef');

                    $("#stickyNoteLauncher").trigger('click');

                    var objQuestionElement = $("#innerScrollContainer").find('[interactiongroupid='+questionId+']').find('[type="widget:quiz"]')[0];
                    if (objQuestionElement) {
                        $(objQuestionElement).trigger('click', true);
                    } else {
                        var page = $(this).find(".dataTitle").html().replace("Page", "");
                        page = $.trim(page.replace('<span class="questionPanelPageSpan"></span> ', ""));
                        objThis.openQuestionPanelData = {
                            interactionid : questionId
                        };
                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', GlobalModel.pageBrkValueArr.indexOf(page));
                    }
                    //console.log(this);
                });
            });

            break;

        case AppConst.QUES_ANS_PANEL:
        
            var imgQuestionWait = $("#imgQuesWait")[0];
            if (imgQuestionWait) {
                imgQuestionWait.src = preloaderPath;
            }
            
            if( ismobile || isWinRT )
            {
            	$("#questionPanelPrintIcon").remove();
            }

            //strReaderPath + "css/6TO12/images/main/loading_icon.gif";

            objThis.questionPanelRef = objComp;
            $(document).bind('qaPanelDataLoadComplete', function() {
            	
                isQAPanelDataLoadComplete = true;
                if (objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'getQuestionPanelVisibility', objThis.stickynoteSectionPanel)) {
                    objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'createQuestionPanelList', objThis.stickynoteSectionPanel, true);
                }
                GlobalModel.isAnnotationDataLoaded = true;
                $(objComp.element).find('[id=questionPanelOptions]').removeClass('ui-disabled');
                $(objComp.element).find('[id=questionPanelQuestionNavigator]').removeClass('ui-disabled');
                try {
                    $("#prevQuestionArrow").data('buttonComp').enable();
                    $("#nextQuestionArrow").data('buttonComp').enable();
                    if (ismobile && navigator.userAgent.match(/(android)/i)) {
                        $("#questionPanelPrintIcon").data('buttonComp').disable();
                        $("#questionPanelPrintIcon").addClass('ui-disabled');
                    } else {
                        $("#questionPanelPrintIcon").data('buttonComp').enable();
                    }
                  
                    if (isNative || isNativePublisher) {
                    	 $("#questionPanelPrintIcon").css("visibility","hidden");
                    }
                } catch(e) {
                }

                $("#prevQuestionArrow").removeClass('navigatorQAPanelDisable');
                $("#nextQuestionArrow").removeClass('navigatorQAPanelDisable');
                $('.questionPanelquesDots').each(function() {
                    try {
                        $(this).removeClass('ui-disabled');
                        $(this).removeClass('navigatorQAPanelDisable');
                        $(this).data('buttonComp').enable();
                    } catch(e) {

                    }

                });

                // $('.questionPanelquesDots').data('buttonComp').enable();
            });
            try {
                $("#prevQuestionArrow").data('buttonComp').disable();
                $("#nextQuestionArrow").data('buttonComp').disable();
                $("#questionPanelPrintIcon").data('buttonComp').disable();
            } catch(e) {

            }

            $(objComp.element).unbind('click').bind('click', function() {
                $("#annotation-bar").css('display', 'none');
            });

            $(objComp.element).find('[id="imageGalleryContractBtn"]').unbind('click').bind('click', function() {
                var collapseBtnRef = this;
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'collapseImageBox', this);
                objThis.imageBoxContracted = true;
            });

            $(document).unbind('imageBoxExpanded').bind('imageBoxExpanded', function() {
                if (objThis.imageBoxContracted == true) {
                    objThis.resizeImagePanel();

                    objThis.imageBoxContracted = false;
                }
                $("#questionAnswerPanel #imageGalleryContractBtn").css("display", "block");
            });

            $(window).bind('resize', function(e) {
            	if( $( "#questionAnswerPanel" ).css( "display" ) == "block" )
            	{
            		setTimeout(function() {
                    	objThis.resizeImagePanel();
                    	objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetImagePos');
                	}, 700);	
            	}
            });

            $(window).bind('orientationchange', function() {
                if ($(objComp.element).css('display') == "block") {
                    setTimeout(function() {
                        objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetImagePos');
                        var imgGalleryHeight = $(objComp.element).height() - $(objComp.element).find("#imageGalleryImageContainer").height();
                        $(objComp.element).find("#imageGalleryImageContainer").height(window.height - imgGalleryHeight);
                        //slideThumbnailPanel
                        setTimeout(function() {
                            var nLeftPos = parseInt($(objThis.imageGalleryRef.element).find("[id=imageOuterContainer]").css('left'));
                            nLeftPos = objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'enableDisableNavBtns', nLeftPos);
                            $(objThis.imageGalleryRef.element).find("[id=imageOuterContainer]").css('left', nLeftPos);
                            objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'slideThumbnailPanel');
                        }, 500);
                    }, 500);
                }
            });

            $(objComp.element).unbind('resized').bind('resized', function() {
                objThis.resizeImagePanel();
            });

            $(objComp.element).unbind('saveCurrentResponse').bind('saveCurrentResponse', function() {
                //console.log('saveCurrentResponse');
                //objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT,'saveCurrentResponse');
            });

            $(objComp.element).unbind('PanelClosed PanelMinimized').bind('PanelClosed PanelMinimized', function(e) {
                objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'saveCurrentResponse');
            });

            $(objComp.element).unbind('recentResponseSaved').bind('recentResponseSaved', function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'setQuestionContent');
            });

            // Closing the Question Answer Panel
            $(objComp.element).find('[id=questionPanelCloseBtn]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'closeDisplayQuesAnsPanel', false);
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'createQuestionPanelList', objThis.stickynoteSectionPanel, true);
            });

            //Minimizing the panel
            $(objComp.element).find('[id=questionPanelMinimizeIcon]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'minimizeMaximizeQAPanel');
            });

            $(objComp.element).find('[id=numberOfQuestionDots]').unbind('questiondotClicked').bind('questiondotClicked', function(e, objQuesDot) {//questiondotClicked
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'handleQuesDotsClick', objQuesDot);
            });

            $(objComp.element).find('[id=prevQuestionArrow]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'handleQAPanelNavigationArrows', -1,objThis.currentGroupQuestionsArray);
            });

            $(objComp.element).find('[id=nextQuestionArrow]').unbind('click').bind('click', function() {
                
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'handleQAPanelNavigationArrows', 1,objThis.currentGroupQuestionsArray);
            });

            $(objComp.element).find('[id=questionPanelPrintIcon]').unbind('click').bind('click', function() {
                var pgSeq = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
                var pageName = GlobalModel.pageBrkValueArr[pgSeq];
                objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'saveCurrentResponse');

                //var objPathManager = getPathManager();
                // var strUri = objPathManager.getIntractivitiesXMLBasePath();
                // var strWindowPath = window.location.pathname.replace("/index.html", "");

                // localStorage.setItem('basePath', window.location.protocol + "//" + window.location.host + "/" + strWindowPath + "/" + strUri);
                //localStorage.setItem('groupXMLName',objThis.groupXMLName);
               
                    objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'handleQAPanelPrintClick');
            });

            $(objComp.element).find('[id=questionPanelSaveButton]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'saveCurrentResponseToServices');
            });
            $(objComp.element).find('[id=quesImageExpandCollapse]').unbind('click').bind('click', function() {
                var minimizeMaximizeBtn = this;
                var isComponentVisible = false;
                var flag = true;
                var mdPageX = 0;
                var mdPageY = 0;
                
                if ($(this).hasClass('quesImageExpand')) {
                    //$(this).removeClass('quesImageExpand').addClass('quesImageMinimize');
					if( $("#qaImageClone").length > 0 )
					{
						$("#qaImageClone").remove();
					}
                    var qaImageClone = $(this).parent().clone();
                    $(qaImageClone).attr('id', 'qaImageClone');
                    $(qaImageClone).appendTo($("#mainShowroom"));
                    $(qaImageClone).css({
                        'width' : 'auto',
                        'height' : 'auto',
                        'position' : 'absolute'
                    });
                    $(qaImageClone).find('.quesImageMinimize').css('position', 'absolute');
                    $(qaImageClone).css( "display", "block" );
                    $(qaImageClone).css( "z-index", 5000 );
                    $(qaImageClone).css( "top", window.height/2 - $(qaImageClone).height()/2 );
                    $(qaImageClone).css( "left", window.width/2 - $(qaImageClone).width()/2 );
                    var qaImageDiv = $(qaImageClone);
                    $(qaImageClone).bind( "mousedown touchstart", function ( event )
                    {
                        event.preventDefault();
                        event.stopPropagation();
                        isComponentVisible = true;
                        if( ismobile && event.originalEvent.touches.length > 1 )
                        {
                            return;
                        }
                        mdPageX = ismobile ? event.originalEvent.touches[0].pageX : event.pageX;
                        mdPageY = ismobile ? event.originalEvent.touches[0].pageY : event.pageY;
                        if( flag )
                        {
                            $(document).bind( "mousemove touchmove", function ( event1 )
                            {
                                flag = false;
                                if( ( ismobile && event1.originalEvent.touches.length > 1 ) || ( !isComponentVisible ) )
                                {
                                    return;
                                }
                                //console.log( flag, "FLag3" );
                                var mmPageX = ismobile ? event1.originalEvent.touches[0].pageX : event1.pageX;
                                var mmPageY = ismobile ? event1.originalEvent.touches[0].pageY : event1.pageY;
                                qaImageDiv.css( "left", parseInt( qaImageDiv.css( "left") ) + ( mmPageX - mdPageX ) );
                                qaImageDiv.css( "top", parseInt( qaImageDiv.css( "top") ) + ( mmPageY - mdPageY ) );
                                mdPageX = mmPageX;
                                mdPageY = mmPageY;
                            } );
                        }
                        
                    } );
                    
                   /* $( window ).bind( "resize", function ()
                    {
                        setTimeout( function (){
                            $(qaImageClone).css( "top", window.height/2 - $(qaImageClone).height()/2 );
                            $(qaImageClone).css( "left", window.width/2 - $(qaImageClone).width()/2 );
                        }, 0 );
                    } );
                    
                     $( window ).bind( "orientationchange", function ()
                    {
                        setTimeout( function (){
                            $(qaImageClone).css( "top", window.height/2 - $(qaImageClone).height()/2 );
                            $(qaImageClone).css( "left", window.width/2 - $(qaImageClone).width()/2 );
                        }, 500 );
                    } ); */
                                        
                    $(qaImageClone).bind( "mouseup touchend", function ( event2 )
                    {
                        isComponentVisible = false;
                    } );
                    
                    $(document).bind( "mouseup touchend", function ( event2 )
                    {
                         isComponentVisible = false;
                        //$(document).unbind( "mousemove" );
                        //$(document).unbind( "touchmove" );
                    } );
                    
                   /* $(document).bind( "mouseleave touchout", function ( event2 )
                    {
                        $(qaImageClone).unbind( "mousemove" );
                        $(qaImageClone).unbind( "touchmove" );
                    } ); */
                    
                    $(qaImageClone).find('[class=quesImageExpand]').unbind('click').bind('click', function() {
                        isComponentVisible = false;
                        //$(minimizeMaximizeBtn).addClass('quesImageExpand').removeClass('quesImageMinimize');
                                      
                        PopupManager.removePopup();
                        $("#qaImageClone").remove();
                      //  $(minimizeMaximizeBtn).parent().show();
                    });
                }
                //window.open($($(this).parent()).find('[id=qaInnerImage]').attr('src'), "_blank");
            });

            $(objComp.element).find('[id=checkAnswer]').unbind('click').bind('click', function() {
                var objChkAnsBtn = this;
                if ($(objChkAnsBtn).hasClass('showQAAnswers')) {
                    objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'displayHideCorrectResponse', 1);
                    $(objChkAnsBtn).removeClass('showQAAnswers').addClass('hideQAAnswers');
                    $(objChkAnsBtn).html('Clear Answer');
                } else {
                    objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'displayHideCorrectResponse', -1);
                    $(objChkAnsBtn).addClass('showQAAnswers').removeClass('hideQAAnswers');
                    $(objChkAnsBtn).html('Check Answer');
                }

            });

            $("#questionAnswerPanel .savetoNoteBookBtn").unbind("click").bind("click", function() {
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'handleSaveToNotebookClick');
            });
            

            break;

        case AppConst.IMAGE_GALLERY_PANEL:
            objThis.imageGalleryRef = objComp;
            $(objComp.element).find('[type="imageGalleryClsBtn"]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'hideImageGalleryPanel');
            });

            $(objComp.element).find('[type="imageInnerContainer"]').each(function() {
                $(this).unbind('click').bind('click', function() {
                    objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'expandImageBox', this);
                });
            });

            $(objComp.element).find('[id=iGZoomInBtn]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'zoomInOutInnerPanelImage', 1);
            });

            $(objComp.element).find('[id=iGZoomOutBtn]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'zoomInOutInnerPanelImage', -1);
            });

            $(objComp.element).find('[id="imageGalleryContractBtn"]').unbind('click').bind('click', function() {
                var collapseBtnRef = this;
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'collapseImageBox', this);
            });

            $(objComp.element).find('[id="nextExpandedImg"]').unbind('click').bind('click', function() {
                objComp.showExpandedImage(1);
                //objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL,'showExpandedImage',1);
            });

            $(objComp.element).find('[id="prevExpandedImg"]').unbind('click').bind('click', function() {
                objComp.showExpandedImage(-1);
                //objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL,'showExpandedImage',-1);
            });

            var totalImages = $('.imageInnerContainer').length;
            var imageContainerW = ($('.imageInnerContainer').width() + parseInt($('.imageInnerContainer').css('margin-left')) + parseInt($('.imageInnerContainer').css('margin-right'))) * totalImages;
            $("#imageOuterContainer").width(imageContainerW);
            var moveLeftBy = (imageContainerW / totalImages) * 3;
            var imgPrevComp = $(objComp.element).find('[type="imageGalleryPrev"]').data('buttonComp');
            var imgNextComp = $(objComp.element).find('[type="imageGalleryNext"]').data('buttonComp');
            imgPrevComp.disable();

            $(objComp.element).find('[type="imageGalleryPrev"]').unbind(imgPrevComp.events.CLICK).bind(imgPrevComp.events.CLICK, function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'navigateToPrevImages');
            });

            $(objComp.element).find('[type="imageGalleryNext"]').unbind(imgNextComp.events.CLICK).bind(imgNextComp.events.CLICK, function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'navigateToNextImages');
            });

            break;

        case AppConst.QUES_ANS_INNER_CONTENT:

            $(objComp).bind('questionDotsLoaded', function(e, grpData) {
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'updateQAPanelDotsOnPageLoad', grpData);
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'updateQuestionAttemptedIndicator', objThis.grpRefrence	);
            });
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function() {
                /**
                 * This function is called when the pages are rendered.
                 */
                objThis.scrollViewRenderCompleteHandler(objComp);
            });
            
            $(document).unbind('newQuestionClicked').bind('newQuestionClicked',function(e,arrObj){
                objThis.questionClickHandler(null,arrObj);
            });
            
            break;

        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            break;
    }

    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.QuestionAnswerOperator.prototype.removeAllNotificationsForQuestionTab = function ()
{
	for( var question in GlobalModel.questionPanelAnswers )
	{
        for(var annotations in GlobalModel.questionPanelAnswers[question] )
        {
        	GlobalModel.questionPanelAnswers[question][annotations].isNotified = false;
        }
	}
	GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
}

MG.QuestionAnswerOperator.prototype.resizeImagePanel = function() {
    var _totalHeights = $("#questionAnswerPanel #questionanswerFooter").height() + $("#questionAnswerPanel #questionanswerInnerContent").innerHeight() + parseInt($("#questionAnswerPanel #questionanswerInnerContent").css("padding-bottom")) + parseInt($("#questionAnswerPanel #questionanswerInnerContent").css("padding-top")) + $($("#questionAnswerPanel .imageInnerContainer")[0]).height() + $("#questionAnswerPanel #questionanswerHeader").height() + parseInt($("#questionAnswerPanel").css("bottom"));
    var _val = window.height - _totalHeights;
    $("#questionAnswerPanel #imageGalleryImageContainer").css("height", _val + "px");
}

MG.QuestionAnswerOperator.prototype.scrollViewRenderCompleteHandler = function(objComp) {
    var objThis = this;
    var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
    var objPageRole = null;
    for (var i = 0; i < arrPageComps.length; i++) {
        objPageRole = $(arrPageComps[i]).data("pagecomp");
        try {
            /**
             * On page load complete, this function will bind click event with every question.
             */
            $(objPageRole).bind(objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function(e) {

                /**
                 * Every group of questions on a particular page is defined inside a section tag, which has a property: interactiongroupid.
                 */
                if ( ( $(e.currentTarget.element).find('[type="widget:quiz"]')[0] != undefined ) ) {
                    var grpRef = $($(e.currentTarget.element).find('section'));
                    for (var i = 0; i < grpRef.length; i++) {
                        if ($(grpRef[i]).attr('interactiongroupid')) {
                            objThis.grpRefrence.push((grpRef[i]));

                        }
                    }
                    /**
                     * objThis.grpRefrence contains the  total number of groups(which have questions) on the loaded page.
                     * objThis.qaBtnRefrence refers to an array of questions, on which the click event is binded.
                     */
                    objThis.qaBtnRefrence = $(objThis.grpRefrence).find('[type="widget:quiz"]');
                    var _page = $(e.currentTarget.element);
                } else {
                    /**
                     * There is no question on the page. So return;
                     */
                    return;
                }

                /**
                 *
                 *  This binds the click of questions on the page
                 */
                $(objThis.qaBtnRefrence).unbind('click').bind('click', function(e, thoughPanel) {
                	$("#questionanswerFooter").css("display", "block");
                    var curRef = this;
                    var objThisParent = $(this).parent();
                    var arrObj = new Object();
                    arrObj.id = $(objThisParent).attr('interactiongroupid');
                    if(!$(this).parent().attr('id')){
                        $(this).parent().attr('id',arrObj.id);
                    }
                    
                	objThis.dataGalleryArray = [];
                    $(this).parent().find('[type="widget:quiz"]').each(function(){
                   		objThis.dataGalleryArray.push($(this).attr("data-galleryid"));
                        $(this).attr('id', $(this).attr('interactionid'));
                    });
                        
                    
                    arrObj.childClicked = $(this).attr('id');
                    objThis.groupXMLName = arrObj.id;
                    
                    /**
                     * arrObj.childClicked
                     * arrObj.id
                     */
                    
                    
                    
                    /**
                     * This will set the current page index. and will jump to the page if group is accessed through question panel.
                     */
                    var pageindex = objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'currentQuestionPageNumber', curRef);
                    if (thoughPanel == true) {
                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageindex);
                    }
                    /**
                     * This will display the QA Panel
                     */
                    objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'closeDisplayQuesAnsPanel', true);
                    /**
                     * This function will handle the process after the question has been clicked.
                     */
                    objThis.questionClickHandler(objComp, arrObj);

                });

                /**
                 * This binds the click of question indicator on the page.
                 */
                $($(objThis.grpRefrence).find('[type="widget:quizStatus"]')).unbind('click').bind('click', function() {
                    $($(this).parent().find('[type="widget:quiz"]')[0]).trigger('click');
                });
                
                /**
                 * When question is clicked in question panel,  objThis.openQuestionPanelData is set to true.
                 * When page renders, this statement is called. InnerScrollContainer searches the first question of the group and triggers click of question.
                 */
        setTimeout(function() {
                if (objThis.openQuestionPanelData) {
                    $($("#innerScrollContainer").find('[interactiongroupid=' + objThis.openQuestionPanelData.interactionid + ']').find('[type="widget:quiz"]')[0]).trigger("click");
                    objThis.openQuestionPanelData = null;
                }
             }, 500);
                // updates the indicator when page is loaded.
                /*setTimeout(function() {
                    objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'updateQuestionAttemptedIndicator', objThis.grpRefrence);
                }, 1500);*/

            });
        } catch(e) {

        }

    }

}

MG.QuestionAnswerOperator.prototype.questionClickHandler = function(objComp, arrObj) {
    var objThis = this;
    var curRef = arrObj.childClicked;
    var flag = false;
    if(GlobalModel.hasDPError("qa"))
        return;
    if( $("#questionAnswerPanel").css("display") == "none" )
    {
    	flag = true;
    }
    objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'loadGroupXML', arrObj);
    if( flag )
    {
    	//objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'resetCommentSection');
    	$("#qaCommentSaveBtn").attr( "disabled", true );
    	$("#qaCommentSaveBtn").addClass( "ui-disabled" ).attr( "aria-disabled", true );
    	objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'addCommentInCommentBox');
    }
    flag = false;
    // When group XML is loaded. Questions Array is created
    $(document).unbind('grpXMLLoadComplete').bind('grpXMLLoadComplete', function() {
        objThis.currentGroupQuestionsArray = objThis.doFunctionCall(AppConst.QUES_ANS_INNER_CONTENT, 'getCurrentGroupQuestionsArray');
        objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'setCurrentQuestionGrpAndArray', arrObj.id,objThis.currentGroupQuestionsArray);
    });

    //These functionalities are processed when question is loaded.
    $(document).unbind('XMLLoadComplete').bind('XMLLoadComplete', function() {
    	//console.log("arrObj",arrObj)
        var currentQuestionAccessed = arrObj.childClicked;
        var indexElem = objThis.currentGroupQuestionsArray.indexOf(currentQuestionAccessed);
        $("#questionPanelPrintIcon").css("display", "block");
		$("#questionPanelMinimizeIcon").css("display", "block");
        /**** IMAGE GALLERY *****/
       //console.log("currentQuestionAccessed ",currentQuestionAccessed,$("#"+currentQuestionAccessed).attr("data-galleryid"));
        //console.log( $(objThis.qaBtnRefrence), $($(objThis.qaBtnRefrence)[indexElem]), indexElem, "$(objThis.qaBtnRefrence)" );
        if (($(objThis.qaBtnRefrence).length > 0 ) && (objThis.dataGalleryArray[indexElem] != undefined )) {
            $("#questionAnswerPanel #imageGalleryThumbnailContainer").css("display", "block");
            $("#questionAnswerPanel #imageGalleryContractBtn").css("display", "none");
            //show image gallery
            objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'showImageGalleryPanel', objThis.dataGalleryArray[indexElem], objThis.questionPanelRef);
            setTimeout(function() {
                objThis.resizeImagePanel();
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetImagePos');
            }, 800);
        } else {
            $("#questionAnswerPanel #imageGalleryThumbnailContainer").css("display", "none");
            $("#questionAnswerPanel #imageGalleryImageContainer").css("display", "none");
            $("#questionAnswerPanel #imageGalleryContractBtn").css("display", "none");
        }
        
        /**** IMAGE GALLERY *****/
       

    });
    objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'updateQuestionPanelDotsStatus', curRef);
}
