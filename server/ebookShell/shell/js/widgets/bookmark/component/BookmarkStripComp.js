( function ( $, undefined )
{
    $.widget( "magic.BookmarkStripComp", $.magic.buttonComp, {
        events: 
        {
            CLICK: 'click',
        },
        options: 
        {
            setEnabled: true
        },
        
        _create: function()
        {
        	var objClass = this;
            $.extend( this.options, this.element.jqmData( 'options' ) );
            this.bookMarkStripElem = null;

            if ( this.options.setEnabled == undefined ) 
            {
            	this.options.setEnabled = true;
            }

            if (this.options.setEnabled == true) {
            
                if ( this.element.attr( "type" ) == 'addRemoveBookmark' ) 
                {
                    this.element.bind( 'click', function( e )
                    {
                        objClass.bookMarkBtnElem = $( this );
                        if ( objClass.options.selected == true ) 
                        {
                            $( this ).attr( 'class', objClass.options.onstyle );
                            objClass.options.selected = false;
                        }
                        else 
                        {
                            $( this ).attr( 'class', objClass.options.offstyle );
                            objClass.bookMarkStripElem = $( objClass.element + '[ type = "addRemoveBookmarkStrip" ]' );
                            objClass.bookMarkStripElem.css( 'display', 'block' );
                        }
                    });
                }
            }
            else 
            {
                this.disable();
            }
        },
        
        _init: function()
        {
            $.magic.magicwidget.prototype._init.call( this );
            this.addListeners();
        },
        
   /*-------------------------------------------------- Add Event Listeners --------------------------------------------------- */ 
    
        addListeners : function ()
        {
        	var objThis = this;
            var _element = $( objThis.element[ 0 ] );       

	       _element.bind( Event.Bookmark.SaveBookmarkForPage , function()
            {
			    $( objThis.element[ 0 ] ).attr( 'class', objThis.options.onstyle );
			    if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
				{     
					$( $( '[ id = "bookContentContainer2" ] ' )[ 0 ] ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );      	
				}
                objThis.options.setEnabled = false;
				objThis.options.selected = true;
            });
            
            _element.bind( Event.Bookmark.DeleteBookmarkForPage, function()
            {
                
				$( objThis.element[ 0 ] ).attr( 'class', objThis.options.offstyle );
                objThis.options.setEnabled = true;
				objThis.options.selected = false;
            });
        },
        
    /*------------------------------------------------------------------------------------------------------------------------------------ */
        
        
        
        
    /*------------------------------------------------------- Enable and Disable ---------------------------------------------------------- */
  
        enable: function ()
        {
            this.element.attr( "disabled", false );
            this.element.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
            return this._setOption( "disabled", false );
        },
        
        disable: function ()
        {
            this.element.attr( "disabled", true );
            this.element.addClass( "ui-disabled" ).attr( "aria-disabled", true );
            return this._setOption( "disabled", true );
        },
        
   /*------------------------------------------------------------------------------------------------------------------------------------ */
      
      
      
      
   /*------------------------------------------------------- Getter and Setters ---------------------------------------------------------- */
    
        setXPosOfStrip: function( updatedLeft )
        {
            this.options.striptLeft = updatedLeft;
        },
        
        getXPosOfStrip: function()
        {
            return this.options.striptLeft;
        },
        
        setSelected: function( blnSelected )
        {
            var thisRef = this;
            this.options.selected = blnSelected;
            if ( blnSelected == true ) 
            {
            
                $( '#btnBookmarkComp' ).attr( 'class', this.options.offstyle );
                $( '#bookStrip' ).css( 'display', 'block' );
            }
            else {
                $( '#btnBookmarkComp' ).attr( 'class', this.options.onstyle );
                $( '#bookStrip' ).css( 'display', 'none' );
            }
            
        },
        
        getSelected: function()
        {
            return this.options.selected;
        }
    
   /*------------------------------------------------------------------------------------------------------------------------------------ */
    
    });
})(jQuery);
