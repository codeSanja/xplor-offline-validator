( function( $, undefined )
{
    $.widget( "magic.bookmarkComp", $.magic.tabViewComp, {
        events: {
            CLICK: 'click',
            ITEM_CLICKED: "item_clicked"
        },
        
        data: {
            type: null
        },
        
        members: {
            sortBy : "pageASC",
            bookScroll : null,
            dropDownOptionsClicked : false,
            bookmarkArr : []
        },
        
        options: {
            setEnabled : true
        },
        
        _create: function()
        {
            $.extend( this.options, this.element.jqmData( 'options' ) );
        },
        
        _init: function()
        {
        	var objThis = this;
        	$.magic.tabViewComp.prototype._init.call( this );

        	/*
        	 * For 2TO5 grade sort the bookmarks in Descending order of Dates
        	 */
        	if( EPubConfig.ReaderType.toUpperCase() != AppConst.GRADE_6TO12 )
        	{
        		this.members.sortBy = "dateDSC";
        	}
        	           
            if ( $( this.element ).attr( "id" ) == "BookmarkPanel" ) 
            {
            	objThis.addEventListeners();
            }
            $(document).bind("updateIScroll",function(){
            	if( $( '#BookMarkSectionPanel' ).css('display') == 'block' && $(HotkeyManager.currentActiveElement).hasClass('bookmarkData') )
            	{
					var Index = $(".focusglow").attr("id");
					if((Number($("#"+Index).offset().top - 127) + parseInt($("#"+Index).css("height"))) > parseInt($("#addedBookmarkslist").height()) || Number($("#"+Index).offset().top - 127)<0)
						objThis.members.bookScroll.scrollToElement("#"+Index,0);
            	}
            });
        },
        
        addEventListeners : function ()
        {
        	var objThis = this;
        	var _element = $( objThis.element );
        	var _dropDownOptions = $( $( objThis.element ).find( "#dd-options" )[ 0 ] );
        	
        /*------------------------------------------------- Panel Opening Event -------------------------------------------------- */
        	
        	_element.unbind( "panelOpened" ).bind( "panelOpened", function()
        	{	
        		objThis.onPanelOpen();
        		HotkeyManager.bookmarkMenuFocusInHandler(); 
        	});
        	
		/*------------------------------------------------------------------------------------------------------------------------ */
			
			
		/*---------------------------------------------------- Resize Event -----------------------------------------------------  */
			
			$( window ).resize( function ()
            {
                objThis.updateViewSize();
            } );
            
        /*------------------------------------------------------------------------------------------------------------------------ */
       
       
        /*-------------------------------------------------- Panel Click Event --------------------------------------------------- */
	       
	       _element.unbind( 'click' ).bind( 'click', function( e )
	       {
                if ( objThis.members.dropDownOptionsClicked == false ) 
                {
                    _dropDownOptions.css( "display", "none" );
                }
                else 
                {
                    objThis.members.dropDownOptionsClicked = false;
                }
                
	        } );
	        
        /*------------------------------------------------------------------------------------------------------------------------ */
       
       
        /*------------------------------------------ Dropdown and its items Click Event ------------------------------------------ */
	       
	       /*
	        * Handles Click on Drop down button
	        */
	       $( _element.find( "#dd-clickable" ) ).unbind( "click" ).bind( "click", function ()
	       {
                objThis.members.dropDownOptionsClicked = true;
                
                /*
                 * If dropdown list is not visible then display it
                 */
                if ( _dropDownOptions.css( "display" ) == "none" ) 
                {
                    _dropDownOptions.css( "display", "block" );
                    
                    /*
                     * handles click of an item on dropdown list.
                     * sorts the bookmark list accordingly and then hide itself.
                     */
                    $( _element.find( "#dd-options li" ) ).unbind( "click" ).bind( "click", function( e )
                    {
                        if ( typeof( e.currentTarget.innerText ) != "undefined" )
                        {
                        	$( _element.find( "#dd-selected" ) ).html( e.currentTarget.innerText );
                        } 
                        else 
                        {
                        	$( _element.find( "#dd-selected" ) ).html( e.currentTarget.textContent );
                        }
                        _dropDownOptions.css( "display", "none" );
                        objThis.members.sortBy = $( $( e.currentTarget ).find( "a" ) ).attr( "value" );
                        objThis.onPanelOpen();
                        
                        var firstElementId = $( $( '.bookmarkData' )[ 0 ] ).attr( 'id' );
                        objThis.members.bookScroll.scrollToElement( "#" + firstElementId, 0 );
                    });
                }
                else // Hide the drop down list
                {
                    _dropDownOptions.css( "display", "none" );
                }
	        });

	    /*------------------------------------------------------------------------------------------------------------------------ */
        },
        
        
        /* Sets the height and width of bookmark list. Refreshes the scroll bar*/
        updateViewSize: function ()
        {
            var objThis = this;
            var _element = $( objThis.element );
            var headerHeight = ( $( "#header" ).css( 'display' ) == 'block' ) ? $( "#header" ).height() : 1;
            
            $( "#addedBookmarkslist" ).height( window.innerHeight - headerHeight - $( "#bookmarkSectionPanelTop" ).height() + 7 );
            if( objThis.members.bookScroll )
            {
            	objThis.members.bookScroll.refresh();
            }
            /*
             * adjust the width of the bookmarks list if scrollbar is visible
             */ 
            if ( ismobile == null ) 
            {
                //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                iScrollClickHandler( _element.find( "[ class = myScrollbarV ]" ), objThis.members.bookScroll, "addedBookmarkslist" );
                
                if ( _element.find( "[ class = myScrollbarV ]" ).html() )
                {
                	$( _element.find( "[ id = addedBookmarks ]" )[ 0 ]).width( 
                    	$( _element.find( "[ id = addedBookmarkslist ]" )[ 0 ] ).width() - 16 );
                } 
                else 
                {
                	$( _element.find( "[ id = addedBookmarks ]" )[ 0 ]).width( 
                    	$( _element.find( "[ id = addedBookmarkslist ]" )[ 0 ] ).width() );
                }
            }
            
        },
        
        
        loadBookmarkedPage: function ( strId )
        {
            $( this ).trigger( this.events.ITEM_CLICKED, parseInt( strId, 10 ) );
        },
        
        
        /**
         * This function is called from MG.PanelViewOperator. 
         * It creates the list that needs to be added in the bookmark section Panel.
         * Triggers an event Event.Bookmark.AddDataToBookmarkSectionPanel( addDataToBookmarkSectionPanel ).
         * It is handled in MG.BookmarkOperator
         */
        onPanelOpen: function ()
        {
        	var objThis = this;
            var bookmarkCreated = false;
            var bookmarkData = "";
            var _date = new Date();
            var _len = objThis.members.bookmarkArr;
            var month = _date.getMonth() + 1;
            $(objThis.element).find( "[ id = addedBookmarkslist ]" ).find('[hotkeyindex]').attr( "hotkeyindex" , "-1" );
            var hotKeyIndex = findLastHotKeyIndex( $(objThis.element).find('[hotkeyindex]') );
            var _bookmarkItem = null;
            
            objThis.members.bookmarkArr = [];
            
            for ( var i = 0; i < GlobalModel.bookMarks.length; i++ ) 
            {
            	for( var j = 0; j < _len; j++ )
            	{
            		if( objThis.members.bookmarkArr[ j ].id == GlobalModel.bookMarks[ i ].id )
            		{
            			bookmarkCreated = true;
            			break;
            		}
            	}
            	if( !bookmarkCreated )
                	objThis.members.bookmarkArr.push( GlobalModel.bookMarks[ i ] );
            }
            
            objThis.bookmarkArrSort( objThis.members.sortBy );
            yesterdaysDate = getYesterdaysDate();
            todaysDate = ( ( '' + month ).length < 2 ? '0' : '' ) + month + '/' +
            			 ( ( '' + _date.getDate() ).length < 2 ? '0' : '' ) +
            			 _date.getDate() + '/' + _date.getFullYear().toString().substr( 2, 2 );
            
            for ( var k = 0; k < objThis.members.bookmarkArr.length; k++ ) 
            {
            	_bookmarkItem = objThis.members.bookmarkArr[ k ];
                
                if ( _bookmarkItem ) 
                {
                    var dayData = "";
                    if ( _bookmarkItem.date == yesterdaysDate ) 
                    {
                        dayData = GlobalModel.localizationData[ "DATE_YESTERDAY" ];
                    }
                    else
                    {
                    	if ( _bookmarkItem.date == todaysDate ) 
                        {
                            dayData = GlobalModel.localizationData[ "DATE_TODAY" ];
                        }
                        else 
                        {
                            dayData = _bookmarkItem.date;
                        }
                    } 
                    bookmarkID = _bookmarkItem.id.replace( 'bookmarkStrip_', '' );
                    bookmarkData += "<div id='bookmarkData_" + bookmarkID + 
                    				"' class='bookmarkData' hotkeyindex='" + hotKeyIndex + "' hotkeygroupid='BookmarkMenu' type='bookmarkData' style='cursor: pointer;text-shadow: none;'>" + 
                    				"<div class='bookMarkStripIcon' type='bookMarkStripIcon'></div>" + 
                    				"<div class='bookmarkDataTextContainer'><span id='bookmarkDataTitle' class='bookmarkDataTitle'>" + 
                    				_bookmarkItem.title + "</span><span id='bookmarkDataDayData' class='bookmarkDataDayData' >" +
                    				 dayData + "</span></div><div id='bkmarkImg' type='bkmarkImg' class='bkmarkImg' style='display: none;'></div></div>"
                    hotKeyIndex = hotKeyIndex + 1;			 
                }
            }

            $( objThis ).trigger( Event.Bookmark.AddDataToBookmarkSectionPanel, bookmarkData ); // this is handled in bookmarkoperator
            if( objThis.members.bookScroll == null ) 
            {
                  setTimeout( function() 
                  {
                      objThis.members.bookScroll = new iScroll( 'addedBookmarkslist', {
                                scrollbarClass: 'myScrollbar'
                      } );   
                      objThis.updateViewSize();
                  },500 );
            } 
            else 
            {
                  setTimeout( function() 
                  {
                      objThis.updateViewSize();
                  },500 );
            }
            
        },
        
        
        bookmarkArrSort: function( option )
        {
        	var objThis = this;
        	
        	switch( option )
			{
				case "dateASC":
					objThis.members.bookmarkArr.sort( function( a, b ) 
					{
						var c = new Date( a.timeMilliseconds );
						var d = new Date( b.timeMilliseconds );
						return c - d;
					} );
					break;
				
				case "dateDSC":
					objThis.members.bookmarkArr.sort( function( a, b ) 
					{
						var c = new Date( a.timeMilliseconds );
						var d = new Date( b.timeMilliseconds );
						return d - c;
					} );
					break;
				
				case "pageASC":
					objThis.members.bookmarkArr.sort( function( a, b ) 
					{
						var c = GlobalModel.pageBrkValueArr.indexOf( a.page );
						var d = GlobalModel.pageBrkValueArr.indexOf( b.page );
						return c - d;
					} );
					break;
					
				case "pageDSC":
					objThis.members.bookmarkArr.sort( function( a, b ) 
					{
						var c = GlobalModel.pageBrkValueArr.indexOf( a.page );
						var d = GlobalModel.pageBrkValueArr.indexOf( b.page );
						return d - c;
					} );
					break;
			}
        }
        
    });
})(jQuery);
