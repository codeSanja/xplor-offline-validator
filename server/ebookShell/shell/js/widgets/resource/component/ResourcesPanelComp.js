/**
 * @author harpreet.saluja
 * This widget manages the resource panel view
 */

var resourceScroll;

( function( $, undefined ) {
	$.widget( "magic.ResourcesPanelComp", $.magic.tabViewComp, 
	{
		objPreviousNodeDetails: null,
		isDataLoaded: false,
		strCurrentLevelIdentifier: -1,
		events:
		{
			RESOURCE_ITEM_CLICK:"resourceItemClick",
			ON_MEDIA_RESOURCE:"onMediaResource",
			ON_WEB_RESOURCE:"onWebResource"
		},
		
		options: 
		{
			isPopoverVisible: false,
			previousPageView : EPubConfig.pageView
		},
		
		member:
		{
			panelOpen: false,
			panelCloseTimeout: 0
		},

		/**
		   * This function is called upon widget creation
		   * @param none
		   * @return void	
		   */
		_create: function()
		{
			$.extend(this.options, this.element.jqmData('options'));
		},
		
		/**
		   * This function is called upon widget initialization
		   * @param none
		   * @return void	
		   */
		_init: function()
		{
			$.magic.tabViewComp.prototype._init.call(this);
			var objClassRef = this; 
			
			$(window).resize(function(){
				objClassRef.updateViewSize();
			});
			
			$(window).orientationchange(function(){
				objClassRef.updateViewSize();
			});
			$(window).bind('orientationchange' , function() {
                        if (getDeviceOrientation() == AppConst.ORIENTATION_LANDSCAPE) {
                            $("#docwindowcontainer").css("width" , "75%");
                            $("#documentContent").css("left","10%");
                        } else {
                               $("#docwindowcontainer").css("width" , "100%");
                               $("#documentContent").css("left","5%");
                        }
			});
			
			$(this.element).unbind("tabDisplayChanged").bind("tabDisplayChanged", function()
			{
				if(objClassRef.member.panelOpen)
				{
					if(resourceScroll == null)
						resourceScroll = new iScroll('ResourcesList', { scrollbarClass: 'myScrollbar' });
						
					objClassRef.updateViewSize();
				}
				else
				{
					objClassRef.onPanelOpen();
				}
			});
			
			$(this.element).unbind("panelOpened").bind("panelOpened", function()
			{
				//alert(objClassRef.isDataLoaded);
				objClassRef.isDataLoaded = true;
				objClassRef.onPanelOpen();
			});
			
			$(this.element).unbind("panelClosed").bind("panelClosed", function()
			{
				objClassRef.onPanelClose();
			});
			
			$(this.element).unbind("beforePanelClose").bind("beforePanelClose", function()
			{
				if(ismobile)
					objClassRef.destroyScroll();
			});
		},
		
		resourceDataLoadCompleteHandler: function(objData, strType)
		{
			var objThis = this;
			var objResourcesData = [];
			var objItem;
			try {
				if (objData.results.length) {
					
					for (var i = 0; i < objData.results.length; i++) {
						//objItem = objThis.getResourceItem((EPubConfig.LocalResource_isAvailable) ? objData.results[i].content.resource[0]: objData.results[i].content.resource[0]);
						objItem = objThis.getResourceItem((EPubConfig.LocalResource_isAvailable) ? objData.results[i] : objData.results[i].content.resource[0]);
						
						objResourcesData.push(objItem);
					}
				}
				else {
					if (objData.results) {
						if (objData.results.content) {
							//objItem = objThis.getResourceItem((EPubConfig.LocalResource_isAvailable) ? objData.results.content.resource[0] : objData.results.content.resource[0]);
							objItem = objThis.getResourceItem((EPubConfig.LocalResource_isAvailable) ? objData.results : objData.results.content.resource[0]);
							objResourcesData.push(objItem);
						}
						else
						{
							objThis.showResourceDataNotFoundMsg();
						}
					}
					else
					{
						objThis.showResourceDataNotFoundMsg();
					}
				}
				if (objResourcesData.length == 0) {
					strListPart_2 = '<div class="blankResourcePanelText">' + GlobalModel.localizationData["RESOURCES_NOT_AVAILABLE"] + '</div>';
					$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
				}
				else
				{
					this.objResources = objResourcesData;
					objThis.createResourcesList();
				}
				
			}
			catch(e)
			{
				objThis.showResourceDataNotFoundMsg();
			}
			
		},
		
		showResourceDataNotFoundMsg: function()
		{
			var strListPart_2 = '<div class="blankResourcePanelText">' + GlobalModel.localizationData["RESOURCES_NOT_AVAILABLE"] + '</div>';
			$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
		},
		
		showResourceDataNotAvailable: function()
		{
			var strListPart_2 = '<div class="blankResourcePanelText">' + GlobalModel.localizationData["RESOURCES_NOT_AVAILABLE_PAGE"] + '</div>';
			$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
		},
		
		getResourceItem: function(objResourceItem)
		{
			var objItem = {};
			
			objItem.id = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.id;
			objItem.GUID = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.HMH_ID;
			objItem.additionalText = objResourceItem.additional_text;
			
			objItem.categorization = objResourceItem.categorization;
			if (EPubConfig.LocalResource_isAvailable == false) {
				if (objResourceItem.levels) {
					if (objResourceItem.levels.level) {
						if (objResourceItem.levels.level.length) {
							for (var x = 0; x < objResourceItem.levels.level.length; x++) {
								if (objResourceItem.levels.level[x].instructional_segment) {
									objItem.categorization = objResourceItem.levels.level[x].instructional_segment.title;
									break;
								}
							}
						}
						else {
							if (objResourceItem.levels.level.instructional_segment) {
								objItem.categorization = objResourceItem.levels.level.instructional_segment.title;
							}
						}
						
					}
				}
			}
			objItem.displayTitle = objResourceItem.display_title;
			objItem.ext = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.ext;
			objItem.genres = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.genres;
			objItem.hierarchy = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.hierarchy;
			
			objItem.meaningfulDescription = objResourceItem.meaningful_description;
			
			
			objItem.mediaType = objResourceItem.media_type;
			objItem.resourceType = objResourceItem.resource_type;
			
			objItem.secondaryURLs = null;//objResourceItem.secondaryURLs;
			
			
			objItem.toolType = (EPubConfig.LocalResource_isAvailable == true) ? "" : objResourceItem.tool_type;
			//objItem.uri = objResourceItem.uri.platform_url + objResourceItem.uri.relative_url;
			objItem.uri = (EPubConfig.LocalResource_isAvailable == true) ? objResourceItem.uri : objResourceItem.uri.relative_url;
			return objItem;
		},
		
		resourceDataLoadFailHandler: function(objData)
		{
			var strListPart_2 = '<div class="blankResourcePanelText">' + GlobalModel.localizationData["ERROR_IN_FETCHING_RESOURCE_DATA"] + '</div>';
			$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
		},
		
		
		/**
		   * This function sets the options
		   * @param none
		   * @return void	
		   */
		_setOption:function()
		{
			
		},

		/**
		   * This function is called when the "content/resources.txt" file is read & loaded in memory.
		   * @param {Object} this data is the JSON which is read temporarily from "content/resources.txt" file
		   * @return void	
		   */
		onLoadResources:function(data)
		{
			try
			{
				this.objResources = eval(data);
			}
			catch(e)
			{
				//do nothing
			}	
		},
		
		/**
		   * This function sets the resources in panel
		   * @param none
		   * @return void	
		   */
		
		createResourcesList: function()
		{
			var objThis = this;
			var strListPart_2 = '<div class="blankResourcePanelText">' + GlobalModel.localizationData["RESOURCES_NOT_AVAILABLE"] + '</div>';
			$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
			
			
			if(this.objResources)
			{
				var objReg = /[ ]+/gi;
				var nTotalResources = this.objResources.length;
				
				strListPart_2 = "<ul id='masterResourceList' style='height: 100%;'></ul>"
				$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
				
				var objMasterResourceList = $("#masterResourceList")[0];
				var objRegSp = /[\:\'\.\,\-\ \!\*\(\)\#]/gi;
				for(var i = 0; i < nTotalResources; i++)
				{
					var objCurrentResource = this.objResources[i];
					var strResType = objCurrentResource.categorization;
					var strResIDType = strResType.replace(objRegSp, "");
					//finding/creating top level node
					strResType = strResType.replace(objReg, "_");
					
					var objTopLevelNode = $(objMasterResourceList).find('[id="li_' + strResIDType + '"]')[0];
					if(objTopLevelNode == null)
					{
					    var tooltip = $("<span>" + objCurrentResource.categorization + "</span>").text();
					    var fntSize = parseInt($(".depth1-resourceNodeText").css("font-size"));
					   // $("body").append("<span style='font-size:"+fntSize+"px'>" + objCurrentResource.categorization + "</span>");
					    
					    objCurrentResource.categorization = getTextWithEllipsis(2,objCurrentResource.categorization,240);
					    var isPopoverReq = (objCurrentResource.categorization.slice( -3 ) == "...") ? true :false;
					    //$("span:last").remove();
						var strParentListItem = '<li id="li_' + strResIDType + '" style="height: auto">';
						strParentListItem += '<div id="div_' + strResIDType + '" class="depth-1-resource depth1-expandableResource" onclick="onExpandableItemClick(\'' + strResIDType + '\', 1)" >';
						if(isPopoverReq) {
							strParentListItem += '<div id="div_' + strResIDType + '_text" class="depth1-resourceNodeText" data-toggle="popover" data-placement="right" data-content="'+tooltip+'" >' + objCurrentResource.categorization + '</div>';
						} else {
							strParentListItem += '<div id="div_' + strResIDType + '_text" class="depth1-resourceNodeText" >' + objCurrentResource.categorization + '</div>';
						}
						strParentListItem += '<div id="div_' + strResIDType + '_image" class="depth1-expandableResourceNodeIcon"></div>';
						strParentListItem += '</div>';
						strParentListItem += '<div style="display:none; height: auto; width:328px" class="resource-' + strResIDType + '">';
						strParentListItem += '<ul id="ul_' + strResIDType + '" />'
						strParentListItem += '</div>';
						strParentListItem += '<div id="div_' + strResIDType + '_seperator" class="depth1-resourceSeperator" style="height:2px"></div>';
						strParentListItem += '</li>';
						
						$(objMasterResourceList).append(strParentListItem);
						
						//ensuring we have level one reference;
						objTopLevelNode = $(objMasterResourceList).find('[id="li_' + strResIDType + '"]')[0];
						//$(objTopLevelNode).attr("title",tooltip);
												
						/*if(isPopoverReq) {alert("here")
							$(objTopLevelNode).children('.depth1-resourceNodeText').attr("data-toggle","popover");
							$(objTopLevelNode).children('.depth1-resourceNodeText').attr("data-placement","right");
							$(objTopLevelNode).children('.depth1-resourceNodeText').attr("data-content",tooltip);	
						}*/
						
						//data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."
						
					}
					
					var strResSubCategory = objCurrentResource.displayTitle;
					strResSubCategory = strResSubCategory.replace(objReg, "_");
					
					//finding/creating second level node
					var objSecondLevelNode = $(objTopLevelNode).find('[id="li_' + strResIDType + '_' + i + '"]')[0];
					//if(objSecondLevelNode == null)
					//{
						
					    var tooltip = $("<span>" + objCurrentResource.displayTitle + "</span>").text();
					    // $("body").append('<span class="lastSpan" >' + tooltip + '</span>');
					      
					    objCurrentResource.displayTitle = getTextWithEllipsis(2,objCurrentResource.displayTitle,275)
					    var isPopoverReq1 = (objCurrentResource.displayTitle.slice( -3 ) == "...") ? true :false;
					    //$(".lastSpan").remove();
						var str = objCurrentResource.uri;
					    //var str = objCurrentResource.uri.platform_url + objCurrentResource.uri.relative_url;
					    str = str.replace(/\'/gi, "%27");
					    var strInternalResource = ' onclick="openSecondaryURL(\'' + str + '\')" resourcetype="external"';
						if(objCurrentResource.resourceType == "internal")
						{
							strInternalResource = '  onclick="openSecondaryURL(event)" mediaurl="'+ str +'" resourcetype="internal" mediatype="'+objCurrentResource.mediaType+'" '
						}
						//var strSecondLevelItem = '<li id="li_' + strResIDType + '_' + i + '"  onclick="openSecondaryURL(\'' + objCurrentResource.uri + '\', event)"'+strInternalResource+'>';
						var strSecondLevelItem = '<li id="li_' + strResIDType + '_' + i +'"'+strInternalResource+'>';
						strSecondLevelItem += '<div id="div_' + strResIDType + '_' + i + '" class="depth-2-resource depth2-expandableResource">';
						if (objCurrentResource.resourceType == "internal") {
							if (objCurrentResource.mediaType == 'video') {
							strSecondLevelItem += '<div class="resourceVideoIcon"></div>';
							} else if (objCurrentResource.mediaType == 'audio') {
								strSecondLevelItem += '<div class="resourceAudioIcon" data-role="buttonComp" type="pageLevelAudioHotSpot" off-state="audioHotspotContent" on-state="playAudioContent"> </div>';
							} else if (objCurrentResource.mediaType == 'pdf' || objCurrentResource.mediaType =='word' || objCurrentResource.mediaType =='xls') {
								strSecondLevelItem += '<div class="resource'+objCurrentResource.mediaType+'Icon"></div>';
						
							}
						}
						if(isPopoverReq1) {
							strSecondLevelItem += '<div id="div_' + strResIDType + '_' + i + '_text" class="depth2-resourceNodeText" data-toggle="popover" data-placement="right" data-content="'+tooltip+'" >' + objCurrentResource.displayTitle + '</div>';
						} else {
							strSecondLevelItem += '<div id="div_' + strResIDType + '_' + i + '_text" class="depth2-resourceNodeText">' + objCurrentResource.displayTitle + '</div>';	
						}
						
						strSecondLevelItem += '<div id="div_' + strResIDType + '_' + i + '_image" class="depth2-expandableResourceNodeIcon" style="visibility:hidden;"></div>';
						strSecondLevelItem += '</div>';
						strSecondLevelItem += '<div style="display:none; width:328px;" class="resource-' + strResIDType + '_' + i + '">'
						strSecondLevelItem += '<ul id="ul_' + strResIDType + '_' + i + '" />'
						strSecondLevelItem += '</div>';
						strSecondLevelItem += '</li>';
						
						var obj = $(objTopLevelNode).find('[id="ul_' + strResIDType + '"]');
						obj.append(strSecondLevelItem);
						//ensuring we have level two reference;
						objSecondLevelNode = obj.find('[id="li_' + strResIDType + '_' + i + '"]')[0];
						//$(objSecondLevelNode).attr("title",tooltip);
						
						
						/*if(isPopoverReq1) {
							$(objSecondLevelNode).children('.depth2-resourceNodeText').attr("data-toggle","popover");
							$(objSecondLevelNode).children('.depth2-resourceNodeText').attr("data-placement","right");
							$(objSecondLevelNode).children('.depth2-resourceNodeText').attr("data-content",tooltip);
						}*/
					//}
					
					//creating a node for actual resource...
					
					var strResourceNode = '<li class="depth3-resourceNode" url="' + str + '" type="' + strResIDType + '"' +strInternalResource +'>';
					strResourceNode += '<div onclick="resourceItemClick(event)" class="resourcePrimaryLink" ><div class="resourceDisplayTitle">' + objCurrentResource.displayTitle + '</div>';
					strResourceNode += '<div class="resourceDescription">' + objCurrentResource.meaningfulDescription + '</div>';
					strResourceNode += '<div style="height:66px; width:88px; margin:10px 0 10px 220px; cursor: pointer;background: url(\'' + getPathManager().appendReaderPath(objCurrentResource.iconURL) +'\') no-repeat 0 0 !important"></div></div>';
					
					/*if(objCurrentResource.secondaryURLs)
					{
						if(objCurrentResource.secondaryURLs.icons)
						{
							if (objCurrentResource.secondaryURLs.icons.length) {
								
								//console.log("objCurrentResource.secondaryURLs.icons.length: ", objCurrentResource.secondaryURLs.icons.length);
								var iSecondaryIconsCount = objCurrentResource.secondaryURLs.icons.length;
								
								var iLeft = 262;
								var iBottom = 22;
								
								for (var j = 0; j < iSecondaryIconsCount; j++) {
									var objSecondaryURL = objCurrentResource.secondaryURLs.icons[j];
									
									if (j == 0) {
										strResourceNode += '<div class="resourceSecondaryLink" onclick="openSecondaryURL(\'' + objSecondaryURL.url + '\')" >';
										strResourceNode += '<div style="width: 193px; height: 24px; text-align: left; font-size: 11px; overflow:hidden;">' + objSecondaryURL.title + '</div>';
									}
									
									strResourceNode += '<div style="bottom:' + iBottom + 'px; left:' + iLeft + 'px" class="secondaryURL-' + objSecondaryURL.type + '"></div>';
									
									iLeft -= 35;
									iBottom += 27;
								}
							}
						}
					}
					else{*/
						strResourceNode += '<div style="width: 100%; height:0px; margin-left:45px; margin-top: 10px; margin-bottom: 10px;">';
					//}
					
					strResourceNode += '</div>';
					
					strResourceNode += '</li>';
					
					$(objSecondLevelNode).find('[id="ul_' + strResIDType + '_' + i + '"]').append(strResourceNode);
					
				}
			}
			
			//create and update scroller;alert()
			if(resourceScroll == null) {
			       resourceScroll = new iScroll('ResourcesList', { scrollbarClass: 'myScrollbar' });
			 }
			objThis.updateViewSize();
		},
		
		/**
		   * This function returns the DOM element of Panel
		   * @param 	none
		   * @return 	{Object} DOM element of ResourcesPanel
		   */
		getResourcePanelViewComp:function(){
        	return $('#ResourcesPanel');
    	},
		
		/**
		   * This function destroys the this object
		   * @param 	none
		   * @return 	none
		   */
		destroy: function()
		{
			$.widget.prototype.destroy.call( this );		
		},
		
		/**
		   * This function handles the click on resource item
		   * @param 	(Object) simulates CLICK mouse event
		   * @return 	none
		   */
		onItemClicked:function(event)
		{
			var obj = $(event.target).closest('[class=depth3-resourceNode]');
			
			var strURL = obj.attr("url");
			var strType = obj.attr("type");
			
			switch(strType)
			{
				case AppConst.HTML:
				default:
					window.open(strURL, "_blank");
					break;
			}
		},
		
		/**
		   * This function launches secondary urls
		   * @param 	(String) url to launch/opem
		   * @return 	none
		   */
		openSecondaryURL: function (strURL)
		{
			window.open(strURL, "_blank");
		},
		
		/**
		   * This function handles the click on expandable node
		   * @param 	(String) id of element which is clicked
		   * @param 	(Object) class of children of element
		   * @return 	none
		   */
		onExpandableItemClick:function(strChildIdentidier, iDepthLevel)
		{
			var objThis = this;
			var fnToggleNode = function(childIdentifier, depthLevel)
			{
				var strDisplayType = $("#masterResourceList").find('[class="resource-' + childIdentifier + '"]').css("display");
				
				strDisplayType = (strDisplayType == "none") ? "block" : "none";
				
				//$('.resource-' + childIdentifier).css("display", strDisplayType);
				if(strDisplayType == "block")
				{
					$("#masterResourceList").find('[id="div_' + childIdentifier + '"]').removeClass('depth' + depthLevel + '-expandableResource');
					$("#masterResourceList").find('[id="div_' + childIdentifier + '"]').addClass('depth' + depthLevel + '-expandedResource');
					
					$("#masterResourceList").find('[id="div_' + childIdentifier + '_image"]').removeClass('depth' + depthLevel + '-expandableResourceNodeIcon');
					$("#masterResourceList").find('[id="div_' + childIdentifier + '_image"]').addClass('depth' + depthLevel + '-expandedResourceNodeIcon');
					
					if(depthLevel == 1)
					{
						$("#masterResourceList").find('[id="div_' + childIdentifier + '_seperator"]').css("height", "10px");
					}
					objThis.updateViewSize("div_" + childIdentifier);
					$("#masterResourceList").find('[class="resource-' + childIdentifier + '"]').slideDown(500, "easeInOutExpo",function(){
                        //resourceScroll.refresh();
                        objThis.updateViewSize("div_" + childIdentifier);
                    });
				}
				else {
					$("#masterResourceList").find('[id="div_' + childIdentifier + '"]').removeClass('depth' + depthLevel + '-expandedResource');
					$("#masterResourceList").find('[id="div_' + childIdentifier + '"]').addClass('depth' + depthLevel + '-expandableResource');
					
					$("#masterResourceList").find('[id="div_' + childIdentifier + '_image"]').removeClass('depth' + depthLevel + '-expandedResourceNodeIcon');
					$("#masterResourceList").find('[id="div_' + childIdentifier + '_image"]').addClass('depth' + depthLevel + '-expandableResourceNodeIcon');
					
					if(depthLevel == 1)
					{
						$("#masterResourceList").find('[id="div_' + childIdentifier + '_seperator"]').css("height", "2px");
					}
					
					//setting display none of child being close just a millisecond earlier than it completely closes because otherwise the moment height becomes 0, 
					//and before the element hides, the height is reset to auto which is causing a flicker.
					setTimeout(function(){
						$("#masterResourceList").find('[class="resource-' + childIdentifier + '"]').css("display", "none");
					}, 499);
					
					$("#masterResourceList").find('[class="resource-' + childIdentifier + '"]').slideUp(500, "easeInOutExpo",function(){
                        //resourceScroll.refresh();
                        objThis.updateViewSize();
                    });
				}
			}
			
			//if previous top level node is open and it is not same as current node, close it
			if(this.objPreviousNodeDetails != null && this.objPreviousNodeDetails != strChildIdentidier && iDepthLevel == 1)
			{
				//fnToggleNode(this.objPreviousNodeDetails, 1);
			}
			
			//toggle currently clicked node;
			fnToggleNode(strChildIdentidier, iDepthLevel);
			
			if(iDepthLevel == 1)
			{
				if(this.objPreviousNodeDetails == strChildIdentidier)
				{
					this.objPreviousNodeDetails = null;
				}
				else
				{
					this.objPreviousNodeDetails = strChildIdentidier;
				}
			}
			
			//update scroller;
			//this.updateViewSize("div_" + strChildIdentidier);
			if(resourceScroll)
                {
                    resourceScroll.refresh();
                }
		},
		
		updateViewSize: function(Index){
			var objThis = this;
			setTimeout(function(){
                                        $("[data-toggle=popover]").popover({ trigger: "hover", container: "#ResourcesPanel" })
                                        .click(function(e) {
                                        e.preventDefault()
                                    })   
				var nHeight = ($("#header").css("display") == "block") ? window.innerHeight - $("#header").height() : window.innerHeight - 1;  
				
				$("#ResourcesList").height(nHeight - 84);
				if(resourceScroll)
				{
					resourceScroll.refresh();
				}
				
				if(Index)
				{
				          try {
				                    var obj = $("." + Index.replace("div_", "resource-")).find("li:first").find("div:first");
                                                            var nResourcesListTop = $("#ResourcesList").offset().top;
                                                            if((Number(obj.offset().top - nResourcesListTop) + parseInt(obj.css("height"))) > parseInt($("#ResourcesList").height()) || Number(obj.offset().top - nResourcesListTop) < 0)
                                                            {
                                                                      resourceScroll.scrollToElement(obj[0],500);
                                                            }
				          } catch(e) {
				                    
				          }
				}
				//adjust the width of the ResourceList if scrollbar is visible
				if(ismobile == null)
				{
					//Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
					removeiScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"));
					iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"),resourceScroll,"ResourcesList");
					
					if($(objThis.element).find("[class=myScrollbarV]").html())
						$($(objThis.element).find("[id=masterResourceList]")[0]).width($($(objThis.element)).width() - 23);
					else	
						$($(objThis.element).find("[id=masterResourceList]")[0]).width($($(objThis.element)).width() - 2);
				}
				
			}, 50);
		},
		
		/**
		   * This function is called when panel is open. It is used to update/set the resource panel list based on the current Unit/Lesson/Section/Page
		   * @param 	none
		   * @return 	void
		   */
		onPanelOpen:function()
		{
			var objThis = this;
			clearTimeout(this.member.panelCloseTimeout);
			this.member.panelCloseTimeout = 0;
			var iPageIndex;
			if (EPubConfig.pageView == 'doublePage')
			{
				iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex - 1 ]).attr('pagesequence');
			}
			else
			{
				iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
			}
			
			var strLevel = "";//(GlobalModel.ePubGlobalSettings.Page[iPageIndex]["L5"]) ? GlobalModel.ePubGlobalSettings.Page[iPageIndex]["L5"] : "";
            if (EPubConfig.LocalResource_isAvailable == false)
			{
				//console.log("iPageIndex: ", iPageIndex);
				if(!GlobalModel.ePubGlobalSettings.Page[iPageIndex].mds_isAvailable && EPubConfig.legacyTOC == false)
				{
					GlobalModel.currentMDSData = objPageData;
					objThis.showResourceDataNotAvailable();
					return;
				}
				var objPageData = GlobalModel.pageDataBySequence["pagesequence_" + iPageIndex];
				if(GlobalModel.currentMDSData && objThis.options.previousPageView == EPubConfig.pageView )
				{
					if(EPubConfig.legacyTOC == false && EPubConfig.MDSVersion == 3.2)
					{
						if(objPageData.mdsAttr == GlobalModel.currentMDSData.mdsAttr )
						{
							objThis.updateViewSize();
							return;
						}
					}
					else
					{
						if(GlobalModel.currentMDSData.L0 == objPageData.L0 && GlobalModel.currentMDSData.L1 == objPageData.L1 && GlobalModel.currentMDSData.L2 == objPageData.L2 && GlobalModel.currentMDSData.L3 == objPageData.L3 && GlobalModel.currentMDSData.L4 == objPageData.L4 && GlobalModel.currentMDSData.L5 == objPageData.L5 && GlobalModel.currentMDSData.L6 == objPageData.L6)
						{
							objThis.updateViewSize();
							return;
						}
					}
				}
				objThis.options.previousPageView = EPubConfig.pageView;
				GlobalModel.currentMDSData = objPageData;
				var strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"></div></div>';
				$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
				
				var nCallCounter = 1;
				var nResponseSuccessCounter = 0;
				var nResponseCounter = 0;
				var objRightPageData;
				var arrResourceData = [];
				var objResponseData = {results: []};
				//console.log("EPubConfig.pageView: ", EPubConfig.pageView, (iPageIndex + 1), GlobalModel.pageDataBySequence["pagesequence_" + (iPageIndex + 1)]);
				if (EPubConfig.pageView == 'doublePage')
				{
					objRightPageData = GlobalModel.pageDataBySequence["pagesequence_" + (parseInt(iPageIndex) + 1)]
					if(objRightPageData != undefined)
					{
						nCallCounter = 2;
					}
				}
				
				var funcSuccessHandler = function(data)
				{
					nResponseSuccessCounter++;
					//console.log("nResponseSuccessCounter: ", nResponseSuccessCounter);
					nResponseCounter++;
					if(nCallCounter == 1)
					{
						objThis.resourceDataLoadCompleteHandler(data);
						return;
					}
					if(data.results.length)
					{
						if(data.results.length > 0)
						{
							objResponseData.results = objResponseData.results.concat(data.results);
						}
					}
					//console.log("data: ", data);
					//console.log("arrResourceData: ", objResponseData);
					if(nResponseSuccessCounter > 0 && nResponseCounter == nCallCounter)
					{
						objThis.resourceDataLoadCompleteHandler(objResponseData);
					}
				}
				var funcErrorHandler = function(data)
				{
					nResponseCounter++;
					if(nCallCounter == 1)
					{
						objThis.resourceDataLoadFailHandler(data);
						return;
					}
					if(nResponseCounter == nCallCounter && nResponseSuccessCounter == 0)
					{
						objThis.resourceDataLoadFailHandler(data);
					}
					if(nResponseCounter == nCallCounter && nResponseSuccessCounter > 0)
					{
						objThis.resourceDataLoadCompleteHandler(objResponseData);
					}
				}
				
				var animTimer = setTimeout(function(){
					objThis.strCurrentLevelIdentifier = strLevel;
					
					var strUrl = getPathManager().getEPubResourcesPath(objPageData);
					
					ServiceManager.getResourcesJSON({
						url: strUrl, //getPathManager().getEPubResourcesPath(),
						caller: this,
						success: funcSuccessHandler,
						error: funcErrorHandler,
						//success: "resourceDataLoadCompleteHandler",
						//error: "resourceDataLoadFailHandler",
						calltype: "remote"
					});
					
					if(objRightPageData != undefined)
					{
						//console.log("objRightPageData: ", objRightPageData,objPageData, objRightPageData.mdsAttr, objPageData.mdsAttr);
						var getRightPageResources = false;
						if(EPubConfig.legacyTOC == false && EPubConfig.MDSVersion == 3.2)
						{
							if(objRightPageData.mdsAttr != objPageData.mdsAttr )
							{
								getRightPageResources = true;
							}
						}
						else
						{
							if( !(objRightPageData.L0 == objPageData.L0 && objRightPageData.L1 == objPageData.L1 && objRightPageData.L2 == objPageData.L2 && objRightPageData.L3 == objPageData.L3 && objRightPageData.L4 == objPageData.L4 && objRightPageData.L5 == objPageData.L5 && objRightPageData.L6 == objPageData.L6) )
							{
								getRightPageResources = true;
							}
						}
						
						
						if(getRightPageResources)
							{
								strUrl = getPathManager().getEPubResourcesPath(objRightPageData);
								console.log("strUrl: ", strUrl);
								ServiceManager.getResourcesJSON({
									url: strUrl, //getPathManager().getEPubResourcesPath(),
									caller: this,
									success: funcSuccessHandler,
									error: funcErrorHandler,
									calltype: "remote"
								});
							}
							else
							{
								nCallCounter = 1;
							}
						
					}
					
					clearTimeout(animTimer);
					
					strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';
					$(objThis.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
					
				}, 500);
				
			}
			else
			{
				var strUrl = getPathManager().getEPubResourcesPath(strLevel);
				if(EPubConfig.Resources_isAvailable) {
					if(this.objResources == undefined)
					{
						ServiceManager.getResourcesJSON({
							url: strUrl, //getPathManager().getEPubResourcesPath(),
							caller: this,
							success: "resourceDataLoadCompleteHandler",
							error: "resourceDataLoadFailHandler",
							calltype: "remote"
						});
						var strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';
						$(this.element).find('[id="ResourcesListContainer"]').html(strListPart_2);
					}
				}
				
				
			}
			
			this.member.panelOpen = true;
		},
		
		/**
		   * This function is called when panel is closed and deletes the resource list to lighten the application;
		   * @param 	none
		   * @return 	void
		   */
		onPanelClose:function()
		{
			var objThis = this;
			if(resourceScroll)
			{
				resourceScroll.refresh();
			}
			//this.member.panelCloseTimeout = setTimeout(function(){
//				$(objThis.element).find('[id="ResourcesListContainer"]').html('');
//				objThis.objPreviousNodeDetails = null;
//				objThis.destroyScroll();
//				objThis.member.panelOpen = false;
			//},500);
		},
		
		destroyScroll: function()
		{
			/*if(resourceScroll)
			{
				//remove scroll, destroy it and nullify it's reference;
				resourceScroll.destroy();
				resourceScroll = null;
				removeiScrollClickHandler($(this.element));
				console.log("here");
			}*/
		}
	});
	
	var bResourceItemClicked = false;
	
	/**
	   * This function handles the click on resource item
	   * @param 	(Object) simulates CLICK mouse event
	   * @return 	none
	   */
	resourceItemClick = function(event)
	{
		bResourceItemClicked = true;
		
		var objDOM = $('[data-role="ResourcesPanelComp"]');
		var objComp = objDOM.data("ResourcesPanelComp");
		
		objComp.onItemClicked(event);
		bResourceItemClicked = false;
		
	},
	
	/**
	   * This function handles the click on expandable node
	   * @param 	(String) id of element which is clicked
	   * @param 	(Object) class of children of element
	   * @return 	none
	   */
	onExpandableItemClick = function (strChildIdentidier, iDepthLevel)
	{
		if(bResourceItemClicked == false)
		{
			var objDOM = $('[data-role="ResourcesPanelComp"]');
			var objComp = objDOM.data("ResourcesPanelComp");
			
			objComp.onExpandableItemClick(strChildIdentidier, iDepthLevel);
		}
		else
		{
			bResourceItemClicked = false;
		}
		
	}
	
	/**
	   * This function launches secondary urls
	   * @param 	(String) url to launch/opem
	   * @return 	none
	   */
	openSecondaryURL = function (strData)
	{
		bResourceItemClicked = true;
		if(typeof(strData) == "string")
			{
				var objDOM = $('[data-role="ResourcesPanelComp"]');
				var objComp = objDOM.data("ResourcesPanelComp");
				objComp.openSecondaryURL(strData);
				$($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel');
			}
			else
			{
				//console.log(strData.target);
				var obj = $(strData.currentTarget);
				var strURL = obj.attr("mediaurl");
				var mediaType = obj.attr("mediatype");
				//console.log('mediaType' , mediaType);
				if (mediaType == 'video') {
				$(document).trigger("PLAY_VIDEO", strURL);
				} else if (mediaType == 'audio') {
					$(document).trigger("RESOURCE_PLAY_AUDIO", strURL);
				}
				else if(mediaType == 'pdf' || mediaType == 'word' || mediaType == 'xls' )
				{
					strURL = getPathManager().getExternalAssetPath(strURL, mediaType);
					var _objDocumentData = {};
					_objDocumentData.type = AppConst.ACTIVITY_DATA;
					_objDocumentData.url = strURL;
					
					if(isNative || isNativePublisher)
					{
					var docWin = '<div id="documentCloseButton" type="closeButton"></div><div id="docwindowcontainer" style="width: 100%; height: 80%;" align="center"><iframe id="documentObject" style="width: 80%; height: 100%; border:2px solid darkgray;"></iframe></div>';
					
						$("#documentContent").css("display","block");
						$("#documentMask").css("display","block");
						$("#documentContent").html(docWin);
							
						
						if (getDeviceOrientation() == AppConst.ORIENTATION_LANDSCAPE) {
                            $("#docwindowcontainer").css("width" , "75%");
                            	$("#documentContent").css("left","10%");
                        } else {
                               $("#docwindowcontainer").css("width" , "100%");
                               	$("#documentContent").css("left","5%");
                        }
						$("#documentContent").attr("src",strURL);
						$("#documentObject").attr("src" , strURL);
						console.log($("#documentObject").width());
						$("#documentCloseButton").css("left" , $("#documentObject").width() + 90 +"px");
						
						$("#documentCloseButton").bind("click", function(e){
								PopupManager.removePopup();
								$("#documentContent").html("");
								$("#documentContent").css("display" , "none");
								$("#documentMask").css("display","none");
							});
					} else {
						window.open(strURL, "_blank");
					}
				}
			}
		    bResourceItemClicked = false;
	}
})( jQuery );
