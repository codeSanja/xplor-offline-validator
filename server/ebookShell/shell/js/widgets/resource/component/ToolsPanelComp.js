/**
 * @author harpreet.saluja
 * This widget manages the resource panel view
 */

var toolScroll;

(function($, undefined) {
	$.widget("magic.ToolsPanelComp", $.magic.tabViewComp, {
		objPreviousNodeDetails : null,

		events : {
			RESOURCE_ITEM_CLICK : "resourceItemClick",
			ON_MEDIA_RESOURCE : "onMediaResource",
			ON_WEB_RESOURCE : "onWebResource"
		},

		options : {
			toolsData: null
		},

		/**
		 * This function is called upon widget creation
		 * @param none
		 * @return void
		 */
		_create : function() {
			$.extend(this.options, this.element.jqmData('options'));
		},

		/**
		 * This function is called upon widget initialization
		 * @param none
		 * @return void
		 */
		_init : function() {
			$.magic.tabViewComp.prototype._init.call(this);
			var objClassRef = this;
			var scrollViewCompRef = $("#bookContentContainer").data('scrollviewcomp');
			/*$(scrollViewCompRef).bind(scrollViewCompRef.events.PAGE_RENDER_COMPLETE,function(){
			    objClassRef.loadToolsContent();
			});*/

			$(window).resize(function() {
				objClassRef.updateViewSize();
			});

			$(window).orientationchange(function() {
				objClassRef.updateViewSize();
			});

			$(this.element).unbind("tabDisplayChanged panelOpened").bind("tabDisplayChanged panelOpened", function() {
				if(objClassRef.options.toolsData == null) {
					$("#ToolsListContainer").html('<div id="loaderContainer" style="width: 100%;" align="center"><div class="pagePreloader"></div></div>');
					objClassRef.loadToolsContent();
				}				
				objClassRef.destroyScroll();
				//create and update scroller;
				toolScroll = new iScroll('ToolsList', {
					scrollbarClass : 'myScrollbar',
					hScrollbar : false
				});
				objClassRef.updateViewSize();
			});
			
			$(this.element).unbind("beforePanelClose").bind("beforePanelClose", function()
			{
				if(ismobile)
				{
					objClassRef.destroyScroll();
				}
			});
		},
		
		loadToolsContent: function(){
		    var objClassRef = this;
		    var objPathManager = getPathManager();
            var strSrcId = objClassRef.options.src;
            var strSrcPath = objPathManager.getFilePath(strSrcId);

            var toolsPath = EPubConfig.toolsXMLPath;
            var strUri = objPathManager.getStandardsPath(toolsPath);
            
            if(EPubConfig.Tools_isAvailable) {
	            // If file type is JSON 
	            if(toolsPath.indexOf('.json') != -1) {
	                $.getJSON(strUri, function(objData) {
	                	objClassRef.options.toolsData = objData;
	                    if(objData && objData.tools)
	                    {
	                        objData = objData.tools;
	                        objClassRef.createToolsList(objData);
	                    }
	                    else
	                    {
	                        var toolsListStr = '<div>' + GlobalModel.localizationData["TOOLS_NOT_AVAILABLE"] + '</div>';
	                        $("#ToolsListContainer").html(toolsListStr);
	                    }
	                }).fail( function(){
	                    var toolsListStr = '<div>' + GlobalModel.localizationData["ERROR_IN_FETCHING_DATA"] + '</div>';
	                    $("#ToolsListContainer").html(toolsListStr);
	                })
	            } 
	            // If file type is xml 
	            else if(toolsPath.indexOf('.xml') != -1) {
	                $.ajax({
	                    type : "GET",
	                    url : strUri,
	                    success : function(data) {
	                        // converted xml data object in to json object
	                        var objData = $.xml2json(data);
	                        objClassRef.options.toolsData = objData;
	                        objClassRef.createToolsList(objData);
	                    },
	                    error: function(request, status, error) {
	                        console.warn("error " + status)
	                    }
	                });
	            } 
		    }
		},
		
		destroyScroll: function()
		{
			/*if(toolScroll)
			{
				toolScroll.destroy();
				toolScroll = null;
				$(this.element).find('[class="myScrollbarV"]').remove();
			}
			*/
		},

		/**
		 * This function sets the options
		 * @param none
		 * @return void
		 */
		_setOption : function() {

		},

		createToolsList : function(objData) {
			var toolsListStr = '<ul>';
			if(objData.tool != undefined && objData.tool.length > 0) {
				for(var i=0; i<objData.tool.length; i++ ) {
					toolsListStr += '<li onclick="launchTool(\''+objData.tool[i].url+'\')">'+objData.tool[i].name+'</li>';
				}
			}
			else if( objData.tool != undefined && typeof objData.tool == "object" ) //single tool item
			{
				toolsListStr += '<li onclick="launchTool(\''+objData.tool.url+'\')">'+objData.tool.name+'</li>';
			}
			else
			{
				toolsListStr +=  '<div>' + GlobalModel.localizationData["TOOLS_NOT_AVAILABLE"] + '</div>';
			}
			toolsListStr += '</ul>';
			$("#ToolsListContainer").html(toolsListStr);
			this.updateViewSize();
		},

		/**
		 * This function returns the DOM element of Panel
		 * @param 	none
		 * @return 	{Object} DOM element of ResourcesPanel
		 */
		getResourcePanelViewComp : function() {
			return $('#ResourcesPanel');
		},

		/**
		 * This function destroys the this object
		 * @param 	none
		 * @return 	none
		 */
		destroy : function() {
			$.widget.prototype.destroy.call(this);
		},

		/**
		 * This function launches a tool
		 * @param 	(String) url to launch/opem
		 * @return 	none
		 */
		launchTool : function(strURL) {
			window.open(strURL, "_blank");
		},

		updateViewSize : function() {
			var objThis = this;
			setTimeout(function() {
				var nHeight = ($("#header").css("display") == "block") ? window.innerHeight - $("#header").height() : window.innerHeight - 1;
	
				$("#ToolsList").height(nHeight - 84);
				if (toolScroll) {
					toolScroll.refresh();
				}
				
				//adjust the width of the ResourceList if scrollbar is visible
				if (ismobile == null) {
					//Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
					iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), toolScroll, "ToolsList");
					if ($(objThis.element).find("[class=myScrollbarV]").html())
						$($(objThis.element).find("[id=ToolsListContainer]")[0]).width($($(objThis.element)).width() - 23);
					else
						$($(objThis.element).find("[id=ToolsListContainer]")[0]).width($($(objThis.element)).width() - 2);
				}
			},500);
		}
	});

	/**
	 * This function launches a tool
	 * @param 	(String) url to launch/opem
	 * @return 	none
	 */
	launchTool = function(strURL) {
		var objDOM = $('[data-role="ToolsPanelComp"]');
		var objComp = objDOM.data("ToolsPanelComp");

		objComp.launchTool(strURL);
	}
})(jQuery); 