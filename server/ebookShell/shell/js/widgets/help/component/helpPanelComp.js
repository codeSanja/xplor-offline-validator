/**
 * @author abhishek.shakya
 */
( function( $, undefined ) {
$.widget( "magic.helpPanelComp", $.magic.magicwidget, 
{
	options: 
	{
		
	},
		////////////////////////NOTEPANEL
	_getPanel: function()
	{
		return  this.getVar('objPanel');
	},
	_create: function()
	{
		$.extend(this.options, this.element.jqmData('options'));
		
	},
	_init: function()
	{
        $.magic.magicwidget.prototype._init.call(this);
		self = this;
		
		self.updatePosition(self.element);
		$(window).bind('resize', function(){
			self.updatePosition();
		});

		$( $( self )[0].element ).find( ".helpPanelStartGuide" ).bind( "click", function ()
		{
			window.open( getPathManager().getHelpPath(), "_blank" );
		} );
		
		$( $( self )[0].element ).find( ".helpPanelAbout" ).bind( "click", function ()
		{
			 $( document ).trigger( "launchHelpAboutPopup" );
		} );
		
		
		//$(this.element).find(".viewSwitchBtnParent").css("display", "none");
	},
	
	onPanelOpen: function() {
		
	},
	
	onPanelClose: function() {
		 
	},
	
	launchPanel : function () {
		$("#helpPanelArrow").css( "display", "block" );
	   	$("#helpBtnPanel").css( "display", "block" );
		this.updatePosition();
	},
	
	updatePosition:function(){
       $(this.element).parent().css('top', $("#helpLauncher").offset().top + $("#helpLauncher").height()/2);
       $(this.element).parent().css('left' , $($("#helpLauncher").parent().find('[id="helpLauncher"]')).width());
       $(this.element).css('top', 0);
	}
	
})
})( jQuery );
