/**
 * @author Magic s/w pvt ltd
 */

(function($, undefined){
    $.widget("magic.glossaryPanelComp", $.magic.magicwidget, {
        isPageLoaded: false,
        options: {
            mediaSrc: "",
            oggMediaSrc: "",
            isXHTML:false
        },
        
        events: {},
        
        member://member variables
        {
            glossaryData: null,
            isAudioPlaying: false,
            audioBtnRef: "",
            glossaryAudioButton: null,
        },
        
        _create: function()
        {
            $.extend(this.options, this.element.jqmData('options'));
        },
        
        _init: function()
        {
            $.magic.magicwidget.prototype._init.call(this);
            var objThis = this
            objThis.member.glossaryAudioButton = $(objThis.element).find('[id=glossaryAudioButton]');
            
            /*-----------------------------------------Glossary close button click event ---------------------------------------------- */
           
            $(this.element).find('[id=closeButton]').unbind('click').bind('click', function()
            {
                $(objThis.member.glossaryAudioButton).removeClass('glossaryAudioPlay');
                PopupManager.removePopup($(objThis.element).attr('id'));
                $(objThis.member.glossaryAudioButton).removeClass('glossaryAudioPlay');
            	$('#player')[0].currentTime = 0;
            	$('#player')[0].pause();
            });
            /*------------------------------------------------------------------------------------------------------------------------ */
			
			var objFullGlossButton = $(this.element).find('[id=externalURLLink]')[0];
			if(EPubConfig.FULL_GLOSSARY_PATH == "")
			{
				$(objFullGlossButton).css('display', 'none');
				$(this.element).height($(this.element).height() - 35);
			}
			else
			{
				$(objFullGlossButton).unbind('click').bind('click', function(){
	                window.open(EPubConfig.FULL_GLOSSARY_PATH, '_blank');
	            });
			}
            
            /*-----------------------------------------Glossary Audio button click event ---------------------------------------------- */
           
            $(objThis.member.glossaryAudioButton).unbind('click').bind('click', function(){
               objThis.member.audioBtnRef = this;
            	if( !$(objThis.member.audioBtnRef).hasClass('glossaryAudioPlay') ){
            		$('#player')[0].currentTime = 0;
            		$('#player')[0].play();
            		$(objThis.member.audioBtnRef).addClass('glossaryAudioPlay');
            	}
            	else{
            		$('#player')[0].pause();
            		$(objThis.member.audioBtnRef).removeClass('glossaryAudioPlay');
            	}
            	$("#player")[0].addEventListener('ended', function(){
				    $(objThis.member.audioBtnRef).removeClass('glossaryAudioPlay');
				});
            });
            /*------------------------------------------------------------------------------------------------------------------------ */
        },
        
        /**
         * This function stops the glossary audio when the popup is closed through popup manager.
         * @param {}
         * @return {}
         */
        closeGlossaryPopUp: function(){
            var objThis = this;
            if( $(objThis.element).css("display") == "block" )
            {
            	$(objThis.member.glossaryAudioButton).removeClass('glossaryAudioPlay');
	            if($('#player')[0].readyState == 4){
	            	$('#player')[0].currentTime = 0;
	            }
	            $('#player')[0].pause();
            }
        },
        
        /**
		 * This function loads the Glossary JSON/XML/XHTML file
		 * @param {}
		 * @return {}
		 */
        loadGlossaryContent: function() {
            var objThis = this;
            var type = "GET";
            var url = getPathManager().getEPubGlossaryPath();
            
            if(EPubConfig.Glossary_isAvailable) { 
	            // If file type is JSON 
	            if(url.indexOf('.json') != -1) {
	                $.getJSON(url, function(objData) {
	                    if(objData.glossary) {
	                        objData = objData.glossary;
	                    }
						objThis.parseAndStoreGlossaryData(objData);
	                });
	            }
	            // If file type is xml
	            else if(url.indexOf('.xml') != -1) {
	                $.ajax({
	                    type: "GET",
	                    url: url,
	                    success: function(data){
	                        var objData = $.xml2json(data);
	                        objThis.glossaryDataLoadComplete(data);
	                    }
	                    
	                });
	            }
	            // If file type is xhtml
	            else if(url.indexOf('.xhtml') != -1) {
	                $.ajax({
	                    type: "GET",
	                    url: url,
	                    success: function(data){
	                    	objThis.options.isXHTML = true;
	                    	objThis.member.glossaryData = data;
	                    }
	                    
	                });
	            }
	        }
        },
        
        /*
		 * This function returns the (is XHTML) type of footnote Data
		 * @return{Boolean}
		 */
        isglossaryXHTML : function() {
			var objThis = this;
			return objThis.options.isXHTML;
		},
		
		/*
		 * This function sets the glossaryData on glossary JSON/XML/XHTML load complete
		 * @param{String}
		 */
        glossaryDataLoadComplete: function(objData){
            var strXML;
            
            if (window.ActiveXObject) 
            {
                var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
                xmlObject.async = false;
                strXML = objData;
            }
            else 
            {
                strXML = new XMLSerializer().serializeToString(objData);
            }

            var glossaryData = $.xml2json(strXML);
            this.parseAndStoreGlossaryData(glossaryData);
        },
		
		/**
		 * This function parse and store the glossary definition.
		 * If any error is encountered, it displays an appropriate error message.
		 * @param {Object} objData
		 * @return {}
		 */
		parseAndStoreGlossaryData: function(objData)
		{
			var objGData = {};
			var strProp;
			var strTerm;
			var objReg = /[\s]*/gi;
			for(var str in objData.txt.letters)
			{
				var objLn = objData.txt.letters[str].ln;
				
				if (objLn) {
					if (objLn.length) {
						for (var i = 0; i < objLn.length; i++) {
							//console.log(objLn[i].word.term.toLowerCase());
							strTerm = objLn[i].word.term.toLowerCase();
							strTerm = strTerm.replace(objReg, "");
							objGData[strTerm] = {};
							
							for (strProp in objLn[i].word) {
								objGData[strTerm][strProp] = objLn[i].word[strProp].trim();
							}
							
						}
					}
					else
					{
						strTerm = objLn.word.term.toLowerCase();
						strTerm = strTerm.replace(objReg, "");
						objGData[objLn.word.term] = {};
						for (strProp in objLn.word) {
							objGData[objLn.word.term][strProp] = objLn.word[strProp].trim();
						}
					}
				}
			}
			
			objThis.member.glossaryData = objGData;
		},
        
        /**
         * This function finds the glossary definition and sets it in required element if no error is encountered.
         * If any error is encountered, it displays an appropriate error message.
         * @param {Object} strWord
         * @return {Boolean} Whether or not any error was encountered while setting the definition or not
         */
        setGlossaryDefinition: function(strWord){
			var strOriginalWord = strWord;
			strWord = strWord.toLowerCase();
			var objReg = /[\s]*/gi;
            var objThis = this;
            var fnDisplayGlossaryErrorAlertMessage = function(strLocalizationID){
                $("#alertTxt").html(GlobalModel.localizationData[strLocalizationID]);
                setTimeout(function(){
                    PopupManager.addPopup($("#alertPopUpPanel"), null, {
                        isModal: true,
                        isCentered: true,
                        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                        isTapLayoutDisabled: true
                    });
                }, 0);
            }
            
            //variable to set localization ID of error message if any error is encountered
            var strGlossaryErrorMessage = "GLOSSARY_DATA_ERROR";
            var glossaryData = objThis.member.glossaryData;
            var glossaryTextDiv = $(objThis.element).find('[id=glossaryTextDiv]');
            try {
	            
	            if(objThis.options.isXHTML)
	            {
	            	strGlossaryErrorMessage = "GLOSSARY_TERM_MISSING";
	            	var mathedData = $(glossaryData).find("#gloss-"+strWord);
	            	var strAudioPath = $(mathedData.next().find("audio")[0]).attr("src");
	            	$(mathedData).next().find("a").remove(":contains('return')");
					$(mathedData).next().find("p").css("white-space","normal");
	            	objThis.options.mediaSrc = getPathManager().getEPubAudioPath(strAudioPath);
	                objThis.options.oggMediaSrc = getPathManager().getEPubOGGAudioPath(strAudioPath);
	                if(!$(mathedData).next().is("dd"))
	                {
	                	strGlossaryErrorMessage = "GLOSSARY_TERM_MISSING";
	                    throw new Error("");
	                }
	                if( EPubConfig.ReaderType.toUpperCase() == AppConst.GRADE_PREKTO1 )
	                {
	                	var data = $(mathedData).attr("title");
	               		$(glossaryTextDiv).html('<span class="glossaryPopupHeadingText">' + data + '</span><br/>');
	                }
	                else
	                {
	                	var data = $(mathedData).next().html();
	               	 	$(glossaryTextDiv).html('<span class="glossaryPopupHeadingText">' + $(mathedData).attr("title") + '</span><br/>'+ data);
	                }
	                $(glossaryTextDiv).css("padding-left","40px");
	            	objThis.setAudioMedia();
	            	return true;
	            }
            	else
            	{
	                var bGlossaryTermFound = false;
	                strWord = strWord.replace(objReg, "");
	                if (glossaryData && glossaryData[strWord]) {
	                    var strGlossaryDef = '<span class="glossaryPopupHeadingText">' + strOriginalWord + '</span><br/>';
						bGlossaryTermFound = true;
	                    var strAudioPath = glossaryData[strWord].audio;
	                    if( EPubConfig.ReaderType.toUpperCase() != AppConst.GRADE_PREKTO1 )
	               	 	{
	                    	strGlossaryDef += glossaryData[strWord].def;
	                   }
	                }
	                
	                //if glossary term is missing
	                if (bGlossaryTermFound == false) {
	                    strGlossaryErrorMessage = "GLOSSARY_TERM_MISSING";
	                    throw new Error("");
	                }
	                
	                if (strAudioPath == "") {
	                    $(objThis.member.glossaryAudioButton).css("display", "none");
	                    $(glossaryTextDiv).css("padding-left", "0px");
	                    this.options.mediaSrc = strAudioPath;
	                }
	                else {
	                    $(objThis.member.glossaryAudioButton).css("display", "block");
	                    $(glossaryTextDiv).css("padding-left", "40px");
	                    
	                    this.options.mediaSrc = getPathManager().getEPubAudioPath(strAudioPath);
	                    this.options.oggMediaSrc = getPathManager().getEPubOGGAudioPath(strAudioPath);
	                }

	               $(glossaryTextDiv).html(strGlossaryDef);
	                objThis.setAudioMedia();
	                return true;
	            }
            } 
            catch (e) {
                fnDisplayGlossaryErrorAlertMessage(strGlossaryErrorMessage);
                return false;
            }
            
        },
        
        /**
         * This function will set the media file associated with the current glossary.
         */
        setAudioMedia: function(){
            var objThis = this;
            
            //if audio path is blank or null, do nothing in regard to audio for current glossary term
            if(objThis.options.mediaSrc == "" || objThis.options.mediaSrc == null)
                return;
            
            var fnDisplayAudioErrorMessage = function(){
                $("#alertTxt").html(GlobalModel.localizationData["GLOSSARY_AUDIO_PATH_INCORRECT"]);
                setTimeout(function(){
                    PopupManager.addPopup($("#alertPopUpPanel"), null, {
                        isModal: true,
                        isCentered: true,
                        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                        isTapLayoutDisabled: true
                    });
                }, 0);
            }
            
            //ensure if file path has both file name and file extension
            var temp = objThis.options.mediaSrc.split(".");
            
            if (temp.length == 1) {
                fnDisplayAudioErrorMessage();
                return;
            }
            else {
                var filename = temp[0].split("/")
                
                if (filename.length > 1 && (filename[filename.length - 1] == "" || filename[filename.length - 1] == null)) {
                    fnDisplayAudioErrorMessage();
                    return;
                }
            }
            
			var option = {
				autoplay: true,
				currentPageFrame: objThis.element,
				glossaryAudioPath: objThis.options.mediaSrc,
				controls: false
			};
		    
			objThis.objAudio = new AudioPlayer();
		    objThis.objAudio.setMediaElement(option);
            
        }
    })
})(jQuery);
