function NotePopup()
{
	var noteContainer = null;
	var self = this;
	var _componentsJSON = { "stickynoteTextArea" : { "event":"click,paste", "type" : "textarea" }, 
	                        "closeButton" : { "event":"click", "type" : "button" }, 
	                        "saveBtn" : { "event":"click", "type" : "button" }, 
				            "deleteBtn" : { "event":"click", "type" : "button" },
				            "savetoNoteBookButton" : { "event":"click", "type" : "checkbox" },
						    "tagInputBox" : { "event":"paste,keyup,change", "type" : "textinput" }, 
						    "tagAddBtn" : { "event":"click", "type" : "button" } };
	var _textArea = null;
	var _tempTagListArray = [];
	var _tagListArray = [];
	var _isNewNote = true; // must be set only in showPopup function
	var _currentNoteIcon = null; // must be set only in showPopup function
	var _currentNoteSelectionClass = null;
	var _isAlertPopupOpenedFromThisClass = false;
	var scrollviewcompRef = null;
	var _currentModeIsNote = false;
	
	this.addListeners = function ()
	{
		var container = noteContainer;
		scrollviewcompRef = $( "#bookContentContainer" ).data( 'scrollviewcomp' );
		if ( !( $.browser.msie && $.browser.version == 10 ) && !isWinRT && $.browser.version != 11 )
        {
        	var eventName = "input propertychange";
        }
        else
        {
        	var eventName = "keyup";
        }
        
        _textArea = container.find( '[type = "stickynoteTextArea"]' );
        _textArea.bind( "keydown" , function ( event ) { if(!(event.ctrlKey && event.which == 88) && event.which != 9 ){event.stopPropagation();}} );
        _textArea.bind( eventName, function () { self.textAreaTextChangeHandler(); } );
        container.find( '#commentTextArea' ).bind( eventName, function () { self.commentTextAreaTextChangeHandler(); } );
        noteContainer.find(".tagInputBox").bind( eventName, self.tagInputBoxchangeHandler );
        noteContainer.find( "#addCommentBtn" ).bind( "click", self.addCommentBtnClickHandler );
        container.find( '#commentTextArea' ).bind( "click", function () { self.commentTextAreaClickHandler(); } );
        noteContainer.find(".tagInputBox").bind('keydown', 'tab', function(evt){
         	noteContainer.find(".tagInputBox").blur();
			HotkeyManager.tabKeyHandler();
			return false;
		});
        noteContainer.find( "#addRecipientButton" ).bind( "click" , function (){
         	var launchStudentPanel = function ()
         	{
         		HotkeyManager.nullifyActiveElements();
         		var tempDiv = "<div id='divTomakeOnlyStudentPanelInteractive' class='overlayMaskStyleSemiTransparent'></div>";
         	  	$('#pg').append( tempDiv );
         	  	$("#divTomakeOnlyStudentPanelInteractive").css("display","block");
         	 	$("#studentPanel").addClass("forcefullyIncreaseZIndexofStudentPanel");
         	    $("#teacherStudentPanelBtn").trigger( "click" );
         	    
         	    $("#divTomakeOnlyStudentPanelInteractive").bind("click", function ( event ){
         	    	event.preventDefault();
         	    	event.stopPropagation();
         	    });
         	    
         	    $("#divTomakeOnlyStudentPanelInteractive").dblclick(function ( event1 ){
        	    	event1.preventDefault();
        	    	event1.stopPropagation();
         	    });
         	};
         	
         	var addEventOnTagsCanotBeSavedPopup = function ()
         	{
         		$( "#tagsCantBeSavedWithSharedAnnotationPopupCancelBtn").unbind( "click" ).bind( "click", function (){
         			PopupManager.removePopup();
         			lauchNotePopupAgain();
         		});
         		
         		$( "#tagsCantBeSavedWithSharedAnnotationPopupContinueBtn").unbind( "click" ).bind( "click", function (){
         			PopupManager.removePopup();
         			lauchNotePopupAgain();
         			launchStudentPanel();
         		});
         	};
         	
         	var lauchNotePopupAgain = function (){
         		var selectionClass = _currentNoteSelectionClass
         		var launcherObject = null;
         		if( self._isNewNote )
         		{
         			if( $( selectionClass ).html().substr(0,5) != "<span" )
					{
						$($( selectionClass )[0]).prepend( "<span></span>" );	
					}
					launcherObject = $($( selectionClass ).find('span')[0]);
         		}
				else if( selectionClass.indexOf( 'noteselection' ) == -1 && selectionClass.indexOf( 'tseselect' ) == -1 )
				{
					if( $( selectionClass ).html().substr(0,5) != "<span" )
					{
						$($( selectionClass )[0]).prepend( "<span></span>" );	
					}
					launcherObject = $($( selectionClass ).find('span')[0]);
				}
				else
				{
					if( selectionClass.indexOf( 'tseselect' ) == -1 )
					{
						selectionClass = "#btnStickyNote_" + selectionClass.replace( '.noteselection_', '' );
					}
					else
					{
						selectionClass = "#btnStickyNote_"  + selectionClass.replace( '.tseselect', '' );
					}
					launcherObject = $( selectionClass );
				}
         		PopupManager.addPopup( $( "#stickyNoteContainer" ), launcherObject , {
		                isModal : true,
		                hasPointer : true,
		                offsetScale : GlobalModel.currentScalePercent,
		                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
		                isTapLayoutDisabled : true
		            } );
		        setTimeout(function(){
		         	if ( !ismobile )
	         		 {
						self.removeFocusForcefully();         		 	
	         		 }
		         },100)
         		 
         	};
         	
         	 if( $("#stickyNoteContainer #tagList .tagOuter").length > 0 )
         	 {
         		$( "#stickyNoteContainer" ).css("z-index", 4000);
         	 	addEventOnTagsCanotBeSavedPopup();
         	 	PopupManager.addPopup( $("#tagsCantBeSavedWithSharedAnnotationPopup"), $( this ), {
	                isModal : false,
	                hasPointer : true,
	                offsetScale : GlobalModel.currentScalePercent,
	                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
	                isTapLayoutDisabled : true
            	} );
         	 }
         	 else
         	 {
         	 	launchStudentPanel();
         	 }
         } );
        
        for( var i in _componentsJSON )
        {
        	var item = _componentsJSON[ i ];
        	var eventArr = item.event.split( "," );
        	var selector = "";
        	
        	for( var j = 0; j < eventArr.length; j++ )
        	{
        		if( item.type == "textinput" )
        		{
        			selector = "." + i;
        		}
        		else
        		{
        			selector = "'[type = " + i + " ]'";
        		}
        		container.find( selector ).unbind( eventArr[ j ] ).bind( eventArr[ j ], self[ i + eventArr[ j ] + "Handler" ] );
        	}
        }
        
        $( scrollviewcompRef ).bind( "alertokclick", function ()
        {
        	if( !_isAlertPopupOpenedFromThisClass )
        	{
        		return;
        	}
        	_isAlertPopupOpenedFromThisClass = false;
        	
        	self.launchPopup( _currentNoteSelectionClass );
		} );
	};
	
	this.removeFocusForcefully = function ()
	{
		$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
        $("#inputToRemove").focus();
        $("#inputToRemove").blur();
        $("#inputToRemove").remove();  
	};
	
	this.showPopup = function ( isNew, noteIconID, currentSelectionClass ,isOpenNoteForEdit, editFromPanel )
	{
	    PopupManager.removePopup();
	    var noteObj = null;
	    var self = this;
		self._isNewNote = isNew;
		_currentNoteIcon = $( "#" + noteIconID );
		this.disableEnableComponents ( "all", "disable" );
		self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "enable" );
		noteContainer.find( "#tagInputBox" ).removeAttr("readonly");
		this.setSaveToNotebookCheckboxState( false );
		this.hideTagSection( true );
		noteContainer.find( "#commentTextArea" ).html( "" );
		_currentNoteSelectionClass = "." + currentSelectionClass;
		this.disableHighlightingOptions( false );
		if( !EPubConfig.My_Notebook_isAvailable )
		{
			$( ".savetonotebook" ).remove();
			$( ".stickyNoteTags" ).remove();
			$( ".tagBorder" ).remove();
			noteContainer.find( '[type = savetoNoteBookButton]' ).remove();
		}
		
		if( !self._isNewNote )
		{
		    _currentModeIsNote = ( ( currentSelectionClass.match("noteselection") == null ) && ( currentSelectionClass.match("tseselect") == null ) ) ?  false : true;
			
			var populateObjectInPopupForNote = function ( obj )
			{
			    if( obj != undefined )
			    {
			        self.setSaveToNotebookCheckboxState( !parseInt( obj.savetonote ) );// 0 means checkbox is selected 1 means it is not
			        if( ( GlobalModel.BookEdition == "SE" && obj.shared_with ) )
			        {
			        	self.hideTagSection( true );
			        }
			        else if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
			    	{
				    	if( !obj.shared_with && obj.tagsLists && obj.tagsLists.length > 0 )// student note or highlight with tags
				    	{
				    		self.hideTagSection( false );
				    	}
				    	else
				    	{
				    		self.hideTagSection( true );
				    	}
			    	}
				    else
			    	{
			    		self.hideTagSection( parseInt( obj.savetonote ) );
			    	}
    				_textArea.html ( obj.title );
    				if(obj.tagsLists != undefined)
    				{
    					for( var i = 0; i < obj.tagsLists.length; i++ )
    					{
    						self.addTag( obj.tagsLists[ i ] );
    					}
    				}
    				if( obj.comments )
    				{
    				    noteContainer.find( "#commentTextArea" ).html( obj.comments.body_text );
    				}
    				// TODO teacher sharing
    				if( obj.shared_with )
    				{
    				    noteContainer.find( "#commentTextArea" ).html( obj.title );
    				}
    				noteContainer.find( "#commentTextArea" ).attr( "contenteditable" , false );
    				_textArea.find( "a" ).unbind( "click" ).bind( "click",function (e) {e.stopPropagation();} );
    				noteContainer.find( "#commentTextArea" ).find( "a" ).unbind( "click" ).bind( "click",function (e) {e.stopPropagation();} );
			    }
			};
			
			var populateObjectInPopupForHighlight = function ( obj )
			{
			    var colorStr = obj.selectioncolor;
			    colorStr = colorStr.replace( "selectioncolor", "" );
			    noteContainer.find( ".styleButtonSelected" ).removeClass( "styleButtonSelected" ).addClass( "styleButton" );
			    if( colorStr.match( "_underline" ) != null )
			    {
			        $( noteContainer.find( ".styleContainer span" )[ 0 ] ).addClass( "styleButtonSelected" );
			        colorStr.replace( "_underline", "" );
			    }
			    else
			    {
			        $( noteContainer.find( ".styleContainer span" )[ 1 ] ).addClass( "styleButtonSelected" );
			    }
			    colorStr = parseInt( colorStr ) - 1;
			    noteContainer.find( ".colorButtonSelected" ).removeClass( "colorButtonSelected" );
			    $( noteContainer.find( ".colorButton" )[ colorStr ] ).addClass( "colorButtonSelected" );
			    
			    self.setSaveToNotebookCheckboxState( !parseInt( obj.savetonote ) );// 0 means checkbox is selected 1 means it is not
                if( GlobalModel.BookEdition == "SE" && obj.shared_with )
                {
                	self.hideTagSection( true );
                }
			    else if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
		    	{
			    	if( !obj.shared_with && obj.tagsLists && obj.tagsLists.length > 0 )// student note or highlight with tags
			    	{
			    		self.hideTagSection( false );
			    	}
			    	else
			    	{
			    		self.hideTagSection( true );
			    	}
		    	}
			    else
		    	{
		    		self.hideTagSection( parseInt( obj.savetonote ) );
		    	}
    					
                if(obj.tagsLists != undefined)
                {
                    for( var i = 0; i < obj.tagsLists.length; i++ )
                    {
                        self.addTag( obj.tagsLists[ i ] );
                    }
                }
                if( obj.comments )
				{
				    noteContainer.find( "#commentTextArea" ).html( obj.comments.body_text );
				}
                
                if( obj.userComment )
                {
                	if( obj.shared_with && obj.shared_with.results && GlobalModel.BookEdition == "SE"  )
                	{
                		noteContainer.find( "#commentTextArea" ).html( obj.userComment.body_text );
                	}
                	else
                	{
                		_textArea.html( obj.userComment.body_text );
                	}
                	
                }
			};
			
			var id = self.getIDFromCurrentSelectionClass();
			noteObj = self.findNoteObjectInGlobalModel( id );
			if( noteObj.shared_with && noteObj.type == "highlight" && GlobalModel.BookEdition == "SE"
				&& !noteObj.userComment)
			{
				if( !editFromPanel )
				{
					$( self ).trigger( "showHighlightDeleteConfirmationPopup", _currentNoteSelectionClass );
				}
				$( scrollviewcompRef ).trigger( 'closeOpenedPanel' );
				return;
			}
			// disable Highlight options if note i edited
            if( _currentModeIsNote )
            {
                self.disableHighlightingOptions( true );
                populateObjectInPopupForNote( noteObj );
            }
			else
			{
			    populateObjectInPopupForHighlight( noteObj );
			}
			this.disableEnableComponents ( "deleteBtn,savetoNoteBookButton", "enable" );
			_textArea.attr( "contenteditable" , false );
		}
		else
		{
		    _currentModeIsNote = false;
		   /* if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
		    {
		    	_currentModeIsNote = true;
		    }
		    else
		    { */
		        self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
		   // }
		}
		
		$( scrollviewcompRef ).trigger( 'closeOpenedPanel' );
		
    		if($(_currentNoteSelectionClass).length > 0 && !( $(_currentNoteSelectionClass).offset().top > 50 && 
    		   $(_currentNoteSelectionClass).offset().top < ( $(window).height() - 20 * GlobalModel.currentScalePercent ) && 
    		   $( _currentNoteSelectionClass).offset().left < ( $(window).width() - 30 ) ) )
        	{
                scrollviewcompRef.resetZoom();
                if ( EPubConfig.pageView.toUpperCase() == 'SCROLL' ) 
    			{
    	            if ( $( _currentNoteSelectionClass ).offset().top > $(window).height() - 20 * GlobalModel.currentScalePercent )
    	            {
    					var scrollBy = $( _currentNoteSelectionClass ).offset().top + $("#bookContentContainer2").scrollTop();
    	            	$("#bookContentContainer2").scrollTop( scrollBy - $(window).height()/2 );	
    	            }
    	        }
            }
        setTimeout( function () 
		{
			self.disableEnableSingleComponent( noteContainer.find( "#addRecipientButton" ), "enable" );
			GlobalModel.currentAnnotationObject = noteObj;
			GlobalModel.NewNoteSelectedStudents = [];
		    self.setLayoutBasedOnStudentEdition( noteObj );
			self.launchPopup( _currentNoteSelectionClass );
			
			if ( !ismobile && ( isOpenNoteForEdit && noteObj && noteObj.type == "stickynote" )) 
			{
				if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && !noteObj.shared_with )
				{
					return;
				}
	            _textArea.attr( "contenteditable" , true );
	            _textArea.find( "a" ).unbind( "click" );
	            _textArea.removeClass( "contentEditableFalseDiv" );
	            _textArea.focus();
	            self.placeCaretAtEnd( _textArea[ 0 ] );
	        }
			else
			{
				if( !ismobile )
		        {
		         $( "#pg" ).focus();
		        }
			}
    	    /*  if( ( isNew ) && ( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) )
	        {
	        	noteContainer.find( "#commentTextArea" ).focus();
	        } */
		}, 0 ); 
	};
	
	this.setLayoutBasedOnStudentEdition = function ( noteObj )
	{
	    var self = this;
	    if( self._isNewNote || ( noteObj && noteObj.type == "highlight" ) )
        {
        	noteContainer.find( ".colorContainer" ).css( "display", "block" );
        	noteContainer.find( ".styleContainer" ).css( "display", "block" );
        }
        else
        {
        	noteContainer.find( ".colorContainer" ).css( "display", "none" );
        	noteContainer.find( ".styleContainer" ).css( "display", "none" );
        }
	    switch( GlobalModel.BookEdition )
	    {
	        case "TSE":
	        noteContainer.find( "#addRecipientButton").css( "display", "block" );
	        if( GlobalModel.selectedStudentData )
	        {
	        	self.disableEnableSingleComponent( noteContainer.find( "#stickynoteTextArea" ), "enable" );
	            noteContainer.find( "#addRecipientButton").css( "display", "none" );
                if( ( self._isNewNote ) || ( noteObj.shared_with ) )// teacher standalone note or highlight
                {
                    noteContainer.find( "#commentTextArea" ).css( "display", "none" );
                    noteContainer.find( "#addCommentBtn" ).css( "display", "none" );
                    noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "none" );
                    $('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(90);
                    /*if( self._isNewNote || ( noteObj.type == "highlight" ) )
                    {
                    	noteContainer.find( ".colorContainer" ).css( "display", "block" );
                    	noteContainer.find( ".styleContainer" ).css( "display", "block" );
                    }
                    else
                    {
                    	noteContainer.find( ".colorContainer" ).css( "display", "none" );
                    	noteContainer.find( ".styleContainer" ).css( "display", "none" );
                    }*/
                    noteContainer.find( "#stickynoteTextArea" ).css( "display", "block" );
                    noteContainer.find( "#stickynoteTextArea" ).removeClass( "contentEditableFalseDiv" );
                    if( !self._isNewNote )
                    {
                        self.disableEnableComponents( "deleteBtn", "enable" );
                    }
                    else
                    {
                        // TODO set focus inside comment box
                    }
                }
                else // student note and highlight
                {
                    if( noteObj.type == "stickynote" )
                    {
                        if( noteObj.comments )
                        {
                            noteContainer.find( "#commentTextArea" ).css( "display", "block" );
                            $( "#stickyNoteContainer .addCommentBtnDownArrow" ).addClass( 'rotateAddCommentBtnDownArrow' );
	       					$('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(30);
	       					self.disableEnableComponents( "deleteBtn", "enable" );
                        }
                        else
                        {
                            noteContainer.find( "#commentTextArea" ).css( "display", "none" );
                            $( "#stickyNoteContainer .addCommentBtnDownArrow" ).removeClass( 'rotateAddCommentBtnDownArrow' );
	       					$('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(90);
	       					self.disableEnableComponents( "deleteBtn", "disable" );
                        }
                        noteContainer.find( "#stickynoteTextArea" ).css( "display", "block" );
                        noteContainer.find( "#addCommentBtn" ).css( "display", "block" );
                        noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "block" );
                        noteContainer.find( "#stickynoteTextArea" ).addClass( "contentEditableFalseDiv" );
                    }
                    else
                    {
                        noteContainer.find( "#commentTextArea" ).css( "display", "block" );
                        noteContainer.find( "#addCommentBtn" ).css( "display", "none" );
                        noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "none" );
						$('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(90);
                        if( noteObj.userComment )
                        {
                        	noteContainer.find( "#stickynoteTextArea" ).css( "display", "block" );
                        	noteContainer.find( "#stickynoteTextArea" ).addClass( "contentEditableFalseDiv" );
                        	$('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(30);
                        }
                        else
                        {
                        	noteContainer.find( "#stickynoteTextArea" ).css( "display", "none" );
                        	noteContainer.find( "#stickynoteTextArea" ).removeClass( "contentEditableFalseDiv" );
                        }
                        if( noteObj.comments )
                        {
                        	self.disableEnableComponents( "deleteBtn", "enable" );
                        }
                        else
                        {
                        	self.disableEnableComponents( "deleteBtn", "disable" );
                        }
                    }
                 	noteContainer.find( ".colorContainer" ).css( "display", "none" );
             		noteContainer.find( ".styleContainer" ).css( "display", "none" );
                    
                 }
                 
                 noteContainer.find( "#stickyNotePanelHeading" ).find( "span" ).css( "display", "none" );
                 noteContainer.find( ".savetonotebook" ).css( "display", "none" );
                 noteContainer.find( "#tagInputBox" ).css( "display", "none" );
                 noteContainer.find( "#tagAddBtn" ).css( "display", "none" );
                 noteContainer.find( ".stickyNoteTags .tagClsBtn" ).css( "display", "none" );
                 self.disableEnableComponents( "savetoNoteBookButton", "disable" );
	        }
	        else
	        {
	        	noteContainer.find( "#stickyNotePanelHeading" ).find( "span" ).css( "display", "block" );
	            noteContainer.find( "#addCommentBtn" ).css( "display", "none" );
	            noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "none" );
                noteContainer.find( "#commentTextArea").css( "display", "none" );
                noteContainer.find( "#stickynoteTextArea" ).css( "display", "block" );
                noteContainer.find( "#addRecipientButton").css( "display", "block" );
                noteContainer.find( "#tagInputBox" ).css( "display", "block" );
                noteContainer.find( "#tagAddBtn" ).css( "display", "block" );
                noteContainer.find( ".savetonotebook" ).css( "display", "block" );
                /*if( noteObj && noteObj.teachersSharedWith && noteObj.teachersSharedWith.results )
                {
                	if( noteObj.type == "highlight" )
                	{
                		self.disableEnableSingleComponent( noteContainer.find( "#stickynoteTextArea" ), "disable" );
                	}
                	else
                	{
                		self.disableEnableSingleComponent( noteContainer.find( "#stickynoteTextArea" ), "enable" );
                	}
                }
                else
                {
                	self.disableEnableSingleComponent( noteContainer.find( "#stickynoteTextArea" ), "enable" );
                }*/
	        }
            break;
            
            case "SE":
            {
            	noteContainer.find( "#addRecipientButton").css( "display", "none" );
                noteContainer.find( "#addCommentBtn" ).css( "display", "none" );
                noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "none" );
                noteContainer.find( "#commentTextArea" ).attr( "contenteditable", false );
                noteContainer.find( "#commentTextArea" ).addClass( "contentEditableFalseDiv" );
                if( !self._isNewNote && noteObj.comments )
                {
                    noteContainer.find( "#commentTextArea").css( "display", "block" );
                }
                else
                {
                    noteContainer.find( "#commentTextArea").css( "display", "none" );
                }
                
                if( !self._isNewNote && noteObj.shared_with )
                {
                    noteContainer.find( "#commentTextArea").css( "display", "block" );
                    noteContainer.find( ".colorContainer" ).css( "display", "none" );
                    noteContainer.find( ".styleContainer" ).css( "display", "none" );
                    noteContainer.find( "#stickynoteTextArea" ).css( "display", "none" );
                    noteContainer.find( "#stickyNotePanelHeading" ).find( "span" ).css( "display", "none" );
                    noteContainer.find( ".savetonotebook" ).css( "display", "none" );
                    self.disableEnableComponents( "savetoNoteBookButton", "disable" );
                    self.disableEnableComponents( "deleteBtn", "enable" );
                    noteContainer.find( "#stickynoteSaveBtn" ).css( "display", "none" );
                }
                else
                {
                	//noteContainer.find( ".colorContainer" ).css( "display", "block" );
                    //noteContainer.find( ".styleContainer" ).css( "display", "block" );
                    noteContainer.find( "#stickynoteTextArea" ).css( "display", "block" );
                    noteContainer.find( "#stickyNotePanelHeading" ).find( "span" ).css( "display", "block" );
                    noteContainer.find( ".savetonotebook" ).css( "display", "block" );
                    self.disableEnableComponents( "savetoNoteBookButton", "enable" );
                    noteContainer.find( "#stickynoteSaveBtn" ).css( "display", "block" );
                	if( noteObj && noteObj.comments )
                	{
                		if( noteObj.type == "stickynote" )
                		{
                			//noteContainer.find( ".colorContainer" ).css( "display", "none" );
                    		//noteContainer.find( ".styleContainer" ).css( "display", "none" );
                		}
                		else
                		{
                			if( !noteObj.userComment )
                			{
                				//noteContainer.find( "#stickynoteTextArea" ).css( "display", "none" );
                			}
                		}
                	}
                }
                break;
            }
            
            default:
               noteContainer.find( "#addCommentBtn" ).css( "display", "none" );
               noteContainer.find( "#addCommentBtnPlaceHolderForMobile" ).css( "display", "none" );
               noteContainer.find( "#commentTextArea").css( "display", "none" );
               noteContainer.find( "#addRecipientButton").css( "display", "none" );
            break;
	    }
	};
	this.disableEnableSingleComponent = function ( component, operation )
	{
	    if( operation == "disable" )
        {
            component.attr( "disabled", true );
            component.addClass( "ui-disabled" ).attr( "aria-disabled", true );
        }
        else
        {
            component.attr( "disabled", false );
            component.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
        }
	};
	
	this.createHighlightColors = function ()
	{
	    if( EPubConfig.ReaderType != "6TO12" )
	    {
	        noteContainer.find('.colorContainer').remove();
	        noteContainer.find('.styleContainer').remove();
	        return;
	    }
	    var self = this;
	    var nIconLeftPosition = 10;
        for (var i = 0; i < EPubConfig.Highlight_colors.length; i++) 
        {
            $( 'head' ).append( "<style id=highlightcolor"+ ( i + 1 ) +">.selectioncolor" + ( i + 1 ) + "{background:" + EPubConfig.Page_Highlight_colors[ i ] + 
                                "; cursor:pointer;}</style>");
                                
    		$( 'head' ).append( "<style>#color" + ( i + 1 ) + "{background:" + EPubConfig.Highlight_colors[ i ] + ";display:block; position: absolute; left:"
                                 + nIconLeftPosition + "px;}</style>" );
                                
            $( 'head' ).append( "<style id=highlightcolorUnderline"+ ( i + 1 ) +">.selectioncolor" + ( i + 1 ) + "_underline{border-bottom: 3px double " + EPubConfig.Highlight_colors[ i ] +
                                "; cursor:pointer;}</style>" );
            
            $( 'head' ).append( "<style>.selectioncolor" + ( i + 1 ) + "_icon{background: url('" + strReaderPath + 
                                "css/6TO12/images/stickynote/highlight-pencil_panel.png') " + EPubConfig.Highlight_colors[ i ] + ";}</style>" );
    
            noteContainer.find('.colorContainer').append( '<span id="color' + ( i + 1 ) + '" type="color" class="colorButton"></span>' );
            
            nIconLeftPosition += 29;
         }
         
         $( 'head' ).append( "<style>#highlightUnderline{background: url('" + strReaderPath +
                             "css/6TO12/images/main/highlightStyleIcon.png') no-repeat ;background-position:3px 3px;" + 
                             "height:21px;width:21px;left:10px;position:absolute;float:left;}</style>" );

         $( 'head' ).append( "<style>#highlightBack{background: url('" + strReaderPath + 
                             "css/6TO12/images/main/highlightStyleIcon.png') no-repeat ;" + 
                             "background-position:-28px 2px;height21px;width:21px;left:40px;position:absolute;}</style>" );
          
         $( noteContainer.find( ".colorContainer .colorButton" )[ 0 ] ).addClass( "colorButtonSelected" );
         self.addEventOnHighlightColors();
         self.addEventOnHighlightUnderlineAndBack();

	};
	
	this.addEventOnHighlightColors = function ()
	{
	    var self = this;	    
	    noteContainer.find( ".colorContainer .colorButton" ).bind( "click", function ( event )
	    {
	        event.preventDefault();
	        event.stopPropagation();
	        
	        if( $( event.target ).hasClass( "colorButtonSelected") )
	        {
	            return;
	        }
	        
	        noteContainer.find( ".colorContainer .colorButton" ).removeClass( "colorButtonSelected" );
	        $( event.target ).addClass( "colorButtonSelected" );
	        self.updateHighlightColor();
            self.disableEnableComponents( "saveBtn", true );
	    } );
	};
	
	this.addEventOnHighlightUnderlineAndBack = function ()
    {
        var self = this;
        var container = $( noteContainer.find( ".styleContainer" ) [ 0 ] );
        container.find( "[type='style']" ).bind( "click", function ( event )
        {
            if( $( event.target ).hasClass( "styleButtonSelected" ) )
            {
                return;
            }
            container.find( "[type='style']" ).removeClass( "styleButtonSelected" ).addClass( "styleButton" );
            $( event.target ).addClass( "styleButtonSelected" );
            self.updateHighlightColor();
            self.disableEnableComponents( "saveBtn", true );
        } );
    };
    
    this.updateHighlightColor = function ()
    {
       /* if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        {
            return;
        } */
        var self = this;
        var clone = _currentNoteSelectionClass;
        var temp = 0;
        var id = self.getIDFromCurrentSelectionClass();
        var noteObj = self.findNoteObjectInGlobalModel( id );
            
        if( !_currentModeIsNote || ( noteObj && noteObj.type == "highlight" ) )
        {
        	temp = noteContainer.find( ".colorContainer .colorButton" ).index( $( ".colorButtonSelected" ) ) + 1;
        	temp += ( $( noteContainer.find( ".styleContainer span" )[ 0 ] ).hasClass( "styleButtonSelected" ) ) ? "_underline" : "";
            self.restoreExistingAnnoClass( "selectioncolor" + temp +  " selection" + id );
            //$( clone ).addClass( "selectioncolor" + temp +  " selection" + id );
            _currentNoteSelectionClass = ".selection" + id ;
        }
        else
        {
        	var _currentNoteSelectionClassUpdated = "";
        	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        	{
        		var className = "tseselect" + id + " tseselect";
        		_currentNoteSelectionClassUpdated = ".tseselect" + id;
        	}
        	else
        	{
        		var className = "noteselection_" + id + " noteselection";
        		_currentNoteSelectionClassUpdated = ".noteselection_" + id;
        	}
            
            self.restoreExistingAnnoClass( className );
            _currentNoteSelectionClass = _currentNoteSelectionClassUpdated;
        }
    };
	
	this.launchPopup = function ( selectionClass )
	{
		$( this ).trigger( "resetEditFromPanel" ); 
		if( !EPubConfig.GroupSharing_isAvailable || !EPubConfig.AnnotationSharing_isAvailable )
		{
			$( "#addRecipientButton" ).remove();
		}
			
	    if( EPubConfig.ReaderType == '6TO12' && ismobile )
	    {
	        PopupManager.addPopup( $( "#stickyNoteContainer" ), null, {
                isModal : true,
                hasPointer : true,
                offsetScale : GlobalModel.currentScalePercent,
                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                isTapLayoutDisabled : true
            } );
	        $( "#stickyNoteContainer" ).addClass( "forcefullyPositionNotePopupOnTopForMobileDevices" );
	        $( "#popupArrowDiv" ).remove();
            return;
	    }
	    
	    var nHotSpotTopPos = $( selectionClass ).offset().left;
	    var nPopupHeight = $( "#stickyNoteContainer" ).height();
	    var launcherObject = null;
		if( self._isNewNote || (selectionClass.indexOf( 'noteselection' ) == -1 && selectionClass.indexOf( 'tseselect' ) == -1) )
		{
			if( $( selectionClass ).html().substr(0,5) != "<span" )
			{
				$($( selectionClass )[0]).prepend( "<span></span>" );	
			}
			launcherObject = $($( selectionClass ).find('span')[0]);
		}
		else
		{
			if( selectionClass.indexOf( 'tseselect' ) == -1 )
			{
				selectionClass = "#btnStickyNote_" + selectionClass.replace( '.noteselection_', '' );
			}
			else
			{
				selectionClass = "#btnStickyNote_"  + selectionClass.replace( '.tseselect', '' );
			}
			launcherObject = $( selectionClass );
		}
	    PopupManager.addPopup( $( "#stickyNoteContainer" ), launcherObject , {
                isModal : true,
                hasPointer : true,
                offsetScale : GlobalModel.currentScalePercent,
                popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
                isTapLayoutDisabled : true
        } );
        if( isWinRT && WinRTPointerType == "touch" )
        {
        	window.getSelection().removeAllRanges();
        }
	};
	
	this.findNoteObjectInGlobalModel = function ( id )
	{
	    
		for( var i = 0; i < GlobalModel.annotations.length; i++ )
		{
			if( GlobalModel.annotations[ i ].id == id )// && GlobalModel.annotations[ i ].type == str )
			{
				return GlobalModel.annotations[ i ];
			}
		}
	};
	
	this.disableEnableComponents = function ( componentType, operation  )
	{
		applyOperationOnComponent = function ( component )
		{
			self.disableEnableSingleComponent( component, operation );
		};
		
		disableEnableBasedOnType = function ( type, selector, operation )
		{
			switch( type )
			{
				case "button":
					applyOperationOnComponent( noteContainer.find( selector ) );			
				break;
				
				case "textarea":
					noteContainer.find( selector ).html( "" );
				break;
				
				case "textinput":
					noteContainer.find( selector ).val( "" );
				break;
				
				case "checkbox":
					applyOperationOnComponent( noteContainer.find( selector ) );
				break;
			}
		};
		
		if( componentType != "" && componentType != "all" )
		{
			var arr = componentType.split( "," );
			for ( var i = 0; i < arr.length; i++ )
			{
				if( _componentsJSON[ arr[ i ] ].type == "textinput" )
        		{
        			var selector = "." + arr[ i ];
        		}
        		else
        		{
        			var selector = "'[type = " + arr[ i ] + " ]'";
        		}
				disableEnableBasedOnType( _componentsJSON[ arr[ i ] ].type, selector, operation );
			}
		}
		else if( componentType == "all" )
		{
			for ( var j in _componentsJSON )
			{
				if( j == "closeButton" )
					continue;

				if( _componentsJSON[ j ].type == "textinput" )
        		{
        			var selector = "." + j;
        		}
        		else
        		{
        			var selector = "'[type = " + j + " ]'";
        		}
				disableEnableBasedOnType( _componentsJSON[ j ].type, selector, operation );
			}
		}
	};
	
	this.setSaveToNotebookCheckboxState = function ( check /* Boolean */ )
	{
		if( !check  )
		{
			noteContainer.find( '[type = savetoNoteBookButton]' ).removeClass( 'savetoNoteBookBtnChk' ).addClass( 'savetoNoteBookBtn' );
		}
		else
		{
			noteContainer.find( '[type = savetoNoteBookButton]' ).removeClass( 'savetoNoteBookBtn' ).addClass( 'savetoNoteBookBtnChk' );
		}
	},
	
	this.hideTagSection = function ( bool )
	{
		if( bool )
		{
			noteContainer.find( '.stickyNoteTags' ).css( "display", "none" );
			noteContainer.find( '.tagBorder' ).css( "display", "none" );
			_tempTagListArray = [];
			noteContainer.find( "#tagList .tagOuter" ).remove();
		}
		else
		{
			noteContainer.find( '.stickyNoteTags' ).css( "display", "block" );
			noteContainer.find( '.tagBorder' ).css( "display", "block" );
		}
	};
	
	this.addCommentBtnClickHandler = function ()
	{
	   var display = "";
	   if( noteContainer.find( "#commentTextArea").css( "display" ) == "block" )
	   {
	       $( "#stickyNoteContainer .addCommentBtnDownArrow" ).removeClass( 'rotateAddCommentBtnDownArrow' );
	       $('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(90);
	       display = "none";
	   } 
	   else
	   {
	       $( "#stickyNoteContainer .addCommentBtnDownArrow" ).addClass( 'rotateAddCommentBtnDownArrow' );
	       $('.mobileDevice #stickynoteTextArea, .mobileDevice .commentTextArea' ).height(30);
	       display = "block";
	   }
	   noteContainer.find( "#commentTextArea").css( "display" , display );
	   PopupManager.positionPopup();
	};
	
	this.closeButtonclickHandler = function ()
	{
		if( $("#tagsCantBeSavedWithSharedAnnotationPopup").css( "display" ) == "block" )
		{
			return;
		}
		PopupManager.removePopup();
		if( self._isNewNote )
		{
			self.deleteBtnclickHandler();
			//$( self ).trigger( "reCreateHighlight" );
			return;
		}
		//$( self ).trigger( "reCreateHighlight" );
		self.restoreExistingAnnoClass(); 
		if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
		{
			if( GlobalModel.currentAnnotationObject && GlobalModel.currentAnnotationObject.tempTeachersSharedWith )
			{
				delete GlobalModel.currentAnnotationObject.tempTeachersSharedWith;
			}
		}
	};
	
	this.restoreSelection = function ()
	{
	    var id = self.getIDFromCurrentSelectionClass();
	    var obj = {};
	    
	    for( var i = 0; i < GlobalModel.annotations.length; i++ )
	    {
	        obj = GlobalModel.annotations[ i ];
	        if( obj.id == id )// && !obj.shared_with )
	        {
	            switch( obj.type )
	            {
	                case "stickynote":
	                   if( ( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && obj.shared_with ) || ( obj.shared_with ) )
	                   {
	                   		$( _currentNoteSelectionClass ).removeClass().addClass( "tseselect" + id ).addClass( "tseselect" );
	                   }
	                   else
	                   {
	                   	$( _currentNoteSelectionClass ).removeClass().addClass( "noteselection_" + id ).addClass( "noteselection" );
	                   }
	                   
	                break;
	                
	                case "highlight":
	                   $( _currentNoteSelectionClass ).removeClass().addClass( "selection" + id ).addClass( obj.selectioncolor );
	                break;
	            }
	        }
	    }
	};
	
	this.saveBtnclickHandler = function ()
	{
		$( $( _currentNoteSelectionClass )[0] ).find( "span:first" ).remove( );
		try
		{
			if (! ( $.browser.msie && $.browser.version == 10 ) && !isWinRT && $.browser.version != 11 ) 
			{
			    self.modifyContent( $( _textArea )[ 0 ] );
			    self.modifyContent( noteContainer.find( "#commentTextArea" )[ 0 ] );
			}
			var sticknoteText = _textArea.text();
			var commentBoxText = noteContainer.find( "#commentTextArea" ).text();
			if ( isInteger( EPubConfig.maxCharactersInNotes ) == false )
			{
			    throw new Error( "Value of maxCharactersInNotes is not an integer" );
			}
			 
			if ( EPubConfig.maxCharactersInNotes < 1 )
			{
			    throw new Error( "Max character limit in notes is set to less than one" );
			}
             
            if( sticknoteText.length > EPubConfig.maxCharactersInNotes || commentBoxText.length > EPubConfig.maxCharactersInNotes )    
            {
            	self.showAlertPopup( GlobalModel.localizationData["STICKY_NOTE_LIMIT"].replace("$1", EPubConfig.maxCharactersInNotes) );
            } 
            else
            {
            	var obj = {};
            	var eventName = "saveNewNote";
            	if( !_currentModeIsNote )
            	{
            	    eventName = "saveNewHighlight";
            	    obj.selectionClass = _currentNoteSelectionClass;
            	}
				obj.tagsLists = _tempTagListArray.slice( 0 );
				obj.title = _textArea.html();
                obj.id = self.getIDFromCurrentSelectionClass();
                obj.isSwapping = false;
            	if( self._isNewNote )
            	{
            	    if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && _currentModeIsNote )
            	    {
            	        obj.title = noteContainer.find( "#stickynoteTextArea" ).html();
            	    }
            	    
					$( self ).trigger( eventName, [ obj ] );   		
            	}
            	else
            	{
            	    var tempObj = self.findNoteObjectInGlobalModel( obj.id );
            	    
            	    if( _currentModeIsNote )
            	    {
            	        if( tempObj.type == "stickynote" )
            	        {
            	            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            	            {
            	                obj.title = noteContainer.find( "#commentTextArea" ).html();
            	                if( tempObj.shared_with )
            	                {
            	                	obj.title = noteContainer.find( "#stickynoteTextArea" ).html();
            	                	eventName = "updateExistingNote"; 
            	                }
            	                else
            	                {
	            	               	if( tempObj.comments )// || tempObj.shared_with )
	            	                {
	            	                    eventName = "updateCommentToNote"; 
	            	                }
	            	                else
	            	                {
	            	                    eventName = "addCommentToNote"; 
	            	                }
            	                }
            	                
            	            }           	            
            	            else if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
            	            {
            	            	self.addRemoveSharedStudents( tempObj );
            	                eventName = "updateExistingNote"; 
            	            }
            	            else
            	            {
            	            	eventName = "updateExistingNote"; 
            	            }
            	        }
            	        else
            	        {
            	            /* obj.selectionClass = _currentNoteSelectionClass;
            	            $( self ).trigger( "deleteAnnotation", [ obj ] );
            	            obj.isSwapping = true;
            	            PopupManager.removePopup();
            	            if( tempObj.comments )
                            {
                            	$( "#btnStickyNote_" + obj.id ).removeClass( "btnStickyNoteTeacherComment" );
                            	$( "#btnStickyNote_" + obj.id ).removeClass( "btnStickyNoteTeacherCommentHighlight" );
                            }
            	            $( self ).trigger( "saveNewNote", [ obj ] ); 
            	            return; */
            	        	if( tempObj.userComment )
                        	{
            	        		obj.userComment = {};
                        		obj.userComment.body_text = _textArea.html();
                        	}
                        	else
                        	{
                        		obj.userComment = {};
                        		obj.userComment.body_text = _textArea.html();
                        	}
            	        	PopupManager.removePopup();
            	        	if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
		            	    {
		            	    	self.addRemoveSharedStudents( tempObj );
		            	    }
            	        	$( self ).trigger( "updateExistingHighlight", [ obj ] ); 
            	        	return;
            	        }
            	    }
            	    else
            	    {
            	        if( tempObj.type == "highlight" )
                        {
            	        	if( tempObj.userComment )
                        	{
            	        		obj.userComment = {};
                        		obj.userComment.body_text = _textArea.html();
                        	}
                        	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
		            	    {
		            	        obj.title = noteContainer.find( "#commentTextArea" ).html();
		            	        if( tempObj.shared_with )
		            	        {
		            	        	eventName = "updateExistingHighlight";
		            	        }
		            	        else
		            	        {
		            	        	if( tempObj.comments )// || tempObj.shared_with )
	            	                {
	            	                	obj.type = 2;
	            	                    eventName = "updateCommentToHighlight"; 
	            	                }
	            	                else
	            	                {
	            	                    eventName = "addCommentToHighlight"; 
	            	                }
		            	        }
		            	    }
		            	    else if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
		            	    {
		            	    	self.addRemoveSharedStudents( tempObj );
            	                eventName = "updateExistingHighlight"; 
		            	    }
		            	    else
		            	    {
		            	    	eventName = "updateExistingHighlight";
		            	    }
                        }
                        else
                        {
                            /* console.log( obj, " Swapping of Note to Highlight");
                            obj.selectionClass = _currentNoteSelectionClass;
                            $( self ).trigger( "deleteAnnotation", [ obj ] );
                            obj.isSwapping = true;
                            PopupManager.removePopup();
                            if( tempObj.comments )
                            {
                            	$( "#btnStickyNote_" + obj.id ).removeClass( "btnStickyNoteTeacherComment" );
                            	$( "#btnStickyNote_" + obj.id ).removeClass( "btnStickyNoteTeacherCommentHighlight" );
                            }
                            $( self ).trigger( "saveNewHighlight", [ obj ] );
                            return; */
                        }
            	    } 
            	    //console.log( obj, " This is ServiceManager Object")
            		$( self ).trigger( eventName, [ obj ] );
            	}
            	PopupManager.removePopup();
            }      
		}
		catch ( error )
		{
		    PopupManager.removePopup();
		}
	};
	
	this.addRemoveSharedStudents = function( tempObj )
	{
		if( tempObj.tempTeachersSharedWith && tempObj.tempTeachersSharedWith.results )// this means done button is clicked
		{
			var oldList = [];
			var deletionIdArr = [];
        	if( tempObj.teachersSharedWith && tempObj.teachersSharedWith.results && 
        		tempObj.teachersSharedWith.results.length > 0 )
        	{
        		oldList = tempObj.teachersSharedWith.results;
        		for( var oldIndex = 0; oldIndex < oldList.length; oldIndex++ )
        		{
        			deletionIdArr.push( oldList[ oldIndex ].id );
        		}
        		tempObj.teachersSharedWith = {};
		        tempObj.teachersSharedWith.results = [];//tempObj.tempTeachersSharedWith;
        		if( !tempObj.tempTeachersSharedWith.results || !tempObj.tempTeachersSharedWith.results.length ||
        				tempObj.tempTeachersSharedWith.results.length == 0 )
        		{
        			delete tempObj.teachersSharedWith;
        			delete tempObj.tempTeachersSharedWith;
					if( tempObj.type != "highlight" )
					{
						$( "#btnStickyNote_" + tempObj.id ).removeClass( "btnStickyNoteGroupSharing" );
					}
					else
					{
						$( "#btnStickyNote_" + tempObj.id ).css( "top", "0px");
						$( "#btnStickyNote_" + tempObj.id ).css( "left", "0px");
						$( "#btnStickyNote_" + tempObj.id ).css( "display", "none" );
						$( "#btnStickyNote_" + tempObj.id ).removeClass( "btnHighlightGroupSharing" );
					}
        		}
        		ServiceManager.removeSharedStudents( deletionIdArr, function (){
        			
        			if( !tempObj.tempTeachersSharedWith || !tempObj.tempTeachersSharedWith.results || !tempObj.tempTeachersSharedWith.results.length ||
        				tempObj.tempTeachersSharedWith.results.length == 0 )
        			{
        				return;
        			}
        			var newList = tempObj.tempTeachersSharedWith.results;
		        	var newListArr = [];
		        	for( var newIndex = 0; newIndex < newList.length; newIndex++ )
		        	{
		        		var str = $("#studentPanel").find("[platformid=" + newList[ newIndex ].platformid + "]").attr("refid");
		        		newListArr.push( str + "," + newList[ newIndex ].platformid );
		        		tempObj.teachersSharedWith.results.push( newList[ newIndex ] );
		        	}
		        	if( tempObj.type != "highlight" )
					{
						$( "#btnStickyNote_" + tempObj.id ).addClass( "btnStickyNoteGroupSharing" );
					}
					else
					{
						$( "#btnStickyNote_" + tempObj.id ).addClass( "btnHighlightGroupSharing" );
					}
		        	
					//var _obj = {};
					//_obj.id = tempObj.id;
					//$( self ).trigger( "repositionIcon", [ _obj ] );
		            delete tempObj.tempTeachersSharedWith;
		        	ServiceManager.addSharedStudents( tempObj.annotationID, newListArr, function ( data )
		        	{
		        		if( data && !data.length )
		        		{
		        			tempObj.teachersSharedWith.results.push( data )
		        		}
		        		else if( data.length )
		        		{
		        			tempObj.teachersSharedWith.results = data;
		        		}
		        	} );
        		} );
        	}
        	else
        	{
        		var newList = tempObj.tempTeachersSharedWith.results;
        		
	        	var newListArr = [];
	        	for( var newIndex = 0; newIndex < newList.length; newIndex++ )
	        	{
	        		var str = $("#studentPanel").find("[platformid=" + newList[ newIndex ].platformid + "]").attr("refid");
	        		newListArr.push( str + "," + newList[ newIndex ].platformid );
	        	}
	        	tempObj.teachersSharedWith = {};
	            tempObj.teachersSharedWith = tempObj.tempTeachersSharedWith;
	            delete tempObj.tempTeachersSharedWith;
	            if( newList.length == 0 )
	            {
	            	delete tempObj.teachersSharedWith;
	            }
	            if( newList.length > 0 )
				{
					if( tempObj.type != "highlight" )
					{
						$( "#btnStickyNote_" + tempObj.id ).addClass( "btnStickyNoteGroupSharing" );
					}
					else
					{
						var _obj = {};
						_obj.id = tempObj.id;
						$( self ).trigger( "repositionIcon", [ _obj ] );
						$( "#btnStickyNote_" + tempObj.id ).addClass( "btnHighlightGroupSharing" );
					}
				}
	        	ServiceManager.addSharedStudents( tempObj.annotationID, newListArr, function ( data )
	        	{
	        		if( data && !data.length )
	        		{
	        			tempObj.teachersSharedWith.results = [];
	        			tempObj.teachersSharedWith.results.push( data );
	        		}
	        		else
	        		{
	        			tempObj.teachersSharedWith.results = data;
	        		}
	        	} );
        	}
        	
		}
	};
	
	this.tagInputBoxchangeHandler = function ()
	{
	    var stateOfButton = self.checkIfButtonIsToBeEnabled ( "tagAddBtn" );
        self.disableEnableComponents( "tagAddBtn", stateOfButton );
	};
	
	this.showAlertPopup = function ( message )
	{
		_textArea.blur();
        PopupManager.removePopup();
        if ( ismobile )
        {
        	document.onselectionchange = null;
            $("#annotation-bar").css("display","none");
            $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
            $("#inputToRemove").focus();
            $("#inputToRemove").blur();
            $("#inputToRemove").remove();   
        }
        $( "#alertTxt" ).html( message );
        PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        } );
        
        _isAlertPopupOpenedFromThisClass = true;
	};
	
	this.deleteBtnclickHandler = function ()
	{
		$( $( _currentNoteSelectionClass )[0] ).find( "span:first" ).remove( );
		if( self._isNewNote )
		{
			self.restoreExistingAnnoClass();
			$( "#btnStickyNote" ).remove();
		}
		else
		{
		    var id = self.getIDFromCurrentSelectionClass();
            var noteObj = self.findNoteObjectInGlobalModel( id );
		    if( ( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) || ( noteObj.shared_with ) )
		    {
		        $( self ).trigger( "showCommentDeletionConfirmationPopup", [ _currentNoteSelectionClass.replace( ".", "" ) ] );
		    }
		    else
		    {
		        $( self ).trigger( "showDeleteConfirmationPopup", [ _currentNoteSelectionClass.replace( ".", "" ) ] );
		    }
		}
	};
	
	this.restoreExistingAnnoClass = function ( classNameToAppend )
	{
		var addClassToAnnoTag = function ( annoTag )
		{
			annoTag.removeClass()
			var annoArr = annoTag.attr( "annoArr" ).split( "," );
			var len = annoArr.length;
			var isFirstHighlightEncountered = false;
			var isFirstUnderlineHighlightEncountered = false;
			
			if( classNameToAppend == undefined )
			{
				classNameToAppend = "";
			}
			for( var index = len - 1; index >=0; index -- )
			{
				var annoItem = annoArr[ index ];
				var idColorArr = annoItem.split( ":" );
				if( classNameToAppend.split(" ")[1] != idColorArr[ 0 ] )
				{
					
					if( idColorArr[ 1 ].indexOf('underline') != -1 )
					{
						if( isFirstUnderlineHighlightEncountered == false )
						{
							annoTag.addClass( idColorArr[ 0 ] );
							annoTag.addClass( idColorArr[ 1 ] );
						}
						else
						{
							annoTag.addClass( idColorArr[ 0 ] );
						}
						isFirstUnderlineHighlightEncountered = true;
					}
					else if( idColorArr[ 1 ].indexOf('selectioncolor') != -1 )
					{
						if( isFirstHighlightEncountered == false )
						{
							annoTag.addClass( idColorArr[ 0 ] );
							annoTag.addClass( idColorArr[ 1 ] );
						}
						else
						{
							annoTag.addClass( idColorArr[ 0 ] );
						}
						isFirstHighlightEncountered = true;
					}
					else
					{
						annoTag.addClass( idColorArr[ 0 ] );
					}
				}
			}
			
			if( classNameToAppend != undefined )
			{
				if( classNameToAppend.indexOf('selectioncolor') != -1 )
				{
					//to change the precedence of current selected color
					var temp = noteContainer.find( ".colorContainer .colorButton" ).index( $( ".colorButtonSelected" ) ) + 1;
					var isUnderline = false;
					if( $( noteContainer.find( ".styleContainer span" )[ 0 ] ).hasClass( "styleButtonSelected" ) )
					{
						isUnderline = true;
					}
					if( isUnderline )
					{
						$( "#highlightcolorUnderline" + temp ).remove();
						$("head").append("<style id=highlightcolorUnderline"+ temp +">.selectioncolor" + temp + "_underline{border-bottom: 3px double " + EPubConfig.Highlight_colors[ temp - 1 ] +
                                "; cursor:pointer;}</style>");
					}
					else
					{
						$( "#highlightcolor" + temp ).remove();
						$("head").append("<style id=highlightcolor"+ temp +">.selectioncolor" + temp + "{background:" + EPubConfig.Page_Highlight_colors[ temp - 1 ] + 
                                "; cursor:pointer;}</style>");	
					}
				}
				annoTag.addClass( classNameToAppend );
			}
			
		}
		var _currentNoteSelectionArr = $( _currentNoteSelectionClass );
	    for ( var i = 0; i < _currentNoteSelectionArr.length ; i++ )
	    {
	    	var existingAnnoInfo = $( _currentNoteSelectionArr[i] ).attr( "annoArr" );
	    	if( existingAnnoInfo != undefined )
	    	{
	    		existingAnnoArr = existingAnnoInfo.split(",");
	    		addClassToAnnoTag( $( _currentNoteSelectionArr[i] ) );
	    	}
	    	else if( classNameToAppend != undefined )
	    	{
	    		$( _currentNoteSelectionArr[i] ).removeClass().addClass( classNameToAppend );
	    	}
	    	else
	    	{
	    		$( _currentNoteSelectionArr[i] ).removeClass();
	    	}
	    }
		     
	};
	
	/* Text area */
	this.stickynoteTextAreapasteHandler = function ()
	{
		self.textAreaTextChangeHandler();
	};
	
	this.textAreaTextChangeHandler = function ()
	{
		self.disableEnableSingleComponent( noteContainer.find( "#addRecipientButton" ), "enable" );
	    if( $( _textArea ).attr("contenteditable") == "false" )
	    {
	        return;
	    }
		var anchorArr = $( _textArea ).find( '[ type = custom ]' );
        for( var i = 0; i < anchorArr.length; i++ )
        {
        	var strToMatch = $( anchorArr[i] ).attr( "href" );
        	if($( anchorArr[i] ).text().indexOf("http") == -1)
        	{
        		strToMatch = $( anchorArr[i] ).attr( "href" ).replace( "http://","" );
        	}
        	if( $( anchorArr[i] ).text() != strToMatch )
        	{
        		$( anchorArr[i] ).remove();
       	 	}
       	}
		if ( $.trim( _textArea.text() ) != "" || _textArea.html().toString().indexOf( "<img" ) > -1) 
		{
		    _currentModeIsNote = true;
			self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
			//self.disableHighlightingOptions( true );
			self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "enable" );
			noteContainer.find( "#tagInputBox" ).removeAttr("readonly");
			if( self._isNewNote )
			{
				if( !noteContainer.find( '.colorContainer' ).hasClass( "ui-disabled" ) )
				{
					self.disableHighlightingOptions( true );
					self.updateHighlightColor();
				}
			}
		}
		else
		{
	    	var id = self.getIDFromCurrentSelectionClass();
        	var obj = self.findNoteObjectInGlobalModel( id );
        	self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "enable" );
        	noteContainer.find( "#tagInputBox" ).removeAttr("readonly");
        	if( self._isNewNote )
			{
				self.updateHighlightColor();
			}
        	else
        	{
        		self.disableEnableSingleComponent( noteContainer.find( "#addRecipientButton" ), "disable" );
        	}
        	
        	if( obj )
        	{
        		if( obj.type == "highlight" )
        		{
        			self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
        			self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "enable" );
        			noteContainer.find( "#tagInputBox" ).removeAttr("readonly");
        		}
        		else
        		{
        			self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "disable" );
        			self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "disable" );
        			noteContainer.find( "#tagInputBox" ).attr("readonly","readonly");
        		}
        		if( !obj.comments )
        		{
        			_currentModeIsNote = false;
				    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData
				    && obj.teachersSharedWith && obj.teachersSharedWith.results && obj.type == "stickynote" )
				    {
				    	self.disableEnableSingleComponent( noteContainer.find( "#addRecipientButton" ), "disable" );
				        //self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "disable" );
				        //self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "disable" );
				        return;
				    }
				    else
				    {
				    	//self.updateHighlightColor();
				        //self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
				        //self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "enable" );
				    }
	        		
        		}
        		else // has comment
        		{
        			//self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "disable" ); 
        			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
        			//self.disableEnableSingleComponent( noteContainer.find( ".stickyNoteTags" ), "disable" );
        		}
        	}
		    else // behaving as isNewNote
		    {
		    	_currentModeIsNote = false;	
		    	self.updateHighlightColor();
		    	self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
		    }
        	if( self._isNewNote )
			{
        		self.disableHighlightingOptions( false );
			}
		}
		
	};
	
	this.commentTextAreaTextChangeHandler = function ()
	{
	    if( noteContainer.find( "#commentTextArea" ).attr("contenteditable") == "false" )
        {
            return;
        }
	    var anchorArr = $( "#commentTextArea" ).find( '[ type = custom ]' );
        for( var i = 0; i < anchorArr.length; i++ )
        {
            var strToMatch = $( anchorArr[i] ).attr( "href" );
        	if($( anchorArr[i] ).text().indexOf("http") == -1)
        	{
        		strToMatch = $( anchorArr[i] ).attr( "href" ).replace( "http://","" );
        	}
        	if( $( anchorArr[i] ).text() != strToMatch )
            {
                $( anchorArr[i] ).remove();
            }
        }
    //    _currentModeIsNote = true;
        if ( $.trim( noteContainer.find( "#commentTextArea" ).text() ) != "" ) 
        {
            self.disableEnableComponents( "saveBtn", "enable" );
          //  self.disableEnableComponents( "savetoNoteBookButton", "enable" );
        }
        else
        {
            self.disableEnableComponents( "saveBtn", "disable" );
           // self.disableEnableComponents( "savetoNoteBookButton", "disable" );
        }
	};
	
	this.commentTextAreaClickHandler = function()
	{
		if( noteContainer.find( '#commentTextArea' ).hasClass( "contentEditableFalseDiv" ) )
	    {
	        // do nothing
	    }
	    else
	    {
		    setTimeout( function(){ noteContainer.find( '#commentTextArea' ).find( "a" ).unbind( "click" );
		    noteContainer.find( '#commentTextArea' ).attr( "contenteditable" , true );
		    noteContainer.find( '#commentTextArea' ).focus();
		    noteContainer.find( '#commentTextArea' ).removeClass( "contentEditableFalseDiv" ); } , 0 );
	    }
	},
	
	this.disableHighlightingOptions = function ( bool )
	{
	    var self = this;
	    var id = self.getIDFromCurrentSelectionClass();
        var obj = self.findNoteObjectInGlobalModel( id );
	    
	    noteContainer.find( ".colorContainer" ).attr( "disabled", bool );
	    noteContainer.find( ".styleContainer" ).attr( "disabled", bool );
	    if( bool )
	    {
    	    noteContainer.find( ".colorContainer" ).addClass( "ui-disabled" ).attr( "aria-disabled", bool );	  
            noteContainer.find( ".styleContainer" ).addClass( "ui-disabled" ).attr( "aria-disabled", bool );
	    }
	    else
	    {
            noteContainer.find( ".colorContainer" ).removeClass( "ui-disabled" ).attr( "aria-disabled", bool );      
            noteContainer.find( ".styleContainer" ).removeClass( "ui-disabled" ).attr( "aria-disabled", bool ); 
	    }
	    noteContainer.find( ".colorContainer .colorButton" ).removeClass( "colorButtonSelected" );        
        noteContainer.find( ".styleContainer span" ).removeClass( "styleButtonSelected" ).addClass( "styleButton" ); 
	    if( self._isNewNote || ( obj && obj.type == "stickynote" ) )
	    {
    	    $( noteContainer.find( ".colorContainer .colorButton" )[ EPubConfig.Highlight_colors.indexOf( EPubConfig.Highlights_default_color ) ] ).addClass( "colorButtonSelected" );
    	    $( noteContainer.find( ".styleContainer span" )[ 1 ] ).addClass( "styleButtonSelected" );
	    }
	    else
	    {
	        var colorStr = obj.selectioncolor;
            colorStr = colorStr.replace( "selectioncolor", "" );
            if( colorStr.match( "_underline" ) != null )
            {
                $( noteContainer.find( ".styleContainer span" )[ 0 ] ).addClass( "styleButtonSelected" );
                colorStr.replace( "_underline", "" );
            }
            else
            {
                $( noteContainer.find( ".styleContainer span" )[ 1 ] ).addClass( "styleButtonSelected" );
            }
            colorStr = parseInt( colorStr ) - 1;
            $( noteContainer.find( ".colorButton" )[ colorStr ] ).addClass( "colorButtonSelected" );
	    } 
	};
	
	this.stickynoteTextAreaclickHandler = function ()
	{
		var id = self.getIDFromCurrentSelectionClass();
        var obj = self.findNoteObjectInGlobalModel( id );
        $(".focusglow").removeClass("focusglow");
        HotkeyManager.currentActiveElementIndex = 1;
	    if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && ( obj && !obj.shared_with ) )
	    {
	        // do nothing
	    }
	    else
	    {
		   setTimeout( function() { _textArea.find( "a" ).unbind( "click" );
		   	_textArea.attr( "contenteditable" , true );
		   	_textArea.focus();
		    _textArea.removeClass( "contentEditableFalseDiv" ); } , 0 );
	    }
	};
	
	
	/* Tag */
	this.savetoNoteBookButtonclickHandler = function ()
	{
		self.setSaveToNotebookCheckboxState( !noteContainer.find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) );
		var ischeckBoxChecked = noteContainer.find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' );
		
		if( !ischeckBoxChecked )
		{
			self.hideTagSection ( true );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
		}
		else
		{
			self.hideTagSection ( false );
		}
		self.disableEnableComponents( "saveBtn", "enable" );
		PopupManager.positionPopup();
	};
	
	this.tagInputBoxpasteHandler = function ()
	{	   
	    setTimeout(function () {
	        noteContainer.find(".tagInputBox").trigger("keyup");
	    }, 100);
	};
	
	this.tagInputBoxkeyupHandler = function ( event )
	{
		var stateOfButton = self.checkIfButtonIsToBeEnabled ( "tagAddBtn" );
		self.disableEnableComponents( "tagAddBtn", stateOfButton );
		if( stateOfButton == "enable" )
		{                                    
			$(noteContainer.find( '[id = tagInputBox]' )).autocomplete({
                source : _tagListArray
            });
			if( event.keyCode == "13" )
			{
				self.disableEnableComponents( "saveBtn", "enable" );
				self.tagAddBtnclickHandler();
			}
		}
	};
	
	this.removeScriptTag = function( textContent ) 
	{
	    textContent = textContent.replace( /<script(.*?)>/gim, "" );
	    textContent = textContent.replace( /<\/script(.*?)>/gim, "" );
	    return textContent;
	};
	
	/**
	 * This function will modify text part from content. It will convert url written by user into clickable links
	 */
	this.modifyContent = function ( el )
	{
	
	    var arrContentEditableDivChildren = [];
	    for ( var startIndex = 0, endIndex = el.childNodes.length; startIndex < endIndex; startIndex++ ) 
	    {
	        var item = el.childNodes[ startIndex ];
	        
	        //if node is of type text
	        if ( item.nodeType == 3 ) 
	        {
	            var strModifiedText = this.modifyContentforRegExp( item.data );
	            arrContentEditableDivChildren.push( strModifiedText );
	        }
	        else 
	        {
	            if ( $(item).html() != "" ) {
	                var elTemp = this.modifyContent( $(item)[0] );
	                
	                //ensuring all links (including copied from other websites) open in new tab
	                if ( item.nodeName == "A" )
	                {
	                	elTemp[0].target = "_blank";
	                }
	                
	                arrContentEditableDivChildren.push( elTemp );
	            }
	            else {
	                arrContentEditableDivChildren.push( item );
	            }
	        }    
	    }
	    
	    $(el).html( "" );
	    for ( var i = 0; i < arrContentEditableDivChildren.length; i++ ) 
	    {
	        $(el).append( arrContentEditableDivChildren[i] );
	    }
	    
	    return $(el);
	};
	
	/**
	 * This function will modify content to find regular Expression
	 */
	this.modifyContentforRegExp = function( StickyContent )
	{
	    StickyContent = StickyContent.replace(/<script(.*?)>/gim, "");
	    StickyContent = StickyContent.replace(/<\/script(.*?)>/gim, "");
	    StickyContent = StickyContent.replace(/\&nbsp\;/gim, " ");
	    StickyContent = StickyContent.replace(/<a([^<>]+)>/gim, "");
	    StickyContent = StickyContent.replace(/<\/a>/gim, "");
	    StickyContent = StickyContent.replace(/([^\/\'])(http\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/([^\/\'])(https\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/([^\/])(www\.[^ \,<>\.]+.\.[^ \,<>]+)/gim, "$1<a href='http:\/\/$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/^(www\.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='http:\/\/$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	    StickyContent = StickyContent.replace(/^(http\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	    StickyContent = StickyContent.replace(/^(https\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	
	    return StickyContent;
	};
	
	this.checkIfButtonIsToBeEnabled = function ( type )
	{
		switch ( type )
		{
			case "tagAddBtn" :
				if( $.trim( noteContainer.find( ".tagInputBox" ).val() ) == "" )
				{
					return "disable";
				}
				else
				{
					return "enable";
				}
			break;
			
			case "saveBtn" : 
				if( $.trim( noteContainer.find( '[type = "stickynoteTextArea"]' ).text() ) == "" )
				{
					return "disable";
				}
				else
				{
					return "enable";
				}
			break;
			
			case "deleteBtn" :
			break;
		}
	};
	
	this.tagAddBtnclickHandler = function ()
	{
		var tagInputBoxRef = noteContainer.find( '[id = "tagInputBox"]' );
		var id = self.getIDFromCurrentSelectionClass();
		noteObj = self.findNoteObjectInGlobalModel( id );
		$(tagInputBoxRef).val( self.removeScriptTag( $( tagInputBoxRef ).val() ) );
		var tagStr = $.trim( tagInputBoxRef.val() );
		$(".ui-autocomplete").css("display", "none");
		if( _tempTagListArray.length == EPubConfig.Tags_Per_Note )
		{
			self.showAlertPopup( GlobalModel.localizationData[ "MAXIMUM_TAG_ADDED" ] );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
			return;
		}
		else if( $.inArray( tagStr, _tempTagListArray ) != -1 )
		{
			self.showAlertPopup( GlobalModel.localizationData[ "SAME_TAG_ADDED" ] );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
			return;
		}
		self.addTag( tagStr );
        self.disableEnableComponents( "saveBtn", "enable" );
	};
	
	this.addTag = function ( tagStr )
	{
		_tempTagListArray.push( tagStr );
		if( $.inArray( tagStr, _tagListArray ) == -1 )
		{
			_tagListArray.push( tagStr );
		}
		_tempTagListArray.sort();

		 var tagDiv = "<div class='tagOuter'><div class='tag'>" + 
		 			  tagStr + "</div><span class='tagTweek'><div id='tag' data-role='buttonComp'" + 
		 			  " class='tagClsBtn' type='closeButton'></div></span></div>";
         noteContainer.find( '[id = tagList]' ).append( tagDiv );
         self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
		 if( noteContainer.hasClass( 'noteContainerLayoutchanged' ) )
		 {
		 	// do nothing
		 }
		 else
		 {
		 	if( noteContainer.css("display") == "block" )
		 	{
		 	   PopupManager.positionPopup(); 
		 	}
		 }
		 
		 /* Close button click of the newly added tag */
		 noteContainer.find( ".tagClsBtn" ).last().bind( "click", function ()
		 {
		 	var tagText = $( this ).parent().parent().find( ".tag" ).text();
		 	_tempTagListArray.splice( $.inArray( tagText, _tempTagListArray ), 1 );
		 	$( this ).parent().parent().remove();	 	
		 	self.disableEnableComponents( "saveBtn", "enable" );
		 	self.disableEnableComponents( "savetoNoteBookButton", "enable" );
		 	PopupManager.positionPopup();
		 } );
	};
	
	this.createTagListArray = function ()
	{
		for( var i = 0; i < GlobalModel.annotations.length; i++ )
		{
			if( GlobalModel.annotations[ i ].type == "stickynote" )
			{
				for( var j = 0; j < GlobalModel.annotations[ i ].tagsLists.length; j++ )
				{
					if( $.inArray( GlobalModel.annotations[ i ].tagsLists[j] , _tagListArray ) == -1 )
						_tagListArray.push(GlobalModel.annotations[ i ].tagsLists[j]);
				}
			}
		}	
	};	
	
	/**
     * @description	This method sets the carat index to end of text in a contenteditable div
     * @param {Object} HTML element (contenteditable div)
     * @return void
     */
    this.placeCaretAtEnd = function(el) {
        if ( typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") 
        {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } 
        else if ( typeof document.body.createTextRange != "undefined") 
        {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    };
    
    this.getIDFromCurrentSelectionClass = function ()
    {
        var self = this;
        var retVal = _currentNoteSelectionClass;
        retVal = retVal.replace(".noteselection_", "");
        retVal = retVal.replace(".selection", "");
        retVal = retVal.replace("noteselection_", "");
        retVal = retVal.replace("selection", "");
        retVal = retVal.replace("tseselect", "");
        retVal = retVal.replace(".tseselect", "");
        retVal = retVal.replace(".", "");
        return retVal;
    };
    
	noteContainer = $( "#stickyNoteContainer" );
	
	if( ismobile )
	{
		noteContainer.addClass("mobileDevice");
		var addCommentClone = $('#addCommentBtn').clone();
		noteContainer.find('#addCommentBtn').remove();
		noteContainer.find('#addCommentBtnPlaceHolderForMobile').append( addCommentClone );
	}
		
	self.addListeners ();
	self.createHighlightColors();
	self.createTagListArray();		
}