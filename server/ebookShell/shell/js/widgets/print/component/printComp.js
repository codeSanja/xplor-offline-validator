/**
* @author ankit.jain
*/
( function( $, undefined ) {
    $.widget("magic.printComp",$.magic.buttonComp,
	{
		events:
		{
			//btn_click : 'click',
			//CLICK	  : 'clicked'
		},
		data:
		{
			
		},
		member:
		{
			journalData:null
		},
		options: 
		{
		},
		_create: function()
		{
			$.extend(this.options, this.element.jqmData('options'));
		},
		_init:function(){
			var objThis = this;
			$(objThis.element).unbind("click").bind("click",function(e) {
				PopupManager.removePopup();
				e.stopPropagation();
				$(objThis.element).trigger("clicked");
				objThis.sortNotes(AppConst.SORT_BY);
				objThis.setSelected();
	        });
	        
	        $(document).unbind("hidePrintPanel").bind("hidePrintPanel",function() {
	        	$("[type='printPanelComp']").hide();
	        	//objThis.setUnSelected();
				$(objThis.element).removeClass().addClass("btnPrintComp");
	        })
	        
	        
		},
		
		setSelected: function() {
			//$(this.element).css({"background-image":"url('css/print/image/Print_selected.png') !important"});
		},
		
		setUnSelected: function() {
			//$(this.element).css({"background-image":"url('css/print/image/Print.png') !important"});
		},
		
		setJournalData:function(objData)
		{
			this.member.journalData = objData;
		},
		
		doPrint: function(val) {
			if(val == 'currPage') {
				this.printPage();
			} else {
				this.printNote();
			}	
		},
				
		printPage: function() {
			this.printelem("#childShowroom", "page", null);
		},
		
		printNote: function() {
			this.printelem("#notesContainer", "note", null);
		},
		
		printelem:function(elem, printType, cssfile) {
			if(printType == 'page') {
				if(AppConst.CURRENT_SCREEN_TYPE == true)
				{
				}
				else{
					$("#pageImage").css("width","100%");
					$("#pageImage").css("height","100%");
				}
				$(elem).printArea({mode: "iframe", popClose: false, elemType : 'page'});
			} else {
				
				var printPanel = $(document.getElementById('notesContainer'));
				//console.log($(printPanel));
				var printRow = $(printPanel.find('[class="RowJournal"]'));
				var len = printPanel.find('[class="RowJournal"]').length;
				$("#notesContainer").css({
					
				})
				
				//.creationDate .notesPage .notesText
				$(".RowJournal").css({
					"text-align": "left",
					"margin-top" : "30px"
				});
				$(".notesRow").css("text-align","left");
				$(".creationDate").css({
					"text-align":"left",
					"font-size" : "10px",
					"font-weight" : "noraml",
					"font-family" : "Halvetica",
					"color":"#8c8c8c"
				});
				$(".notesPage").css({
					"text-align":"left",
					"font-size" : "12px",
					"font-weight" : "bold",
					"font-family" : "Halvetica"	
				});
				$(".notesText").css({
					"text-align":"left",
					"font-size" : "12px",
					"font-weight" : "noraml",
					"font-family" : "Halvetica",
					"margin-top" : "10px"
				});
				$('td').css("text-align","left");
				$('tr').css("text-align","left");
				$('td').attr("align","left");
				if(AppConst.CURRENT_SCREEN_TYPE == true)
				{
				}
				else{
					//$("img").css("width","100%");
					//$("img").css("height","100%");
				}
				$(elem).printArea({mode: "iframe", popClose: false, elemType : 'note'});
			}
		},
		
		sortNotes: function(sortBy) {
			var ownerContext = this;
			var objModel	= this.member.journalData;
			if(AppConst.SORT_BY=="timestamp")
			{
				$(this.byDateElement).css({"font-weight":"bold"});
				$(this.byPageElement).css({"font-weight":"normal"});
			}
			else
			{
				$(this.byPageElement).css({"font-weight":"bold"});
				$(this.byDateElement).css({"font-weight":"normal"});
			}
	        this.objFilteredList = $.Enumerable.From(this.member.journalData)
								.Where(
									function (x) { 
									    return x;
									})
	       						.OrderBy(
									function (x) {
									    
									    return x[sortBy];
									})
								.Select(
										function (x) {
										    return x;
										})
								.ToArray();
	
		        if (this.objFilteredList.length <= 0) {
					$(this.element).find("[type=notesContainer]").css("padding-top",15);
					$(this.element).find("[type=notesContainer]").css("text-align","center");
					$(this.element).find("[type=notesContainer]").html('No Notes Item Found.');
		            return false;
		        }
		        else {
					$(this.element).find("[type=notesContainer]").css("padding-top",0);
		        }
			this.appendNotesRow(this.objFilteredList);
		},
		
		appendNotesRow : function(objData)
		{
			var self = this;
			$("[type=notesContainer]").html('');
			for(var i=0; i<objData.length;i++)
			{
				var newData = objData[i];
				var str = newData.text;
				str = str.replace(/\n/g,'<br>');
				var noteRow = '<div class="RowJournal"><div id="notesRow'+i+'" pageIndex="'+newData.pageIndex+'" class="notesRow"><div id="notesPage" class="notesPage">'+"  "+''+newData.pageLabel+'</div><div id="creationDate" class="creationDate">Date- '+newData.datetime+'</div><div id="notesText" class="notesText">'+str+'</div></div></div>';
				$("[type=notesContainer]").append(noteRow)
			}
			
			$(".notesRow").unbind('click').bind('click',function(e)
			{
				var pageIndex = parseInt($(this).attr("pageIndex"))+1;
				$(self.element).trigger(self.events.JUMP_CLICKED, pageIndex);
			});
			
		}
    });    
}(jQuery));
