function getTodaysDate()
{
	var d = new Date();
   var month = d.getMonth() + 1;
   var day = d.getDate();
   var year = d.getFullYear();
   var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
   
   return dateOfCreation;
}

/**
 * This function calculates the yesterday's full date
 * return It return the yesterday's date to onPanelOpen function
 */
function getYesterdaysDate() 
{

    var d = new Date();
    var previousMonth = d.getMonth() + 1;
    var previousDay = d.getDate() - 1;
    var previousYear = d.getFullYear();

    if (previousDay == 0) {
        previousMonth = previousMonth - 1;
        if (previousMonth == 0) {
            previousYear = previousYear - 1;
            previousMonth = 12;
            previousDay = 31
        } else {
            switch (previousMonth) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    previousDay = 31;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    previousDay = 30;
                    break;
                case 2:
                    if ((previousYear % 4) == 0) {
                        previousDay = 29;
                    } else {
                        previousDay = 28;
                    }

            }
        }
    } else {
        // day is simply the previous day
    }

    yesterdaysDate = (('' + previousMonth).length < 2 ? '0' : '') + previousMonth + '/' + (('' + previousDay).length < 2 ? '0' : '') + previousDay + '/' + previousYear.toString().substr(2, 2);
    return yesterdaysDate;
}

function isNumber(value)
{
	if(typeof value === 'number')
		return true;
	
	return false;
}

function isInteger(value) {
	if (isNumber(value) && Math.floor(value) == value)
		return true;

	return false;
}

function isArray(object)
{
	if(object == null || object == undefined)
		return false;
	
	if(typeof object === 'string' || typeof object === 'number' || typeof object === 'boolean' || (typeof object === "object" && object.length == null))
	{
		return false;
	}
	
	return true;
}

//setting browser information
var nVer = navigator.appVersion;
var nAgt = navigator.userAgent;
var browserName = navigator.appName;
var fullVersion = '' + parseFloat(navigator.appVersion);
var majorVersion = parseInt(navigator.appVersion, 10);
var nameOffset, verOffset, ix;

// In Opera, the true version is after "Opera" or after "Version"
if (( verOffset = nAgt.indexOf("Opera")) != -1) {
	browserName = "Opera";
	fullVersion = nAgt.substring(verOffset + 6);
	if (( verOffset = nAgt.indexOf("Version")) != -1)
		fullVersion = nAgt.substring(verOffset + 8);
}
// In MSIE, the true version is after "MSIE" in userAgent
else if (( verOffset = nAgt.indexOf("MSIE")) != -1) {
	browserName = "Microsoft Internet Explorer";
	fullVersion = nAgt.substring(verOffset + 5);
}
// In Chrome, the true version is after "Chrome"
else if (( verOffset = nAgt.indexOf("Chrome")) != -1) {
	browserName = "Chrome";
	fullVersion = nAgt.substring(verOffset + 7);
}
// In Safari, the true version is after "Safari" or after "Version"
else if (( verOffset = nAgt.indexOf("Safari")) != -1) {
	browserName = "Safari";
	fullVersion = nAgt.substring(verOffset + 7);
	if (( verOffset = nAgt.indexOf("Version")) != -1)
		fullVersion = nAgt.substring(verOffset + 8);
}
// In Firefox, the true version is after "Firefox"
else if (( verOffset = nAgt.indexOf("Firefox")) != -1) {
	browserName = "Firefox";
	fullVersion = nAgt.substring(verOffset + 8);
}
// In most other browsers, "name/version" is at the end of userAgent
else if (( nameOffset = nAgt.lastIndexOf(' ') + 1) < ( verOffset = nAgt.lastIndexOf('/'))) {
	browserName = nAgt.substring(nameOffset, verOffset);
	fullVersion = nAgt.substring(verOffset + 1);
	if (browserName.toLowerCase() == browserName.toUpperCase()) {
		browserName = navigator.appName;
	}
}
// trim the fullVersion string at semicolon/space if present
if (( ix = fullVersion.indexOf(";")) != -1)
	fullVersion = fullVersion.substring(0, ix);
if (( ix = fullVersion.indexOf(" ")) != -1)
	fullVersion = fullVersion.substring(0, ix);

majorVersion = parseInt('' + fullVersion, 10);
if (isNaN(majorVersion)) {
	fullVersion = '' + parseFloat(navigator.appVersion);
	majorVersion = parseInt(navigator.appVersion, 10);
}


/** Jquery.ellipsis.js **/
$.fn.ellipsis = function()
        {
                return this.each(function()
                {
                        var el = $(this);

                        if(el.css("overflow") == "hidden")
                        {
                                var text = el.html();
                                var multiline = el.hasClass('multiline');
                                var t = $(this.cloneNode(true))
                                        .hide()
                                        .css('position', 'absolute')
                                        .css('overflow', 'visible')
                                        .width(multiline ? el.width() : 'auto')
                                        .height(multiline ? 'auto' : el.height())
                                        ;

                                el.after(t);

                                function height() { return t.height() > el.height(); };
                                function width() { return t.width() > el.width(); };

                                var func = multiline ? height : width;

								var arrTxt = text.split(" ");
                                while (arrTxt.length > 0 && func())
                                {
                                        text = text.substr(0, text.lastIndexOf(" "));
                                        t.html(text + " ...");
                                }

                                el.html(t.html());
                                t.remove();
                        }
                });
        };
        
/*
 This function gives the orientation of devices after compensating the diffrence in the angle returned by android and ipad.
 * */
function getDeviceOrientation(){
	
	var nOrient = window.orientation;
	var strOrient = "";
	if(nOrient == undefined && getInternetExplorerVersion() == -1)//For firefox
	{
		var test = window.matchMedia("(orientation: portrait)");
		if(test.matches) {
		    // Changed to portrait
		    strOrient = AppConst.ORIENTATION_PORTRAIT;
		}else {
		    // Changed to landscape
		    strOrient = AppConst.ORIENTATION_LANDSCAPE;
		}
		return strOrient;
	}
	
	if (nOrient == 0 || nOrient == 180) 
	{
    	strOrient = AppConst.ORIENTATION_PORTRAIT;
    	if(navigator.userAgent.match(/(android)/i))
    	{
    		strOrient = AppConst.ORIENTATION_LANDSCAPE;
    	}
    }
	else
	{
		strOrient = AppConst.ORIENTATION_LANDSCAPE;
		if(navigator.userAgent.match(/(android)/i))
		{
			strOrient = AppConst.ORIENTATION_PORTRAIT;
		}
	}
	
	return strOrient;
}

function getTextWithEllipsis(numLines, strTextToWrap, nWidth, className)
{
	strTextToWrap = $.trim(strTextToWrap);
	var objRegSpace = /[\n]/gi;
	strTextToWrap = strTextToWrap.replace(objRegSpace, " ");
	objRegSpace = /[\t]/gi;
	strTextToWrap = strTextToWrap.replace(objRegSpace, " ");
	objRegSpace = /[  ]+/gi;
	strTextToWrap = strTextToWrap.replace(objRegSpace, " ");

	if(numLines < 1)
	{
		return strTextToWrap;
	}
	var ts = $.now();
	var strStyle = "";
	$('body').append('<div class="'+ className +'" id="tempDiv" style="visibility:hidden; position:absolute; top:0px; height:0px; z-index: -1000;display: table-cell;"></div>');
	if($("."+className).css("font-size"))
		strStyle += "font-size :" + $("."+className).css("font-size") + ";";
	if($("."+className).css("font-family"))
		strStyle += "font-family :" + $("."+className).css("font-family") + ";";
	if($("."+className).css("font-weight"))
		strStyle += "font-weight :" + $("."+className).css("font-weight") + ";";
	$("#tempDiv").remove();
	$('body').append('<div id="tempEllipsisContainer'+ts+'" style="visibility:hidden; position:absolute; top:0px; height:0px; z-index: -1000;'+strStyle+'"></div>')
	var objContainer = $('#tempEllipsisContainer'+ts);
	
	var strText =  strTextToWrap;///$(this).text();
	var arrTxt = strText.split(" ");
	var strFinalText = "";
	var nWordCounter = 0;
	var j = 0;
	
	var objElement;
	$(objContainer).append('<span id="ellipsisline' + ts + '">' + strText + "</span>");
	objElement = $(objContainer).find('[id=ellipsisline' + ts + ']');
	
	if ($(objElement).width() <= nWidth) {
		$(objElement).remove();
		$(objContainer).remove();
		return strText;
	}
	$(objElement).remove();
	for(var i = 0; i < numLines; i++)
	{
		objElement = $(objContainer).find('[id=ellipsisline' + ts + ']');
		if(objElement)
		{
			$(objElement).remove();
		}
		$(objContainer).append('<span id="ellipsisline' + ts + '">' + strText + "</span>");
		objElement = $(objContainer).find('[id=ellipsisline' + ts + ']');
		
		if ($(objElement).width() <= nWidth) {
			i = numLines - 1;	
			strFinalText += strText;//strTextToWrap;
			$(objElement).remove();
			$(objContainer).remove();
			return strFinalText;	
		}
		$(objElement).remove();
		var t = "";
		var tempT
		
		if(i < numLines - 1)
		{
			for (j = nWordCounter; j < arrTxt.length; j++)
			{
				tempT = t;
				if(tempT != "")
					t = tempT + " " + arrTxt[j];
				else
					t = arrTxt[j];
				$(objContainer).append('<span id="ellipsisline' + i + '">' + tempT + " " + arrTxt[j] + "</span>");
				objElement = $(objContainer).find('[id=ellipsisline' + i + ']');
				
				if($(objElement).width()  > nWidth)
				{
					strFinalText += tempT + "<br/>";
					j = arrTxt.length;
				}
				else
				{
					nWordCounter++;
					strText = $.trim(strText.substring(parseInt(strText.indexOf(" "))));
				}
				$(objElement).remove();
			}
		}
		else if(i == numLines - 1)
		{
			for (j = nWordCounter; j < arrTxt.length; j++)
			{
				//
				tempT = t;
				if(tempT != "")
					t = tempT + " " + arrTxt[j];
				else
					t = arrTxt[j];
				firstWord = false;
				$(objContainer).append('<span id="ellipsisline' + i + '">' + tempT + " " + arrTxt[j] + " ...</span>");
				objElement = $(objContainer).find('[id=ellipsisline' + i + ']');
				if($(objElement).width()  > nWidth)
				{
					strFinalText += tempT + " ...";
					j = arrTxt.length;
				}
				$(objElement).remove();
				nWordCounter++;
			}
		}
	}
	$(objElement).remove();
	$(objContainer).remove();
	
	if(strFinalText.toLowerCase().indexOf("<i>") != -1 && strFinalText.toLowerCase().indexOf("</i>") == -1)
		strFinalText += "</i>";
	
	if(strFinalText.toLowerCase().indexOf("<b>") != -1 && strFinalText.toLowerCase().indexOf("</b>") == -1)
		strFinalText += "</b>";
	
	if(strFinalText.toLowerCase().indexOf("<u>") != -1 && strFinalText.toLowerCase().indexOf("</u>") == -1)
		strFinalText += "</u>";
	return strFinalText;
	
   	
};
function getTextWithoutTags(strTextToUnWrap)
{
	$('body').append('<span id="tempTagsUnwrap" style="visibility:hidden; position:absolute; top:0px; height:0px; z-index: -1000;">'+ strTextToUnWrap +'</span>')
	var strText = $('#tempTagsUnwrap').text();
	$("#tempTagsUnwrap").remove();
   return strText;
};


var MainController = {};

MainController.updateBrandingBar = function()
{
    var programBrandingImage = $('#programBranding').find('img');
    if (EPubConfig.programBrandingLogo && EPubConfig.programBrandingImage) {
        if ($.trim(EPubConfig.programBrandingLogo) != "") {
            $(programBrandingImage).attr('src', getPathManager().getProgramBrandingImagePath(EPubConfig.programBrandingLogo));

        } else {
            $(programBrandingImage).css('visibility', 'hidden');
        }
        var imgDiv2 = $('#logoImgDiv2').find('img');
        var imgDiv = $('#logoImgDiv').find('img');
        if ($.trim(EPubConfig.programBrandingImage) != "") {
            $(imgDiv2).attr('src', getPathManager().getProgramBrandingImagePath(EPubConfig.programBrandingImage));
        } else {
            $(imgDiv2).css('visibility', 'hidden');
        }
    } else {
        $(programBrandingImage).attr('src', getPathManager().getProgramBrandingImagePath(EPubConfig.programBrandingImage));
        $(imgDiv).attr('src', getPathManager().appendReaderPath(EPubConfig.logoImgDivSrc));
        $(imgDiv2).attr('src', getPathManager().appendReaderPath(EPubConfig.logoImgDiv2Src));
    }

    if (EPubConfig.MainLogo) {
        if ($.trim(EPubConfig.MainLogo) != "") {
            $(imgDiv).attr('src', getPathManager().getProgramBrandingImagePath(EPubConfig.MainLogo));
        } else {
            $(imgDiv).css('visibility', 'hidden');
        }
    } else {
        $(imgDiv).attr('src', getPathManager().appendReaderPath(EPubConfig.logoImgDivSrc));
    }

    if (EPubConfig.programBrandingBackgroundColor) {
        if ($.trim(EPubConfig.programBrandingBackgroundColor) != "") {
            $('#header').css("background-color", EPubConfig.programBrandingBackgroundColor);
            $('#header').css("background-image", "linear-gradient(" + EPubConfig.programBrandingBackgroundColor + ", " + EPubConfig.programBrandingBackgroundColor + ")");
        }
    }

    if (EPubConfig.programBrandingBackgroundImage) {
        if ($.trim(EPubConfig.programBrandingBackgroundImage) != "") {
            $('#header').css("background-image", "url(" + getPathManager().getProgramBrandingImagePath(EPubConfig.programBrandingBackgroundImage) + ")");
        }
    }
}

var LocalizationManager = {};
LocalizationManager.setTooltipText = function()
{
    $("#prevBtnLeft").attr("title", GlobalModel.localizationData["PREVIOUS_PAGE_TEXT"]);
    $("#nextBtnRight").attr("title", GlobalModel.localizationData["NEXT_PAGE_TEXT"]);
    $("[type='singlePageLauncher']").attr("title", GlobalModel.localizationData["SINGLE_PAGE_VIEW_TEXT"]);
    $("[type='doublePageLauncher']").attr("title", GlobalModel.localizationData["DOUBLE_PAGE_VIEW_TEXT"]);
    $("[type='scrollPageLauncher']").attr("title", GlobalModel.localizationData["SCROLL_PAGE_VIEW_TEXT"]);


    $("#nativeCloseBtn").attr("title", GlobalModel.localizationData["SIDE_BACKBTN"]);
    $("#btnTocLauncher").attr("title", GlobalModel.localizationData["CONTENT_LAUNCHER_TEXT"]);
    $("#btnResourcePanel").attr("title", GlobalModel.localizationData["RESOURCE_LAUNCHER_TEXT"]);
    $("#bookmarkSectionLauncher").attr("title", GlobalModel.localizationData["BOOKMARK_LAUNCHER_TEXT"]);
    $("#stickyNoteLauncher").attr("title", GlobalModel.localizationData["ANNOTATION_LAUNCHER_TEXT"]);
    $("#zoomBtn").attr("title", GlobalModel.localizationData["PAGEVIEW_LAUNCHER_TEXT"]);
    $("#helpLauncher").attr("title", GlobalModel.localizationData["HELP_LAUNCHER_TEXT"]);
    $("#languageSelector").attr("title", GlobalModel.localizationData["LANGUAGE_LAUNCHER_TEXT"]);
    $("#btnStandardsPanel").attr("title", GlobalModel.localizationData["STANDARDS_LAUNCHER_TEXT"]);
    $("#printBtn").attr("title", GlobalModel.localizationData["PRINT_LAUNCHER_TEXT"]);
    $(".zoomTxt").text(GlobalModel.localizationData["ZOOM_TEXT"]);
    $(".zoomSliderParent").attr("title", GlobalModel.localizationData["ZOOM_SLIDER_PARENT"]);
}

decodeBase64 = function(input) {
    var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = _keyStr.indexOf(input.charAt(i++));
        enc2 = _keyStr.indexOf(input.charAt(i++));
        enc3 = _keyStr.indexOf(input.charAt(i++));
        enc4 = _keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = _utf8_decode(output);

    return output;
};

_utf8_decode = function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return string;
}

removeSelection = function ()
{
    if (window.getSelection) {
        if (window.getSelection().empty) { // Chrome
            window.getSelection().empty();
        }
        else 
            if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
    }
    else 
        if (document.selection) { // IE?
            document.selection.empty();
        }
}

getQueryStringHash = function (queryString) {
    console.log("queryString", queryString);
    var vars = [], hash;
    var hashes = queryString.split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

findLastHotKeyIndex = function( arrHotKey ) {
	var maxHotKey = 0;
	for(var i = 0; i < arrHotKey.length; i++ )
	{
		if( maxHotKey < parseInt($(arrHotKey[i]).attr("hotkeyindex")) )
		{
			maxHotKey = parseInt($(arrHotKey[i]).attr("hotkeyindex"));
		}
	}
	maxHotKey++;
	return maxHotKey;
}

setHotkeyIndex = function( arrHotKey, hotKey ) {
	for(var i = 0; i < arrHotKey.length; i++ )
	{
		$(arrHotKey[i]).attr( "hotkeyindex", hotKey );
		hotKey++;
	}
}