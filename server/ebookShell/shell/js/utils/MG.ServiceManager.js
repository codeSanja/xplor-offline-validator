/**
 * @author
 * This file defines functions for Data Persistance and Search API
 */
var ServiceManager = {

    arrOutputFields: ["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos"],
    events: {
        LOAD_COMPLETE_EVENT_TYPE_1: "bookmarkDataLoadComplete",
        LOAD_COMPLETE_EVENT_TYPE_2: "highlightDataLoadComplete",
        LOAD_COMPLETE_EVENT_TYPE_3: "stickyNotesDataLoadComplete",
        LOAD_COMPLETE_EVENT_TYPE_8: "autoBookmarkDataLoadComplete",
        LOAD_COMPLETE_EVENT_TYPE_9: "qaPanelDataLoadComplete"
	},
    searchService : "", //value will set after config.txt file loads;
	isServiceAuthenticated : false,
    getResults : null,
	noteLoadComplete : false,
	highlightLoadComplete : false,
	standaloneCommentNote : [],
	standaloneCommentHighlight : [],
	commentWithinNote : [],
	commentInQA : [],
	
    initializeAnnotationServiceAPI: function()
    {
        var objThis = this;
        objThis.setBookEdition();
      //  GlobalModel.selectedStudentData = {refId: 'c5f57a3a-7806-456b-bf3e-5ca194f20358'};
        if( ServiceManager.DPLive )
        {
            annService.init ({
                clientId        : 'eBook',
                forceDefaultServiceSettings: true,
                successHandler : function(){
                    ServiceManager.isServiceAuthenticated = true;
					if(sessionStorage.selectedStudentData != undefined && EPubConfig.AnnotationSharing_isAvailable != false)
					{
						ServiceManager.setStudentData(JSON.parse(sessionStorage.selectedStudentData));
					}
					else
					{
						ServiceManager.Annotations.fetchAllAnnotations();
					}
                    
                    $(document).trigger("annotationServiceInitiated");
                },
                errorHandler    : function(err){}
            });
        }
        else
        {
            if(location.port == "8080")
            {
                var objInitData = {
                    clientId        : 'eBook',
                    serverURL       : 'http://dubv-dpqaweb01.dubeng.local/', //DP QA ENV
                    usePostTunneling: false,
                    nextLinkDomainStripping : false,
                    successHandler : function(){
                        objThis.DPLive = true;
                        ServiceManager.isServiceAuthenticated = true;
						
						if(sessionStorage.selectedStudentData != undefined && EPubConfig.AnnotationSharing_isAvailable != false)
						{
							ServiceManager.setStudentData(JSON.parse(sessionStorage.selectedStudentData));
						}
						else
						{
							ServiceManager.Annotations.fetchAllAnnotations();
						}
                        
                        $(document).trigger("annotationServiceInitiated");
                    },
                    errorHandler    : function(err){
                        
                    }
                };

                if(document.location.hostname == "localhost")
                {
                    objInitData.cors = true;
                }
				var objTokenDetailsTeacher = {
                        userId : '6191f01ef52a41b09053b3f71e99a73d',
                        role : 'Instructor',
                        platform: 'HMOF'
                    }
				var objTokenDetailsStudent = {
                        userId : 'f3e42bbca0bc468894c7365c2d3506b1',
                        role : 'Student',
                        platform: 'HMOF'
                }
                var objLoginDetails = objTokenDetailsTeacher;
                if(_objQueryParams.student != undefined)
                {
                    objLoginDetails = objTokenDetailsStudent;
                }
                annServiceTokenGenerator.getToken (objLoginDetails,
                    function(mytoken) {
                        objInitData.token = mytoken;
                        annService.init (objInitData);
                    },
                    function(xhr, error) {console.log('error generating token: ', error);}
                );
            }
        }
    },
    
    getSharedStudents : function ( annotationID, successHandler, errorHandler )
    {
    	annService.custom
    	.getUsersForAnnotationId( annotationID )
        .done(function (data) {
            successHandler( data );
        })
        .fail(function (err) {
            errorHandler( err );
        })
        .run();
    },
    
    removeSharedStudents : function ( arr, successHandler )
    {
    	annService.AnnotationSharedWith
		.remove( arr )
		.done(function(data) {
		     successHandler();
		})
		.fail(function(err) {
		     console.log(err);
		})
		.run();
    },
    
    getStudentDataForNewAnnotation : function ( annoId, successHandler )
    {
    	annService.Annotations
		.findOne( annoId )
		.withTags()              
		.withSharedAnnotations()
		.done(function(data) {
		      successHandler( data );
		})
		.fail(function(err) {
		      console.log('fail: ', err);
		})
		  .run();
    },
    
    addSharedStudents : function ( annotationID, arr, successHandler )
    {
    	annService.AnnotationSharedWith.addUsersToAnnotation(annotationID, arr ).done( successHandler )
        .fail(function (err) {
            console.log('fail: ', err);
        })
        .run();
    },

    Annotations: {
        allAnnotations: [],
		
        fetch: function(typeid, nextLink)
        {
            var objThis = ServiceManager.Annotations;
            var serviceResponseHandler = function(results, nextLink)
            {
                if( typeid == 3 || typeid == 2 )
                {
                    var tempArr = [];
                    for( var i = 0; i < results.length; i++ )
                    {
                        if( results[ i ].shared_with && results[ i ].shared_with.results && results[ i ].shared_with.results.length > 0 )
                        {
                        	if( ( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData ) ||
                        		( GlobalModel.BookEdition == "TE" ) )
                        	{
                        		tempArr.push( results[ i ] );
                        	}
                        }
                        else
                        {
                            tempArr.push( results[ i ] );
                        }
                    }
                    results = tempArr;
                }
                
				ServiceManager.Annotations.allAnnotations = ServiceManager.Annotations.allAnnotations.concat(results);
                if(objThis['arrAnnoType'+typeid] == undefined)
                {
                    objThis['arrAnnoType'+typeid] = [];
                }

                objThis['arrAnnoType'+typeid] = objThis['arrAnnoType'+typeid].concat( results );
                
                if(nextLink == undefined || nextLink == "")
                {
                    ServiceManager.storeAndParseAnnotationData(typeid, objThis['arrAnnoType'+typeid]);
                }
                else
                {
                    objThis.fetch(typeid, nextLink);
                }
            };

            function onError () 
            {
                //if ( strCompleteEvent != "" )
                    $( document ).trigger( ServiceManager.events['LOAD_COMPLETE_EVENT_TYPE_' + typeid] );
                    if( typeid == 3 || typeid == 2 )
                    {
                        $("#mainShowroom").css("opacity", "1");
                        $("#appPreloaderContainer").css("display","none");
                    }
                    
                    
            }


            if(nextLink != undefined && nextLink != "")
            {
            	if(GlobalModel.selectedStudentData)
                {
                	annService.Annotations
                           .find()
                           .forUser(GlobalModel.selectedStudentData.refId)
                           .next(nextLink)
                           .done(serviceResponseHandler)
                           .fail(function(){ serviceResponseHandler( [] );})
                           .run();
                }
                else
                {
                	annService.Annotations
                           .find()
                           .next(nextLink)
                           .done(serviceResponseHandler)
                           .fail(function(){ serviceResponseHandler( [] );})
                           .run();
                }
            }
            else
            {
                objThis['arrAnnoType'+typeid] = [];
                if(GlobalModel.selectedStudentData)
                {
                    annService.Annotations
                                .find({ type_id : typeid, content_id : EPubConfig.Identifier })
                                .outputFields(["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos"])       /* optional: array of strings representing name of fields that should be included in each response row*/
                                .forUser(GlobalModel.selectedStudentData.refId)
                                .withTags().order("updated_date", 'desc')
                                .limit(0) 
                                .parseDates() 
                                .done(serviceResponseHandler)
                                .fail(onError)
                                .run();
                }
                else
                {
                    annService.Annotations
                                .find({ type_id : typeid, content_id : EPubConfig.Identifier })
                                .fields(["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos"])       /* optional: array of strings representing name of fields that should be included in each response row*/
                                .withTags().order("updated_date", 'desc')
                                .limit(0) 
                                .parseDates()
                                .withShared()
                                .done(serviceResponseHandler)
                                .fail(onError)
                                .run();
                }
            }
        },
		
        create: function(arrObj , type , successHandler)
        {
            // TODO send call to service if teacher shares annotation
            if( !ServiceManager.DPLive )
            {
                return;
            }
            var objThis = ServiceManager.Annotations;
            var objToPost = objThis.formatPostData(arrObj, type);
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
                annService.Annotations
                .create( objToPost )
                .shareTo( GlobalModel.selectedStudentData.refId + "," + GlobalModel.selectedStudentData.platformId )
                .done(function( data ) {
                    successHandler( data );
                })
                .fail(function(err) {
                    console.log(err);
                })
                .run();
                return;
            }

            if(arrObj.tagsLists && arrObj.tagsLists.length > 0)
            {
            	if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData &&
            	objToPost.teachersSharedWith && objToPost.teachersSharedWith.results && 
            	objToPost.teachersSharedWith.results.length > 0 )
            	{
            		var newList = objToPost.teachersSharedWith.results;
		        	var newListStr = "";
		        	for( var newIndex = 0; newIndex < newList.length; newIndex++ )
		        	{
		        		var str = $("#studentPanel").find("[platformid=" + newList[ newIndex ].platformid + "]").attr("refid");
		        		newListStr += str + "," + newList[ newIndex ].platformid + ( ( newIndex == newList.length - 1 ) ? "" : ";" ) ;
		        	}
		        	delete objToPost.teachersSharedWith;
            		annService.Annotations
                            .create( objToPost )
                            .shareTo( newListStr )
                            .tags( arrObj.tagsLists )
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
            	}
            	else
            	{
            		annService.Annotations
                            .create( objToPost )
                            .tags( arrObj.tagsLists )
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
            	}
            }
            else
            {
            	if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData &&
            	objToPost.teachersSharedWith && objToPost.teachersSharedWith.results && 
            	objToPost.teachersSharedWith.results.length > 0 )
            	{
            		var newList = objToPost.teachersSharedWith.results;
		        	var newListStr = "";
		        	for( var newIndex = 0; newIndex < newList.length; newIndex++ )
		        	{
		        		var str = $("#studentPanel").find("[platformid=" + newList[ newIndex ].platformid + "]").attr("refid");
		        		newListStr += str + "," + newList[ newIndex ].platformid + ( ( newIndex == newList.length - 1 ) ? "" : ";" ) ;
		        	}
		        	delete objToPost.teachersSharedWith;
            		annService.Annotations
                            .create( objToPost )
                            .shareTo( newListStr )
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
            	}
            	else
            	{
            		annService.Annotations
                            .create( objToPost )
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
            	}
            	                
            }
        },
        update: function(typeid , objData)
        {
        	// TODO send call to service if teacher updates annotation or update students with whom annotation is shared
			var objThis = ServiceManager.Annotations;
			var sucessHandler = function()
			{
				if(typeid == 8)
				{
					console.log( "INSIDE SUCESS: HMH_DP_ON" )
	                $.cookie( "HMH_DP" , "ON" );
	                $.cookie( "HMH_DP_ALERTED" , "FALSE" );
				}
				else
				{
					console.log("Updated");
				}
			} 
			
			var errorHandler = function(err)
			{
				if(typeid == 8)
				{
					console.log( "INSIDE FAIL: HMH_DP_OFF" )
                    GlobalModel.hmh_dp_off_counter++;
                    $.cookie( "HMH_DP" , "OFF" );
                    $.cookie( "HMH_DP_ERRCOUNT" , GlobalModel.hmh_dp_off_counter );
                    if ( $.cookie( "HMH_DP_ERRCOUNT_TS" ) == null )
                    {
                        $.cookie( "HMH_DP_ERRCOUNT_TS" , new Date().getTime() );
                    }
				}
				else
				{
					console.log(err);
				}
			}
			var objToPost = objThis.formatPostData(objData, typeid, "update");
			var createtags = function ()
			{
				if( objToPost.tagsLists && objToPost.tagsLists.length > 0 )
				{
					var tagsArr = [];
					
					for( var i = 0; i < objToPost.tagsLists.length; i++ )
					{
						var obj = {};
						obj.value = objToPost.tagsLists[ i ];
						tagsArr.push( obj );
					}
					annService.Tags.create( tagsArr )
						    .commonFields( { 'annotation_id' : annotationid , 'content_id': EPubConfig.Identifier } )
		                    .done( function( data ) {
		                        console.log( data, " Created Tags" );
		                    })
		                    .fail( function( err ) {
		                        console.log(err);
		                    })
		                    .run();
		           
		           delete objToPost.tagsLists;        
				}
			}
			if( typeid == 8 && (( GlobalModel.AutomaticBookmarkAnnID == undefined ) || ( GlobalModel.AutomaticBookmarkAnnID == 0 )) )
			{
				return;
			}
			var annotationid = objData.annotation_id;
			
			if(typeid == 8)
			{
				annotationid = GlobalModel.AutomaticBookmarkAnnID;
			}
			else if(typeid == 9)
			{
				annotationid = GlobalModel.questionPanelAnswers[objData.parentGrpId][objData.id].annotation_id;
			}
			if( ( typeid == 3 || typeid == 2 ) && !( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) )
			{
				annService.Tags.find( { annotation_id:annotationid} ).done( function( data ){
				var tempArr = [];
				 for( var k = 0; k < data.length; k++ )
				 {
				 	tempArr.push( data[ k ].tag_id );
				 }
				 if( tempArr.length > 0 )
				 {
				 	annService.Tags.remove( tempArr )
					 .done( function(){ createtags();
					 annService.Annotations.update( annotationid , objToPost ).done( sucessHandler ).fail(errorHandler).run();
					 console.log("DELETE OK TAGS"); } )
					 .fail( function( err ){ console.log(err); } )
					 .run();
				 	return;
				 }
				 else
					{
					 createtags();
					 annService.Annotations.update( annotationid , objToPost ).done( sucessHandler ).fail(errorHandler).run();
					 return;
					}
				 
				} ).fail( function (err){console.log(err)} ).run();
				return;
			}
			annService.Annotations.update( annotationid , objToPost ).done( sucessHandler ).fail(errorHandler).run();
        },
        remove: function( id, type )
        {
            if ( ServiceManager.DPLive && ServiceManager.isServiceAuthenticated ) 
            {
                annService.Annotations
                            .remove( id )
                            .done( function (){ console.log( "DELETE OK" ); } )
                            .fail( function ( err ){ console.log( err ); } )
                            .run();
            }
        },
        fetchAllAnnotations: function()
        {
            if (ServiceManager.DPLive && ServiceManager.isServiceAuthenticated) {
                GlobalModel.annotations = [];
                GlobalModel.bookMarks = [];
                GlobalModel.AnnotationIDArr = [];
                GlobalModel.QAAnnotationIDArr = [];
                GlobalModel.questionPanelAnswers = {};
                GlobalModel.questionPanelAnswersForAnnoPanel = [];
                GlobalModel.QAAnnotationIDArr = [];
                GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
                $("anno").unbind();
                objThis.highlightLoadComplete = false;
                objThis.noteLoadComplete = false;
                $( ".btnStickyNoteHotspot" ).remove();
                $( "anno" ).removeClass();
                $( "anno" ).removeAttr('annoArr');
                $( ".btnBookmarkComp-on" ).removeClass().addClass( "btnBookmarkComp-off" );
                if ( $("#audioPopUp").css("display") == "block" ) 
				{
					$("#audioPlayerCloseBtn").trigger('click');
				}
                if ( GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" )
                {
                	$( $( '[ id = "bookContentContainer2" ] ' )[ 0 ] ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );
                }
                $(".contentEditableFalseDiv").removeClass("contentEditableFalseDiv");
                ServiceManager.Annotations.fetch( 1 );
                ServiceManager.Annotations.fetch( 2 );
                ServiceManager.Annotations.fetch( 3 );
                GlobalModel.AutomaticBookmarkDate = 0;
                GlobalModel.AutomaticBookmarkAnnID = 0;
               ServiceManager.Annotations.fetch( 8 );                
                ServiceManager.Annotations.fetch( 9 );
            }
        },
		removeAllAnnotations: function(){
			for(var i = 0; i < ServiceManager.Annotations.allAnnotations.length; i++)
			{
				ServiceManager.Annotations.remove(ServiceManager.Annotations.allAnnotations[i].annotation_id);
			}
		},
        formatPostData: function(arrObj, typeid, postDataType /* create or update */)
        {
            var objToPost = {};
            var location = "";
            
            if(arrObj.selectioncolor != undefined)
            {
                var colordata = arrObj.selectioncolor.replace( 'selectioncolor' , '' );
                colordata = colordata.replace( '_underline' , '' );
                objToPost.color = colordata;
            }
            if (postDataType == "update") {
            	var annotationid = arrObj.annotation_id;
				if (typeid == 8) {
					objToPost.path = GlobalModel.getMyNoteBookPagePath( arrObj.toString() );
				}
				else {
					//console.log( arrObj, " ********************************")
					if (typeid == 2) {
						objToPost.visibility_id = (arrObj.savetonote) ? 1 : 3;
						//console.log("hererer")
						/*if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
					    {
					    	//console.log( arrObj, "&&&&&&&&&&&&!!!!!!!!!!!!!!!!!!!!!!!")
					        objToPost = {};
					        objToPost.title = arrObj.selectionID;
							objToPost.style = arrObj.style.toString();
							//console.log( objToPost, " &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
					    }
					    else
					    {*/
					    	//console.log( arrObj.selectionID, " Thisi s selection ID")
					    	// TODO create object if annotation is shared with students
						    objToPost.visibility_id = (arrObj.savetonote) ? 1 : 3;
							objToPost.title = arrObj.selectionID;
							objToPost.style = arrObj.style.toString();
							if( arrObj.tagsLists && arrObj.tagsLists.length > 0 )
							{
								objToPost.tagsLists = arrObj.tagsLists;
							}
					    //}
						
					}
					else 
						if (typeid == 3) {
							objToPost.visibility_id = (arrObj.savetoNote) ? 1 : 3;
							// TODO create object if annotation is shared with students
						  /*  if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
						    {
						        objToPost = {};
						        objToPost.body_text = arrObj.title;
						        objToPost.content_id = EPubConfig.Identifier;
						    }
						    else
						    { */
						        objToPost.title = "note_text";
								if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
						   	    {
						   	    	objToPost.title = "comment";
						   	    }
    							objToPost.body_text = arrObj.stickyContent;
    							objToPost.object_id = arrObj.objectID;
    							objToPost.data = arrObj.rangeObject;
    							if( arrObj.tagsLists && arrObj.tagsLists.length > 0 )
    							{
    								objToPost.tagsLists = arrObj.tagsLists;
    							}
    							objToPost.visibility_id = (arrObj.savetonote) ? 1 : 3;
    							//objToPost.oldTagsList = arrObj.oldTagsList;
						    //}
							
						}
						else 
							if (typeid == 9) {
								objToPost.body_text = arrObj.response.toString();
								objToPost.flag_id = arrObj.flag_id;
								objToPost.visibility_id = (arrObj.savetonote) ? 1 : 3;
							}
							else
							 if( typeid == 7 )
							 {
							     objToPost = {};
							     objToPost.body_text = arrObj.title.toString();
                                 objToPost.content_id =  EPubConfig.Identifier;
							 }
				}
			}
			else {
				objToPost.type_id = typeid;
				objToPost.content_id = EPubConfig.Identifier;
				objToPost.title = arrObj.title;
				if (typeid == 1) {
					objToPost.object_id = arrObj.page.toString();
				}
				else 
					if (typeid == 2) {
						// TODO add object of students 
						objToPost.title = arrObj.selectionID;
						objToPost.object_id = arrObj.pageNumber;
						objToPost.body_text = arrObj.title;
						objToPost.data = arrObj.rangeObject;
						objToPost.style = arrObj.style.toString();
						objToPost.coord_y = parseInt(arrObj.top);
						objToPost.visibility_id = (arrObj.savetonote) ? 1 : 3;
						if( arrObj.teachersSharedWith )
						{
							objToPost.teachersSharedWith = arrObj.teachersSharedWith;
						}
					}
					else 
						if (typeid == 3) {
							// TODO add object of students 
							objToPost.object_id = arrObj.pagenum;
							objToPost.body_text = arrObj.stickyContent;
							objToPost.data = arrObj.rangeObject;
							objToPost.visibility_id = (arrObj.savetoNote) ? 1 : 3;
							objToPost.start_pos = parseInt(arrObj.id);
							objToPost.coord_x = arrObj.icontop;
							objToPost.coord_y = arrObj.iconleft;
							if( arrObj.teachersSharedWith )
							{
								objToPost.teachersSharedWith = arrObj.teachersSharedWith;
							}
						}
						else 
							if (typeid == 8) {
							
							}
							else 
								if (typeid == 9) {
									objToPost.title = arrObj.interactivityType;
									objToPost.object_id = arrObj.id.toString();
									objToPost.body_text = arrObj.response.toString();
									objToPost.data = arrObj.parentGrpId;
									objToPost.visibility_id = (arrObj.savetoNote) ? 1 : 3;
									objToPost.flag_id = arrObj.flag_id;
								}
								else
								{
								    if( typeid == 7 )
								    {
								    	if( arrObj.qaType )
								    	{
								    		objToPost.title = "Comment";
									        objToPost.body_text = arrObj.title;
									        objToPost.parent_id = arrObj.annotationId;
									        objToPost.object_id = arrObj.pageNumber;
								    	}
								    	else
								    	{
								    		objToPost.title = "Comment";
									        objToPost.body_text = arrObj.title;
									        objToPost.visibility_id = (arrObj.savetoNote) ? 1 : 3;
									        objToPost.parent_id = arrObj.annotationId;
									        objToPost.object_id = arrObj.pageNumber;
								    	}
								        
								    }
								}
				if (typeid == 9)
				{
					location = GlobalModel.getMyNoteBookPagePath( $.trim( arrObj.pageNumber.replace( '<span class="questionPanelPageSpan"></span> ' , "" ) ) );
				}
				else
				{
					location = GlobalModel.getMyNoteBookPagePath( objToPost.object_id );
				}
				objToPost.path = location;
			}
            objToPost = ServiceManager.addVolumeDetails( objToPost );

            return objToPost;
        }
    },

    getUserRole: function()
    {
        var userRole = (EPubConfig.Component_type == AppConst.TEACHER_EDITION) ? AppConst.USERTYPE_TEACHER : AppConst.USERTYPE_STUDENT;
        EPubConfig.platformId = "hmof";
        try{
            var Hmh_Auth = $.cookie("HMH_AUTH");// "SecurityToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlzcyI6Imh0dHBzOi8vbXkuaHJ3LmNvbSIsIm5iZiI6MTQwNzgxOTE1NywiZXhwIjoxNDA4MjUxMTU3LCJQbGF0Zm9ybUlkIjoiSE1PRiIsIlJvbGVzIjoiVGVhY2hlciIsIkRpc3BsYXlOYW1lIjoiV29sdHogSmFjayIsIkxhdW5jaFVybCI6Imh0dHA6Ly9teS1yZXZpZXctY2VydC5ocncuY29tIiwic3ViIjoiY249V29sdHosdWlkPTMyNkE3NUE2MjM2NzQxNDk2NzlGRDZCNDlDMjU2NzZFLG89MDAyMDUxNjkifQ.MA_cQRV0Zfwz3Xw5PViPFUcbAZTAZHDK2NtmzgtN3_c&Domain=my-review-cert.hrw.com&Path=%2f&Expires=08%2f17%2f2014+04%3a52%3a37";
    	    var SecurityToken = getQueryStringHash(Hmh_Auth)["SecurityToken"];
    	    var userInfo = decodeBase64(SecurityToken);
            var objRegex = /Roles[\s\S]*?(,)/gi;
            var userRoleDet = userInfo.match(objRegex);
            objRegex = /,/gi;
            userRole = userRoleDet[0].replace(objRegex, "").split(":")[1].replace(/["]/g, "");
            
			objRegex = /PlatformId[\s\S]*?(,)/gi;
            var arrPlatformId = userInfo.match(objRegex);
			objRegex = /,/gi;
            platformId = arrPlatformId[0].replace(objRegex, "").split(":")[1].replace(/["]/g, "");
			
			if(platformId == "HMOF")
			{
				EPubConfig.platformId = "hmof";
			} 
			else if(platformId == "TCK")
			{
				EPubConfig.platformId = "tc";
			}
        }
        catch(e){
            console.log("ERROR: No Cookie data found");
        };

        if(userRole == AppConst.USERTYPE_INSTRUCTOR)
        {
            userRole = AppConst.USERTYPE_TEACHER;
        }        

		if(document.location.hostname == "localhost")
		{
			userRole = AppConst.USERTYPE_TEACHER;
			if(_objQueryParams.student != undefined)
            {
                userRole = AppConst.USERTYPE_STUDENT;
            }
		}
		
        return userRole;
        
    },
    
    setBookEdition : function ()
    {
        var userRole = this.getUserRole();
        if(userRole == AppConst.USERTYPE_TEACHER && EPubConfig.Component_type == AppConst.TEACHER_EDITION) 
        {
            GlobalModel.BookEdition = "TE";
        }
        else if(userRole == AppConst.USERTYPE_TEACHER && EPubConfig.Component_type == AppConst.STUDENT_EDITION)
        {
            GlobalModel.BookEdition = "TSE";
        }
        else if(userRole == AppConst.USERTYPE_STUDENT && EPubConfig.Component_type == AppConst.STUDENT_EDITION)
        {
            GlobalModel.BookEdition = "SE";
        }
    },
    
    storeAndParseAnnotationData : function(type, results, isComment)
    {
    	var strCompleteEvent = ServiceManager.events['LOAD_COMPLETE_EVENT_TYPE_' + type];
        var objThis = this;
        var bookmarkAddedPagesID = [];
        var hasAutomaticBookmark = false;
        if( isComment )
        {
            strCompleteEvent = "";
        }
        if (results.length > 0) {
            for (var i = 0; i < results.length; i++) {

                if (results[i].content_id == EPubConfig.Identifier) {
                    var arrObj = {};
                    var annodate;
                    if (results[i].updated_date) {
                        annodate = results[i].updated_date;
                    } else {
                        annodate = results[i].created_date;
                    }

                    var month = annodate.getMonth() + 1;
                    var day = annodate.getDate();
                    var year = annodate.getFullYear();
                    var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);

                    var isSamePath;
                    
                    switch(type) {

                        case 1:
                            arrObj = {};
                            //Bookmark
                                                                
                            isSamePath = true;
                            if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                            {
                                isSamePath = objThis.checkPathforVolume(results[i].path);
                            }

                            if(isSamePath)
                            {
                                var page;
                                if(results[i].path.toString().indexOf("page=") != -1)
                                {
                                    var parser = document.createElement('a');
                                    parser.href = results[i].path.toString();
                                    var parts = parser.search.substr(1).split("&");
                                    for (var k = 0; k < parts.length; k++) {
                                        if(parts[k] != ""){
                                            var queryParamAndValue = parts[k].split("=");
                                            
                                            if(queryParamAndValue[0] == "page"){
                                                page = queryParamAndValue[1];
                                            }
                                                                                            
                                        }
                                    }
                                    arrObj.id = "bookmarkStrip_" + page;
                                }
                                else
                                {
                                    arrObj.id = results[i].path;
                                }

                                if(arrObj.id.split("_")[1] == "NaN")
                                    break;
                                arrObj.title = GlobalModel.localizationData['PAGE_TEXT_FREE'] + "" + arrObj.id.split("_")[1];
                                arrObj.page = results[i].object_id;
                                arrObj.date = dateOfCreation;
                                arrObj.timeMilliseconds = annodate;
                                arrObj.annotation_id = results[i].annotation_id;

                                if ($.inArray(arrObj.id, bookmarkAddedPagesID) == -1) {
                                    if ((EPubConfig.ReaderType.toUpperCase() == "2TO5" || EPubConfig.ReaderType.toUpperCase() == "PREKTO1") && GlobalModel.bookMarks.length < 5) {
                                        GlobalModel.bookMarks.push(arrObj);
                                        bookmarkAddedPagesID.push(arrObj.id);
                                        $("#" + arrObj.id).trigger( Event.Bookmark.SaveBookmarkForPage );
                                    } else if (EPubConfig.ReaderType.toUpperCase() != "2TO5" && EPubConfig.ReaderType.toUpperCase() != "PREKTO1") {
                                        GlobalModel.bookMarks.push(arrObj);
                                        bookmarkAddedPagesID.push(arrObj.id);
                                        $("#" + arrObj.id).trigger( Event.Bookmark.SaveBookmarkForPage );
                                    }
                                }                                           
                            
                            }

                            break;
                        case 2:
                            //Highlight
                            function getElementsByXPath(doc, xpath) {
                                var nodes = [];
                                try {
                                    var result = doc.evaluate(xpath, doc, null, XPathResult.ANY_TYPE, null);

                                    for (var item = result.iterateNext(); item; item = result.iterateNext()) {
                                        nodes.push(item);
                                    }
                                } catch (exc) {
                                    // Invalid xpath expressions make their way here sometimes.  If that happens,
                                    // we still want to return an empty set without an exception.
                                }
                                return nodes;
                            }

                            var rangedata;

                            rangedata = document.createRange();
                            if(results[i].data != null)
                            {
                                var Obj = $.xml2json(results[i].data);
                            }
                            else
                            {
                                if(results[i].path.toString().indexOf("Container") == -1){
                                    console.log("Error in range Object:",results[i].path);
                                    break;
                                }
                                else{
                                    var Obj = $.xml2json(results[i].path);  
                                }   
                            }
                            
                            isSamePath = true;
                            if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                            {
                                isSamePath = objThis.checkPathforVolume(results[i].path);
                            }
							
                            if(isSamePath)
                            {
                                var startC = getElementsByXPath(document, Obj.startContainer);
                                var endC = getElementsByXPath(document, Obj.endContainer);
                                var startO = parseInt(Obj.startOffset);
                                var endO = parseInt(Obj.endOffset);

                                try {

                                    rangedata.setStart($(startC)[0].firstChild, startO);
                                    rangedata.setEnd($(endC)[0].firstChild, endO);

                                } catch (err) {
                                    //console.log(err);
                                }
                                
                                arrObj.annotationID = results[i].annotation_id;
                                arrObj.title = results[i].body_text;
                                arrObj.id = parseInt(GlobalModel.totalStickyNotes);
                                arrObj.date = dateOfCreation;
                                arrObj.type = "highlight";
                                if(results[i].visibility_id == 3)
                                    arrObj.savetonote = 0;
                                else
                                    arrObj.savetonote = 1;
                                arrObj.range = rangedata;
                                if(results[i].data != null)
                                     arrObj.rangeObject = results[i].data;
                                else
                                     arrObj.rangeObject = results[i].path;   
                                arrObj.style = parseInt(results[i].style);
                                if (arrObj.style == 1) {
                                    arrObj.selectioncolor = 'selectioncolor' + results[i].color + '_underline';
                                } else {
                                    arrObj.selectioncolor = 'selectioncolor' + results[i].color;
                                }
                                arrObj.selectionID = '.selection' + (GlobalModel.totalStickyNotes);
                                arrObj.timeMilliseconds = annodate;
                                arrObj.top = results[i].coord_y;
                                arrObj.right = results[i].coord_x;
								if( GlobalModel.AutomaticBookmarkDate != 0   )
                                {
                                	if( GlobalModel.AutomaticBookmarkDate.getTime() < annodate.getTime() )
                                	{
                                		arrObj.isNotified = true;
                                	}
                                	else
                                	{
                                		arrObj.isNotified = false;
                                	}
                                }
                                arrObj.tagsLists = [];
                                if( results[i].tags.results )
                                {
						            for ( var j = 0; j < results[i].tags.results.length; j++ ) 
						            {
						                arrObj.tagsLists.push( results[i].tags.results[ j ].value );
						            }
					            }
					            
					            if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && 
					            results[ i ].shared_with && results[ i ].shared_with.results && results[ i ].shared_with.results.length > 0
					            && EPubConfig.AnnotationSharing_isAvailable )
                        		{
                        			arrObj.teachersSharedWith = results[i].shared_with;
                        		}
                                
                                arrObj.pageNumber = results[i].object_id;
                               // console.log( results[i], isComment, " This is service manager")
                                if( isComment == true )
	                            {
	                                arrObj.shared_with = results[i].shared_with;
	                               // console.log(results[i].shared_with,arrObj,arrObj.shared_with, " Inside")
	                                strCompleteEvent = "";
	                            }
	                            else
	                            {
	                            	if(results[i].type_id != 3)
	                            	{
	                            		GlobalModel.AnnotationIDArr.push( results[i].annotation_id );
	                            	}
	                                
	                            }
	                            //console.log( arrObj, " This is service manager")
	                            GlobalModel.annotations.push(arrObj);
                                GlobalModel.totalStickyNotes++;
                               }
                            break;

                        //sticky note data;
                        case 3:
                            var isSamePath = true;
                            if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                            {
                                isSamePath = objThis.checkPathforVolume(results[i].path);
                            }
                            
                            if(isSamePath)
                            {
                                arrObj.title = results[i].body_text;
                                //arrObj.id = results[i].object_id;
                                arrObj.id = parseInt(GlobalModel.totalStickyNotes);

                                arrObj.type = "stickynote";
                                //arrObj.pageNumber = results[i].start_pos;
                                arrObj.pageNumber = results[i].object_id;
                                if(results[i].visibility_id == 3)
                                    arrObj.savetonote = 0;
                                else
                                    arrObj.savetonote = 1;
                                arrObj.range = results[i].data;
                                arrObj.date = dateOfCreation;
                                arrObj.timeMilliseconds = annodate;
                                arrObj.icontop = results[i].coord_x;
                                arrObj.iconleft = results[i].coord_y;
                                arrObj.annotationID = results[i].annotation_id;
                                if( GlobalModel.AutomaticBookmarkDate != 0   )
                                {
                                	if( GlobalModel.AutomaticBookmarkDate.getTime() < annodate.getTime() )
                                	{
                                		arrObj.isNotified = true;
                                	}
                                	else
                                	{
                                		arrObj.isNotified = false;
                                	}
                                }
                                arrObj.tagsLists = [];
                                if( results[i].tags.results )
                                {
                                    for ( var j = 0; j < results[i].tags.results.length; j++ ) 
    					            {
    					                arrObj.tagsLists.push( results[i].tags.results[ j ].value );
    					            }
                                }
								if(results[i].type_id != 2)
								{
									GlobalModel.annotations.push(arrObj);
                                	GlobalModel.totalStickyNotes++;
								}
	                        	
                            }
                            
                            if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && 
					            results[ i ].shared_with && results[ i ].shared_with.results && results[ i ].shared_with.results.length > 0 
					            && EPubConfig.AnnotationSharing_isAvailable )
                    		{
                    			arrObj.teachersSharedWith = results[i].shared_with;
                    		}
                    		
                            if( isComment == true )
                            {
                                arrObj.shared_with = results[i].shared_with;
                                strCompleteEvent = "";
                            }
                            else
                            {
                            	if(results[i].type_id != 2)
                            	{
                            		GlobalModel.AnnotationIDArr.push( results[i].annotation_id );
                            	}
                                
                            }
                            break;

                        case 8:
                            //Automatic BookMark
                            if( !hasAutomaticBookmark )
                            {
                                var isSamePath = true;
                                if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                {
                                    isSamePath = objThis.checkPathforVolume(results[i].path);
                                }
                                
                                if(isSamePath)
                                {
                                	if (results[i].annotation_id) {
                                    	//console.log( results[i].annotation_id , " Autobookmark")
                                        GlobalModel.AutomaticBookmarkAnnID = results[i].annotation_id;
                                        GlobalModel.AutomaticBookmarkDate = annodate;
                                    }
                                    
                                    if( GlobalModel.selectedStudentData && GlobalModel.BookEdition == "TSE" )
                                    {
                                    	return;
                                    }
                                    
                                    var page;
                                    hasAutomaticBookmark = true;
                                    if(results[i].path.toString().indexOf("page=") != -1)
                                    {
                                        var parser = document.createElement('a');
                                        parser.href = results[i].path.toString();
                                        var parts = parser.search.substr(1).split("&");
                                        for (var k = 0; k < parts.length; k++) {
                                            if(parts[k] != ""){
                                                var queryParamAndValue = parts[k].split("=");
                                                
                                                if(queryParamAndValue[0] == "page"){
                                                    page = queryParamAndValue[1];
                                                }
                                                                                                
                                            }
                                        }
                                        results[i].path = page;
                                    }
                                    
                                    var currentPageSequence = GlobalModel.pageBrkValueArr.indexOf(String(results[i].path));
                                    if (currentPageSequence < GlobalModel.pageBrkValueArr.length && currentPageSequence > -1) {
                                        if(!_objQueryParams.page){
                                            GlobalModel.currentPageIndex = parseInt(currentPageSequence);
                                       }
                                    }

                                 }
                                 if( GlobalModel.annotations.length > 0 && !GlobalModel.annotations[ 0 ].isNotified )
                                 {
                                 	for( var index = 0; index < GlobalModel.annotations.length; index++ )
                                 	{
                                 		var time = GlobalModel.annotations[ index ].timeMilliseconds.getTime();
                                 		if( time > GlobalModel.AutomaticBookmarkDate )
                                 		{
                                 			GlobalModel.annotations[ index ].isNotified = true;
                                 		}
                                 		else
                                 		{
                                 			GlobalModel.annotations[ index ].isNotified = false;
                                 		}
                                 	}
                                 }
                             }
                            break;
                            
                        //QA Panel
                        case 9:
                            //title denotes interactivityType
                            //object_id denotes element id
                            //book_text is the user response

                            if (GlobalModel.questionPanelAnswers == null)
                                GlobalModel.questionPanelAnswers = {};

                            if (GlobalModel.questionPanelAnswersForAnnoPanel == null)
                                GlobalModel.questionPanelAnswersForAnnoPanel = [];

                            if (GlobalModel.questionPanelAnswers[results[i].data] == null)
                                GlobalModel.questionPanelAnswers[results[i].data] = {};
                            var visibility_id;
                            if(results[i].visibility_id == 3)
                                visibility_id = 0;
                            else
                                visibility_id = 1;
                            
                            var isSamePath = true;
                            if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                            {
                                isSamePath = objThis.checkPathforVolume(results[i].path);
                            }
                            
                            if(isSamePath)
                            {
                                var page;
                                if(results[i].path.toString().indexOf("page=") != -1)
                                {
                                    var parser = document.createElement('a');
                                    parser.href = results[i].path.toString();
                                    var parts = parser.search.substr(1).split("&");
                                    for (var k = 0; k < parts.length; k++) {
                                        if(parts[k] != ""){
                                            var queryParamAndValue = parts[k].split("=");
                                            
                                            if(queryParamAndValue[0] == "page"){
                                                page = queryParamAndValue[1];
                                            }
                                                                                            
                                        }
                                    }
                                }
                                else
                                {
                                    page = results[i].path;
                                }
                                var objData = {
                                    id : results[i].object_id,
                                    response : results[i].body_text,
                                    pageNumber : page,
                                    interactivityType : results[i].title,
                                    creationDate : dateOfCreation,
                                    parentGrpId : results[i].data,
                                    flag_id : results[i].flag_id,
                                    annotation_id : results[i].annotation_id,
                                    savetonote : visibility_id
                                };
                                GlobalModel.QAAnnotationIDArr.push( objData.annotation_id );
                                GlobalModel.questionPanelAnswers[results[i].data][results[i].object_id] = objData;
                                var flag = -1;
                                if (results[i].flag_id == 1) {
                                    for (var k = 0; k < GlobalModel.questionPanelAnswersForAnnoPanel.length; k++) {
                                        if (GlobalModel.questionPanelAnswersForAnnoPanel[k].parentGrpId == results[i].data) {
                                            flag = 1;
                                            break;
                                        }
                                    }
                                    if (flag == -1) {
                                        GlobalModel.questionPanelAnswersForAnnoPanel.push(objData);
                                    }
                                }
                                //objThis.updateCookieData("qapanel");
                            }
                            break;

                        default:
                            break;
                    }

                }
            }
        } 
        
        if(strCompleteEvent == "autoBookmarkDataLoadComplete")
        {
            if( !hasAutomaticBookmark )
            {
                 var objToPost = {};
                 objToPost.content_id = EPubConfig.Identifier;
                 objToPost.type_id = type;
                 objToPost.path = GlobalModel.getMyNoteBookPagePath( "0" );
                 objToPost = ServiceManager.addVolumeDetails( objToPost );
                                         
                 annService.Annotations
                 .create( objToPost )
                 .done( function(data) {
                     GlobalModel.AutomaticBookmarkAnnID = data.annotation_id;
                 })
                 .fail( function( err ) {
                     console.log(err);
                 })
                 .run();
            }
        }
        if(strCompleteEvent == "stickyNotesDataLoadComplete")
        {
        	if( EPubConfig.AnnotationSharing_isAvailable )
        	{
        		objThis.noteLoadComplete = true;
        	}
        }
        else if(strCompleteEvent == "highlightDataLoadComplete")
        {
        	if( EPubConfig.AnnotationSharing_isAvailable )
        	{
        		objThis.highlightLoadComplete = true;
        	}
            
        }
        else if( strCompleteEvent == "qaPanelDataLoadComplete")
    	{
        	if( EPubConfig.AnnotationSharing_isAvailable )
        	{
        		if( GlobalModel.BookEdition == "SE" )
    			{
        			objThis.getQAComments( "SE" );
    			}
        		else if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    			{
        			objThis.getQAComments( "TE" );
    			}
        	}
    	}
        //complete event handler;
        if(objThis.highlightLoadComplete && objThis.noteLoadComplete)
        {
        	objThis.highlightLoadComplete =false;
        	objThis.noteLoadComplete = false;
			
			ServiceManager.getComments();
			
        	 //console.log( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData)
        	/*if( GlobalModel.BookEdition == "SE" ) //|| GlobalModel.selectedStudentData )
            {
				ServiceManager.getComments( "SE", false );
				ServiceManager.getComments( "SE", true );
			}
			else if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	ServiceManager.getComments( "TE", false );
            	ServiceManager.getComments( "TE", true );
            }
            else if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData )
            {
            	$("#mainShowroom").css("opacity", "1");
                $("#appPreloaderContainer").css("display","none");
            }*/
        }
        
        if (strCompleteEvent != "") 
        {
        	if( strCompleteEvent == "highlightDataLoadComplete" || strCompleteEvent == "stickyNotesDataLoadComplete" )
        	{
        		if( !EPubConfig.AnnotationSharing_isAvailable )
        		{
        			$(document).trigger(strCompleteEvent);
        		}
        	}
        	else
        	{
        		$(document).trigger(strCompleteEvent);
        	}
        }
    },
    
    getQAComments : function ( type, nextLink )
    {
    	var successHandler = function ( _data, str, nLink )
    	{
    		if( nLink != undefined && nLink != "" )
			{
				ServiceManager.commentInQA = $.merge( ServiceManager.commentInQA, _data );
        		ServiceManager.getQAComments( str, nLink );
			}
			else
			{
				ServiceManager.commentInQA = $.merge( ServiceManager.commentInQA, _data );
				populateCommentInQA( ServiceManager.commentInQA );
	            GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );// TODO
	            ServiceManager.commentInQA = [];
	            $(document).trigger( "qaCommentsLoaded" );// TODO
			}
    	}
    	
    	var populateCommentInQA = function ( data )
    	{
    		var isAnnoIDFound = false;
    		for(var comments = 0;comments < data.length; comments++)
            {
    			isAnnoIDFound = false;
    			for( var question in GlobalModel.questionPanelAnswers )
    			{
	                for(var annotations in GlobalModel.questionPanelAnswers[question] )
	                {
	                    if(data[comments].parent_id == GlobalModel.questionPanelAnswers[question][annotations].annotation_id)
	                    {
	                        var annodate;
	                        if (data[comments].updated_date) {
	                            annodate = data[comments].updated_date;
	                        } else {
	                            annodate = data[comments].created_date;
	                        }
	                        data[comments].timeMilliseconds = annodate;
	                        if( GlobalModel.AutomaticBookmarkDate != 0   )
	                        {
	                        	//console.log( GlobalModel.AutomaticBookmarkDate.getTime(), annodate.getTime(), " This is ServiceManager")
	                        	if( GlobalModel.AutomaticBookmarkDate.getTime() < annodate.getTime() )
	                        	{
	                        		GlobalModel.questionPanelAnswers[question][annotations].isNotified = true;
	                        	}
	                        	else
	                        	{
	                        		GlobalModel.questionPanelAnswers[question][annotations].isNotified = false;
	                        	}
	                        }
	
	                        var month = annodate.getMonth() + 1;
	                        var day = annodate.getDate();
	                        var year = annodate.getFullYear();
	                        var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
	                        data[comments].date = dateOfCreation;
	                        // TODO change question group icon and also add icon in QA panel on right side
	                        //$( "#btnStickyNote_" + GlobalModel.annotations[annotations].id ).addClass( "btnStickyNoteTeacherCommentHighlight" );
	                        
	                        $( "#" + question + "_indicator" ).addClass( "quizStatusQACommentAdded" );
	                        GlobalModel.questionPanelAnswers[question][annotations].comments = data[comments];
	                        isAnnoIDFound = true;
	                    }
	                }
    			}
    			
            }
    		
    	}

    	if( type == "TE" )
		{
    		if( nextLink != undefined && nextLink != "" )
			{
				annService.Annotations.find().next( nextLink ).done( function( data, nLink ){
					successHandler( data, "TE", nLink );
            	} ).run()
			}
			else
			{
				if( GlobalModel.QAAnnotationIDArr.length > 0 )
				{
					annService.Annotations.find( {parent_id:GlobalModel.QAAnnotationIDArr} ).done( function( data, nLink ){
						successHandler( data, "TE", nLink );
            		} ).fail( function(error){
						console.log("Error: ", error);
					}).run()
				}
			}
		}
    	else if( type == "SE" )
    	{
    		if( nextLink != undefined && nextLink != "" )
			{
				annService.Annotations.findComments().next(nextLink).done( function( data, nLink ) 
                {
                	successHandler( data, "SE", nLink  );
                } ).fail(function(err) {
                        console.log('fail: ', err); }).run();
			}
			else
			{
				annService.Annotations.findComments().done( function( data, nLink ) 
                {
                	successHandler( data, "SE", nLink );
                } ).fail(function(err) {
                        console.log('fail: ', err); }).run();
			}
    	}
    },
    
    triggerNoteHighlightCompleteEvent : function ()
    {
    	GlobalModel.annotations.sort( function( a, b ) 
		{
			var c = a.annotationID;
			var d = b.annotationID;
			return c - d;
		} );
		$(document).trigger("stickyNotesDataLoadComplete");
		$(document).trigger("highlightDataLoadComplete");
    },
    
    getComments : function( type, isStandalone, nextLink  )
    {
    	var objThis = this;   
		var objFuncForSubsequentServiceCall = null;
		var isServiceCallCompleted = false;
		 	
    	var successHandler = function ( data, bool, nLink, objCaller  )
    	{
    		var createNoteHighlightCommentArr = function ( data ){
    			for( var num = 0; num < data.length; num++ )
    			{
    				if( data[ num ].type_id == 3 )
    				{
    					ServiceManager.standaloneCommentNote.push( data[ num ]);
    				}
    				else if( data[ num ].type_id == 2 )
    				{
    					ServiceManager.standaloneCommentHighlight.push( data[ num ]);
    				}
    			}
    		}
			
			var strEvent = "";
    		
    		if( !bool )// standalone comment or not
    		{
    			if( nLink != undefined && nLink != "" )
    			{
    				ServiceManager.commentWithinNote = $.merge( ServiceManager.commentWithinNote, data );
            		objCaller( nLink );
    			}
    			else
    			{
    				ServiceManager.commentWithinNote = $.merge( ServiceManager.commentWithinNote, data );
            		objThis.populateCommentInNote( ServiceManager.commentWithinNote );
		            GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
		            ServiceManager.commentWithinNote = [];
		            $(document).trigger( "noteHighlightCommentsLoaded" );
					$("#mainShowroom").css("opacity", "1");
            		//$("#appPreloaderContainer").css("display","none");
            		isServiceCallCompleted = true;
            		
    			}
    		}
    		else
    		{
    			if( nLink != undefined && nLink != "" )
    			{
    				createNoteHighlightCommentArr( data );
    				objCaller( nLink );
    			}
    			else
    			{
					if(objFuncForSubsequentServiceCall != null)
					{
						objFuncForSubsequentServiceCall();
						objFuncForSubsequentServiceCall = null;
					}
					else
					{
						isServiceCallCompleted = true;
					}
    				createNoteHighlightCommentArr( data );
    				ServiceManager.storeAndParseAnnotationData( 3 , ServiceManager.standaloneCommentNote, true );
    				ServiceManager.storeAndParseAnnotationData( 2 , ServiceManager.standaloneCommentHighlight, true );
                    $(document).trigger( "standaloneCommentsLoaded" );
					$("#mainShowroom").css("opacity", "1");
            		//$("#appPreloaderContainer").css("display","none");
                    
                    GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
                    ServiceManager.standaloneCommentNote = [];
                    ServiceManager.standaloneCommentHighlight = [];                    
    			}
    		}
			if( isServiceCallCompleted )
			{
				objThis.triggerNoteHighlightCompleteEvent();
				setTimeout( function(){
					$("#appPreloaderContainer").css("display","none");
				},2000);
			}
			
    	}
		var fetchChildNoteAndTeacherCommentForOtherUser = function(nextLink)
		{
			if (nextLink != undefined && nextLink != "") {
				annService.Annotations.find().next(nextLink).done(function(data, nLink){
					successHandler(data, false, nLink,  fetchChildNoteAndTeacherCommentForOtherUser);
					//fetchChildNoteAndTeacherCommentForOtherUser(nLink);
				}).run()
			}
			else
			{
				annService.Annotations
                            .find({ type_id : 7, content_id : EPubConfig.Identifier })
                            .fields(["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos", "parent_id"])       /* optional: array of strings representing name of fields that should be included in each response row*/
							.forUser(GlobalModel.selectedStudentData.refId)
                            .withTags().order("updated_date", 'desc')
                            .limit(0) 
                            .parseDates()
                            .withShared()
                            .done(function( data, nLink ){
    							successHandler( data, false, nLink,  fetchChildNoteAndTeacherCommentForOtherUser);
								//fetchChildNoteAndTeacherCommentForOtherUser(nLink);
	                		} ).fail(function(error){
	                			objThis.triggerNoteHighlightCompleteEvent();
								console.log("Error: ", error);
							})
                            .run();
			}
		}
		
		var fetchChildNoteAndTeacherCommentForCurrentUser = function(nextLink)
		{
			if (nextLink != undefined && nextLink != "") {
				annService.Annotations.find().next(nextLink).done(function(data, nLink){
					successHandler(data, false, nLink, fetchChildNoteAndTeacherCommentForCurrentUser);
					//fetchChildNoteAndTeacherCommentForCurrentUser(nLink);
				}).run()
			}
			else
			{
				annService.Annotations
                            .find({ type_id : 7, content_id : EPubConfig.Identifier })
                            .fields(["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos", "parent_id"])       /* optional: array of strings representing name of fields that should be included in each response row*/
							.withTags().order("updated_date", 'desc')
                            .limit(0) 
                            .parseDates()
                            .withShared()
                            .done(function( data, nLink ){
    							successHandler( data, false, nLink, fetchChildNoteAndTeacherCommentForCurrentUser );
								//fetchChildNoteAndTeacherCommentForCurrentUser(nLink);
	                		} ).fail(function(error){
	                			objThis.triggerNoteHighlightCompleteEvent();
								console.log("Error: ", error);
							})
                            .run();
			}
		}
		var fetchStandAloneCommentForCurrentUser = function(nextLink)
		{
			if (nextLink != undefined && nextLink != "") {
				annService.Annotations.find({ content_id : EPubConfig.Identifier }).next( nextLink ).withSharedOnly().done( function( data, nLink ){
						successHandler( data, true, nLink, fetchStandAloneCommentForCurrentUser );
						//fetchStandAloneComment(nLink);
                	} ).run()
			}
			else
			{
				
				annService.Annotations.find({ content_id : EPubConfig.Identifier }).withSharedOnly().done( function( data, nLink ){
					successHandler( data, true, nLink, fetchStandAloneCommentForCurrentUser );
        		} ).fail( function(error){
        			objThis.triggerNoteHighlightCompleteEvent();
					console.log("Error: ", error);
				}).run()
			}
		}
		
		var fetchStandAloneCommentForOtherUser = function(nextLink)
		{
			if (nextLink != undefined && nextLink != "") {
				annService.Annotations.find({content_id: EPubConfig.Identifier}).forUser( GlobalModel.selectedStudentData.refId ).next(nextLink)
    					.withSharedOnly().done( function( data, nLink ){
						successHandler( data, true, nLink, fetchStandAloneCommentForOtherUser );
                	} ).run()
			}
			else
			{
				annService.Annotations.find({content_id: EPubConfig.Identifier}).forUser( GlobalModel.selectedStudentData.refId ).withSharedOnly().done( function( data, nLink ){
						successHandler( data, true, nLink, fetchStandAloneCommentForOtherUser );
            		} ).fail( function(error){
            			objThis.triggerNoteHighlightCompleteEvent();
						console.log("Error: ", error);
					}).run()
				
			}
		}
		//console.log('calling get comments--------------------------',GlobalModel.BookEdition, GlobalModel.selectedStudentData,GlobalModel.AnnotationIDArr);
		if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
		{
			//console.log("condition 1")
			objFuncForSubsequentServiceCall = fetchChildNoteAndTeacherCommentForOtherUser;
			fetchStandAloneCommentForOtherUser();
		}
		else if(GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData)
		{
			//console.log("condition 2")
			fetchChildNoteAndTeacherCommentForCurrentUser();
		}
		else if(GlobalModel.BookEdition == "SE")
		{
			//console.log("condition 3")
			objFuncForSubsequentServiceCall = fetchChildNoteAndTeacherCommentForCurrentUser;
			fetchStandAloneCommentForCurrentUser();
		}
    	else if(GlobalModel.BookEdition == "TE")
		{
			//console.log("condition 4")
			fetchChildNoteAndTeacherCommentForCurrentUser();
		}
    	
    },
    
    populateCommentInNote : function ( data )
    {
    	for(var comments = 0;comments < data.length; comments++)
        {
            for(var annotations = 0; annotations < GlobalModel.annotations.length; annotations++)
            {
                if(data[comments].parent_id == GlobalModel.annotations[annotations].annotationID)
                {
                    var annodate;
                    if (data[comments].updated_date) {
                        annodate = data[comments].updated_date;
                    } else {
                        annodate = data[comments].created_date;
                    }
                    data[comments].timeMilliseconds = annodate;
                    
                    if( GlobalModel.AutomaticBookmarkDate != 0   )
                    {
                    	if( GlobalModel.AutomaticBookmarkDate.getTime() < annodate.getTime() )
                    	{
                    		GlobalModel.annotations[annotations].isNotified = true;
                    	}
                    	else
                    	{
                    		GlobalModel.annotations[annotations].isNotified = false;
                    	}
                    }

                    var month = annodate.getMonth() + 1;
                    var day = annodate.getDate();
                    var year = annodate.getFullYear();
                    var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
                    data[comments].date = dateOfCreation;
                    if( GlobalModel.annotations[annotations].type == "highlight" )
                    {
                    	$( "#btnStickyNote_" + GlobalModel.annotations[annotations].id ).addClass( "btnStickyNoteTeacherCommentHighlight" );
                    }
                    else
                    {
                    	$( "#btnStickyNote_" + GlobalModel.annotations[annotations].id ).addClass( "btnStickyNoteTeacherComment" );
                    }
                    //GlobalModel.annotations[annotations].comments = data[comments];
					//console.log('highlight: ', GlobalModel.annotations[annotations])
					if(data[comments].shared_with)
					{
						if(data[comments].shared_with.results != undefined)
						{
							if(data[comments].shared_with.results.length != undefined)
							{
								if(data[comments].shared_with.results.length > 0)
								{
									if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData &&
										GlobalModel.annotations[annotations].type == "highlight"
										&& GlobalModel.annotations[annotations].shared_with != undefined)
									{
										GlobalModel.annotations[annotations].userComment = data[comments];
									}
									else if(GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData 
											&& GlobalModel.annotations[annotations].shared_with == undefined 
											&& GlobalModel.annotations[annotations].type == "highlight" )
									{
										GlobalModel.annotations[annotations].userComment = data[comments];
									}
									else if( GlobalModel.annotations[annotations].shared_with != undefined &&
											GlobalModel.BookEdition == "SE" && GlobalModel.annotations[annotations].type == "highlight" )
									{
										GlobalModel.annotations[annotations].userComment = data[comments];
									}
									else
									{
										GlobalModel.annotations[annotations].comments = data[comments];
									}
								}
								else
								{
									GlobalModel.annotations[annotations].userComment = data[comments];
								}
							}
						}
					}
					break;
                }
            }
        }
    },
    
    isDPLive : function() {
        var objThis = this;
        if(EPubConfig.tenant == "hmh")
        {
            if(EPubConfig.isDemo)
            {
                $.cookie("HMH_DP", "OFF");
                return false;
            }
            if($.cookie('Authn') != null)
            {
                GlobalModel.isToStoreAnnotationInCookie = true;
                $.cookie("HMH_DP", "ON");
                $.cookie("HMH_DP_ALERTED", "FALSE");
                
                //$.cookie("Authn", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJpc3MiOiJodHRwczovL215Lmhydy5jb20iLCJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlhdCI6MTQxMjgyODczNywic3ViIjoiY25cdTAwM2RETUFSU0hBTEwxNTksdWlkXHUwMDNkRE1BUlNIQUxMMTU5LG9cdTAwM2Q5OTAwMDIwMCxkY1x1MDAzZDk5MDAwMjAwLHN0XHUwMDNkTi9BLGNcdTAwM2ROL0EiLCJodHRwOi8vd3d3Lmltc2dsb2JhbC5vcmcvaW1zcHVybC9saXMvdjEvdm9jYWIvcGVyc29uIjpbIkluc3RydWN0b3IiXSwiUGxhdGZvcm1JZCI6IkhNT0YiLCJleHAiOjE0MTI4MzIzMzd9DQrXnnsb6ICzd+qUoSjGsEfnpLfQku6SkuOslxVg5L6A")
                var strAuthCookieVal = decodeBase64($.cookie('Authn'));
                strAuthCookieVal = strAuthCookieVal.toLowerCase();
				if(strAuthCookieVal.indexOf('uniqueidentifier') == -1)
                {
                    $.cookie("HMH_DP", "OFF");
					$.cookie("HMH_DP_ALERTED", "TRUE");
					EPubConfig.AnnotationSharing_isAvailable = false;
                    return false;
                }

                return GlobalModel.DPLive;
            }
            else if($.cookie('SCK_REF') != null)
            {
                for(var i = 0;i<whiteListcookieArray.length;i++)
                {
                    if(objThis.isValidWhitelistItem(whiteListcookieArray[i], $.cookie('SCK_REF')))
                    {
                        GlobalModel.isToStoreAnnotationInCookie = true;
                        $.cookie("HMH_DP", "ON");
                        $.cookie("HMH_DP_ALERTED", "FALSE");
                        return GlobalModel.DPLive;
                    }
                }
                $.cookie("HMH_DP", "OFF");
                if(_objQueryParams.skiplogin)
                {
                    //return false;
                }
                GlobalModel.isLoginPopup = true;
                return false;
            }
            else
            {
                if(strReaderPath != "")
                {
                    $.cookie("HMH_DP", "OFF");
                    if(_objQueryParams.skiplogin)
                    {
                    //  return false;
                    }
                    GlobalModel.isLoginPopup = true;
                }
                return false;
            }
        } 
        else 
        {
            return GlobalModel.DPLive;
        }
    },
    
    isValidWhitelistItem: function(strWhitelist, strCookie)
    {
        var objReg = /\^/gi;//(^.*brainhoney.com.$|^.gwinnett.k12.ga.us.$|^.desire2learn.com.$|^.mstarlms.com.$|^.focuslearn.org.$|^.*browardschools.com.$)/gi;
        if(objReg.test(strWhitelist))
        {
            strWhitelist = strWhitelist.replace(objReg, "");
            
            objReg = /\.\*\$/gi;
            strWhitelist = strWhitelist.replace(objReg, "");
             
            var strRegExpString = strWhitelist;
            objReg = new RegExp(strWhitelist, "gi");
            var val = objReg.test(strCookie);
            
            return val;
        }
        else
        {
            if (strWhitelist == $.cookie('SCK_REF'))
            {
                return true;
            }
        }
        return false;
        
         
    },
    
    checkPathforVolume: function(storedLocation)
    {
        var currentLocation = window.location.href;
        if(storedLocation == null)
        {
            return false;
        }
            
        if(currentLocation.split("?")[0].toString() == storedLocation.split("?")[0].toString())
        {
            return true;
        }
         return false;
	},    

    getStudentList: function (objCaller) {
		
		var strDomain = "";
		if(window.location.protocol.indexOf("https") == -1)
		{
			//strDomain = "https://" + window.location.hostname+(window.location.port ? ':'+window.location.port : '');
		}
		
		var studentListDataLoadCompleteHandler = function(objData)
		{
			console.log("studentListDataLoadCompleteHandler")
			if (sessionStorage.studentList == undefined) {
				sessionStorage.studentList = JSON.stringify(objData);
			}
			objCaller.success(objData);
		}
		
        if (document.location.hostname == "localhost")
        {
			if(sessionStorage.studentList)
			{
				studentListDataLoadCompleteHandler(JSON.parse(sessionStorage.studentList));
				return;
			}
			var url = getPathManager().getStudentListPath();
            $.getJSON(url, function (objData) {
               //objCaller.success(objData);
			   studentListDataLoadCompleteHandler(objData);
            }).error(function () {
            	objCaller.error();
            });
            return;
        }
        var uid = "";
        var sifAccessToken = "";
        var objStudentList = {};
        var sectionsList = [];
        var nClassCounter = 0;
		if(sessionStorage.studentList)
		{
			studentListDataLoadCompleteHandler(JSON.parse(sessionStorage.studentList));
			return;
		}
		

        var StudentList = function(objClassData)
        {           
            var strURL = strDomain + '/api/identity/v1/sectionRosters/'+objClassData.refId+';contextId=' + EPubConfig.platformId;
            $.ajax({
                type:"GET",
                url:strURL,
                headers: { 
                    "Authorization": sifAccessToken
                },
                success: function(data)
                {
                    nClassCounter++;

                    if (data.students) {
                        if (data.students.length) {
                            objClassData.students = [];
                            for (var i = 0; i < data.students.length; i++) {
                                var objSData = {}
								var objStudentData = data.students[i];
								objSData.firstName = "";
								objSData.lastName = "";
								if(objStudentData.name != undefined)
								{
									if(objStudentData.name.nameOfRecord != undefined)
									{
										if(objStudentData.name.nameOfRecord.familyName != undefined)
										{
											objSData.lastName = objStudentData.name.nameOfRecord.familyName;
										}
										
										if(objStudentData.name.nameOfRecord.givenName != undefined)
										{
											objSData.firstName = objStudentData.name.nameOfRecord.givenName;
										}
										
										if(objStudentData.name.nameOfRecord.middleName != undefined)
										{
											objSData.middleName = objStudentData.name.nameOfRecord.middleName;
										}
									}
								}
								if(objSData.firstName != "" || objSData.lastName != "")
								{
									objSData.id = objStudentData.refId;
                                    objSData.refId = objStudentData.refId;
									objSData.platformId = objStudentData.localId.idValue;
									objClassData.students.push(objSData);
								}
							}
                        }
                        if (nClassCounter == objStudentList.value.length) {
                            //objCaller.success(objStudentList);
							studentListDataLoadCompleteHandler(objStudentList);
                        }
                    }                    
                },
                error: function(e)
                {
                    nClassCounter++;
                    if(nClassCounter == sectionsList.length)
                    {
                        //objCaller.success(objStudentList);
						studentListDataLoadCompleteHandler(objStudentList);
                    }
                }
            })
        }

        function getclasses(){
            var strURL = strDomain + '/api/identity/v1/staffPersons/staffPerson/'+uid+'/section;contextId=' + EPubConfig.platformId;
            $.ajax({
                type:"GET",
                url:strURL,
                headers: { 
                    "Authorization": sifAccessToken
                },
                success: function(data)
                {
                    objStudentList.value = [];

                    if(data.sections)
                    {

                        sectionsList = data.sections;
                        var strClasses = "";
                        for(var i = 0; i < data.sections.length; i++)
                        {
                            var objClass = {};
                            objClass.classname = data.sections[i].name;
                            objClass.id = data.sections[i].refId;
                            objClass.refId = data.sections[i].refId;
                            objClass.localId = data.sections[i].localId.idValue;
                            objStudentList.value.push(objClass);
                            var objSL = new StudentList(objStudentList.value[objStudentList.value.length - 1]);
                            
                        }
                        
                    }
                },
                error: function(e)
                {
                    objCaller.error();
                }
            })
        }
        
        var requrl = strDomain + "/api/identity/v1/authorize;contextId=" + EPubConfig.platformId + "?&redirect=false&response_type=token" 
        requrl += "&scope=openid%20profile"
        requrl += "&client_id=HMH_EBOOK"
        requrl += "&platform=HMOF"
        requrl += "&state=fsklsda-1-1"
        requrl += "&redirect_uri=http://"+ document.location.host+"/";
        $.ajax({
            type:"GET",
            url:requrl,
            contentType: "application/json",
            headers: { 
                "content-type": 'application/x-www-form-urlencoded'
            },
            success: function(data)
            {
                sifAccessToken = data.access_token;
                uid = data.ref_id;
                getclasses();
            },
            error: function(e)
            {
                objCaller.error();
            }
        })
                
    },

    addVolumeDetails: function ( obj )
    {
        if( EPubConfig.Volume > -1 )
        {
            obj.l1 = EPubConfig.Volume + "";
        }
        return obj;
    },

    setStudentData: function(objData)
    {
    	GlobalModel.selectedStudentData = objData;
		
		sessionStorage.selectedStudentData = JSON.stringify(objData);

        ServiceManager.Annotations.fetchAllAnnotations();
    },

    getSearchResults : function(objInputData) {

        var searchText = objInputData.searchText;
        
        var nStart = (objInputData.start) ? objInputData.start : 1;
        var strVolume = "";

        if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName != undefined && EPubConfig.VolumeName != "")
        {
            strVolume = "+" + EPubConfig.VolumeName + ":" + EPubConfig.Volume;
        }

        var requestURL = this.searchService + searchText + "+ISBN:" + EPubConfig.Identifier + strVolume + "&start=" + nStart + "&pageLength=10&format=json";
        var searchResults;

        var objThis = this;

        $.getJSON(requestURL, function(objData) {
            objInputData.caller[objInputData.success](objData, objInputData.calltype);
        }).fail(function(jqxhr, textStatus, error) {
            objInputData.caller[objInputData.error](error);
        });
        
    },

    getResourcesJSON : function(objInputData) {
        $.getJSON(objInputData.url, function(objData) {
            objInputData.success(objData, objInputData.calltype);
        }).fail(function(jqxhr, textStatus, error) {
            objInputData.error(error, objInputData.calltype);
        });

    }

}