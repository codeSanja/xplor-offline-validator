/**
 * @author
 * This file defines functions for Data Persistance and Search API
 */
var ServiceManager = {

    searchService : "", //value will set after config.txt file loads;
    isServiceAuthenticated : false,
    getResults : null,
    noteLoadComplete : false,
    highlightLoadComplete : false,

    getUserRole: function()
    {
        var Authn = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlzcyI6Imh0dHBzOi8vbXkuaHJ3LmNvbSIsIm5iZiI6MTQwNjUzNDAxNCwiZXhwIjoxNDA2OTY2MDE0LCJQbGF0Zm9ybUlkIjoiSE1PRiIsIlJvbGVzIjoiVGVhY2hlciIsIkRpc3BsYXlOYW1lIjoiV29sdHogSmFjayIsIkxhdW5jaFVybCI6Imh0dHA6Ly9teS1yZXZpZXctY2VydC5ocncuY29tIiwic3ViIjoiY249V29sdHosdWlkPTMyNkE3NUE2MjM2NzQxNDk2NzlGRDZCNDlDMjU2NzZFLG89MDAyMDUxNjkifQ";
        var userInfo = decodeBase64(Authn);//$.cookie('Authn')    
        var objRegex = /Roles[\s\S]*?(,)/gi;
        var userRoleDet = userInfo.match(objRegex);
        objRegex = /,/gi;
        var userRole = userRoleDet[0].replace(objRegex, "").split(":")[1].replace(/["]/g, "");
        if(userRole == AppConst.USERTYPE_INSTRUCTOR)
        {
            userRole = AppConst.USERTYPE_TEACHER;
        }
        //console.log("userrole", userRole);
        return userRole;
        
    },

    initializeAnnotationServiceAPI: function()
    {
        var objThis = this;
        if( ServiceManager.DPLive )
        {
            annService.init ({
                clientId        : 'eBook',
                forceDefaultServiceSettings: true,
                successHandler : function(){
                    ServiceManager.isServiceAuthenticated = true;
                    ServiceManager.getAnnotations( 8 , 0 );
                    ServiceManager.getAnnotations( 9 , 0 );
                    $(document).trigger("annotationServiceInitiated");
                },
                errorHandler    : function(err){}
            });
        }
        else
        {
            if(location.port == "8080")
            {
                var objInitData = {
                    clientId        : 'eBook',
                    serverURL       : 'http://dubv-dpqaweb01.dubeng.local/', //DP QA ENV
                    usePostTunneling: false,
                    successHandler : function(){
                        objThis.DPLive = true;
                        ServiceManager.isServiceAuthenticated = true;
                        ServiceManager.getAnnotations( 8 , 0 );
                        ServiceManager.getAnnotations( 9 , 0 );
                        $(document).trigger("annotationServiceInitiated");
                    },
                    errorHandler    : function(err){
                        
                    }
                };

                if(document.location.hostname == "localhost")
                {
                    objInitData.cors = true;
                }

                annServiceTokenGenerator.getToken ({
                        userId : 'EBOOKINTSTUD1',
                        role : 'Student',
                        platform: 'HMOF'
                    },
                    function(mytoken) {
                        objInitData.token = mytoken;
                        annService.init (objInitData);
                    },
                    function(xhr, error) {console.log('error generating token: ', error);}
                );
            }
        }
    },

    /**
     *Get tags information from Data Persistance Server
     *@param {int} id
     *@param {string} type
     */
    getTagsOfAnnotation : function( id , successHandler, errorHandler ) 
    {
        var objThis = this;
        if ( this.DPLive && objThis.isServiceAuthenticated ) 
        {
            objThis.getContext().Tags
            .find( { annotation_id: id } )
            .done( successHandler )
            .fail( errorHandler )
            .run();
        }

    },

    /**
     *Retrieve Annotations from Data Persistance Server
     *@param {int} type
     */
    getAnnotations : function(type, startPos, nextLink) {
        var objThis = this;
        var bookmarkAddedPagesID = [];
       
        if (this.DPLive && objThis.isServiceAuthenticated) {
            var strCompleteEvent = "";

            switch(type) {
                case 1:
                    strCompleteEvent = Event.Bookmark.BookmarkDataLoadComplete;
                    break;
                case 3:
                    strCompleteEvent = "stickyNotesDataLoadComplete";
                    break;
                case 2:
                    strCompleteEvent = "highlightDataLoadComplete";
                    break;
                case 8:
                    strCompleteEvent = "autoBookmarkDataLoadComplete";
                    break;
                case 9:
                    strCompleteEvent = "qaPanelDataLoadComplete";
                    break;
            }

            function onError () 
            {
                //alert("On Error");
                if ( strCompleteEvent != "" )
                    $( document ).trigger( strCompleteEvent );
            }
            
            function getresults( results , nextLink ) 
            {

                switch( type )
                {
                    case 1:
                        GlobalModel.bookmarksData = GlobalModel.bookmarksData.concat( results )
                    break;
                    case 2:
                        GlobalModel.highlightData = GlobalModel.highlightData.concat( results );
                    break;
                    case 3:
                        console.log('results: ', results);
                        GlobalModel.stikyNoteData = GlobalModel.stikyNoteData.concat( results );
                    break;
                    case 9:
                        GlobalModel.QAData = GlobalModel.QAData.concat( results );
                    break;
                    
                }
                if( nextLink != "" && nextLink != undefined )
                {
                    
                    switch( type ) 
                    {
                        case 1:
                            /*if(startPos == 0 && localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                            {
                                console.log("local Storage retrived for bookmark");
                                var bookmarkArr = jQuery.parseJSON(localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                                console.log("bookmark:::::",bookmarkArr[0].created_date,results[0].created_date);
                                if(bookmarkArr[0].created_date != results[0].created_date)
                                {
                                    objThis.getAnnotations(type, GlobalModel.bookmarksData.length);
                                }
                                else
                                {
                                    console.log("bookamrk created date matched");
                                    objThis.storeAndParseAnnotationData(type, bookmarkArr);
                                }
                            }
                            else*/
                            {
                                //console.log("local Storage didn't retrived for bookmark");
                                objThis.getAnnotations( type, GlobalModel.bookmarksData.length , nextLink );
                            }
                        break;
                        case 2:
                            /*if(startPos == 0 && localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                            {
                                var highlightArr = jQuery.parseJSON(localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                                if(highlightArr[0].created_date != results[0].created_date)
                                {
                                    objThis.getAnnotations(type, GlobalModel.highlightData.length);
                                }
                                else
                                {
                                    objThis.storeAndParseAnnotationData(type, highlightArr);
                                }
                            }
                            else*/
                            {
                                objThis.getAnnotations( type, GlobalModel.highlightData.length , nextLink );
                            }
                            
                        break;
                        case 3:
                            /*if(startPos == 0 && localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                            {
                                var stickyNoteArr = jQuery.parseJSON(localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                                if(stickyNoteArr[0].created_date != results[0].created_date)
                                {
                                    objThis.getAnnotations(type, GlobalModel.stikyNoteData.length);
                                }
                                else
                                {
                                    objThis.storeAndParseAnnotationData(type, stickyNoteArr);
                                }
                            }
                            else*/
                            {
                                objThis.getAnnotations( type, GlobalModel.stikyNoteData.length , nextLink );
                            }
                        break;
                        case 9:
                            /*if(startPos == 0 && localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                            {
                                var qaArr = jQuery.parseJSON(localStorage.getItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type))
                                if(qaArr[0].created_date != results[0].created_date)
                                {
                                    objThis.getAnnotations(type, GlobalModel.QAData.length);
                                }
                                else
                                {
                                    objThis.storeAndParseAnnotationData(type, qaArr);
                                }
                            }
                            else*/
                            {
                                objThis.getAnnotations( type, GlobalModel.QAData.length , nextLink );
                            }
                        break;
                        case 8 : 
                         objThis.storeAndParseAnnotationData( type, results );
                        break;
                    }
                }
                else
                {
                    switch( type ) 
                    {
                        case 1:
                            //console.log("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type)
                            //localStorage.setItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type , JSON.stringify(GlobalModel.bookmarksData));
                            objThis.storeAndParseAnnotationData( type , GlobalModel.bookmarksData );
                        break;
                        case 2:
                            //localStorage.setItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type , JSON.stringify(GlobalModel.highlightData));
                            objThis.storeAndParseAnnotationData( type , GlobalModel.highlightData );
                        break;
                        case 3:
                            //localStorage.setItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type , JSON.stringify(GlobalModel.stikyNoteData));
                            objThis.storeAndParseAnnotationData( type , GlobalModel.stikyNoteData );
                        break;
                        case 9:
                            //localStorage.setItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type , JSON.stringify(GlobalModel.QAData));
                            objThis.storeAndParseAnnotationData( type , GlobalModel.QAData );
                        break;
                        case 8:
                            objThis.storeAndParseAnnotationData( type, results );
                        break;
                        
                    }
                }
            }

            if( nextLink == "" || nextLink == undefined )
            {
                objThis.getContext().Annotations
                .find({ type_id : type, content_id : EPubConfig.Identifier })
                .fields(["annotation_id", "style", "title", "content_id", "data", "flag_id", "updated_date", "created_date", "object_id", "path", "color", "body_text", "visibility_id", "coord_y", "coord_x", "start_pos"])       /* optional: array of strings representing name of fields that should be included in each response row*/
                .order("updated_date", 'desc') /* optional: field is a string or array of strings representing fields that the results should be ordered by. 'desc' is optional, default is 'asc' order. */
                .limit(startPos)     /* optional: from is an offset, count is number of results, both Numbers */
                .parseDates()           /* optional: date fields (i.e. created_date will be returned as JS Date objects and not strings. Default: false */
                .done(getresults)
                .fail(onError)
                .run();
            }
            else
            {
                objThis.getContext().Annotations
                   .find()
                   .next(nextLink)
                   .done(getresults)
                   .fail(onError)
                   .run();
            }
                
                   
        } else {
            
            switch(type) {
                case 9:
                    setTimeout(function() {
                        $(document).trigger("qaPanelDataLoadComplete");
                    }, 1000);
                    break;
            }
        }

    },

    /**
     *Delete Annotation on Data Persistance Server
     *@param {string} query
     *@param {string} type
     */
    deleteAnnotation : function ( id, type ) 
    {
        var objThis = this;
        if ( this.DPLive && objThis.isServiceAuthenticated ) 
        {
            switch( type ) 
            {
                case 3:
                        objThis.updateCookieData("stickynote");
                break;

                default:
                        objThis.updateCookieData("highlight");
                        objThis.updateCookieData("bookmark");
                break;
            }

            objThis.getContext().Annotations
            .remove( id )
            .done( function (){ console.log( "DELETE OK" ); } )
            .fail( function ( err ){ console.log( err ); } )
            .run();
        }
    },
    
    addVolumeDetails: function ( obj )
    {
        if( EPubConfig.Volume > -1 )
        {
            obj.l1 = EPubConfig.Volume + "";
        }
        return obj;
    },
    
    getVolumeDetails: function ()
    {
        if( EPubConfig.Volume > -1 )
        {
            return EPubConfig.VolumeName + "" + EPubConfig.Volume;
        }
        return "";
    },

    /**
     *Create Annotation on Data Persistance Server
     *@param {Object} arrObj
     *@param {string} type
     */
    createAnnotation : function ( arrObj , type , successHandler ) 
    {
        var objThis = this;
        var objToPost = {};
        if ( this.DPLive && objThis.isServiceAuthenticated ) 
        {
            switch( type ) 
            {
                //Bookmark
                case 1:
                        objThis.updateCookieData( "bookmark" );
                        var location = GlobalModel.getMyNoteBookPagePath( arrObj.page.toString() );
                        //objToPost.content_id = EPubConfig.Identifier;
                        //objToPost.type_id = type;
                        //objToPost.title = arrObj.title;
                        //objToPost.object_id = arrObj.page.toString();
                        //objToPost.path = location;
                        objToPost = objThis.addVolumeDetails( objToPost ); // USED TO ADD l1 PARAMETER FOR VOLUME
                        
                        objThis.getContext().Annotations
                            .create( objToPost )
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
                        //localStorage.setItem("_" + GlobalModel.uid + "_" + EPubConfig.Identifier + "_" + objThis.getVolumeDetails() + "_" + type , JSON.stringify(GlobalModel.bookmarks));
                break;
                
                //Highlight
                case 2:
                        objThis.updateCookieData( "highlight" );
                        var colordata = arrObj.selectioncolor.replace( 'selectioncolor' , '' );
                        colordata = colordata.replace( '_underline' , '' );
                        var location = GlobalModel.getMyNoteBookPagePath( arrObj.pageNumber );
                        var visibility_id;
                        if( arrObj.savetonote )
                            visibility_id = 1;
                        else    
                            visibility_id = 3;
                            
                        //objToPost.content_id = EPubConfig.Identifier;
                        //objToPost.type_id = type;
                        //objToPost.color = colordata;
                        //objToPost.body_text = arrObj.title;
                        //objToPost.visibility_id = visibility_id;
                        //objToPost.coord_y = parseInt( arrObj.top );
                        //objToPost.title = arrObj.selectionID;
                        //objToPost.object_id = arrObj.pageNumber;
                        //objToPost.data = arrObj.rangeObject;
                        //objToPost.path = location;
                        //objToPost.style = arrObj.style.toString();
                        objToPost = objThis.addVolumeDetails( objToPost );
                                                
                        if( arrObj.tagsLists.length )
                        {
                            objThis.getContext().Annotations
                                .create( objToPost )
                                .tags( arrObj.tagsLists )
                                .done( successHandler )
                                .fail( function( err ) {
                                    console.log(err);
                                })
                                .run();
                        }
                        else
                        {
                            objThis.getContext().Annotations
                                .create( objToPost )
                                .done( successHandler )
                                .fail( function( err ) {
                                    console.log(err);
                                })
                                .run();
                        }
                        
                break;

                //QA Panel
                case 9:
                        objThis.updateCookieData( "qapanel" );
                        var visibility_id;
                        if( arrObj.savetonote )
                            visibility_id = 3;
                        else    
                            visibility_id = 1;
                        var location = GlobalModel.getMyNoteBookPagePath( $.trim( arrObj.pageNumber.replace( '<span class="questionPanelPageSpan"></span> ' , "" ) ) );
                        
                        //objToPost.content_id = EPubConfig.Identifier;
                        //objToPost.type_id = type;
                        //objToPost.title = arrObj.interactivityType;
                        //objToPost.body_text = arrObj.response.toString();
                        //objToPost.object_id = arrObj.id.toString();
                        //objToPost.path = location;
                        objToPost.data = arrObj.parentGrpId;
                        //objToPost.flag_id = arrObj.flag_id;
                        //objToPost.visibility_id = visibility_id;
                        objToPost = objThis.addVolumeDetails( objToPost );
                        
                        annService.Annotations
                            .create(objToPost)
                            .done( successHandler )
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
                break;
               
                case 3 :
                        objThis.updateCookieData( "stickynote" );
                        var visibility_id;

                        if( arrObj.savetoNote )
                            visibility_id = 1;
                        else    
                            visibility_id = 3;
                        var location = GlobalModel.getMyNoteBookPagePath( arrObj.pagenum );
                        
                        //objToPost.content_id = EPubConfig.Identifier;
                        objToPost.start_pos = parseInt( arrObj.id );
                        //objToPost.path = location;
                        //objToPost.type_id = type;
                        //objToPost.title = arrObj.title;
                        objToPost.coord_x = arrObj.icontop;
                        //objToPost.coord_y = arrObj.iconleft;
                        //objToPost.object_id = arrObj.pagenum;
                        //objToPost.visibility_id = visibility_id;
                        //objToPost.body_text = arrObj.stickyContent;
                        objToPost.data = arrObj.rangeObject;
                        objToPost = objThis.addVolumeDetails( objToPost );
                        
                        if( arrObj.tagsLists.length )
                        {
                            objThis.getContext().Annotations
                                .create( objToPost )
                                .tags( arrObj.tagsLists )
                                .done( successHandler )
                                .fail( function( err ) {
                                    console.log(err);
                                })
                                .run();
                        }
                        else
                        {
                            objThis.getContext().Annotations
                                .create( objToPost )
                                .done( successHandler )
                                .fail( function( err ) {
                                    console.log(err);
                                })
                                .run();
                        }
                break;

                default:
                    break;
            }

        }
    },

    /**
     *Update Annotation on Data Persistance Server
     *@param {string} type
     *@param {int} index
     */
    updateAnnotation : function( type , objData ) 
    {
        var objThis = this;
        var objToPost = {};
        if ( this.DPLive && objThis.isServiceAuthenticated ) 
        {
            switch( type ) 
            {
                case 8:
                        var sucessHandler = function () 
                        {
                            console.log( "INSIDE SUCESS: HMH_DP_ON" )
                            $.cookie( "HMH_DP" , "ON" );
                            $.cookie( "HMH_DP_ALERTED" , "FALSE" );
                        }
                        var errorHandler = function () 
                        {
                            console.log( "INSIDE FAIL: HMH_DP_OFF" )
                            GlobalModel.hmh_dp_off_counter++;
                            $.cookie( "HMH_DP" , "OFF" );
                            $.cookie( "HMH_DP_ERRCOUNT" , GlobalModel.hmh_dp_off_counter );
                            if ( $.cookie( "HMH_DP_ERRCOUNT_TS" ) == null )
                            {
                                $.cookie( "HMH_DP_ERRCOUNT_TS" , new Date().getTime() );
                            }
                        }
                        if (objData) 
                        {
                                if( ( GlobalModel.AutomaticBookmarkAnnID != undefined ) && ( GlobalModel.AutomaticBookmarkAnnID != 0 ) )
                                {
                                    var location = GlobalModel.getMyNoteBookPagePath( objData.toString() );
                                    objToPost.path = location;
                                    objToPost = objThis.addVolumeDetails( objToPost );
                                    
                                    objThis.getContext().Annotations
                                    .update( GlobalModel.AutomaticBookmarkAnnID , objToPost ) 
                                    .done( sucessHandler )
                                    .fail(errorHandler)
                                    .run();
                                }
                         }

                break;

                case 2:
                    
                        objThis.updateCookieData( "highlight" );
                        var colordata = objData.selectioncolor.replace( 'selectioncolor' , '' );
                        colordata = colordata.replace( '_underline' , '' );
                        var annotationid = objData.annotation_id;
                        var visibility_id;
                        if( objData.savetonote )
                            visibility_id = 1;
                        else    
                            visibility_id = 3;
                        objToPost.color = colordata;
                        objToPost.visibility_id = visibility_id;
                        objToPost.title = objData.selectionID;
                        objToPost.style = objData.style.toString();
                        objToPost = objThis.addVolumeDetails( objToPost );
                        
                        objThis.getContext().Annotations
                        .update( annotationid , objToPost ) 
                        .done( function ( ) {
                            console.log("Updated");
                        } )
                        .fail(function( err ) {
                            console.log(err);
                        })
                        .run();

                break;

                case 9:
                        objThis.updateCookieData( "qapanel" );
                        var objToPost = {};
                        var annotationid = GlobalModel.questionPanelAnswers[objData.parentGrpId][objData.id].annotation_id;
                        var visibility_id;
                        if( objData.savetonote )
                            visibility_id = 3;
                        else    
                            visibility_id = 1;
                            
                        objToPost.body_text = objData.response.toString();
                        objToPost.visibility_id = visibility_id;
                        objToPost.flag_id = objData.flag_id;
                        objToPost = objThis.addVolumeDetails( objToPost );
                        objThis.getContext().Annotations
                            .update( annotationid , objToPost ) 
                            .done( function ( ) {
                                console.log("Updated");
                            } )
                            .fail(function( err ) {
                                console.log(err);
                            })
                            .run();
                                    
                                
                        
                break;

                case 3 : 
                        objThis.updateCookieData( "stickynote" );
                        var _annotationid = objData.annotation_id;
                        var visibility_id;
                        if( objData.savetonote )
                            visibility_id = 1;
                        else    
                            visibility_id = 3;
                            
                        objToPost.title = "note_text";
                        objToPost.visibility_id = visibility_id;
                        objToPost.body_text = objData.stickyContent;
                        objToPost.object_id = objData.objectID;
                        objToPost.data = objData.rangeObject;
                        objToPost = objThis.addVolumeDetails( objToPost );

                        objThis.getContext().Annotations
                        .update( _annotationid , objToPost ) 
                        .done( function ( ) {
                            console.log("Updated");
                        } )
                        .fail(function( err ) {
                            console.log(err);
                        })
                        .run();
                        
                break;

                default:
                    break;
            }
        }
    },

    /**
     *Update Annotation with Tags on Data Persistance Server
     *@param {string} type
     *@param {object} objData
     */
    updateAnnotationAndTags : function( objData, iType ) 
    {
        var objThis = this;
        var objToPost = {};
        if ( this.DPLive && objThis.isServiceAuthenticated ) 
        {
            var objThis = this;
            var annotationid = objData.annotation_id;
            //updating tag list
            function updateTagList( tagresultsdata ) 
            {
                var arrTags = objData.tagsLists;
                var arrTagsToAdd = [];
                //creating new tags;
                for ( var i = 0; i < arrTags.length; i++ ) 
                {
                    var result = $.grep( tagresultsdata , function(e) 
                    {
                        return e.value == arrTags[i];
                    });

                    if ( result.length == 0 ) 
                    {
                        //adding new tag in list of tags to be created
                        arrTagsToAdd.push( { 'value' : arrTags[i] } );
                    }
                }
                
                if ( arrTagsToAdd.length != 0 ) 
                {
                    objThis.getContext().Tags
                        .create( arrTagsToAdd )
                        .commonFields( { 'annotation_id' : annotationid , 'content_id': EPubConfig.Identifier } )
                        .done( function(  ) {
                            console.log( "created tags" );
                        })
                        .fail( function( err ) {
                            console.log(err);
                        })
                        .run();
                }  
                //deleting deleted tags;
                var arrTagsToDelete = [];
                for ( i = 0; i < tagresultsdata.length; i++ ) 
                {
                    var result = $.grep( arrTags , function(e) 
                    {
                        return e == tagresultsdata[i].value
                    });

                    if ( result.length == 0 ) 
                    {
                        arrTagsToDelete.push( tagresultsdata[i].tag_id );
                    }
                }

                if (arrTagsToDelete.length > 0)
                {
                    objThis.getContext().Tags
                       .remove( arrTagsToDelete )
                       .done( function (){ console.log("DELETE OK"); } )
                       .fail( function ( err ){ console.log( err ); } )
                       .run()
                }
            }
            
            switch( iType ) 
            {
                case 3:
                       objThis.updateAnnotation( iType , objData);
                        //fetching tag list from server for current annotation;
                        objThis.updateCookieData( "stickynote" );
                        
                break;
                
                case 2:
                        objThis.updateAnnotation( iType , objData);
                        //fetching tag list from server for current annotation;
                        objThis.updateCookieData( "highlight" );
                break;

            }
            
            objThis.getContext().Tags
            .find( { annotation_id: annotationid } )
            .done( updateTagList )
            .fail( function (err){console.log(err)} )
            .run();
        }
    },

    fetchData : function ( urlString ) 
    {
        return $.ajax({
            url : urlString,
            type : 'GET',
            async : false,
            processData : false,
            crossDomain : true
        });

    },

    getSearchResults : function(objInputData) {

        var searchText = objInputData.searchText;
        //console.log("searchText: ", searchText);
        var nStart = (objInputData.start) ? objInputData.start : 1;
        var strVolume = "";

        if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName != undefined && EPubConfig.VolumeName != "")
        {
            strVolume = "+" + EPubConfig.VolumeName + ":" + EPubConfig.Volume;
        }

        var requestURL = this.searchService + searchText + "+ISBN:" + EPubConfig.Identifier + strVolume + "&start=" + nStart + "&pageLength=10&format=json";
        var searchResults;

        var objThis = this;

        $.getJSON(requestURL, function(objData) {
            objInputData.caller[objInputData.success](objData, objInputData.calltype);
        }).fail(function(jqxhr, textStatus, error) {
            objInputData.caller[objInputData.error](error);
        });
        
    },

    getResourcesJSON : function(objInputData) {
        //console.log( objInputData );
        //objInputData.url = "content/en/ese_9780544157217_/OPS/data/resources/resources.json";
        $.getJSON(objInputData.url, function(objData) {
            objInputData.caller[objInputData.success](objData, objInputData.calltype);
            //callBackFunc(objData, strType);
            //objClassRef.objResources = objData;
            //console.log(objClassRef.objResources);
        }).fail(function(jqxhr, textStatus, error) {
        //  console.log(error);
            objInputData.caller[objInputData.error](error, objInputData.calltype);
        });
    },

    getContext : function() {
        if (isNative || navigator.userAgent.match(/Desktop/i) != null ) {
            return DatabaseManager;
        } else {
            if (EPubConfig.tenant == "hmh") {
                return annService;
            } else {
                return WebData;
            }
        }

    },

    updateCookieData : function(type) {
        /*var cookieData = "";
        try{
            if(GlobalModel.isToStoreAnnotationInCookie)
            {
                if(type == "bookmark")
                {
                    for(var i = 0;i< GlobalModel.bookMarks.length;i++)
                    {
                            var tempObject = {};
                            for (var key in GlobalModel.bookMarks[i]) {
                                tempObject[key] = GlobalModel.bookMarks[i][key];
                            }
                            cookieData += JSON.stringify(tempObject) + "#";
                    }
                    $.cookie("HMH_DP_BOOKMARK_DATA", cookieData.slice(0,-1));
                }
                else if(type == "qapanel")
                {
                    cookieData += JSON.stringify(GlobalModel.questionPanelAnswers);
                    $.cookie("HMH_DP_QAPANEL_DATA", cookieData);
                }
                else
                {
                    for(var i = 0;i< GlobalModel.annotations.length;i++)
                    {
                        if(GlobalModel.annotations[i].type == type)
                        {
                            var tempObject = {};
                            for (var key in GlobalModel.annotations[i]) {
                                if(key == "annotation_id" || key == "title")
                                    tempObject[key] = GlobalModel.annotations[i][key];
                            }
                            cookieData += JSON.stringify(tempObject) + "#";
                        }
                        
                    }
                    if(type == "stickynote")
                        $.cookie("HMH_DP_NOTE_DATA", cookieData.slice(0,-1));
                    else if(type == "highlight")
                    {
                        $.cookie("HMH_DP_HIGHLIGHT_DATA", cookieData.slice(0,-1));  
                    }
                                
                }   
            }
        }
        catch(e){
            
        }*/ 
    },
    
    isDPLive : function() {
        var objThis = this;
        if(EPubConfig.tenant == "hmh")
        {
            if(EPubConfig.isDemo)
            {
                $.cookie("HMH_DP", "OFF");
                return false;
            }
            if($.cookie('Authn') != null)
            {
                GlobalModel.isToStoreAnnotationInCookie = true;
                $.cookie("HMH_DP", "ON");
                $.cookie("HMH_DP_ALERTED", "FALSE");
                return GlobalModel.DPLive;
            }
            else if($.cookie('SCK_REF') != null)
            {
                for(var i = 0;i<whiteListcookieArray.length;i++)
                {
                    //console.log(whiteListcookieArray[i], "-----", objThis.isValidWhitelistItem(whiteListcookieArray[i], strCookie));
                    //return;
                    if(objThis.isValidWhitelistItem(whiteListcookieArray[i], $.cookie('SCK_REF')))
                    {
                        GlobalModel.isToStoreAnnotationInCookie = true;
                        $.cookie("HMH_DP", "ON");
                        $.cookie("HMH_DP_ALERTED", "FALSE");
                        return GlobalModel.DPLive;
                    }
                }
                $.cookie("HMH_DP", "OFF");
                if(_objQueryParams.skiplogin)
                {
                    //return false;
                }
                GlobalModel.isLoginPopup = true;
                return false;
            }
            else
            {
                if(strReaderPath != "")
                {
                    $.cookie("HMH_DP", "OFF");
                    if(_objQueryParams.skiplogin)
                    {
                    //  return false;
                    }
                    GlobalModel.isLoginPopup = true;
                }
                return false;
            }
        } 
        else 
        {
            return GlobalModel.DPLive;
        }
    },
    
    isValidWhitelistItem: function(strWhitelist, strCookie)
    {
        var objReg = /\^/gi;//(^.*brainhoney.com.$|^.gwinnett.k12.ga.us.$|^.desire2learn.com.$|^.mstarlms.com.$|^.focuslearn.org.$|^.*browardschools.com.$)/gi;
        //console.log("strWhiteliststrWhiteliststrWhiteliststrWhitelist: ", objReg.test(strWhitelist))
        if(objReg.test(strWhitelist))
        {
            //var strWhitelist = "^.*brainhoney.com.$|^.gwinnett.k12.ga.us.$|^.desire2learn.com.$|^.mstarlms.com.$|^.focuslearn.org.$|^.*browardschools.com.$";
            //var strURL = "http://abccat.defdogs.brainhoney.com.abc";
            //var strURL = "http://abccat.defdogs.gwinsnett.k12.ga.us/abc";
             
            //var objReg = /\^/gi;//(^.*brainhoney.com.$|^.gwinnett.k12.ga.us.$|^.desire2learn.com.$|^.mstarlms.com.$|^.focuslearn.org.$|^.*browardschools.com.$)/gi;
            strWhitelist = strWhitelist.replace(objReg, "");
            
            objReg = /\.\*\$/gi;
            strWhitelist = strWhitelist.replace(objReg, "");
            
            //objReg = /[\*]/gi;
            //strWhitelist = strWhitelist.replace(objReg, "[\\s\\S]*");
            
            
             
            var strRegExpString = strWhitelist;
            objReg = new RegExp(strWhitelist, "gi");
            var val = objReg.test(strCookie);
            //console.log(strURL.match(objReg));
            return val;
        }
        else
        {
            if (strWhitelist == $.cookie('SCK_REF'))
            {
                return true;
            }
        }
        return false;
        
         
    },
    
    checkPathforVolume: function(storedLocation)
    {
        var currentLocation = window.location.href;
        if(storedLocation == null)
        {
            return false;
        }
            
        if(currentLocation.split("?")[0].toString() == storedLocation.split("?")[0].toString())
        {
            return true;
        }
    
        return false;
    },
    
    storeAndParseAnnotationData : function(type, results)
    {
        var strCompleteEvent = "";
        var objThis = this;
        var bookmarkAddedPagesID = [];
        var hasAutomaticBookmark = false;
        
        switch(type) {
            case 1:
                strCompleteEvent = Event.Bookmark.BookmarkDataLoadComplete;
                break;
            case 3:
                strCompleteEvent = "stickyNotesDataLoadComplete";
                break;
            case 2:
                strCompleteEvent = "highlightDataLoadComplete";
                break;
            case 8:
                strCompleteEvent = "autoBookmarkDataLoadComplete";
                break;
            case 9:
                strCompleteEvent = "qaPanelDataLoadComplete";
                break;
        }
                        if (results.length > 0) {
                    for (var i = 0; i < results.length; i++) {

                        if (results[i].content_id == EPubConfig.Identifier) {
                            var arrObj = new Array();

                            var annodate;
                            if (results[i].updated_date) {
                                annodate = results[i].updated_date;
                            } else {
                                annodate = results[i].created_date;
                            }

                            var month = annodate.getMonth() + 1;
                            var day = annodate.getDate();
                            var year = annodate.getFullYear();
                            var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);

                            var isSamePath;
                            
                            switch(type) {

                                case 1:
                                    arrObj = {};
                                    //Bookmark
                                                                        
                                    isSamePath = true;
                                    if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                    {
                                        isSamePath = objThis.checkPathforVolume(results[i].path);
                                    }

                                    if(isSamePath)
                                    {
                                        var page;
                                        if(results[i].path.toString().indexOf("page=") != -1)
                                        {
                                            var parser = document.createElement('a');
                                            parser.href = results[i].path.toString();
                                            var parts = parser.search.substr(1).split("&");
                                            for (var k = 0; k < parts.length; k++) {
                                                if(parts[k] != ""){
                                                    var queryParamAndValue = parts[k].split("=");
                                                    
                                                    if(queryParamAndValue[0] == "page"){
                                                        page = queryParamAndValue[1];
                                                    }
                                                                                                    
                                                }
                                            }
                                            arrObj.id = "bookmarkStrip_" + page;
                                        }
                                        else
                                        {
                                            arrObj.id = results[i].path;
                                        }

                                        if(arrObj.id.split("_")[1] == "NaN")
                                            break;
                                        arrObj.title = GlobalModel.localizationData['PAGE_TEXT_FREE'] + "" + arrObj.id.split("_")[1];
                                        arrObj.page = results[i].object_id;
                                        arrObj.date = dateOfCreation;
                                        arrObj.timeMilliseconds = annodate;
                                        arrObj.annotation_id = results[i].annotation_id;

                                        if ($.inArray(arrObj.id, bookmarkAddedPagesID) == -1) {
                                            if ((EPubConfig.ReaderType.toUpperCase() == "2TO5" || EPubConfig.ReaderType.toUpperCase() == "PREKTO1") && GlobalModel.bookMarks.length < 5) {
                                                GlobalModel.bookMarks.push(arrObj);
                                                bookmarkAddedPagesID.push(arrObj.id);
                                                $("#" + arrObj.id).trigger( Event.Bookmark.SaveBookmarkForPage );
                                            } else if (EPubConfig.ReaderType.toUpperCase() != "2TO5" && EPubConfig.ReaderType.toUpperCase() != "PREKTO1") {
                                                GlobalModel.bookMarks.push(arrObj);
                                                bookmarkAddedPagesID.push(arrObj.id);
                                                $("#" + arrObj.id).trigger( Event.Bookmark.SaveBookmarkForPage );
                                            }
                                        }                                           
                                    
                                    }

                                    break;
                                case 2:
                                    //Highlight

                                    function getElementsByXPath(doc, xpath) {
                                        var nodes = [];
                                        try {
                                            var result = doc.evaluate(xpath, doc, null, XPathResult.ANY_TYPE, null);

                                            for (var item = result.iterateNext(); item; item = result.iterateNext()) {
                                                nodes.push(item);
                                            }
                                        } catch (exc) {
                                            // Invalid xpath expressions make their way here sometimes.  If that happens,
                                            // we still want to return an empty set without an exception.
                                        }
                                        return nodes;
                                    }

                                    var rangedata;

                                    rangedata = document.createRange();
                                    if(results[i].data != null)
                                    {
                                        var Obj = $.xml2json(results[i].data);
                                    }
                                    else
                                    {
                                        if(results[i].path.toString().indexOf("Container") == -1){
                                            console.log("Error in range Object:",results[i].path);
                                            break;
                                        }
                                        else{
                                            var Obj = $.xml2json(results[i].path);  
                                        }   
                                    }
                                    
                                    isSamePath = true;
                                    if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                    {
                                        isSamePath = objThis.checkPathforVolume(results[i].path);
                                    }
                                    if(isSamePath)
                                    {
                                        var startC = getElementsByXPath(document, Obj.startContainer);
                                        var endC = getElementsByXPath(document, Obj.endContainer);
                                        var startO = parseInt(Obj.startOffset);
                                        var endO = parseInt(Obj.endOffset);
    
                                        try {
    
                                            rangedata.setStart($(startC)[0].firstChild, startO);
                                            rangedata.setEnd($(endC)[0].firstChild, endO);
    
                                        } catch (err) {
                                            //console.log(err);
                                        }
                                        arrObj.annotationID = results[i].annotation_id;
                                        arrObj.title = results[i].body_text;
                                        arrObj.id = parseInt(GlobalModel.totalStickyNotes);
                                        arrObj.date = dateOfCreation;
                                        arrObj.type = "highlight";
                                        if(results[i].visibility_id == 3)
                                            arrObj.savetonote = 0;
                                        else
                                            arrObj.savetonote = 1;
                                        arrObj.range = rangedata;
                                        if(results[i].data != null)
                                             arrObj.rangeObject = results[i].data;
                                        else
                                             arrObj.rangeObject = results[i].path;   
                                        arrObj.style = parseInt(results[i].style);
                                        if (arrObj.style == 1) {
                                            arrObj.selectioncolor = 'selectioncolor' + results[i].color + '_underline';
                                        } else {
                                            arrObj.selectioncolor = 'selectioncolor' + results[i].color;
                                        }
                                        arrObj.selectionID = '.selection' + (GlobalModel.totalStickyNotes);
                                        arrObj.timeMilliseconds = annodate;
                                        arrObj.top = results[i].coord_y;
                                        arrObj.right = results[i].coord_x;
                                        //arrObj.pageNumber = results[i].start_pos;
                                        arrObj.pageNumber = results[i].object_id;
                                        GlobalModel.annotations.push(arrObj);
                                        GlobalModel.totalStickyNotes++;
                                       }
                                    break;

                                //sticky note data;
                                case 3:
                                    var isSamePath = true;
                                    if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                    {
                                        isSamePath = objThis.checkPathforVolume(results[i].path);
                                    }
                                    
                                    if(isSamePath)
                                    {
                                        arrObj.title = results[i].body_text;
                                        //arrObj.id = results[i].object_id;
                                        arrObj.id = parseInt(GlobalModel.totalStickyNotes);
    
                                        arrObj.type = "stickynote";
                                        //arrObj.pageNumber = results[i].start_pos;
                                        arrObj.pageNumber = results[i].object_id;
                                        if(results[i].visibility_id == 3)
                                            arrObj.savetonote = 0;
                                        else
                                            arrObj.savetonote = 1;
                                        arrObj.range = results[i].data;
                                        arrObj.date = dateOfCreation;
                                        arrObj.timeMilliseconds = annodate;
                                        arrObj.icontop = results[i].coord_x;
                                        arrObj.iconleft = results[i].coord_y;
                                        arrObj.annotationID = results[i].annotation_id;
                                        
                                        GlobalModel.annotations.push(arrObj);
                                        GlobalModel.totalStickyNotes++;
                                    }

                                    break;

                                case 8:
                                    //Automatic BookMark
                                    if( !hasAutomaticBookmark )
                                    {
                                        var isSamePath = true;
                                        if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                        {
                                            isSamePath = objThis.checkPathforVolume(results[i].path);
                                        }
                                        
                                        if(isSamePath)
                                        {
                                            var page;
                                            hasAutomaticBookmark = true;
                                            if(results[i].path.toString().indexOf("page=") != -1)
                                            {
                                                var parser = document.createElement('a');
                                                parser.href = results[i].path.toString();
                                                var parts = parser.search.substr(1).split("&");
                                                for (var k = 0; k < parts.length; k++) {
                                                    if(parts[k] != ""){
                                                        var queryParamAndValue = parts[k].split("=");
                                                        
                                                        if(queryParamAndValue[0] == "page"){
                                                            page = queryParamAndValue[1];
                                                        }
                                                                                                        
                                                    }
                                                }
                                                results[i].path = page;
                                            }
                                            
                                            var currentPageSequence = GlobalModel.pageBrkValueArr.indexOf(String(results[i].path));
                                            if (currentPageSequence < GlobalModel.pageBrkValueArr.length && currentPageSequence > -1) {
                                                if(!_objQueryParams.page){
                                                    GlobalModel.currentPageIndex = parseInt(currentPageSequence);
                                               }
                                            }
        
                                            if (results[i].annotation_id) {
                                                GlobalModel.AutomaticBookmarkAnnID = results[i].annotation_id;
                                            }
                                         }
                                     }
                                    break;

                                //QA Panel
                                case 9:
                                    //title denotes interactivityType
                                    //object_id denotes element id
                                    //book_text is the user response

                                    if (GlobalModel.questionPanelAnswers == null)
                                        GlobalModel.questionPanelAnswers = {};

                                    if (GlobalModel.questionPanelAnswersForAnnoPanel == null)
                                        GlobalModel.questionPanelAnswersForAnnoPanel = [];

                                    if (GlobalModel.questionPanelAnswers[results[i].data] == null)
                                        GlobalModel.questionPanelAnswers[results[i].data] = {};
                                    var visibility_id;
                                    if(results[i].visibility_id == 3)
                                        visibility_id = 1;
                                    else
                                        visibility_id = 0;
                                    
                                    var isSamePath = true;
                                    if(parseInt(EPubConfig.Volume) > 0 && EPubConfig.VolumeName && EPubConfig.VolumeName != "")
                                    {
                                        isSamePath = objThis.checkPathforVolume(results[i].path);
                                    }
                                    
                                    if(isSamePath)
                                    {
                                        var page;
                                        if(results[i].path.toString().indexOf("page=") != -1)
                                        {
                                            var parser = document.createElement('a');
                                            parser.href = results[i].path.toString();
                                            var parts = parser.search.substr(1).split("&");
                                            for (var k = 0; k < parts.length; k++) {
                                                if(parts[k] != ""){
                                                    var queryParamAndValue = parts[k].split("=");
                                                    
                                                    if(queryParamAndValue[0] == "page"){
                                                        page = queryParamAndValue[1];
                                                    }
                                                                                                    
                                                }
                                            }
                                        }
                                        else
                                        {
                                            page = results[i].path;
                                        }
                                        var objData = {
                                            id : results[i].object_id,
                                            response : results[i].body_text,
                                            pageNumber : page,
                                            interactivityType : results[i].title,
                                            creationDate : dateOfCreation,
                                            parentGrpId : results[i].data,
                                            flag_id : results[i].flag_id,
                                            annotation_id : results[i].annotation_id,
                                            savetonote : visibility_id
                                        };
                                        GlobalModel.questionPanelAnswers[results[i].data][results[i].object_id] = objData;
                                        var flag = -1;
                                        if (results[i].flag_id == 1) {
                                            for (var k = 0; k < GlobalModel.questionPanelAnswersForAnnoPanel.length; k++) {
                                                if (GlobalModel.questionPanelAnswersForAnnoPanel[k].parentGrpId == results[i].data) {
                                                    flag = 1;
                                                    break;
                                                }
                                            }
                                            if (flag == -1) {
                                                GlobalModel.questionPanelAnswersForAnnoPanel.push(objData);
                                            }
                                        }
                                        objThis.updateCookieData("qapanel");
                                    }
                                    break;

                                default:
                                    break;
                            }

                        }
                    }
                } else {
                    switch(type) {

                        case 8:
                            //Automatic Bookmark
                            var objToPost = {};
                            objToPost.content_id = EPubConfig.Identifier;
                            objToPost.type_id = type;
                            objToPost.path = "0";
                            objToPost = objThis.addVolumeDetails( objToPost );
                                                        
                            objThis.getContext().Annotations
                            .create( objToPost )
                            .done( function(data) {
                                GlobalModel.AutomaticBookmarkAnnID = data.annotation_id;
                            })
                            .fail( function( err ) {
                                console.log(err);
                            })
                            .run();
                            break;

                        default:
                            break;
                    }

                }
                if(strCompleteEvent == "autoBookmarkDataLoadComplete")
                {
                    if( !hasAutomaticBookmark )
                    {
                         var objToPost = {};
                         objToPost.content_id = EPubConfig.Identifier;
                         objToPost.type_id = type;
                         objToPost.path = "0";
                         objToPost = objThis.addVolumeDetails( objToPost );
                                                 
                         objThis.getContext().Annotations
                         .create( objToPost )
                         .done( function(data) {
                             GlobalModel.AutomaticBookmarkAnnID = data.annotation_id;
                         })
                         .fail( function( err ) {
                             console.log(err);
                         })
                         .run();
                    }
                }
                if(strCompleteEvent == "stickyNotesDataLoadComplete")
                {
                    objThis.noteLoadComplete = true;
                    objThis.updateCookieData("stickynote");
                }
                else if(strCompleteEvent == "highlightDataLoadComplete")
                {
                    objThis.highlightLoadComplete = true;
                    objThis.updateCookieData("highlight");                  
                }   
                else if(strCompleteEvent == Event.Bookmark.BookmarkDataLoadComplete)    
                    objThis.updateCookieData("bookmark");
                
                //complete event handler;
                if (strCompleteEvent != "") 
                {
                    $(document).trigger(strCompleteEvent);
                }

    },

    getStudentList: function (objCaller) {
        var url = getPathManager().getStudentListPath();
        $.getJSON(url, function (objData) {
           console.log("getStudentList: ", objData);

        }).error(function () {
            
        });
        var uid = "";
        var sifAccessToken = "";
        var objStudentList = {};
        var sectionsList = [];
        var nClassCounter = 0;

        var StudentList = function(objClassData)
        {
            console.log("objClassData: ", objClassData);
            var strURL = '/api/identity/v1/sectionRosters/'+objClassData.refId+';contextId=hmof';
            $.ajax({
                type:"GET",
                url:strURL,
                headers: { 
                    "Authorization": sifAccessToken
                },
                success: function(data)
                {
                    nClassCounter++;
                    //$("#tokenContainer").html(JSON.stringify(data));
                    if(data.students.length)
                    {
                        objClassData.students = [];
                        //studentsList = data.students;
                        //var strStudents = "";
                        for(var i = 0; i < data.students.length; i++)
                        {
                            //strStudents += studentsList[i].name.nameOfRecord.givenName + " " + studentsList[i].name.nameOfRecord.familyName + ": "+studentsList[i].refId+"<br/>";
                            var objSData = {
                              lastName: data.students[i].name.nameOfRecord.familyName,
                              firstName: data.students[i].name.nameOfRecord.givenName,
                              id: data.students[i].refId,
                              refId: data.students[i].refId
                           }
                           objClassData.students.push(objSData);

                        }
                        

                    }
                    if(nClassCounter == objStudentList.value.length)
                    {
                        objCaller.success(objStudentList);
                    }
                },
                error: function(e)
                {
                    nClassCounter++;
                    if(nClassCounter == sectionsList.length)
                    {
                        objCaller.success(objStudentList);
                    }
                }
            })
        }

        function getclasses(){
            var strURL = '/api/identity/v1/staffPersons/staffPerson/'+uid+'/section;contextId=hmof';
            $.ajax({
                type:"GET",
                url:strURL,
                headers: { 
                    "Authorization": sifAccessToken
                },
                success: function(data)
                {
                    //$("#tokenContainer").html(JSON.stringify(data));
                    objStudentList.value = [];

                    if(data.sections.length)
                    {

                        sectionsList = data.sections;
                        var strClasses = "";
                        for(var i = 0; i < data.sections.length; i++)
                        {
                            var objClass = {};
                            objClass.class = data.sections[i].name;
                            objClass.id = data.sections[i].refId;
                            objClass.refId = data.sections[i].refId;
                            objClass.localId = data.sections[i].localId.idValue;
                            objStudentList.value.push(objClass);
                            var objSL = new StudentList(objStudentList.value[objStudentList.value.length - 1]);
                            
                        }
                        
                    }
                },
                error: function(e)
                {
                    objCaller.error();
                }
            })
        }
        //console.log(uid);


        var requrl = "/api/identity/v1/authorize?&redirect=false&response_type=token" 
        requrl += "&scope=openid%20profile"
        requrl += "&client_id=DLO"
        requrl += "&platform=HMOF"
        requrl += "&state=fsklsda-1-1"
        requrl += "&redirect_uri=http://"+ document.location.host+"/";
        $.ajax({
                type:"GET",
                url:requrl,
                contentType: "application/json",
                headers: { 
                    "content-type": 'application/x-www-form-urlencoded'
                },
                success: function(data)
                {
                    //$("#tokenContainer").html(JSON.stringify(data));
                    console.log(data);
                    sifAccessToken = data.access_token;
                    uid = data.ref_id;
                    getclasses();
                    //}
                },
                error: function(e)
                {
                    objCaller.error();
                }
            })
                
    }
}

