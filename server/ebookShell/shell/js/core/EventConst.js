/*!
 *  Magic software private limited
 */
var Event = {};

/*
 * Bookmark events
 */
Event.Bookmark = { AddDataToBookmarkSectionPanel : "addDataToBookmarkSectionPanel",
				   LoadPageThroughBookmarkPanel : "loadPageThroughBookmarkPanel",
				   SaveBookmarkForPage : "saveBookmarkForPage",
				   DeleteBookmarkForPage : "deleteBookmarkForPage",
				   BookmarkDataLoadComplete : "bookmarkDataLoadComplete" };

