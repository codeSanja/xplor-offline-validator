/**
 * @author Anand Kumar
 */
//workaround for fixing internal link issue. This should be resolved by making changes in the framework such that internal pages data-roles should also be able to assign same instance of navigation operator as used by player. 'g_currentScreenIndex' variable used as workaround in contrntshowroom.js as well (Arvind)
g_currentScreenIndex = 0;
var prevBtnClick = false;
//

MG.NavigationOperator = function() {
    this.currentScreenIndex = g_currentScreenIndex;
    this.currentIdentifier = "";
    this.isLastScreen = false;
    this.superClass = MG.BaseOperator.prototype;
    this.scrollViewRef = "";
    this.viewLauncherSelected = null;
}
// extend super class
MG.NavigationOperator.prototype = new MG.BaseOperator();
/**
 * This function defines the functionality of components to be executed.
 */
MG.NavigationOperator.prototype.attachComponent = function(objComp) {

    if (objComp == null)
        return;

    var objThis = this;
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    switch (strType) {
        case AppConst.NAV_NEXT:
            $(objComp.element).parent().unbind('click').bind('click', function(e) {
                if ($(this).hasClass('ui-disabled')) {
                    return;
                }
                e.stopPropagation();
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideBrandBar');
                $("#annotation-bar").css('display', 'none');
                GlobalModel.isToAnimate = true;
                $(objThis.scrollViewRef).trigger('closeOpenedPanel');
                objThis.onNextButtonClick();
            });
            $(objComp.element).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e) {
                if ($(this).hasClass('ui-disabled')) {
                    return;
                }
                e.stopPropagation();
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideBrandBar');
                $(objThis.scrollViewRef).trigger('closeOpenedPanel');
                GlobalModel.isToAnimate = true;
                objThis.onNextButtonClick();
            });
            break;
        case AppConst.NAV_PREVIOUS:
            $(objComp.element).parent().unbind('click').bind('click', function(e) {
                if ($(this).hasClass('ui-disabled')) {
                    return;
                }
                e.stopPropagation();
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideBrandBar');
                $("#annotation-bar").css('display', 'none');
                GlobalModel.isToAnimate = true;
                $(objThis.scrollViewRef).trigger('closeOpenedPanel');
                objThis.onPreviousButtonClick();
            });
            $(objComp.element).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e) {
                if ($(this).hasClass('ui-disabled')) {
                    return;
                }
                e.stopPropagation();
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideBrandBar');
                $(objThis.scrollViewRef).trigger('closeOpenedPanel');
                GlobalModel.isToAnimate = true;
                objThis.onPreviousButtonClick();
            });
            break;

        case AppConst.JUMP_TO_PAGE:
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, data) {
                objThis.loadPage(data);
            });
            break;
        case AppConst.TABLE_OF_CONTENT:
            $(objComp).unbind(objComp.events.ITEM_CLICKED).bind(objComp.events.ITEM_CLICKED, function(e, strId) {
                objThis.loadPage(strId);

            });
            break;

        case AppConst.INTERNAL_LINK:
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId) {
                objThis.loadPage(strId);
            });
            break;

        case AppConst.EXTERNAL_TABLE_OF_CONTENT:
            $(objComp).unbind(objComp.events.ITEM_CLICKED).bind(objComp.events.ITEM_CLICKED, function(e, strId) {
                objThis.loadPage(strId);
            });
            break;
        case AppConst.ZOOM_CONTAINER:
            //          this.initSwipeEvents(objComp);
            break;

        case AppConst.RESOURCE_PANEL:
            //
            break;
        case AppConst.EXTERNAL_LINK:
            $(objComp).unbind(objComp.events.EXT_LINK_CLICKED).bind(objComp.events.EXT_LINK_CLICKED, function() {
                objThis.doFunctionCall(AppConst.EXTERNAL_LINK, 'getCurrentPageIndex', g_currentScreenIndex);
                objThis.doFunctionCall(AppConst.PAGE_SHOWROOM, 'saveSession');
            });
            break;
        case AppConst.BOOKMARK_SECTION_PANEL:
            // this event is triggered in bookmarkoperator. it calls loadPage with page index as parameter.
            $(objComp.element).bind( Event.Bookmark.LoadPageThroughBookmarkPanel, function(e, objComp) {

                var pageIndex = $(objComp).attr('id').replace('bookmarkData_', '');
                //parseInt($(objComp).attr('id').replace('bookmarkData_', ''));
                var pageNumValue = GlobalModel.pageBrkValueArr.indexOf(pageIndex)//jQuery.inArray(pageIndex, GlobalModel.pageBrkValueArr);
                objThis.loadPage(pageNumValue);

            });
            break;

        case AppConst.TOC_PANEL_COMP:
            //console.log("NAVIGATIONoperator: 145: ", AppConst.TOC_PANEL_COMP, " ", objComp);
            $(objComp).unbind(objComp.events.TOC_RENDER_COMPLETE).bind(objComp.events.TOC_RENDER_COMPLETE, function(e) {
                objThis.tocLoadCompleteHandler(objComp);
            });

            $(objComp.element).bind('tabDisplayChanged panelOpened panelClosed', function(e) {
                //console.log(AppConst.INDEX_PANEL_COMP);
                objThis.doFunctionCall(AppConst.TOC_PANEL_COMP, 'updateViewSize');
            });

            $(objComp).unbind(objComp.events.TOC_LINK_ITEM_CLICK).bind(objComp.events.TOC_LINK_ITEM_CLICK, function(e) {
                objThis.tocLinkItemClickHandler(objComp);
            });

            var tocTopbarRef = ($(objComp.element[0]).find('[id=tocTopbar]')[0]);
            var tocGotoInputBoxRef = $($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0];
            var tocGotoBtnRef = ($(tocTopbarRef).find("[type=tocGotoBtn]")[0]);
            var isAlertOkClicked = false;
            if (ismobile && EPubConfig.ReaderType != '6TO12') {
                $($($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0]).removeClass("tocGotoInputBox");
                if (navigator.userAgent.match(/(android)/i)) {
                    $($($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0]).addClass("tocGotoInputBoxMobileAndroid");
                } else {
                    $($($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0]).addClass("tocGotoInputBoxMobile");
                }

            }
            if (EPubConfig.ReaderType != '6TO12') {
	            $(tocGotoBtnRef).addClass('ui-disabled');
	            $(tocGotoBtnRef).data('buttonComp').disable();
	            $(tocGotoInputBoxRef).unbind('keyup').bind('keyup', function(e) {
	                if ($(tocGotoInputBoxRef).val().trim() != "") {
	                    $(tocGotoBtnRef).removeClass('ui-disabled');
	                    $(tocGotoBtnRef).data('buttonComp').enable();
	                    if (e.which == 13) {
	                        HotkeyManager.nullifyActiveElements()
	                        $(tocGotoBtnRef).trigger('click');
	                    }
	
	                } else {
	                    $(tocGotoBtnRef).addClass('ui-disabled');
	                    $(tocGotoBtnRef).data('buttonComp').disable();
	                }
	            }); 	
	            $(tocGotoInputBoxRef).bind('keydown', 'tab', function(evt){
					HotkeyManager.tabKeyHandler();
					return false;
				});
	            $(tocGotoInputBoxRef).bind( "input propertychange", function()
	            {
	                $(tocGotoInputBoxRef).unbind('keyup').bind('keyup', function(e) {
	                if ($(tocGotoInputBoxRef).val().trim() != "") {
	                    $(tocGotoBtnRef).removeClass('ui-disabled');
	                    $(tocGotoBtnRef).data('buttonComp').enable();
	                    if (e.which == 13) {
	                        HotkeyManager.nullifyActiveElements()
	                        $(tocGotoBtnRef).trigger('click');
	                    }
	
	                } else {
	                    $(tocGotoBtnRef).addClass('ui-disabled');
	                    $(tocGotoBtnRef).data('buttonComp').disable();
	                }
	            });  	
	            });
	           
	            $(tocGotoInputBoxRef).unbind('paste').bind('paste', function (e) {	               
	                setTimeout(function () { $(tocGotoInputBoxRef).trigger('keyup'); }, 100);
	                
	            });
			
	            var tocGotoBtnRole = $(tocGotoBtnRef).data('buttonComp');
	            $(tocGotoBtnRole).unbind(tocGotoBtnRole.events.CLICK).bind(tocGotoBtnRole.events.CLICK, function() {
	                    if ($(tocGotoBtnRef).hasClass('ui-disabled')) {
	                        return;
	                    }
	                    $(tocGotoInputBoxRef).blur();
	                    var pageIndex = $(tocGotoInputBoxRef).val();
	                    pageIndex = objThis.removeScriptTag(pageIndex);
	                    //console.log( pageIndex, "pageIndex = pageIndex.trim();pageIndex = pageIndex.trim();pageIndex = pageIndex.trim();")
	                    pageIndex = pageIndex.trim();
	                    //Number$(tocGotoInputBoxRef).val()();
	                    //var pageNumValue = d;
	
	                    var pageNumValue = objThis.getPageIndex(pageIndex);
						
	                    if (pageNumValue >= 0 && pageNumValue <= EPubConfig.totalPages && (pageIndex.match(/\s+/g) == null)) {
	
	                        //objThis.loadPage(0);
	                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideLeftSideAnnotationPanel');
	                        $(tocGotoInputBoxRef).val("");
	                        $(tocGotoBtnRef).addClass('ui-disabled');
	                        var curPageSequence = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
	                        var gotoPageIndex = Math.floor(pageNumValue);
	                        if (EPubConfig.pageView == 'doublePage') {
	                            if (curPageSequence == gotoPageIndex || (curPageSequence + 1) == gotoPageIndex) {
	                                return false;
	                            }
	                        } else {
	                            if (curPageSequence == gotoPageIndex) {
	                                return false;
	                            }
	                        }
	
	                        objThis.loadPage(Math.floor(pageNumValue));
	                    } else {
	                        //$(tocGotoInputBoxRef).val("");
	                        $(tocGotoBtnRef).addClass('ui-disabled');
	                        $("#alertTxt").html(GlobalModel.localizationData["TOC_GOTO_PAGE_ALERT_TEXT"]);
	
	                        //setting current page value in text field on click of OK
	                        var fnOkClickHandler = function() {
	                            var iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue');
	                            if (iPageBrkVal == 'start_blank') {
	                                iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex + 1]).attr('pagebreakvalue');
	                            } else if (iPageBrkVal == 'end_blank') {
	                                iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex - 1]).attr('pagebreakvalue');
	                            }
	                            $(tocGotoInputBoxRef).val(iPageBrkVal);
	                            $(tocGotoBtnRef).removeClass('ui-disabled');
	                            $(tocGotoInputBoxRef).focus();
	                            isAlertOkClicked = true;
	                            
	                            $(tocGotoInputBoxRef).unbind('keyup').bind('keyup', function(e) {
	                            	if( e.which != 9 )
	                            	{
	                            		$(tocGotoInputBoxRef).blur();
		                                if ($(tocGotoInputBoxRef).val().trim() != "") {
		                                    $(tocGotoBtnRef).removeClass('ui-disabled');
		                                    
		                                    if (e.which == 13) {
		                                        HotkeyManager.nullifyActiveElements()
		                                        if( !isAlertOkClicked )
		                                        	$(tocGotoBtnRef).trigger('click');
		                                    }
											isAlertOkClicked = false;
		                                } else {
		                                    $(tocGotoBtnRef).addClass('ui-disabled');
		                                }
	                                }
	                            });
	                            $(tocGotoInputBoxRef).trigger('keyup');
	                            setTimeout(function(){
	                            	$(tocGotoInputBoxRef).focus();
	                            },100)
	                        }
	
	                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setAlertOkClickHandler', fnOkClickHandler);
	                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'invalidPagenumberPopup');
	                        //For opening the readers alert box
	                    }
	            });
            }
            else
            {
            	var tocGotoInputBoxRef = $('[id=tocGotoInputBox]')[0];
            	$(tocGotoInputBoxRef).unbind('click').bind('click', function(e) {
            		$(objThis.scrollViewRef).trigger('closeOpenedPanel');
            	});
            	
	            $(tocGotoInputBoxRef).unbind('keyup').bind('keyup', function(e) {
	            	$(tocGotoInputBoxRef).attr("placeholder", GlobalModel.localizationData["TOC_GOTO_PAGE_PLACEHOLDER_TEXT"]);
	                if ($(tocGotoInputBoxRef).val().trim() != "") {
	
	                    if (e.which == 13) {
	                        HotkeyManager.nullifyActiveElements()
	                        if (!isAlertOkClicked) {
			                    $(tocGotoInputBoxRef).blur();
			                    var pageIndex = $(tocGotoInputBoxRef).val();
			                    pageIndex = objThis.removeScriptTag(pageIndex);
								pageIndex = pageIndex.trim();
			                    var pageNumValue = objThis.getPageIndex(pageIndex);
			
			                    if (pageNumValue >= 0 && pageNumValue <= EPubConfig.totalPages && (pageIndex.match(/\s+/g) == null)) {
			
			                        //objThis.loadPage(0);
			                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'hideLeftSideAnnotationPanel');
			                        $(tocGotoInputBoxRef).val("");
			                        var curPageSequence = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
			                        var gotoPageIndex = Math.floor(pageNumValue);
			                        if (EPubConfig.pageView == 'doublePage') {
			                            if (curPageSequence == gotoPageIndex || (curPageSequence + 1) == gotoPageIndex) {
			                                return false;
			                            }
			                        } else {
			                            if (curPageSequence == gotoPageIndex) {
			                                return false;
			                            }
			                        }
			                        objThis.loadPage(Math.floor(pageNumValue));
			                    } else {
			                        //$(tocGotoInputBoxRef).val("");
			                        $("#alertTxt").html(GlobalModel.localizationData["TOC_GOTO_PAGE_ALERT_TEXT"]);
			
			                        //setting current page value in text field on click of OK
			                        var fnOkClickHandler = function(e) {
			                            var iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue');
			                            if (iPageBrkVal == 'start_blank') {
			                                iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex + 1]).attr('pagebreakvalue');
			                            } else if (iPageBrkVal == 'end_blank') {
			                                iPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex - 1]).attr('pagebreakvalue');
			                            }
			                            $(tocGotoInputBoxRef).val(iPageBrkVal);
			                            $(tocGotoInputBoxRef).focus();
			                            if( e.originalEvent == undefined )
			                            	isAlertOkClicked = true;
			                        }
			
			                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setAlertOkClickHandler', fnOkClickHandler);
			                        objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'invalidPagenumberPopup');
			                        //For opening the readers alert box
			
			                    }
			                } else
			                    isAlertOkClicked = false;
	                    }
	
	                } else {
	                    //$(tocGotoBtnRef).addClass('ui-disabled');
	                   // $(tocGotoBtnRef).data('buttonComp').disable();
	                }
            });
            }

            break;

        case AppConst.SCROLL_VIEW_CONTAINER:

            objThis.scrollViewRef = objComp;
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function() {
                // printpage and localsearch functionality removed as it is already written in main.js
                if (_objQueryParams.page) {
                    var pageNumValue = objThis.getPageIndex(_objQueryParams.page);
                    if (pageNumValue > -1) {
                        GlobalModel.currentPageIndex = parseInt(pageNumValue);
                        setTimeout(function() {
                            objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageNumValue);
                        }, 10);
                    }
                }
                
                $("#maskDiv").css("display", "none");
               $("#maskDiv").remove();
            });

            $(objComp).bind(objComp.events.PAGE_INDEX_CHANGE, function(e) {
                objThis.doFunctionCall(AppConst.TOC_PANEL_COMP, 'openTocForPage', GlobalModel.currentPageIndex);
            });
            break;
            
        case AppConst.SCROLL_TO_TOP_BTN:
            $(objComp.element).unbind('mousedown touchstart').bind('mousedown touchstart', function() {
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'scrollEbookToTop');
            });
            break;

        case AppConst.INDEX_PANEL_COMP:
            // this event is triggered in indexPanelComp. it calls loadPage with page index as parameter.
            $(objComp.element).bind('loadPageThorughIndexPanel', function(e, pageNum) {
                var pageNumValue = GlobalModel.pageBrkValueArr.indexOf(String(pageNum));
                GlobalModel.currentPageIndex = parseInt(pageNumValue);

                objThis.loadPage(pageNumValue);

            });
            $(objComp.element).bind('tabDisplayChanged panelOpened panelClosed', function(e) {
                objThis.doFunctionCall(AppConst.INDEX_PANEL_COMP, 'scrollIndexElementToTop');
            });

            break;

        case AppConst.SCROLLPAGELAUNCHER:
            if (EPubConfig.pageView == "SCROLL" || EPubConfig.pageView == "scroll") {
                GlobalModel.originalPageView = "scroll";
                $(objComp.element).addClass('scrollpagelauncherSelected');
            }
            $(objComp.element).unbind('click').bind('click', function() {
                if (!$(objComp.element).hasClass('scrollpagelauncherSelected')) {
                    if (GlobalModel.currentPageIndex != 0 && objThis.viewLauncherSelected == 'double') {
                        // GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 1;
                    }
                    EPubConfig.pageView = "SCROLL";
                    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'changePageView', GlobalModel.currentPageIndex, objComp);
                    objThis.viewLauncherSelected = 'scroll';
                    $(".zoomSliderParent").removeClass('ui-disabled');
                    $(".zoomTxtParent").removeClass('ui-disabled');
                    $(".zoomSmallAlphabhet").removeClass('zoomSmallAlphabhetDisabled');
                    $(".zoomCapitalAlphabhet").removeClass('zoomCapitalAlphabhetDisabled');
                    GlobalModel.originalPageView = "SCROLL";
                    $('.changePageViewBtn').removeClass('dblpagelauncherSelected');
                    $('.changePageViewBtn').removeClass('singlepagelauncherSelected');
                    $(objComp.element).addClass('scrollpagelauncherSelected');
                }
            });
            break;

        case AppConst.SINGLEPAGELAUNCHER:
            if (EPubConfig.pageView == "singlePage") {
                GlobalModel.originalPageView = "singlePage";
                $(objComp.element).addClass('singlepagelauncherSelected');
                
                if(EPubConfig.ReaderType.toUpperCase() == "PREKTO1" && ismobile){
                	$("#zoomBtn").attr('class','zoom-on');
                }
            }
            $(objComp.element).unbind('click').bind('click', function() {
                if (!$(objComp.element).hasClass('singlepagelauncherSelected')) {
                    if (GlobalModel.currentPageIndex != 0 && objThis.viewLauncherSelected == 'double') {
                        //GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 1;
                    }
                    if(EPubConfig.ReaderType.toUpperCase() == "PREKTO1" && ismobile){
                		$("#zoomBtn").attr('class','zoom-on');
                		$("#zoomBtn").addClass("ui-disabled");
                	}
                    EPubConfig.pageView = "singlePage";
                    GlobalModel.originalPageView = "singlePage";
                    objThis.viewLauncherSelected = 'single';
                    $(".zoomSliderParent").removeClass('ui-disabled');
                    $(".zoomTxtParent").removeClass('ui-disabled');
                    $(".zoomSmallAlphabhet").removeClass('zoomSmallAlphabhetDisabled');
                    $(".zoomCapitalAlphabhet").removeClass('zoomCapitalAlphabhetDisabled');
                    $('.changePageViewBtn').removeClass('dblpagelauncherSelected');
                    $('.changePageViewBtn').removeClass('scrollpagelauncherSelected');
                    $(objComp.element).addClass('singlepagelauncherSelected');
                    /****** For checking for dual page spread on direct click *******************/
                    var nIndex = GlobalModel.currentPageIndex;
                    if (nIndex == 0) {
                        nIndex = 1;
                    }
                    var pageBrkVal = $($('[type="PageContainer"]')[nIndex]).attr('pagebreakvalue');
                    var dualPageSpreadForCurrentPage = false;
                    if(GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal] && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal].HasDualPageSpread){
                        dualPageSpreadForCurrentPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal].HasDualPageSpread;
                    }
                    if (dualPageSpreadForCurrentPage) {
                        objThis.scrollViewRef.member.isChangedToDualPageMode = true;
                        if (!(ismobile && (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT))) {
                            EPubConfig.pageView = "doublePage";
                            $(".doublePageLauncher").removeClass('dblpagelauncherSelected');
                            $(".doublePageLauncher").trigger('click', true);
                            //objThis.changePageView(nIndex, objThis);
                        } else {
                            objThis.scrollViewRef.member.previousLandscapeState = 'doublePage';
                        }
                        return;
                    }
                    /****** ************************************************* *******************/
                    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'changePageView', GlobalModel.currentPageIndex, objComp);
                    //objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', GlobalModel.currentPageIndex, objComp);
                }
            });
            break;

        case AppConst.DOUBLEPAGELAUNCHER:
            if (EPubConfig.pageView == "doublePage") {
                GlobalModel.originalPageView = "doublePage";
                objThis.viewLauncherSelected = 'double';
                $(objComp.element).addClass('dblpagelauncherSelected');
                $(".zoomSliderParent").addClass('ui-disabled');
                $(".zoomTxtParent").addClass('ui-disabled');
                $(".zoomSmallAlphabhet").addClass('zoomSmallAlphabhetDisabled');
                $(".zoomCapitalAlphabhet").addClass('zoomCapitalAlphabhetDisabled');
                $('.changePageViewBtn').removeClass('singlepagelauncherSelected');
                $('.changePageViewBtn').removeClass('scrollpagelauncherSelected');
               	if(EPubConfig.ReaderType.toUpperCase() == "PREKTO1" && ismobile){
                	$("#zoomBtn").attr('class','zoom-off');
                }
            }
            $(objComp.element).unbind('click').bind('click', function(e, clickOnPageCheck) {
                objThis.viewLauncherSelected = 'double';
                if (!$(objComp.element).hasClass('dblpagelauncherSelected')) {
                	if(EPubConfig.ReaderType.toUpperCase() == "PREKTO1" && ismobile){
	                	$("#zoomBtn").attr('class','zoom-off');
	                }
                    EPubConfig.pageView = "doublePage";
                    if (!clickOnPageCheck) {
                        GlobalModel.originalPageView = "doublePage";
                        $('.changePageViewBtn').removeClass('singlepagelauncherSelected');
                        $('.changePageViewBtn').removeClass('scrollpagelauncherSelected');
                        $(objComp.element).addClass('dblpagelauncherSelected');
                    }
                    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'changePageView', parseInt(GlobalModel.currentPageIndex), objComp);
                    $(".zoomSliderParent").addClass('ui-disabled');
                    $(".zoomTxtParent").addClass('ui-disabled');
                    $(".zoomSmallAlphabhet").addClass('zoomSmallAlphabhetDisabled');
                    $(".zoomCapitalAlphabhet").addClass('zoomCapitalAlphabhetDisabled');
                }
            });
            break;

        case AppConst.PAGE_VIEW_LAUNCHER:
            $(objComp).unbind('click').bind('click', function() {
                $(objThis.scrollViewRef).trigger('closeOpenedPanel');
                //EPubConfig.pageView = 'doublePage';
                // objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'changePageView', GlobalModel.currentPageIndex, objComp);
            });
            break;

        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            //return false;
            break;
    }

    return this.superClass.attachComponent.call(this, objComp);

}

/**
 * This function will remove script tag from the text to be appended in HTML element. 
 */

MG.NavigationOperator.prototype.removeScriptTag = function(textContent) {
    textContent = textContent.replace(/<script(.*?)>/gim, "");
    textContent = textContent.replace(/<\/script(.*?)>/gim, "");
    return textContent;
    
}

MG.NavigationOperator.prototype.getPageIndex = function(strPage) {
    var pageNumValue = -1;

    for (var i = 0; i < GlobalModel.pageBrkValueArr.length; i++) {
        if (strPage.toLowerCase() === GlobalModel.pageBrkValueArr[i].toLowerCase()) {
            pageNumValue = i;
            break;
        }
    }

    return pageNumValue
}

MG.NavigationOperator.prototype.getPrevSectionIndex = function() {
    var prevSectionIndex = 0;
    for (var i = GlobalModel.navPageArr.length - 1; i >= 0; i--) {
        if (GlobalModel.currentPageIndex > GlobalModel.navPageArr[i]) {
            prevSectionIndex = GlobalModel.navPageArr[i];
            return prevSectionIndex;
        }
    }

}

MG.NavigationOperator.prototype.getNextSectionIndex = function() {
    var nextSectionIndex = 0;
    for (var i = 0; i < GlobalModel.navPageArr.length; i++) {
        if (GlobalModel.currentPageIndex < GlobalModel.navPageArr[i]) {
            nextSectionIndex = GlobalModel.navPageArr[i];
            return nextSectionIndex;
        }
    }
}

MG.NavigationOperator.prototype.tocLoadCompleteHandler = function(objComp) {
    this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setAndLoadPages', objComp.getPageLinkList());
}

MG.NavigationOperator.prototype.tocLinkItemClickHandler = function(objComp) {

    this.loadPage(objComp.getClickedLinkIndex());
    //this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', objComp.getClickedLinkIndex());
    var objButton = this.doFunctionCall(AppConst.EXTERNAL_TOC_LAUNCHER, 'getElement');
    $(objButton).trigger("click");

}
// handler for next button click
MG.NavigationOperator.prototype.onNextButtonClick = function() {
	if(GlobalModel.originalPageView == 'singlePage')
	{
		var _iPageIndex = $($('[type=PageContainer]')[parseInt(GlobalModel.currentPageIndex) + 1]).attr('pagesequence');
	}
	else
	{
		var _iPageIndex = $($('[type=PageContainer]')[parseInt(GlobalModel.currentPageIndex) + 2]).attr('pagesequence');
	}	
	if(_iPageIndex == undefined )
	{
		return;
	}
    var maxPage = 1;
    prevBtnClick = false;
    if (EPubConfig.pageView == 'doublePage') {
        maxPages = EPubConfig.totalPages - 2;
    } else {
        maxPages = EPubConfig.totalPages - 1;
    }
    GlobalModel.currentPageIndex = PageNavigationManager.newPageIndex;
    //console.log("NEXT: ",GlobalModel.currentPageIndex);
    if (GlobalModel.currentPageIndex < maxPages) {
        if (EPubConfig.pageView == 'doublePage') {
            GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) + 2;
        } else {
            GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) + 1;
        }
        //console.log(maxPages, GlobalModel.currentPageIndex);
        var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
        //console.log("iPageIndex: ",iPageIndex);
        this.loadPage(iPageIndex);
    }
}
// handler for back button click
MG.NavigationOperator.prototype.onPreviousButtonClick = function() {
	GlobalModel.currentPageIndex = PageNavigationManager.newPageIndex;
	if(GlobalModel.originalPageView == 'singlePage')
	{
		var _iPageIndex = $($('[type=PageContainer]')[parseInt(GlobalModel.currentPageIndex) - 1]).attr('pagesequence');
	}
	else
	{
		var _iPageIndex = $($('[type=PageContainer]')[parseInt(GlobalModel.currentPageIndex) - 2]).attr('pagesequence');
	}	
	if(_iPageIndex == undefined )
	{
		return;
	}
    prevBtnClick = true;
    //console.log("PREV: ",GlobalModel.currentPageIndex);
    if (GlobalModel.currentPageIndex > 0) {
        if (EPubConfig.pageView == 'doublePage') {
            /**
             * In doublePageView, if the originalpageview is still single_page, then nIndex = nIndex - 1, else nIndex = nIndex - 2;
             * This is because the page to be checked is the previous page, not the previous-1 page.
             */
            if (GlobalModel.originalPageView == 'singlePage') {
                GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 1;
            } else {
                GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 2;
            }
        } else {
            GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 1;
        }
        if (GlobalModel.currentPageIndex < 0) {
            GlobalModel.currentPageIndex = 0;
        }
        var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
        this.loadPage(iPageIndex);
    }
}
/**
 * This function will be used load specific page based on label/identifier
 */
MG.NavigationOperator.prototype.loadPage = function(iPageIndex) {

    var objThis = this;
    this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', iPageIndex);
    objThis.doFunctionCall(AppConst.STANDARD_PANEL, 'toggleBtnState');
}

MG.NavigationOperator.prototype.updateResourceList = function() {
    var screenIdentifer = this.doFunctionCall(AppConst.PAGE_SHOWROOM, 'getScreenIdentifier');
    var parentID = this.doFunctionCall(AppConst.TABLE_OF_CONTENT, 'getCurrChildsParent', screenIdentifer);
    this.doFunctionCall(AppConst.RESOURCE_PANEL, 'getResourceForChapter', parentID);
}
