MG.ZoomOperator = function(){

    this.superClass = MG.BaseOperator.prototype;
    this.magnifyZoomInDataRole = null;
    this.magnifyZoomOutDataRole = null;
    this.values = [75,100,125,150,175,200];
    this.currentZoomValue = 100;
}
// extend super class
MG.ZoomOperator.prototype = new MG.BaseOperator();
/**
 * This function defines the functionality of components to be executed.
 */
MG.ZoomOperator.prototype.attachComponent = function(objComp){

    var objThis = this;
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    switch (strType) {
        case AppConst.ZOOM_IN_BUTTON:
            objThis.magnifyZoomInDataRole = objComp;
            $(objComp).bind(objComp.events.CLICK, function(){
                objThis.onZoomInButtonClick();
                
            //alert('calling zoom in');
            });
            break;
        case AppConst.ZOOM_OUT_BUTTON:
            objThis.magnifyZoomOutDataRole = objComp;
            $(objComp).bind(objComp.events.CLICK, function(){
                objThis.onZoomOutButtonClick();
            //alert('calling zoom out');
            });
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            $(objComp).bind('resetZoomButtons', function(e, nValue){
                objThis.enableZoomInOutButtonState(nValue);
            });
            
            break;
            
        case AppConst.ZOOM_SLIDER_CONTAINER:
            $slider = $("#rangeSlider");
            $("#rangeSlider").slider({
				step: EPubConfig.Zoom_Levels[1] - EPubConfig.Zoom_Levels[0],
				max: EPubConfig.Zoom_Levels[EPubConfig.Zoom_Levels.length - 1],
				min:EPubConfig.Zoom_Levels[0],
				value:100,
				slide: function( event, ui ) {
					var nearestVal = ui.value;
					if(nearestVal == 60){
					    nearestVal = 50;
					}
	                var zoomToVal = (nearestVal/100) * GlobalModel.actualScalePercent;
	                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', zoomToVal);
				}
			});
			$("#rangeSlider a").attr("title", "100%")
			
			$("#rangeSlider").parent().unbind('click').bind('click',function(e){
			   var rangeSliderParentRef = this;
			   var rangeSliderLeftOffset = $("#rangeSlider").offset().left;
			   var rangeSliderWidth = $("#rangeSlider").width();
			   if(e.clientX < rangeSliderLeftOffset){
			       $("#rangeSlider").slider({
                        value : 60
                    });
                    var zoomToVal = (50/100) * GlobalModel.actualScalePercent;
                    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', zoomToVal);
			   }
			   else{
			       if(e.clientX > (rangeSliderLeftOffset + rangeSliderWidth)){
			           $("#rangeSlider").slider({
                        value : 260
                    });
                     var zoomToVal = (260/100) * GlobalModel.actualScalePercent;
                    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', zoomToVal);
			       }
			   }
			});
            /*$($slider).bind("change", function(){
                var nearestVal = objThis.findNearest($($slider).val());
                var zoomToVal = (nearestVal/100) * GlobalModel.actualScalePercent;
                objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', zoomToVal);
                $($slider).val(nearestVal);
               
            });*/
            
            // On slider stopping  update slider position with snapped value
            
            $($slider).bind("slidestop", function(){
                var nearestVal = $($slider).val();
                $($slider).val(nearestVal);
                $($slider).slider('refresh');
                
            });
            
            break;
        case AppConst.ZOOM_PANEL_LAUNCHER_BUTTON:
        	
        	$(objComp).bind('click', function(e){
				$(this.element).removeClass("focusglow");
        		if($(this.element).attr("class") == "zoom-off")
                	$("[type='singlePageLauncher']").trigger("click");
                else	
                	$("[type='doublePageLauncher']").trigger("click");
            });
        break;
        
        default:
            console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            return false;
            break;
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.ZoomOperator.prototype.findNearest = function(goal){
    var closest = null;
    $.each(this.values, function(){
        if (closest == null || Math.abs(this - goal) < Math.abs(closest - goal)) {
            closest = this;
        }
    });
    currentVal = Number(closest);
    return currentVal;
}

MG.ZoomOperator.prototype.onZoomInButtonClick = function(){

    var nVal = this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getScaleValue');
    if (nVal < 2) {
        nVal += 0.25;
        this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', nVal);
        
    }
    this.enableZoomInOutButtonState(nVal);
    
}

MG.ZoomOperator.prototype.onZoomOutButtonClick = function(){
    var nVal = this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getScaleValue');
    if (nVal > 0.5) {
        nVal -= 0.25;
        this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'setZoom', nVal);
        
    }
    this.enableZoomInOutButtonState(nVal);
}

MG.ZoomOperator.prototype.enableZoomInOutButtonState = function(nVal){
    if (nVal > 0.5) {
        //this.magnifyZoomOutDataRole.enable();
    }
    else {
       // this.magnifyZoomOutDataRole.disable();
    }
    
    if (nVal < 2) {
       // this.magnifyZoomInDataRole.enable();
    }
    else {
        //this.magnifyZoomInDataRole.disable();
    }
}



