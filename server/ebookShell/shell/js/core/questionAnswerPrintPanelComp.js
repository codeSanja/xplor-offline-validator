/**
 * @author Sachin.Manchanda
 */
var printableDiv = "";
var loaded, currQuesId, currQuesType, interactivityLength, groupInteractionData, basePath;

/**
 * This function will create the QA Print HTML by loading the groupXML
 * @param {String} groupXMLName This is the reference of the group XML to be printed
 */
function createPrintHtml(_basePath,groupXMLFile){
	basePath = _basePath;
	printableDiv = "";
	loadRequiredXML(groupXMLFile,true);
	
	$("#printBtn").unbind('click').bind('click', function(){
		
		$($("#print")).printArea({
                mode: "iframe",
                popClose: false
        });
	});
}
        
/**
 * This function will load the required XML file
 * @param {String} fileName this is the name of the file to be loaded
 * @param {Boolean} this is true in case group XML
 */
function loadRequiredXML(fileName, bVal){
    var uri = basePath + "/" + fileName + ".xml"
    var returnVal = null;
    $.ajax({
        type: "GET",
        url: uri,
        processData: false,
        contentType: "plain/text",
        success: function(objData){
        questionXMLLoadComplete(objData, bVal);
    },
        error: function(e){
            console.log(e.error);
            // objThis.displayErrorMessage();
        }
        
    });
    
    return returnVal;
}
        
/**
 * This function will handle the load complete of the XML file
 * @param {String} The content of the loaded XML file
 * @param {Boolean} this is true in case group XML
 */
function questionXMLLoadComplete(objData, bVal){
    var strXML;
    if (window.ActiveXObject) {
        var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
        xmlObject.async = false;
        strXML = objData;
    }
    else {
        strXML = new XMLSerializer().serializeToString(objData);
    }
    
    if (bVal)
    {
    	groupInteractionData = $.xml2json(strXML);    
        interactivityLength = groupInteractionData.interaction.interaction.length;
        if(interactivityLength == undefined && typeof groupInteractionData.interaction.interaction == "object"){
            interactivityLength = 1;
        }
        loaded = -1;
    }
    else
    {
    	var innerContent = $('<div id="questionanswerInnerContent" class="questionanswerInnerContent" type="questionanswerInnerContent"><div id="questionPanelSectionName"><div id="questionPanelSectionTitle"></div><div id="questionPanelSectionContent"></div></div><div id="questionPanelQuestion"><div id="questionPanelQuestionContent" class="questionPanelQuestionContent"><div id="theQuestionAsked"></div></div><div id="questionPanelOptions" class="questionPanelOptions"></div><div id="questionImage" class="questionImage"><span class="quesImageExpand"></span></div></div></div>');

    	switch (currQuesType) {

            case 'singleChoice':
                printableDiv += singleChoiceInteractivity($.xml2json(strXML), innerContent).html();
                break;
                
            case 'freeText':
                printableDiv += freeTextInteractivity($.xml2json(strXML), innerContent).html();
                break;
            case 'freeTextWithImage':
                printableDiv += freeTextWithImageInteractivity($.xml2json(strXML), innerContent).html();
                break;
            case 'fillUps':
                printableDiv += fillUpsInteractivity($.xml2json(strXML), innerContent).html();
                break;
            case 'multipleChoice':
                printableDiv += multipleChoiceInteractivity($.xml2json(strXML), innerContent).html();
                break;
    	}
    	
    }
    if(loaded < interactivityLength-1)
    {
       
        loaded++;
        currQuesId = groupInteractionData.interaction.interaction[loaded].id;
        currQuesType = groupInteractionData.interaction.interaction[loaded].type;
        if(loaded)
        	printableDiv +="<div style='border-bottom: 1px solid black;padding: 0px 5px;height: 30px;margin-bottom: 30px;'></div>" 
        loadRequiredXML(groupInteractionData.interaction.interaction[loaded].url.replace('.xml', ''), false);
    }
    else
    {
    	$("#print").html(printableDiv);
    }   
}

function singleChoiceInteractivity(strQAContent,questionInnerContent){
	$(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.choiceInteraction.prompt);
	$(questionInnerContent).find('[id=QAfreeTextImage]').remove();
	$(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
	$(questionInnerContent).find('[id=questionImage]').css("display","block");
	var numberOfChoices = strQAContent.itemBody.choiceInteraction.simpleChoice.length;
	if(numberOfChoices == undefined && typeof strQAContent.itemBody.choiceInteraction.simpleChoice == "Object"){
	    numberOfChoices = 1;
	}
	var optionList = "";
	for(var i=0;i<numberOfChoices;i++){
	    if(numberOfChoices == 1){
	       optionList += "<div class='leftFloat clear marginB15'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice + "</div></div>"; 
	    }
	    else{
	        optionList += "<div class='leftFloat clear marginB15'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice[i] + "</div></div>";
	    }
	}
	$(questionInnerContent).find('[id=questionPanelOptions]').html(optionList);
	return questionInnerContent;
}

function multipleChoiceInteractivity(strQAContent,questionInnerContent){
    $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.choiceInteraction.prompt);
    $(questionInnerContent).find('[id=QAfreeTextImage]').remove();
    $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
    $(questionInnerContent).find('[id=questionImage]').css("display","block");
    var numberOfChoices = strQAContent.itemBody.choiceInteraction.simpleChoice.length;
    if(numberOfChoices == undefined && typeof strQAContent.itemBody.choiceInteraction.simpleChoice == "Object"){
        numberOfChoices = 1;
    }
    var optionList = "";
    for(var i=0;i<numberOfChoices;i++){
        if(numberOfChoices == 1){
           optionList += "<div class='leftFloat clear marginB15'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice + "</div></div>"; 
        }
        else{
            optionList += "<div id='multipleChoiceOption"+ (i+1) + "' class='leftFloat clear marginB15'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice[i] + "</div></div>";
        }
    }
    $(questionInnerContent).find('[id=questionPanelOptions]').html(optionList);
    return questionInnerContent;
}

function freeTextWithImageInteractivity(strQAContent, questionInnerContent){
    var objThis = this;
    $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.div[0].p);
    $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptions").addClass("questionPanelOptionsText");
    $(questionInnerContent).find('[id=questionImage]').css("display", "block");
    var imgSrc = strQAContent.itemBody.div[1].div.object.data;
    $(questionInnerContent).find('[id=questionPanelOptions]').html('<textarea id="freeTextWithImageInteractivityTxtArea" class="freeTextWithImageInteractivityTxtArea"></textarea>');
    return questionInnerContent;
}

function freeTextInteractivity(strQAContent,questionInnerContent){
    $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.div[0].p);
    $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptions").addClass("questionPanelOptionsText");
    $(questionInnerContent).find('[id=questionImage]').css("display","none");
    $(questionInnerContent).find('[id=questionPanelOptions]').html('<textarea id="freeTextInteractivityTxtArea" class="freeTextInteractivityTxtArea"></textarea>');
    return questionInnerContent;
}

function fillUpsInteractivity(strQAContent,questionInnerContent){
    $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.p);
    $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
    $(questionInnerContent).find('[id=questionImage]').css("display","none");
	var numberOffillUps = strQAContent.itemBody.blockquote.length;
    if(numberOffillUps == undefined && typeof strQAContent.itemBody.blockquote == "Object"){
        numberOffillUps = 1;
    }
    var fillUpList = "";
    for(var i=0;i<numberOffillUps;i++){
        var fills = strQAContent.itemBody.blockquote[i].p.length;
        if(fills == undefined && typeof strQAContent.itemBody.blockquote[i].p == "Object"){
            fills = 1;
        }
        fillUpList += "<div class='questionPanelFillup'>" + (i+1) + ".&nbsp  ";
        for(var j=0;j<fills;j++){
        	if(j !=0 )
        	fillUpList += "<input type='text' style='margin:0px 5px' />";
            if(numberOffillUps == 1){
               fillUpList += strQAContent.itemBody.blockquote[i].p;
            }
            else{
               fillUpList += strQAContent.itemBody.blockquote[i].p[j]
            }
        }
        fillUpList += "</div>";
    }
    
    $(questionInnerContent).find('[id=questionPanelOptions]').html(fillUpList);
    return questionInnerContent;
}