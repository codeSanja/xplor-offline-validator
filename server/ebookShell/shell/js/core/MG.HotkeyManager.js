/**
 * Constructor
 */
HotkeyManager = function()
{
	
}
HotkeyManager.currentActiveElementGroup = null;
HotkeyManager.currentActiveElementIndex = 0;
HotkeyManager.currentActiveElement = null;
HotkeyManager.currentActiveElementGroupID = "";

HotkeyManager.prevActiveElementGroupID = "";
HotkeyManager.prevActiveElementIndex = 0;
HotkeyManager.currentActivePageSide = "left";
var scrollviewcompRef = null;
HotkeyManager.init = function()
{
	scrollviewcompRef = $( "#bookContentContainer" ).data( 'scrollviewcomp' );
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		var objThis = this;
		jQuery(document).bind('keyup', 'esc', function(evt){
			objThis.escapeKeyHandler()
		});
		jQuery(document).bind('keydown', 'Ctrl+x', function(evt){
			if(window.getSelection().toString() == ""){
				objThis.promptCloseHandler();
				return false;
			}
		});
		jQuery(document).bind('keydown', 'return', function(evt){
			objThis.enterKeyHandler();
			if($("#stickyNoteContainer").css('display') != "block"){
			    return false;
			}
			
		});
		jQuery(document).bind('keydown', 'tab', function(evt){
			objThis.tabKeyHandler();
			return false;
		});
		jQuery(document).bind('keyup', 'pageup', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
			
			objThis.pageupKeyHandler()
		});
		jQuery(document).bind('keyup', 'pagedown', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.pagedownKeyHandler()
		});
		
		jQuery(document).bind('keyup', 'up', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.upKeyHandler()
		});
		jQuery(document).bind('keyup', 'down', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.downKeyHandler()
		});
		jQuery(document).bind('keyup', 'left', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.leftKeyHandler()
		});
		jQuery(document).bind('keyup', 'right', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.rightKeyHandler()
		});
		
		jQuery(document).bind('keydown', 'Ctrl+m', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.leftMenuFocusInHandler();
			return false;
		});
		jQuery(document).bind('keydown', 'Ctrl+b', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
				
			objThis.pageBodyFocusInHandler();
			return false;
		});
		jQuery(document).bind('keydown', 'Ctrl+q', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
			
			objThis.qaPanelFocusInHandler();
			return false;
		});
		
		jQuery(document).bind('keydown', 'Alt+t', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block' ) 
				return;
			
			objThis.currentActiveElementIndex = 1;
			var objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeyindex=' + objThis.currentActiveElementIndex + ']')[0];
			if ( $(objActiveElement).css( 'display' ) != 'none' )
			{
				objThis.tocPanelOpenHandler();
				return false;
			}
		});
		jQuery(document).bind('keydown', 'Alt+r', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block') 
				return;
				
			if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
			{
				objThis.currentActiveElementIndex = 3;
			}
			else
			{
				objThis.currentActiveElementIndex = 2;
			}
			var objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeyindex=' + objThis.currentActiveElementIndex + ']')[0];
			if ( $(objActiveElement).css( 'display' ) != 'none' || objThis.currentActiveElementGroupID == "TOCMenu" )
			{
				objThis.resourcePanelOpenHandler();
				return false;
			}
		});
		jQuery(document).bind('keydown', 'Alt+b', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block') 
				return;
			
			if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
			{
				objThis.currentActiveElementIndex = 2;
			}
			else
			{
				objThis.currentActiveElementIndex = 3;
			}
			var objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeyindex=' + objThis.currentActiveElementIndex + ']')[0];
			if ( $(objActiveElement).css( 'display' ) != 'none' )
			{
				objThis.bookmarkPanelOpenHandler();
				return false;
			}
		});
		jQuery(document).bind('keydown', 'Alt+n', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block')
				return;
			
			if (EPubConfig.ReaderType.toLowerCase() != 'prekto1')
			{
				objThis.notesPanelOpenHandler();
				return false;
			}
		});
		jQuery(document).bind('keydown', 'Alt+z', function(evt){
			if (objThis.currentActiveElementGroupID == "PopupMenu" || $('#stickyNoteContainer').css('display') == 'block' || $('#imageGalleryPanel').css('display') == 'block') 
				return;
			
			objThis.zoomPanelOpenHandler();
			return false;
		});
		$('#tocGotoInputBox').bind('focus', function(evt){
			HotkeyManager.findAndSetFocus($('#tocGotoInputBox'));
		});
	}
}

HotkeyManager.tocPanelOpenHandler = function()
{
	var objThis = this;
	objThis.leftMenuFocusInHandler();
	objThis.currentActiveElementIndex = 0;
	objThis.setNextActiveElement();
	objThis.enterKeyHandler();
}

HotkeyManager.tocPanelHandler = function()
{
	var objThis = this;
	objThis.tocMenuFocusInHandler();
}

HotkeyManager.resourcePanelOpenHandler = function()
{
	nSetActiveLookCounter = 0;
	if (EPubConfig.Resources_isAvailable || EPubConfig.Tools_isAvailable) {
		var objThis = this;
		objThis.leftMenuFocusInHandler();
		if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
		{
			objThis.currentActiveElementIndex = 2;
		}
		else
		{
			objThis.currentActiveElementIndex = 1;
		}
		objThis.setNextActiveElement();
		objThis.enterKeyHandler();
	}
}

HotkeyManager.bookmarkPanelOpenHandler = function()
{
	if (EPubConfig.Annotations_isAvailable && EPubConfig.BookMark_isAvailable) {
		var objThis = this;
		objThis.leftMenuFocusInHandler();
		if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
		{
			objThis.currentActiveElementIndex = 1;
		}
		else
		{
			objThis.currentActiveElementIndex = 2;
		}
		objThis.setNextActiveElement();
		objThis.enterKeyHandler();
	}
}
HotkeyManager.notesPanelOpenHandler = function()
{
	nSetActiveLookCounter = 0;
	if (EPubConfig.Annotations_isAvailable && EPubConfig.Notes_isAvailable) {
		var objThis = this;
		objThis.leftMenuFocusInHandler();
		if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
		{
			return;
		}
		else
		{
			objThis.leftMenuFocusInHandler();
			objThis.currentActiveElementIndex = 3;
		}
		objThis.setNextActiveElement();
		objThis.enterKeyHandler();
	}
}

HotkeyManager.zoomPanelOpenHandler = function()
{
	if (EPubConfig.Annotations_isAvailable) {
		var objThis = this;
		objThis.leftMenuFocusInHandler();
		if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
		{
			objThis.currentActiveElementIndex = 3;
		}
		else
		{
			objThis.currentActiveElementIndex = 4;
		}
		objThis.setNextActiveElement();
		objThis.enterKeyHandler();
	}
}

HotkeyManager.escapeKeyHandler = function()
{
	var objThis = this;
	var objActiveElement;
	if (objThis.currentActiveElementGroupID == "LeftMenu") {
		if (this.getActiveElementType() == 'buttonComp') {
			$(objThis.currentActiveElement).trigger('click');
		}
	}
	else if (objThis.currentActiveElementGroupID == "PopupMenu") {
		objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeytype=close]')[0];
		if (objThis.currentActiveElement) {
			objThis.currentActiveElement = objActiveElement;
			$(objThis.currentActiveElement).trigger('click');
		}
	}
	else if (objThis.currentActiveElementGroupID == "QAPanel") {
		objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeytype=close]')[0];
		if (objThis.currentActiveElement) {
			objThis.currentActiveElement = objActiveElement;
			$(objThis.currentActiveElement).trigger('click');
		}
	}
	if (objThis.currentActiveElementGroupID != "PopupMenu") {
		objThis.nullifyActiveElements();
	}
}

HotkeyManager.promptCloseHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID == "PopupMenu") {
		objThis.currentActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeytype=close]')[0];
		if (objThis.currentActiveElement) {
			$(objThis.currentActiveElement).trigger('click');
		}
		
	}
	objThis.nullifyActiveElements();
}

HotkeyManager.enterKeyHandler = function()
{
	if(ismobile)
	{
		return;
	}
	var objThis = this;
	if ($(objThis.currentActiveElement).length) {
		if ($(objThis.currentActiveElement).attr('hotkeytype') == "close") {
			
			$(".focusglow").removeClass("focusglow");
			$(objThis.currentActiveElement).trigger('click');
			objThis.nullifyActiveElements();
		
		}
		else {
			if (objThis.currentActiveElementGroupID == "TOCMenu")
			{
				if($(objThis.currentActiveElement).find("[levelid]").length)
				{
					$($(objThis.currentActiveElement).find("div")[0]).trigger('click');
					$(document).trigger("updateHotkeyIndex")
				}
				else
				{
					$(objThis.currentActiveElement).trigger('click');
				}
			}
			else
			{
				if (objThis.currentActiveElementGroupID == "PageViewMenu" && objThis.currentActiveElementIndex == '1') 
				{
					$(objThis.currentActiveElement).trigger('click');
					objThis.pageBodyFocusInHandler();
				}
				else
				{
					$(objThis.currentActiveElement).trigger('click');
				}
					
			}
		}
	}
}

HotkeyManager.tabKeyHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroup) {
		objThis.setNextActiveElement();
	}
	$(document).trigger("updateIScroll");
}

HotkeyManager.pageupKeyHandler = function()
{
	var objThis = this;
	$("#bookContentContainer").scrollTop(0);
	if (objThis.currentActiveElementGroupID != "PopupMenu") {
		objThis.escapeKeyHandler();
		var objScrollViewComp = $($.find('[data-role=scrollviewcomp]')[0]).data('scrollviewcomp');
		
		var nPageDecrement = 1;
		
		if (EPubConfig.pageView == "doublePage") {
			if (GlobalModel.currentPageIndex % 2 == 0) {
			    if (GlobalModel.originalPageView == 'singlePage') {
                        nPageDecrement = 1;
                  }
                else {
                        nPageDecrement = 2;
                    }
			}
		}
		var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
		if (iPageIndex - nPageDecrement > -1) {
			iPageIndex -= nPageDecrement;
			isZoomInOut = false;
			objScrollViewComp.showPageAtIndex(iPageIndex);
		}
	}
}


HotkeyManager.pagedownKeyHandler = function()
{
	var objThis = this;
	$("#bookContentContainer").scrollTop(0);
	if (objThis.currentActiveElementGroupID != "PopupMenu") {
		objThis.escapeKeyHandler();
		var objScrollViewComp = $($.find('[data-role=scrollviewcomp]')[0]).data('scrollviewcomp');
		var nPageIncrement = 1;
		
		if (EPubConfig.pageView == "doublePage") {
				nPageIncrement = 2;
		}
		
		var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
		if (iPageIndex + nPageIncrement < EPubConfig.totalPages) {
			iPageIndex += nPageIncrement;
			isZoomInOut = false;
			objScrollViewComp.showPageAtIndex(iPageIndex);
		}
	}
}


HotkeyManager.upKeyHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID == "PopupMenu" || objThis.currentActiveElementGroupID == "TOCMenu" || objThis.currentActiveElementGroupID == "BookmarkMenu" || objThis.currentActiveElementGroupID == "PageViewMenu") {
		objThis.setPreviousActiveElement();
		$(document).trigger("updateIScroll");
	}
	else
	{
		objThis.escapeKeyHandler();
		
		if (EPubConfig.pageView.toLowerCase() == "scroll") {
			$("#bookContentContainer2").scrollTop($("#bookContentContainer2").scrollTop() - 50);
		}
		else
		{
			$("#bookContentContainer").scrollTop($("#bookContentContainer").scrollTop() - 50);
		}
	}
}

HotkeyManager.downKeyHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID == "PopupMenu" || objThis.currentActiveElementGroupID == "TOCMenu" || objThis.currentActiveElementGroupID == "BookmarkMenu" || objThis.currentActiveElementGroupID == "PageViewMenu") {
		objThis.setNextActiveElement();
		$(document).trigger("updateIScroll");
	}
	else
	{
		objThis.escapeKeyHandler();
		
		if (EPubConfig.pageView.toLowerCase() == "scroll") {
			$("#bookContentContainer2").scrollTop($("#bookContentContainer2").scrollTop() + 50);
		}
		else
		{
			$("#bookContentContainer").scrollTop($("#bookContentContainer").scrollTop() + 50);
		}
	}
}

HotkeyManager.rightKeyHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID == "PopupMenu" || objThis.currentActiveElementGroupID == "PageViewMenu" ) {
		if ( $('#zoomBtnPanel').find('a.ui-slider-handle').hasClass('focusglow') )
		{
			var nearestVal = $("#rangeSlider").slider("value");
			nearestVal = nearestVal + 40;
			$("#rangeSlider").slider("value",nearestVal)
			if(nearestVal == 60){
			    nearestVal = 50;
			}
            var zoomToVal = (nearestVal/100) * GlobalModel.actualScalePercent;
			scrollviewcompRef.setZoom(zoomToVal);
		}
	}
	else
	{
		objThis.escapeKeyHandler();
		if (EPubConfig.pageView.toLowerCase() == "scroll") {
			$("#bookContentContainer2").scrollLeft($("#bookContentContainer2").scrollLeft() + 50);
		}
		else
		{
			$("#bookContentContainer").scrollLeft($("#bookContentContainer").scrollLeft() + 50);
		}
	}
}

HotkeyManager.leftKeyHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID == "PopupMenu" || objThis.currentActiveElementGroupID == "PageViewMenu" ) {
		if ( $('#zoomBtnPanel').find('a.ui-slider-handle').hasClass('focusglow') )
		{
			var nearestVal = $("#rangeSlider").slider("value");
			nearestVal = nearestVal - 40;
			$("#rangeSlider").slider("value",nearestVal)
			if(nearestVal <= 60){
			    nearestVal = 50;
			}
            var zoomToVal = (nearestVal/100) * GlobalModel.actualScalePercent;
			scrollviewcompRef.setZoom(zoomToVal);
		}
	}
	else
	{
		objThis.escapeKeyHandler();
		if (EPubConfig.pageView.toLowerCase() == "scroll") {
			$("#bookContentContainer2").scrollLeft($("#bookContentContainer2").scrollLeft() - 50);
		}
		else
		{
			$("#bookContentContainer").scrollLeft($("#bookContentContainer").scrollLeft() - 50);
		}
	}
}

HotkeyManager.leftMenuFocusInHandler = function()
{
	var objThis = this;
	if (objThis.currentActiveElementGroupID != "PopupMenu") {
		nSetActiveLookCounter = 0;
		objThis.currentActiveElementGroupID = "LeftMenu";
		objThis.currentActiveElementIndex = 0;
		objThis.currentActiveElementGroup = $("#annotationPanel")[0];
		objThis.setNextActiveElement();
	}
}

HotkeyManager.tocMenuFocusInHandler = function()
{
	var objThis = this;
	objThis.currentActiveElementGroupID = "TOCMenu";
	objThis.currentActiveElementIndex = 0;
	var arrHotKey = $("#tocMainPanel").find('[hotkeyindex]').not('[levelid]');
	var hotId = findLastHotKeyIndex( arrHotKey );
	objThis.topTabbedElement = hotId - 1;
	objThis.currentActiveElementIndex = hotId - 1;
	objThis.currentActiveElementGroup = $("#tocMainPanel")[0];
	objThis.setNextActiveElement();
}

HotkeyManager.bookmarkMenuFocusInHandler = function()
{
	var objThis = this;
	objThis.currentActiveElementGroupID = "BookmarkMenu";
	objThis.currentActiveElementIndex = 0;
	objThis.currentActiveElementGroup = $("#BookMarkSectionPanel")[0];
	objThis.setNextActiveElement();
}

HotkeyManager.pageViewFocusInHandler = function()
{
	var objThis = this;
	objThis.currentActiveElementGroupID = "PageViewMenu";
	objThis.currentActiveElementIndex = 0;
	if( EPubConfig.pageView == "doublePage" )
		$('a.ui-slider-handle').attr('hotkeyindex','-1');
	else
		$('a.ui-slider-handle').attr('hotkeyindex','4');
    $('a.ui-slider-handle').attr('hotkeygroupid','PageViewMenu');
	objThis.currentActiveElementGroup = $("#zoomBtnPanel")[0];
	objThis.setNextActiveElement();
}

HotkeyManager.qaPanelFocusInHandler = function()
{
	var objThis = this;
	var objPanel = $("#questionAnswerPanel")[0];
	if ($(objPanel).css("display") == "block") {
		if (objThis.currentActiveElementGroupID != "QAPanel" && objThis.currentActiveElementGroupID != "PopupMenu") {
			objThis.currentActiveElementGroupID = "QAPanel";
			objThis.currentActiveElementIndex = 0;
			objThis.currentActiveElementGroup = $("#questionAnswerPanel")[0];
			objThis.setNextActiveElement();
		}
	}
}
var nSetActiveLookCounter = 0;
HotkeyManager.setNextActiveElement = function()
{
	nSetActiveLookCounter++;
	if(nSetActiveLookCounter > 10)
	{
		return;
	}
	var objThis = this;
	//console.log("#######","setNextActiveElement",objThis.currentActiveElementIndex)
	if (objThis.currentActiveElementGroupID != "PageBody") {
		if($(objThis.currentActiveElementGroup).attr("hotkeygroupid") == "LeftMenu")
		{
			objThis.currentActiveElementGroupID = "LeftMenu";
		}
		objThis.currentActiveElementIndex++;
		var objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeyindex=' + objThis.currentActiveElementIndex + ']')[0];
		//console.log(nSetActiveLookCounter, objActiveElement);
		if (objActiveElement) {
			var classOfActiveElement = $(objActiveElement).attr("class") || "";
			if ($(objActiveElement).hasClass('ui-disabled') || $(objActiveElement).hasClass('contentEditableFalseDiv') || $(objActiveElement).css('visibility') == 'hidden' || $(objActiveElement).css('display') == 'none' 
				|| ($(objActiveElement).parent().css('display') == 'none' && objThis.currentActiveElementGroupID != "PopupMenu") || $(objActiveElement).hasClass('leftPanelTabActive') 
				|| classOfActiveElement.indexOf("Selected") != -1 || $(objActiveElement).is('[disabled=disabled]') || ($(objActiveElement).parent().css('display') == 'none' && $(objActiveElement).parent().hasClass('savetonotebook') )) {
				//objThis.currentActiveElementIndex = 0;
				objThis.setNextActiveElement();
				return;
			}
			else if(($(objThis.currentActiveElement).attr("type") == "text" && $($(objActiveElement).parent()).css('display') == 'none') || ($(objThis.currentActiveElement).attr("elementtype") == "textarea" && $(objActiveElement).css('display') == 'none'))
			{
				//objThis.currentActiveElementIndex = 0;
				objThis.setNextActiveElement();
				return;
			}
			else {
				var objPrevEle = objThis.currentActiveElement;
				//console.log("objThis.currentActiveElement",objThis.currentActiveElement)
				if($(objThis.currentActiveElement).attr("type") == "text" || $(objThis.currentActiveElement).attr("elementtype") == "textarea")//elementtype="textarea"
				{
					$(objThis.currentActiveElement).blur();
				}
				
				objThis.currentActiveElement = objActiveElement;
				
				if (objThis.currentActiveElement) {
					nSetActiveLookCounter = 0;
					$(".focusglow").removeClass("focusglow");
					var ie_str = 'MSIE 10'; 
					var trident = !!navigator.userAgent.match(/Trident\/7.0/);
                    var net = !!navigator.userAgent.match(/.NET4.0E/);
                    var IE11 = trident && net;
                    if( $(objThis.currentActiveElement).attr("type") != "stickynoteTextArea" )
                    {
                    	objThis.removeFocusForcefully();
                    }
					if(navigator.userAgent.indexOf(ie_str) == -1 &&(!IE11)) {
						$(objThis.currentActiveElement).focus(); 
					}
					if(($(objThis.currentActiveElement).attr("type") == "text" && navigator.userAgent.indexOf(ie_str) == -1 && (!IE11)))
					{
						$(objThis.currentActiveElement).focus();
					}
					else
					{
						$(objThis.currentActiveElement).addClass("focusglow");
					}
					
					
				}
			}
		}
		else {
			objThis.currentActiveElementIndex = 0;
			var nHotKeyElementsCount = undefined;
			if(EPubConfig.pageView == "doublePage" && (objThis.currentActiveElementGroupID == "PageBody" || objThis.currentActiveElementGroupID == "") 
				&& $(objThis.currentActiveElementGroup)[0].id != "tocMainPanel" )
			{
				//console.log('after undefined', objThis.currentActivePageSide);
				nHotKeyElementsCount = 0;
				if(objThis.currentActivePageSide == "left")
				{
					nHotKeyElementsCount = objThis.setPageHotkeyElements(true);
					objThis.currentActivePageSide = "right";
				}
				else
				{
					objThis.currentActivePageSide = "left";
					nHotKeyElementsCount = objThis.setPageHotkeyElements();
				}
			}
			if( nHotKeyElementsCount == undefined || nHotKeyElementsCount > 0 )
			{
				objThis.setNextActiveElement();
			}
			else
			{
				nSetActiveLookCounter = 11;
			}
		}
	}
	else
	{
			if (EPubConfig.pageView == "doublePage") {
				//console.log("second page");
				objThis.currentActiveElementIndex = 0;
				//console.log('after undefined', objThis.currentActivePageSide);
				if (objThis.currentActivePageSide == "left") {
					objThis.setPageHotkeyElements(true);
					objThis.currentActivePageSide = "right";
				}
				else {
					objThis.currentActivePageSide = "left";
					objThis.setPageHotkeyElements();
				}
				objThis.setNextActiveElement();
			}
			
		
	}
	if($("a.ui-slider-handle").hasClass("focusglow"))
	{
		$(objThis.currentActiveElement).blur();
	}
}

HotkeyManager.removeFocusForcefully = function ()
{
	$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
    $("#inputToRemove").focus();
    $("#inputToRemove").blur();
    $("#inputToRemove").remove();  
};
	
HotkeyManager.setPreviousActiveElement = function()
{
	nSetActiveLookCounter--;
	if(nSetActiveLookCounter < -10)
	{
		return;
	}
	var objThis = this;
	if (objThis.currentActiveElementGroupID != "PageBody") {
		if ( objThis.currentActiveElementIndex > 1 )
			objThis.currentActiveElementIndex--;
		//console.log(objThis.currentActiveElementIndex);
		var objActiveElement = $(objThis.currentActiveElementGroup).find('[hotkeyindex=' + objThis.currentActiveElementIndex + ']')[0];
		//console.log(nSetActiveLookCounter, objActiveElement);
		if (objActiveElement) {
			var classOfActiveElement = $(objActiveElement).attr("class");
			if ($(objActiveElement).hasClass('ui-disabled') || $(objActiveElement).css('visibility') == 'hidden' || $(objActiveElement).css('display') == 'none' 
				|| ($(objActiveElement).parent().css('display') == 'none' && objThis.currentActiveElementGroupID != "PopupMenu") || $(objActiveElement).hasClass('leftPanelTabActive') 
				|| classOfActiveElement.indexOf("Selected") != -1 ) {
				//objThis.currentActiveElementIndex = 0;
				objThis.setPreviousActiveElement();
				return;
			}
			else if(($(objThis.currentActiveElement).attr("type") == "text" && $($(objActiveElement).parent()).css('display') == 'none') || ($(objThis.currentActiveElement).attr("elementtype") == "textarea" && $(objActiveElement).css('display') == 'none'))
			{
				//objThis.currentActiveElementIndex = 0;
				objThis.setPreviousActiveElement();
				return;
			}
			else {
				var objPrevEle = objThis.currentActiveElement;
				if($(objThis.currentActiveElement).attr("type") == "text" || $(objThis.currentActiveElement).attr("elementtype") == "textarea")//elementtype="textarea"
				{
					$(objThis.currentActiveElement).blur();
				}
				
				objThis.currentActiveElement = objActiveElement;
				
				if (objThis.currentActiveElement) {
					nSetActiveLookCounter = 0;
					$(".focusglow").removeClass("focusglow");
					var ie_str = 'MSIE 10'; 
                    var trident = !!navigator.userAgent.match(/Trident\/7.0/);
                    var net = !!navigator.userAgent.match(/.NET4.0E/);
                    var IE11 = trident && net;
					if(navigator.userAgent.indexOf(ie_str) == -1 && (!IE11)) {
						$(objThis.currentActiveElement).focus(); 
					}
					if($(objThis.currentActiveElement).attr("type") == "text" && navigator.userAgent.indexOf(ie_str) == -1 && (!IE11))
					{
						$(objThis.currentActiveElement).focus();
					}
					else
					{
						$(objThis.currentActiveElement).addClass("focusglow");
					}
					
					
				}
			}
		}
		else {
			
			objThis.currentActiveElementIndex = 0;
			if(EPubConfig.pageView == "doublePage")
			{
				//console.log('after undefined', objThis.currentActivePageSide);
				if(objThis.currentActivePageSide == "left")
				{
					objThis.setPageHotkeyElements(true);
					objThis.currentActivePageSide = "right";
				}
				else
				{
					objThis.currentActivePageSide = "left";
					objThis.setPageHotkeyElements();
				}
			}
			objThis.setPreviousActiveElement();
		}
	}
	else
	{
		
			
			if (EPubConfig.pageView == "doublePage" && objThis.currentActiveElementGroupID != "PopupMenu") {
				//console.log("second page");
				objThis.currentActiveElementIndex = 0;
				//console.log('after undefined', objThis.currentActivePageSide);
				if (objThis.currentActivePageSide == "left") {
					objThis.setPageHotkeyElements(true);
					objThis.currentActivePageSide = "right";
				}
				else {
					objThis.currentActivePageSide = "left";
					objThis.setPageHotkeyElements();
				}
				objThis.setPreviousActiveElement();
			}
			
		
	}
}

HotkeyManager.pageBodyFocusInHandler = function()
{
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		var objThis = this;
		
		if (objThis.currentActiveElementGroupID != "PopupMenu") {
			if (objThis.currentActiveElementGroupID != "PageBody") {
				objThis.currentActivePageSide = "left";
				objThis.setPageHotkeyElements();
				objThis.setNextActiveElement();
			}
			else {
				objThis.currentActiveElementIndex--;
				objThis.setNextActiveElement();
			}
		}
	}
}

HotkeyManager.resetTabElements = function()
{
	var objThis = this;
	//console.log('calling pageBodyFocusInHandler');
}

HotkeyManager.getActiveElementType = function()
{
	var objThis = this;
	var strElementType = "";
	if (objThis.currentActiveElementGroupID == "LeftMenu") {
		if (objThis.currentActiveElement) {
			if ($(objThis.currentActiveElement).attr('data-role') == 'buttonComp') {
			
				var objBtnComp = $(objThis.currentActiveElement).data('buttonComp');
				if (objBtnComp.getSelected() == true) {
					strElementType = 'buttonComp';
				}
			}
		}
	}
	else if (objThis.currentActiveElementGroupID == "PageBody") 
	{
		
	} 
	else if(objThis.currentActiveElementGroupID == "PopupMenu")
	{
		
	}
	return strElementType;
}

HotkeyManager.setPopupFocus = function(objElement)
{
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		var objThis = this;
		objThis.nullifyActiveElements();
		
		if ($(objElement).attr('hotkeygroupid')) {
		
			if ($(objElement).attr('hotkeygroupid') == "PopupMenu") {
				//if (objThis.currentActiveElementGroupID != "PopupMenu") {
				nSetActiveLookCounter = 0;
				
				objThis.prevActiveElementGroupID = objThis.currentActiveElementGroupID;
				objThis.prevActiveElementIndex = objThis.currentActiveElementIndex;
				
				objThis.currentActiveElementGroupID = "PopupMenu";
				objThis.currentActiveElementIndex = 0;
				objThis.currentActiveElementGroup = $(objElement);
				objThis.setNextActiveElement();
			//}
			}
		}
	}
}

HotkeyManager.nullifyActiveElements = function()
{
	//alert('calling ')
	var objThis = this;
	var objElement = $(objThis.currentActiveElement).find('[elementtype=textarea]')[0];
	if(objElement)
	{
		//$(objElement).blur();
	}
	this.currentActiveElementGroup = null;
	this.currentActiveElementIndex = 0;
	this.currentActiveElement = null;
	this.currentActiveElementGroupID = "";
	$(".focusglow").removeClass("focusglow");
	nSetActiveLookCounter = 0;
}


HotkeyManager.setPageHotkeyElements = function(isRightPage)
{
	var nCurrentHIndex = 0;
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		var objThis = this;
		nSetActiveLookCounter = 0;
		var nIndex = GlobalModel.currentPageIndex;
		if (isRightPage) {
			nIndex++;
		}
		var objPageElement = $($('[type="PageContainer"]')[nIndex]);
		//var objNextPageElement;// = $($('[type="PageContainer"]')[GlobalModel.currentPageIndex + 1]);
		if (objPageElement) {
			objThis.nullifyActiveElements();
			//console.log($(objPageElement).find('[type=addRemoveBookmarkStrip]')[0]);
			this.currentActiveElementGroup = objPageElement;
			var objBookmarkElement = $(objPageElement).find('[type=addRemoveBookmarkStrip]')[0];
			
			if($(objBookmarkElement).hasClass('ui-disabled') == false)
			{
				$($(objPageElement).find('[type=addRemoveBookmarkStrip]')[0]).attr('hotkeyindex', "1");
				$($(objPageElement).find('[type=addRemoveBookmarkStrip]')[0]).attr('hotkeygroupid', "PageBody");
				nCurrentHIndex = 1;
			}
			
			// hotkeygroupid="LeftMenu"
			var i;
			var arrElements = $(objPageElement).find('[data-link="internal-link"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type="note"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type="footnote"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[data-linktype="imagegallery"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			if (EPubConfig.Glossary_isAvailable) {
				arrElements = $(objPageElement).find('[type=glossterm]');
				if (arrElements) {
					if (arrElements.length > 0) {
						for (i = 0; i < arrElements.length; i++) {
							nCurrentHIndex++;
							$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
							$(arrElements[i]).attr('hotkeygroupid', "PageBody");
						}
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type=stickyNoteComp]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type=audioHotSpot]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type="widget:quiz"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						if ($(arrElements[i]).attr('interactiongroupid') == undefined) {
							nCurrentHIndex++;
							$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
							$(arrElements[i]).attr('hotkeygroupid', "PageBody");
						}
					}
				}
			}
			arrElements = $(objPageElement).find('[type="widget:quizStatus"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			arrElements = $(objPageElement).find('[type="widget:learnosity"]');
			if (arrElements) {
				if (arrElements.length > 0) {
					for (i = 0; i < arrElements.length; i++) {
						nCurrentHIndex++;
						$(arrElements[i]).attr('hotkeyindex', nCurrentHIndex);
						$(arrElements[i]).attr('hotkeygroupid', "PageBody");
					}
				}
			}
			
			//this.currentActiveElementIndex = 0;
			//this.currentActiveElement = null;
			this.currentActiveElementGroupID = "";
		}
	}
	return nCurrentHIndex;
}

HotkeyManager.findAndSetFocus = function(objElement)
{
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		var objThis = this;
		var strHGId = $(objElement).attr("hotkeygroupid");
		/*
	 objThis.currentActiveElementGroupID = "LeftMenu";
	 objThis.currentActiveElementIndex = 0;
	 objThis.currentActiveElementGroup = $("#annotationPanel")[0];
	 objThis.setNextActiveElement();
	 */
		if (strHGId) {
			if (strHGId == "LeftMenu") {
				objThis.currentActiveElementGroup = $("#annotationPanel")[0];
				objThis.currentActiveElementIndex = parseInt($(objElement).attr("hotkeyindex"));
				objThis.currentActiveElement = $(objElement);
			}
			else 
				if (strHGId == "PageBody") {
					objThis.currentActiveElementGroup = $($('[type="PageContainer"]')[GlobalModel.currentPageIndex]);
					objThis.currentActiveElementIndex = parseInt($(objElement).attr("hotkeyindex"));
					objThis.currentActiveElement = $(objElement);
				}
				else 
					if (strHGId == "TOCMenu") {
						$(".focusglow").removeClass("focusglow");
						objThis.currentActiveElementGroup = $("#tocMainPanel")[0];
						objThis.currentActiveElementIndex = parseInt($(objElement).attr("hotkeyindex"));
						objThis.currentActiveElement = $(objElement);
						$(objThis.currentActiveElement).addClass("focusglow");
						var Index = $(".focusglow").attr("id");
						if ((Number($("#" + Index).offset().top - 157) + parseInt($("#" + Index).css("height"))) > parseInt($("#curTree").height()) || Number($("#" + Index).offset().top - 157) < 0) 
							tocScroll.scrollToElement("#" + Index, 0);
					}
		}
	}
}

HotkeyManager.setPageFocusOnPopupRemove = function()
{
	if (EPubConfig.KeyboardAccessibility_isAvailable) {
		if (objThis.prevActiveElementGroupID == "PageBody") {
			objThis.currentActiveElementGroupID = objThis.prevActiveElementGroupID;
			objThis.currentActiveElementIndex = objThis.prevActiveElementIndex;
			objThis.currentActiveElementIndex--;
			objThis.pageBodyFocusInHandler();
		}
		else {
		
		}
		
	}
}
