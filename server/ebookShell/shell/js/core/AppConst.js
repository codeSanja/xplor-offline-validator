/**
* @description This JS file contains all the application const.
* @author Magic.
*/
function AppConst(){}

AppConst.ATTR_TYPE = 'type';
AppConst.ATTR_AUTHORABLE = 'authorable';
AppConst.DLT_CONFIRM_PANEL = 'dltConfirmPanel';
AppConst.NOTE_LIMIT_PANEL = 'noteLimitPanel';
AppConst.BOOKMARK_SECTION_PANEL = 'BookMarkSectionPanel';
AppConst.BOOKMARK_SECTION_LAUNCHER = 'bookmarkSectionLauncher';
AppConst.BOOKMARK_DLT_CONFIRM_PANEL = 'bookmarkDltConfirmPanel';
AppConst.STICKYNOTE_LAUNCHER = 'stickyNoteLauncher';
AppConst.STICKYNOTE_CONTAINER = 'stickyNoteContainer';
AppConst.HIGHLIGHT_CONTAINER = 'highlightContainer';
AppConst.NOTES_ANNOTATION_BTN = 'notesAnnotationBtn';
AppConst.HIGHLIGHT_ANNOTATION_BTN = 'highlightAnnotationBtn';
AppConst.NOTE_SECTION_PANEL = 'NotesSectionPanel';
AppConst.AUDIO_PLAYER_PANEL = 'audioPlayerPanel';
AppConst.AUDIO_LAUNCHER_BTN = 'audioLauncherBtn';
AppConst.AUDIO_HOTSPOT = 'audioHotspot';
AppConst.MAIN_SHOWROOM = 'mainShowroom';
AppConst.NAV_NEXT = 'next';
AppConst.NAV_PREVIOUS = 'previous';
AppConst.THUMBNAIL_FOOTER_CONTAINER = 'ThumbnailContainer';
AppConst.ZOOM_CONTAINER = 'ZoomContainer';
AppConst.ZOOM_PANEL_COMP = 'zoomPanelComp';
AppConst.PRINT_PANEL_COMP = 'printPanelComp';
AppConst.SCROLL_VIEW_CONTAINER = 'ScrollViewContainer';
AppConst.PAGE_CONTAINER = 'PageContainer';
AppConst.ZOOM_PANEL_LAUNCHER_BUTTON = 'ZoomPanelLauncherBtn';
AppConst.PRINT_PANEL_LAUNCHER_BUTTON = 'PrintPanelLauncherBtn';
AppConst.ZOOM_IN_BUTTON = 'ZoomInButton';
AppConst.ZOOM_OUT_BUTTON = 'ZoomOutButton';
AppConst.VIDEOHOTSPOT_LAUNCHER = 'VideoHotspotLauncher';
AppConst.IMAGEHOTSPOT_LAUNCHER = 'ImageHotspotLauncher';
AppConst.AUDIOHOTSPOT_LAUNCHER = 'AudioHotspotLauncher';
AppConst.VIDEO_LAUNCHER = 'videoLauncher';
AppConst.AUDIO_LAUNCHER = 'audioLauncher';
AppConst.IMAGE_LAUNCHER = 'imageLauncher';
AppConst.STICKY_NOTE_PANEL_COMP = 'stickyNotePanelComp';
AppConst.STICKY_NOTE_COMP = 'stickyNoteComp';
AppConst.STICKY_NOTE_REMOVE = 'stickyNoteRemove';
AppConst.STICKY_NOTE_CHANGE = 'stickyNoteChange';
AppConst.STICKY_NOTE_SAVE = 'stickyNoteSave';
AppConst.HIGHLIGHTER_COMP = 'highlighterComp';
AppConst.HIGHLIGHTER_REMOVE = 'highlighterRemove';
AppConst.HIGHLIGHTER_CHANGE = 'highlighterChange';
AppConst.HIGHLIGHTER_SAVE = 'highlighterSave';
AppConst.TOOL_BUTTON = "toolbutton";
AppConst.TOOLS_PANEL = 'toolsPanel';
AppConst.ADD_REMOVE_BOOKMARK = "addRemoveBookmark";
AppConst.ADD_REMOVE_BOOKMARK_STRIP = "addRemoveBookmarkStrip";
AppConst.CLOSE_ANNOTATION = 'closeAnnotationPopup';

AppConst.GLOSSARY = 'addRemoveGlossary';
AppConst.EXTERNAL_TOC_LAUNCHER = 'extTocPopupLauncher';
AppConst.TOC_PANEL_COMP = 'tocPanelComp';
AppConst.FOOTER_THUMBNAIL_LAUNCHER = 'FooterThumbnailLauncher';
AppConst.THUMBNAIL_SLIDER_CONTAINER = 'thumnailSliderContainer';
AppConst.THUMBNAIL_LINK_CONTAINER = 'thumbnailLinkContainer';
AppConst.FOOTER_POPUP_COMP = 'footerPopup';
AppConst.TEACHERS_AREA = 'teachersArea';
AppConst.MEDIA_SYNC_HOTSPOT = 'mediaSyncHotspot';
AppConst.TEXT_SYNC_AUDIO_LAUNCHER = 'textSyncAudioLauncher';
AppConst.MEDIA_SYNC_RECORDER = 'mediaSyncRecorder';
AppConst.VOICE_RECORDER = "voiceRecorder";
AppConst.RESOURCE_LIST_LAUNCHER = 'btnResourceList';
AppConst.STANDARD_LIST_LAUNCHER = 'btnStandardList';
AppConst.RESOURCE_PANEL_LAUNCHER = 'resourcePanelPopupLauncher';
AppConst.RESOURCE_PANEL = 'ResourcesPanelComp';
AppConst.CLOSE_RESOURCE_PANEL = 'closeResourcePopup';
AppConst.FOOTER = 'footer';
AppConst.FOOTER_EXPAND_BUTTON = 'footerExpandButton';
AppConst.STANDARD_PANEL_LAUNCHER = 'standardPanelPopupLauncher';
AppConst.STANDARD_PANEL = 'StandardsPanelComp';
AppConst.CLOSE_STANDARD_PANEL = 'closeStandardPopup';
////////////////////////////////////////////
AppConst.DRAWING_CANVAS = 'drawingcanvas';
AppConst.DRAWING_COMP = 'drawingComp';
AppConst.HIGHLIGHTER = 'Highlighter';
AppConst.PENCIL = 'Pencil';
AppConst.ERASER = 'Eraser';
AppConst.ERASE_ALL = 'EraserAll';
AppConst.UNDERLINE = 'Underline';
AppConst.STICKYNOTE = 'stickyNoteComp';
AppConst.HOTSPOT = 'hotspotComp';
AppConst.INTERNAL_LINK = 'internalLinks';
AppConst.TEXT_BOX = 'textBoxComp';

AppConst.SHAPES = 'shapes';
AppConst.RECTANGLE = 'rectangle';

AppConst.TEXT_BOX_TOOLS_PANEL_COMP = 'textBoxToolsPanelComp';
AppConst.TOGGLE_AUTHORING = 'toggleAuthoring';

AppConst.AUTHORING_NONE = "authoringNone";
AppConst.AUTHORING_CONTENT = "authoringContent";

AppConst.WEB_LINK_COMP = 'webLinkComp';
AppConst.WEB_LINK_PANEL_COMP = 'webLinkPanelComp';
AppConst.WEB_LINK_REMOVE = 'webLinkRemove';
AppConst.WEB_LINK_SAVE = 'webLinkSave';

AppConst.GLOSSARY_COMP = 'glossaryComp';
AppConst.GLOSSARY_PANEL_COMP = 'glossaryPanelComp';
AppConst.GLOSSARY_SEARCH_COMP = 'glossarySearchComp';
AppConst.GLOSSARY_SEARCH_PANEL_COMP = 'glossarySearchPanelComp';

AppConst.STICKY_CNT_ARR = new Array();
AppConst.HELP_COMP = 'helpComp';
AppConst.PAGE_WIDTH = 'pageWidth';
AppConst.PAGE_HEIGHT = 'pageHight';
AppConst.THUMBNAIL_WIDTH = 'thumbnailWidth';
AppConst.THUMBNAIL_HEIGHT = 'thumbnailHeight';
AppConst.PAGE_LAYOUT_TYPE = 'singlePage';
AppConst.SPLASH = 1;
AppConst.DATA_ARR = 'dataArray';
AppConst.EXTERNAL_LINK = 'externalLink';
AppConst.ORIENTATION = 'landscape';

//z-index

AppConst.ZI_POPUP_MGR_MASK = 5000;
AppConst.ZI_POPUP_ARROW_POINTER = 5050;
AppConst.ZI_PANEL_VIEW_COMP= 5004;
AppConst.ZI_NOTES_MASK =3005;
AppConst.ZI_STICKY_COMP= 10001;
AppConst.NOTE_MARGIN_FROM_RIGHT = 50;

AppConst.BTN_DEL = 'delete_btn';

//Added by Harsh
AppConst.BTN_SAVE = 'save_btn';
AppConst.BTN_RET = 'retrieve_btn';

///COMPONENT constatnt
AppConst.CENTER = "center";
AppConst.FLOAT_RIGHT = "right";
//question comp
AppConst.DND_COMP = 'DndComp';
AppConst.DND_DRAGGABLE_COMP = 'DndDraggableComp';
AppConst.DND_TARGET_COMP = 'DndTargetComp';
AppConst.CHECK_ANSWER = "checkAnswer";
AppConst.RESPONSE_DECLARATION = "responseDeclaration";
AppConst.MODALFEEDBACK_COMP = "modalFeedbackComp";
AppConst.SCREEN_INDEX = '0';
AppConst.SCREEN_IDENTIFIER = '1';
AppConst.PAINT_COMP = 'PaintComp';
AppConst.SHOW_ANSWER = "showAnswer";
AppConst.TRY_AGAIN = "tryAgain";
AppConst.MATCH_COMP = 'MatchComp';

//annotationContainer
AppConst.ANNOTATION_CONTAINER = 'annotationContainer';

//help button links
AppConst.HELP_BACK = "helpBack";
AppConst.HELP_COUNT = 'helpCount';
AppConst.HELP_TITLE = "helpTitle";
AppConst.HELP_CONTAINER = "helpContainer";


AppConst.GRADE_K2 = "K2";
AppConst.GRADE_6TO12 = "6TO12"
AppConst.GRADE_2TO5 = "2TO5";
AppConst.GRADE_PREKTO1 = "PREKTO1";

AppConst.GRADE_7TO12 = "7TO12"
AppConst.GRADE_3TO6 = "3TO6";
AppConst.GRADE_KTO2 = "KTO2";

//views
AppConst.SCROLL = "SCROLL";
AppConst.SCROLL_VIEW = "SCROLL";
AppConst.SCROLL_VIEW_CAMEL_CASING = "scroll";

AppConst.SINGLE_PAGE = "SINGLE_PAGE";
AppConst.SINGLE_PAGE_CAMEL_CASING = "singlePage";

AppConst.DOUBLE_PAGE = "DOUBLE_PAGE";
AppConst.DOUBLE_PAGE_CAMEL_CASING = "doublePage";

//AppConst.BOOK_TITLE = "accessible_epub_3-20121024.epub";
AppConst.BOOK_TITLE = "ScienceFusion";


//Resource Type Constants
AppConst.AUDIO = "AUDIO";
AppConst.VIDEO = "VIDEO";
AppConst.IMAGE = "IMAGE";
AppConst.HTML = "HTML";
AppConst.PDF = "PDF";
AppConst.PPT = "PPT";


//Constants of tasks
AppConst.PARSE_QUERY_STRING = "PARSE_QUERY_STRING";
AppConst.LOAD_BASE_FILES = "LOAD_BASE_FILES";
AppConst.LOAD_APPLICATION_AND_CORE_FILES = "LOAD_APPLICATION_AND_CORE_FILES";
AppConst.LOAD_LOCALIZATION_DATA = "LOAD_LOCALIZATION_DATA";
AppConst.LOAD_CSS = "LOAD_CSS";
AppConst.LOAD_EPUB_GLOABL_SETTINGS = "LOAD_EPUB_GLOABL_SETTINGS";
AppConst.LOAD_TOC_XHTML = "LOAD_TOC_XHTML";
AppConst.LOAD_LAYOUT_HTML = "LOAD_LAYOUT_HTML";
AppConst.LOAD_CONTENT_OPF_SETTINGS = "LOAD_CONTENT_OPF_SETTINGS";
AppConst.INITIALIZE_APP = "INITIALIZE_APP";
AppConst.LOAD_APP_FEATURES = "LOAD_APP_FEATURES";
AppConst.SET_ANALYTICS_DATA = "SET_ANALYTICS_DATA";

//constants for application features
AppConst.BOOKMARK = "BOOKMARK";

AppConst.CONTENTS_PANEL = "CONTENTS_PANEL";
AppConst.TABEL_OF_CONTENTS = "TABEL_OF_CONTENTS";
AppConst.SEARCH_PANEL = "SEARCH_PANEL";
AppConst.QA_PANEL = "QA_PANEL";
AppConst.INDEX_PANEL = "INDEX_PANEL";

AppConst.RESOURCES_TOOLS_PANEL = "CONTENTS_PANEL";
AppConst.RESOURCES_PANEL = "RESOURCES_PANEL";
AppConst.ZOOM = "ZOOM";
AppConst.STICKY_NOTES = "STICKY_NOTES";
AppConst.HIGHLIGHTER = "HIGHLIGHTER";
AppConst.DRAWING = "DRAWING";
AppConst.COPY = "COPY";
AppConst.GLOSSARY = "GLOSSARY";
AppConst.FOOT_NOTE = "FOOT_NOTE";
AppConst.MARKUP_TOOLS = "MARKUP_TOOLS";
AppConst.PAGE_LEVEL_AUDIO = "PAGE_LEVEL_AUDIO";
AppConst.HELP = "HELP";
AppConst.ANNOTATION_PANEL = "ANNOTATION_PANEL";
AppConst.READ_ALOUD = "READ_ALOUD";
AppConst.INDEX_PANEL_COMP = 'indexPanelComp';
AppConst.SEARCH_PANEL_COMP = 'searchPanelComp';
AppConst.TOC_MAIN_PANEL = 'tocMainPanel';
AppConst.RESOURCES_MAIN_PANEL = "resourcesMainPanel";
AppConst.SEC_PANEL_CLOSE_BTN = 'sectionPanelCloseBtn';
AppConst.PAGE_VIEW_LAUNCHER = 'pageViewLauncher';
AppConst.QUES_ANS_PANEL = 'questionAnswerPanel';
AppConst.QUES_ANS_INNER_CONTENT = 'questionanswerInnerContent';
AppConst.ZOOM_SLIDER_CONTAINER = 'zoomSliderContainer';
AppConst.SCROLLPAGELAUNCHER = 'scrollPageLauncher';
AppConst.SINGLEPAGELAUNCHER = 'singlePageLauncher';
AppConst.DOUBLEPAGELAUNCHER = 'doublePageLauncher';
AppConst.VIDEO_PLAYER_PANEL = "videoPlayerPanel"
AppConst.STANDARDS_PANEL = "STANDARDS_PANEL";
AppConst.STANDARDS_MAIN_PANEL = "standardsMainPanel";
AppConst.FOOT_NOTE_PANEL_COMP = 'footNotePanelComp';
AppConst.IMAGE_GALLERY_PANEL = 'imageGalleryPanel';

AppConst.TEACHER_EDITION = "TE";
AppConst.STUDENT_EDITION = "SE";
AppConst.OTHERS_EDITION = "OT";
AppConst.NATIVECLOSEBTN = 'nativeCloseBtn';

AppConst.ORIENTATION_LANDSCAPE = 'landscape';
AppConst.ORIENTATION_PORTRAIT = 'portrait';



// Tin Can Consts
AppConst.VIDEO_PLAY = "videoPlay";
AppConst.AUDIO_PLAY = "audioPlay";
AppConst.SEND_TRACKING_STATEMENT = "sendTrackingStatement";
AppConst.ACTIVITY_DATA = "activityData";
AppConst.PAGE_DATA = "setPageData";
AppConst.AUDIO_TIME_SPENT = "audioTimeSpent";
AppConst.VIDEO_TIME_SPENT = "videoTimeSpent";
AppConst.ACTIVITY_DATA_TIMESPENT = "activityDataTimeSpent";
AppConst.SEARCH_TEXT = "searchText";

AppConst.PAGE_VIEW_CHANGED = 'pageviewchanged';

AppConst.USER_ROLE = "userrole";
AppConst.USERTYPE_PUBLISHER = "publisher";
AppConst.USERTYPE_TEACHER = "Teacher";
AppConst.USERTYPE_INSTRUCTOR = "Instructor";
AppConst.USERTYPE_STUDENT = "Student";
