/**
 * @author sachin.manchanda
 * Class handles the tabs views
 */
var arrTabs = [];
var objToc = $.widget( "magic.tabViewComp", $.magic.magicwidget, 
{
	_init: function()
	{
		var self = this;
		var objComp = $("#"+this.options.tabViewComp)[0];
		$.magic.magicwidget.prototype._init.call(this);	
		
		if(objComp == null)
			return;
		
		//add click handlers to tab buttons
    	arrTabs.push(objComp);
    	$(objComp).unbind("click").bind("click", function(){
            self.tabClickHandler(objComp);
        });
        $("#tocMainPanel").unbind('tocPanelLaunched').bind('tocPanelLaunched',function(){
            self.member.bFirstTimePanelOpen = true;
            self.onPanelOpen();
        });
            
        
	},
    member:{
        bFirstTimePanelOpen: true
    },
	
	/**
   * This function handles when a tab button is clicked in the TOC panel.
   * @param1 {Object} Object of data-role an element is binded with
   * @return void
   */
	tabClickHandler : function(objComp)
	{
		var objThis = this;
		
		//First reset the open TAB ten open the new TAB
		for(var i = 0; i < arrTabs.length; i++)
		{
			var objItem = arrTabs[i];
			var strType = $(objItem).attr("type");
			//ensuring that tabs to deselect are siblings of selected tab;
			if(objComp.parentNode == objItem.parentNode)
			{
				$("#" + strType).trigger('tabDisplayReset');
				$("#" + strType).css("display", "none");
				$(objItem).removeClass("leftPanelTabActive");
				$(objItem).addClass("leftPanelTab");
			}
		}
		
		for(var i = 0; i < arrTabs.length; i++)
		{
			var objItem = arrTabs[i];
			var strType = $(objItem).attr("type");
			
			if(objComp == objItem)
			{
				$("#" + strType).css("display", "block");
				$(objItem).removeClass("leftPanelTab");
				$(objItem).addClass("leftPanelTabActive");
               // var arrElemDatarole = ($("#" + strType).attr('data-role'));
               // console.log($("#" + strType).data(arrElemDatarole));
               //if(objThis.member.bFirstTimePanelOpen == true){
                   $("#" + strType).trigger('tabDisplayChanged');
                   //objThis.member.bFirstTimePanelOpen = false;
               //}
			}
		}
	},
    
    onPanelOpen: function(){
        var tabViewPanelChildren = this.element.find('.tabViewPanel');
        
		for (var i = 0; i < tabViewPanelChildren.length; i++) {
			if ($($(tabViewPanelChildren)[i]).css('display') == "block") {
				$($(tabViewPanelChildren)[i]).trigger('panelOpened');
			}
		}
    },
    
    beforePanelClose: function(){
        var tabViewPanelChildren = this.element.find('.tabViewPanel');
        
		for (var i = 0; i < tabViewPanelChildren.length; i++) {
			$($(tabViewPanelChildren)[i]).trigger('beforePanelClose');
		}
    },
    
    onPanelClose: function(){
        var tabViewPanelChildren = this.element.find('.tabViewPanel');
        
		for (var i = 0; i < tabViewPanelChildren.length; i++) {
			if ($($(tabViewPanelChildren)[i]).css('display') == "block" || $($(tabViewPanelChildren)[i]).attr('always-close') == "") {
				$($(tabViewPanelChildren)[i]).trigger('panelClosed');
			}
		}
    },
	
	destroy: function()
	{
		$.widget.prototype.destroy.call( this );		
	}
});
