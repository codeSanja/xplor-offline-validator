GlobalModel = {};

//GlobalModel.ODataAnnotation = new AnnService();
//annService.init("student1", "student", "ebook.hmhpub.com");

GlobalModel.pageLinkLists = new Array();
GlobalModel.bookMarks = [];
GlobalModel.annotations = [];
GlobalModel.totalStickyNotes = 0;
GlobalModel.istotalHighlightCount = 0;
GlobalModel.DPLive = true;
GlobalModel.pageData = {};
GlobalModel.highlights = [];
GlobalModel.currentPageIndex = 0;
GlobalModel.currentScalePercent = 1;
GlobalModel.currentPageIndexByPos = 0;
GlobalModel.actualScalePercent = 1;
GlobalModel.requiredScalePercent = 1;
GlobalModel.stickyNoteArrayTopPosition = [];
GlobalModel.localizationData = null;
GlobalModel.audioSrc = "content/en/ScienceFusion/OPS/audio/Grade_3_dissolve.mp3";
GlobalModel.ePubGlobalSettings = null;
GlobalModel.pageBrkValueArr = [];
GlobalModel.navPageArr = [];
GlobalModel.questionPanelAnswers = (GlobalModel.questionPanelAnswers == null) ? {} : GlobalModel.questionPanelAnswers;
GlobalModel.questionPanelAnswersForAnnoPanel = [];
GlobalModel.pageLeftValue = 0;
GlobalModel.isToAnimate = false;
GlobalModel.isAnnotationDataLoaded = false;
GlobalModel.imageGalleryDataArr = [];
GlobalModel.bookPromptDataArr = [];
GlobalModel.timeToBlogDataArr = [];
GlobalModel.AutomaticBookmarkDate = 0;
GlobalModel.AutomaticBookmarkAnnID = 0;
GlobalModel.isContentFileLoaded = false;
GlobalModel.contentOpfFileSettings = [];
GlobalModel.originalPageView = "";
GlobalModel.pageDataByPageName = {};
GlobalModel.pageDataByPageBreakValue = {};
GlobalModel.pageDataBySequence = {};
GlobalModel.printCSSPath = ["css/print.css"];
GlobalModel.currentDeviceOrientation = "";
GlobalModel.currentBookContentContaineOverflow = "";
GlobalModel.currentScrollableElement = null;
GlobalModel.currentScrollingElement = null;
GlobalModel.currentMDSData;
GlobalModel.hmh_dp_off_counter = 0;
GlobalModel.uid = null;
GlobalModel.highlightData = [];
GlobalModel.stikyNoteData = [];
GlobalModel.bookmarksData = [];
GlobalModel.QAData = [];
GlobalModel.AddComment = false;
GlobalModel.tocXHTMLData = "";
GlobalModel.layoutHTMLData = "";
GlobalModel.AnnotationIDArr = [];
GlobalModel.QAAnnotationIDArr = [];
GlobalModel.BookEdition = "TSE";
GlobalModel.StudentContentLoaded = false;
GlobalModel.StudentListArr = [];
GlobalModel.NewNoteSelectedStudents = [];

GlobalModel.getAnnotationData = function(pageBreakValue) {
          var currentPageData = [];
          for (var i = 0; i < GlobalModel.annotations.length; i++) {
                    if (GlobalModel.annotations[i].pageNumber == pageBreakValue) {
                              currentPageData.push(GlobalModel.annotations[i]);
                    }
          }
          return currentPageData;
}
GlobalModel.getBookmarkData = function(pageBreakValue) {
          var currentPageData = [];
          for (var i = 0; i < GlobalModel.bookMarks.length; i++) {
                    if (GlobalModel.bookMarks[i].id.replace('bookmarkStrip_', '') == pageBreakValue) {
                              currentPageData.push(GlobalModel.bookMarks[i]);
                    }
          }
          return currentPageData;
}
GlobalModel.hasDPError = function(strWidgetType) {
          if($.cookie("HMH_DP") == "OFF" && $.cookie("HMH_DP_ALERTED") == "FALSE")
        	{
        		if(strWidgetType == "notes" || strWidgetType == "highlight"){
					$("#alertTxt").html(GlobalModel.localizationData["DP_ERROR_MESSAGE_NOTES"]);
				}
				else if(strWidgetType == "qa"){
					$("#alertTxt").html(GlobalModel.localizationData["DP_ERROR_MESSAGE_QA"]);
				}
        		$.cookie("HMH_DP_ALERTED","TRUE");
				setTimeout(function() {
			        PopupManager.addPopup($("#alertPopUpPanel"), null, {
			            isModal : true,
			            isCentered : true,
			            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
			            isTapLayoutDisabled : true
			        });
			    }, 0);
			    return true;      		
        	}
        	else
        	{
        		return false;
        	}
}
GlobalModel.updateTeacherNotificationIcon = function ( idToDisplay ) {
	var mergedArr = [];
	//mergedArr = $.merge( GlobalModel.annotations, mergedArr );
	for( var _tempObj in GlobalModel.questionPanelAnswers )
	{
		for( var _tempObj1 in GlobalModel.questionPanelAnswers[_tempObj] )
		{
			mergedArr.push( GlobalModel.questionPanelAnswers[_tempObj][_tempObj1] );
		}
	}
	for( var i = 0; i < GlobalModel.annotations.length; i++ )
	{
		mergedArr.push( GlobalModel.annotations[ i ] );
	}
	
    if ( mergedArr.length != 0 ) 
    {
        var teacherNoteArr = $.grep(mergedArr, function ( obj ) {
            return ( ( obj.comments || obj.shared_with ) && ( obj.isNotified == true ) && obj.type );
        });
        
        var teacherQAArr = $.grep(mergedArr, function ( obj ) {
            return (  obj.comments && obj.isNotified == true && !obj.type );
        });
        var totalLength = teacherNoteArr.length + teacherQAArr.length;
        
        if ( idToDisplay )
        {
            $( "#" + idToDisplay ).css( "display" , "block" );
        }
        if ( totalLength > 0 ) 
        {
            //$( "#teacherNoteAlert").html( '<span>' + teacherNoteArr.length + '</span>' );
            $( "#teacherNoteAlertleft" ).html( '<span>' + totalLength + '</span>' );
        }
        else
        {
            $("#teacherNoteAlertleft").css("display", "none");
            //$("#teacherNoteAlert").css("display", "none");
        }
        
        if( teacherNoteArr.length > 0 )
        {
        	$( "#teacherNoteAlert").html( '<span>' + teacherNoteArr.length + '</span>' );
        }
        else
        {
        	$("#teacherNoteAlert").css("display", "none");
        }
        
        if( teacherNoteArr.length > 99 )
        {
        	$(".teacherNoteAlert").css("width", "19px");
        }
        else
        {
        	$(".teacherNoteAlert").css("width", "15px");
        }
        
        if( teacherQAArr.length > 0 )
        {
        	$( "#qaAlert").css( "display", "block" );
        	$( "#qaAlert").html( '<span>' + teacherQAArr.length + '</span>' );
        }
        else
        {
        	$( "#qaAlert").css("display", "none");
        }
        
        if( teacherQAArr.length > 99 )
        {
        	$( "#qaAlert").css("width", "19px");
        }
        else
        {
        	$( "#qaAlert").css("width", "15px");
        }
    }
    else
    {
        $("#teacherNoteAlertleft").css("display", "none");
        $("#teacherNoteAlert").css("display", "none");
    }
}
EPubConfig = {
          pageWidth : 820,
          pageHeight : 1171,
          totalPageContainers : 30,
          totalPages : 14,
          pageNamePrefix : "page",
          pageNameExtension : ".xhtml",
          tocFileName : "toc.xhtml",
          indexXMLPath : "index.xml",
          standardsXMLPath : "standards.json",
          notesAnnotationDataPath : "data/annotations/notes_dp.json",
          QADPDataPath : "data/questionAnswer/qaResponse_dp.json",
          imageGalleryXMLPath : "data/imagegallery/imagegallery.xml",
          standardsDescriptionXMLPath : "Florida_Common_Core_Standards_ELA_6-12.json",
          baseContentPath : "content/en",
          toolsXMLPath : "data/tools/tools.json",
          "searchDataUrl" : "data/search/search_data.txt",

          bookTitle : "ePubPOC",
          contentFolderName : "OPS",
          smilFolderName : "smil",
          intractivityPath : "data/questionAnswer",
		  Search_Service_URL: "/api/search/v1/content?q=",
          ePubGlobalSettingsFile : "config.txt",
          ePubContentOpfFile : "content.opf",

          //programBrandingLogo: "brandName.png",
          programBrandingImage : "brandName.png",
          programBrandingTitle : "Program Branding",
          programBrandingSrc : "content/image/brandName.png",
          logoImgDivSrc : "content/image/HMH_logo.png",
          logoImgDiv2Src : "content/image/brandBGImage.png",

          printExternally : "false",
          DP_KEEP_LIVE_URL : "",
          cssFolderName : "css",
          cssFileNames : ["template.css"],

          imageFolderName : "images",
          audioFolderName : "audio",
          videoFolderName : "video",
          fontsFolderName : "fonts",
          //labelsLangPath: "",
          //labelsFileNames: [],
          ReaderType : "",
          LayoutType : "",

          //glossary configurations
          glossaryXMLPath : "content/en/ePubPOC/OPS/data/glossary.xml",
          glossaryType : "in-context",
          FULL_GLOSSARY_PATH : "http://www.google.com",
          MY_NOTEBOOK_PATH : "http://www.google.com",

          //FootNote
          footNoteXMLPath : "data/footnote/footnote.xml",

          inBookPromptXMLPath : "",

          timeToBlogXMLPath : "",

          isPageOrderInSequence : false, //false if page names come from TOC; true if page names are in sequence i.e. page0001.xhtml

          /*
           * Below variables help is communicating with external services. These specify the hierarchy structure of current book
           */

          LevelIdentifier_L0 : "Grade",

          //Level 1 represents volume
          LevelIdentifier_L1 : "Volume",

          //Level 2 represents unit
          LevelIdentifier_L2 : "Unit",

          //Level 3 represents chapter/lesson
          LevelIdentifier_L3 : "Lesson",

          //Level 4 represents section
          LevelIdentifier_L4 : "Section",

          //Level 5 represents page
          LevelIdentifier_L5 : "Page",

          //Level 6 represents page
          LevelIdentifier_L6 : "Page",

          L0 : "false",
          L0_label : "Grade",
          L0_value : "",
          L1 : "false",
          L1_label : "Volume",
          L1_value : "",
          L2 : true,
          L2_label : "Collection",
          L2_value : "",
          L3 : true,
          L3_label : "Lesson",
          L3_value : "",
          L4 : true,
          L4_label : "Section",
          L4_value : "",
          L5 : true,
          L5_label : "Page",
          L5_value : "",
          L6 : "false",
          L6_label : "",
          L6_value : "",

          //TOCStartLevel specifies which level the TOC starts from.
          TOCStartLevel : "L2",

          //	Unit/Lesson/Section/Page
          //If no value is given or variable is not defined Navigation links would not be available
          scrollNavType : 'Page',

          //for theme CSS & themehover CSS
          Theme : "6TO12",

          //Page layout and navigation configurations
          //Page_Layout_Type: "Fixed",
          Page_Navigation_Type : "SCROLL",

          //Annotation configurations;
          //Annotation_Live_Data_Persistence: "",
          Annotation_Service_URL: "/services/mread/",
          Domain_URL : "http://qc-1.mdistribute.magicsw.com",
          InBookPrompt_Service_URL : "",

          //URL to fetch resource list from
          //Resources_Service_URL: "",

          //notes configurations
          Notes_Per_Page : 5,
          Tags_Per_Note : 5,
          //Teacher_Notes_Per_Page: 0,

          //Help configuration
          //Help_Type: "",
          Help_URL : "",
		  Help_Type : "Internal",	
          //Search Configuration
          //Search_URL: "",

          //Highlight configuration
          Highlights_default_color : "#f6d855",
          Highlight_colors : ["#40e6d9", "#31b21f", "#f6d855", "#ee5ee9"],
          Page_Highlight_colors : ["#40e6d9", "#31b21f", "#f6d855", "#ee5ee9"],

          //Carousel/Thumbnail configuration
          //Thumbnail_Path: "",
          // Thumbnail_Width: 0,
          // Thumbnail_Height: 0,

          //Read aloud configuration
          Text_Highlight : 'ON',
          // Read_Aloud_level: "",

          //language to be used for localization;
          LocalizationLanguage : "en",
          defaultLocalizationLanguage : "en",
          Rights : "© Houghton Mifflin Harcourt Publishing Company",
          //Markup_Tool_default_Options: ["Notes", "Highlight"],
          //Markup_default_Color: "#FF00FF",

          //Required For HMH to configure required functionalities
          Glossary_isAvailable : true,
          FootNote_isAvailable : true,
          Annotations_isAvailable : true,
          Markup_Tool_isAvailable : true,
          Resources_isAvailable : true,
          MDSStartLevel : 0,
          Notes_isAvailable : true,
          Highlights_isAvailable : true,
          Help_isAvailable : true,
          BookMark_isAvailable : true,
          ImageAnnotation_isAvailable : true,
          //Tools_isAvailable: false,
          Zoom_isAvailable : true,
          Index_isAvailable : true,
          Search_isAvailable : true,
          searchDataUrl : "data/search/search_data.txt",
          LocalSearch_isAvailable : false,
          LocalResource_isAvailable : false,
          Component_type: "SE",
          Print_isAvailable : false,
          //Carousel_isAvailable: false,
          Read_Aloud_isAvailable : true,
          //TextSizeController_isAvailable: false,
          TOC_isAvailable : true,
          AudioPlayer_isAvailable : true,
          HTML5_audio : false,
          Volume : -1,
          Subject : "",
          Digital_TE_ISBN : "",
          legacyTOC : false,
          Copy_isAvailable : false,
          QAPanel_isAvailable : true,
          My_Notebook_isAvailable : true,
          TwoPageViewDividerLine_isAvailable : true,
          PageNumberInBookmark_isAvailable : true,
          //to be used by reader (internal is available list)
          ContentsPanel_isAvailable : true,
          ResourcesToolsPanel_isAvailable : true,
          NotesQuestionsPanel_isAvailable : true,
          tenant : "hmh",
          /* Start : Modlog_23052013 Aastha for MREAD-310 OBB || configurable goto page functionality */
          Goto_isAvailable : true,
          /* Start : Modlog_23052013 Aastha for MREAD-310 OBB || configurable goto page functionality */
          SinglePageView_isAvailable : true,
          ScrollPageView_isAvailable : true,
          DoublePageView_isAvailable : true,
          DoublePageView_align : "left",
          HomeButton_isAvailable : false,
          Usertype : "",
          ProgramBrandingBar_isAvailable : true,
          bookCoverImageName : "bookCover.jpg",
          HomeButtonURL : "http://www.google.com",
          LogOutURL : "",
          LearnosityActivityURL : "",
          Learnosity_PanelHeight : "50%",
		  resourcesDataPath: "data/resources/resources.json",
		  Zoom_Levels: [60, 100, 140, 180, 220, 260],
		  DEFAULT_HIGHLIGHT : true,
		  AnnotationSharing_isAvailable : false,
		  GroupSharing_isAvailable : true,
          //this is mapping of Component Names and their availability property
          Feature_isAvaliblePropertyMapping : {
                    "STICKY_NOTES" : "Notes_isAvailable",
                    "TEACHER_NOTES" : "AnnotationSharing_isAvailable",
                    "BOOKMARK" : "BookMark_isAvailable",
                    "GLOSSARY" : "Glossary_isAvailable",
                    "FOOT_NOTE" : "FootNote_isAvailable",
                    "HELP" : "Help_isAvailable",
                    "HIGHLIGHTER" : "Highlights_isAvailable",
                    "COPY" : "Copy_isAvailable",
                    "RESOURCES_PANEL" : "Resources_isAvailable",
                    "ZOOM" : "Zoom_isAvailable",
                    "PAGE_LEVEL_AUDIO" : "AudioPlayer_isAvailable",
                    "TABEL_OF_CONTENTS" : "TOC_isAvailable",
                    "SEARCH_PANEL" : "Search_isAvailable",
                    "INDEX_PANEL" : "Index_isAvailable",
                    "ANNOTATION_PANEL" : "Annotations_isAvailable",
                    "READ_ALOUD" : "Read_Aloud_isAvailable",
                    "CONTENTS_PANEL" : "ContentsPanel_isAvailable",
                    "NOTES_QUESTIONS_PANEL" : "NotesQuestionsPanel_isAvailable",
                    "PRINT_PANEL" : "Print_isAvailable",
                    "RESOURCES_TOOLS_PANEL" : "ResourcesToolsPanel_isAvailable",
                    "TOOLS_PANEL" : "Tools_isAvailable",
                    "QA_PANEL" : "QAPanel_isAvailable",
                    "STANDARDS_PANEL" : "Standards_isAvailable",
                    "LANGUAGE_MANAGER" : "MultiLanguage_isAvailable",
                    "MARKUP_TOOLS" : "Markup_Tool_isAvailable",
                    "HOME_BUTTON" : "HomeButton_isAvailable"
          },

          //List of components in side panel. Values of Component (key) is the div ID of the respective component
          SidePanelComponents : {
                    "PAGE_LEVEL_AUDIO" : "audioLauncherBtn",
                    "BOOKMARK" : "bookmarkSectionLauncher",
                    "HELP" : "helpLauncher",
                    "RESOURCES_TOOLS_PANEL" : "btnResourcePanel",
                    "NOTES_QUESTIONS_PANEL" : "stickyNoteLauncher",
                    "PRINT_PANEL" : "printBtn",
                    "CONTENTS_PANEL" : "btnTocLauncher",
                    "ANNOTATION_PANEL" : "annotationPanel",
                    "LANGUAGE_MANAGER" : "languageSelector",
                    "STANDARDS_PANEL" : "btnStandardsPanel",
                    "HOME_BUTTON" : "nativeCloseBtn",
                    "IN_BOOK_PROMPT" : "inBookPrompt"
          },

          TabbedPanels : {
                    "CONTENTS_PANEL" : {
                              "TABEL_OF_CONTENTS" : "tocContents",
                              "SEARCH_PANEL" : "tocSearch",
                              "INDEX_PANEL" : "tocIndex"
                    },

                    "RESOURCES_TOOLS_PANEL" : {
                              "RESOURCES_PANEL" : "resourcesPanelHeading",
                              "TOOLS_PANEL" : "toolsPanelHeading"
                    },

                    "NOTES_QUESTIONS_PANEL" : {
                              "STICKY_NOTES" : "annotationMyNotesTab",
                              "QA_PANEL": "annotationMyQuestionsTab",
                              "TEACHER_NOTES": "annotationTeacherNotesTab"
                    }
          },

          //List of components that are part of annotations and their container IDs
          AnnoToolNames : {
                    "HIGHLIGHTER" : "highlighter-anno",
                    "STICKY_NOTES" : "notes-anno"
          },

          //list of features that are being used in the application (Managed by using HMH settings)
          RequiredApplicationComponents : null,

          scaleToFit : true,

          // If value is true, swipe would be enabled otherwise swipe would be disabled
          swipeNavEnabled : true,

          navDataArr : '',
          pageView : 'singlePage',

          //new settings added on January 15, 2013 (mentioned this comment for documentation purpose)
          ContentId : "", //32 character string representing the Book GUID or ISBN
          book_entitlement : "standard",
          maxCharactersInNotes : 1000,

          // Duration after which branding bar would auto-hide inspite of user's inactivity
          brandHideAfterDuration : '10000',
          brandHideInDuration : 400,

          // Added: Tuesday, 22/1/2013. This will specify the class level of the book.
          isForClass2To5 : false,

          hasPreloaderForPages : true,

          prevPagesToKeepInMemory : 3,
          nextPagesToKeepInMemory : 5,
          pagesToPrecache : 2,
          paginationThreshold : 10,
          TextHighlight_Color : "yellow",
		  KeyboardAccessibility_isAvailable: true
}


GlobalModel.getAngle = function(objCenterPoint, objTargetPoint)
{
	var nXDist = objTargetPoint.xpos - objCenterPoint.xpos;
	var nYDist = objTargetPoint.ypos - objCenterPoint.ypos;
	
	var nAngle = Math.atan2(nYDist, nXDist) * 180 / Math.PI;
	
	return nAngle;
}

GlobalModel.getPosition = function(nAngle, nRadius, objCenterPoint)
{
	var objPosPoint = {};//new Point();
	
	nAngle = nAngle * Math.PI / 180;
	
	objPosPoint.xpos = (Math.cos(nAngle) * nRadius) + objCenterPoint.xpos;
	objPosPoint.ypos = (Math.sin(nAngle) * nRadius) + objCenterPoint.ypos;
	
	return objPosPoint;
}

GlobalModel.getPageContainersSize = function()
{
	var nWidth = 0;
	var nHeight = 0;
	if (EPubConfig.pageView == 'singlePage')
	{
		nWidth = $("#innerScrollContainer").width() * GlobalModel.currentScalePercent;
		nHeight = EPubConfig.pageHeight * GlobalModel.currentScalePercent;
	}
	else if(EPubConfig.pageView == 'doublePage')
	{
		nWidth = $("#innerScrollContainer").width() * GlobalModel.currentScalePercent;
		nHeight = EPubConfig.pageHeight * GlobalModel.currentScalePercent;
	}
	else
	{
		nWidth = EPubConfig.pageWidth * GlobalModel.currentScalePercent;
		nHeight = $("#bookContentContainer").data('scrollviewcomp').options.contentHeight * GlobalModel.currentScalePercent * EPubConfig.totalPages;
	}
	
	return {width: nWidth, height: nHeight};
}

GlobalModel.getPageContainersActualSize = function()
{
	var nWidth = 0;
	var nHeight = 0;
	if (EPubConfig.pageView == 'singlePage')
	{
		nWidth = $("#innerScrollContainer").width() * GlobalModel.actualScalePercent;
		nHeight = EPubConfig.pageHeight * GlobalModel.actualScalePercent;
	}
	else if(EPubConfig.pageView == 'doublePage')
	{
		nWidth = $("#innerScrollContainer").width() * GlobalModel.actualScalePercent;
		nHeight = EPubConfig.pageHeight * GlobalModel.actualScalePercent;
	}
	else
	{
		nWidth = EPubConfig.pageWidth * GlobalModel.actualScalePercent;
		nHeight = $("#bookContentContainer").data('scrollviewcomp').options.contentHeight * GlobalModel.currentScalePercent * EPubConfig.totalPages;
	}
	
	return {width: nWidth, height: nHeight};
}

GlobalModel.getCurrentPageElement = function()
{
	return $("#scrollMainContainer").find("[data-role=pagecomp]")[GlobalModel.currentPageIndex];
}

GlobalModel.getPageDataByPageName = function(pageName)
{
    return GlobalModel.pageDataByPageName[pageName];
}

GlobalModel.getPageBreakValueByIndex = function(nIndex)
{
    return GlobalModel.pageDataBySequence["pagesequence_" + nIndex].pageBreakValue;
}
GlobalModel.getMyNoteBookPagePath = function(pVal)
{
	var location = window.location.href;
	var objReg;
	objReg = /page=/gi;
	if(objReg.test(location))
	{
		objReg = /page=[\s\S]*?(&)/gi;
		if(objReg.test(location))
		{
			location = location.replace(objReg, "page=" + pVal + "&");
		}
		else
		{
			objReg = /page=[\s\S]*/gi;
			location = location.replace(objReg, "page=" + pVal);
		}
		return location;
	}
	location = location + ((location.indexOf("?") > -1) ? "&page=" + pVal : "?page=" + pVal);
	return location;
}