/**
 * @author lakshay.ahuja
 */
var GlobalVar_VolumeLevel = 1.0;
var ignore_timeupdate = false;
var currentTimeAudio = 0;
var audioInterval = null;
var audioTimeOut = null;
var audioIncrTime = 0.001;
var jplayerCurrTime = 0.000;
var isSeeked = null;
$.widget("magic.audioPlayer", $.magic.magicwidget, {
	events : {
		PLAY : 'play',
		PAUSE : 'pause',
		STOP : 'stop',
		RECORD : 'record',
		DELETE : 'delete',
		CLOSE : 'close',
		TOGGLE_HIGHLIGHT : 'toggle_highlight',
		AUDIO_LOAD_COMPLETE : 'audio_load_complete',
		SLIDER_SLIDE_STOP : "SLIDER_SLIDE_STOP",
		PROGRESS_BAR_SLIDING : "PROGRESS_BAR_SLIDING",
		AUDIO_LOAD_ERROR: "AUDIO_LOAD_ERROR",
		AUDIO_ENDED: "AUDIO_ENDED",
		TIME_UPDATED: "TIME_UPDATED"
	},

	options : {
		media : null,
		src : null,
		cls_up : null,
		cls_down : null,
		setEnabled : true,
		currentTime : 0,
		duration : 0,
		audioPlayerPaused: false
	},

	members : {
		seekStartTime : 0,
		audioStopped : false,
		progressBarSlider : "", // Reference to progress bar of the player
		volumeBarSlider : "", //Refernce to the volume slider
		volumeSliderValue : 100
	},
	_create : function() {
		this.htmlElement = {};
	},

	/**
	 * This function eill initialize the progressbar and volume slider.
	 */
	_init : function() {
		var self = this;
		self.members.seekStartTime = 0;
		//$.extend(this.options, dataOptions);
		$.magic.magicwidget.prototype._init.call(this);
		this.htmlElement.media = $(this.element).find('[class=jp-jplayer]');
		self.members.progressBarSlider = $(this.element).find('[id=progressBarSlider]');
		self.members.volumeBarSlider = $(this.element).find('[id=volumeSlider]');

		$(self.members.progressBarSlider).slider({
			//Config
			range : "min",
			min : 1,
			value : 0,
			max : 100,
			//animate: true,
			start : function(event, ui) {
			},

			//Slider Event
			slide : function(event, ui) {//When the slider is sliding
				$(self).trigger(self.events.PAUSE);
				$(self.htmlElement.media).jPlayer("pause");
                //$(self.htmlElement.media).jPlayer("playHead", $(self.members.progressBarSlider).slider('value'));   //commented after discussion with Sumanta
				$(self.members.progressBarSlider).removeClass('ui-disabled');
				$(self.element).find('[class=jp-progress]').removeClass('ui-disabled');
				$(self.element).find('[class=jp-stop]').parent().removeClass('ui-disabled');
				$(self).trigger(self.events.PROGRESS_BAR_SLIDING);
			},

			stop : function(event, ui) {
				if ($(self.members.progressBarSlider).slider('value') == 100) {
					
					$(self.members.progressBarSlider).addClass('ui-disabled');
					$(self.element).find('[class=jp-progress]').addClass('ui-disabled');
					$(self.element).find('[class=jp-stop]').parent().addClass('ui-disabled');
					$(self.htmlElement.media).jPlayer("stop");
					
					$(self).trigger(self.events.AUDIO_ENDED);
				} else {
					$(self.htmlElement.media).jPlayer("playHead", $(self.members.progressBarSlider).slider('value'));
					
					$(self.members.prgressBarSlider).removeClass('ui-disabled');
					$(self.element).find('[class=jp-progress]').removeClass('ui-disabled');
					$(self.element).find('[class=jp-stop]').parent().removeClass('ui-disabled');
					setTimeout(function(){
						$(self).trigger(self.events.SLIDER_SLIDE_STOP);
						if(self.options.audioPlayerPaused == false)
						{
							
								$(self.htmlElement.media).jPlayer("play");	
								$(self.element).find('#jp-pause-parent').css("display", "block");	
							
							
						}
					},200);
				}
			},
		});

		$(self.members.volumeBarSlider).slider({
			orientation : "vertical",
			range : "min",
			min : 0,
			max : 100,
			value : self.members.volumeSliderValue,
			//animate: true,
			slide : function(event, ui) {
				$(self.htmlElement.media).jPlayer("volume", ui.value / 100);
                self.members.volumeSliderValue = ui.value;
			},
			stop : function(event, ui) {
			},
		});

	},

	setOptions : function(dataOptions) {
		this.setMediaSource(dataOptions.src); // Set Media Source
	},

	setPosition : function() {

		$(this.element).css({
			'left' : '101',
			'top' : '7'
		});

	},

	setMediaSource : function(mediaSrc) {
		var self = this;
		var oggMediaSrc = mediaSrc.replace('mp3','ogg');
		$(this).jPlayer("setMedia", {
				mp3 : mediaSrc,
				oga: oggMediaSrc
		});
	},
	/**
	 * This function is called when the play button is clicked. Called from mediaplayer operator.
	 */
	startPlaying : function() {

		var self = this;
		self.members.audioStopped = false;
		var platThroughCnt = 0;
		// this variable is used to stop multiple calls of "canplaythrough" method of jplayer.
		/**
		 * If panel play button is clicked to play audio
		 * Starts here
		 */
		
		$(self.members.progressBarSlider).removeClass('ui-disabled');
		$(self.element).parent().find('[id=jp-progress]').removeClass('ui-disabled');
		$(self.element).parent().find('[class=jp-stop]').parent().removeClass('ui-disabled');
		$(self.htmlElement.media).jPlayer("play");

		/**
		 * Ends here
		 */

	},

	/**
	 * This player is called when we have to stop the play event.
	 */
	stopPlaying : function() {
		var self = this;
        $(self.members.progressBarSlider).slider('value', 0);
		$(self.htmlElement.media).jPlayer("clearMedia");
		$(self.htmlElement.media).jPlayer("destroy");
		$(self.htmlElement.media).jPlayer("stop");
		$(self.members.volumeBarSlider).slider('value', self.members.volumeSliderValue);
		
		self.options.currentTime = 0;
	},
    
	/**
	 * To display the audio panel
	 */
	displayAudioPanel : function() {
		this.hidingQAPAnel();
        $($(this.element).parent()).css('display', 'block');
        
	},
    
    hidingQAPAnel: function(){
        if($("#questionAnswerPanel").css('display') == "block"){
            $("#questionPanelCloseBtn").trigger('click');
        }
        
    },

//questionPanelCloseBtn
	/**
	 * To set the media file associated with the audio hotspot.
	 * @param {Object} mediaSrc : the media file the player will play.
	 * @param {float/Number} fStartDuration: the duration from where the audio has to start
	 */
	setContent : function(mediaSrc, fStartDuration) {
		var self = this;
		clearInterval(audioInterval);
		clearTimeout(audioTimeOut);
		audioIncrTime= 0;
		$(self.htmlElement.media).jPlayer("destroy");
		$(self.members.volumeBarSlider).slider('value', self.members.volumeSliderValue);
		if (mediaSrc == undefined) {
			var mediaSrc = GlobalModel.audioSrc;
		}
		
		var oggMediaSrc = mediaSrc.replace('mp3','ogg');
		var srcArr = mediaSrc.split("/");
		var src = srcArr[srcArr.length-1];
		var _objAudio = {};
		_objAudio.type = AppConst.AUDIO_PLAY;
		_objAudio.src = src;
		//AnalyticsManager.setAudioData(EPubConfig.Identifier, src);
		$(document).trigger( AppConst.SEND_TRACKING_STATEMENT, [_objAudio] );
		
		// .mp3 format of audio files.
		$(this.htmlElement.media).jPlayer({
			ready : function() {
				$(self).trigger(self.events.AUDIO_LOAD_COMPLETE)
				/*
				 * To fixed Auto play issue with firefox made a seprate call first set media then call a play function  
				 */

				if(navigator.userAgent.indexOf("Firefox") != -1) {
					$(this).jPlayer("setMedia", {
						mp3 : mediaSrc,
						oga: oggMediaSrc
					}); // auto play issue with firefox
				}
				else{
					$(this).jPlayer("setMedia", {
						mp3 : mediaSrc,
						oga: oggMediaSrc
					}).jPlayer("play", fStartDuration); // auto play
				}
				if(navigator.userAgent.indexOf("Firefox") != -1) {
					$(self.htmlElement.media).jPlayer("play","0");
				}
			},

			play : function() {
			},
			
			error: function(e)
			{
				if(e.jPlayer.error.message == $.jPlayer.errorMsg.URL)
					$(self).trigger(self.events.AUDIO_LOAD_ERROR);
			},
			
			progressbarclicked: function()
			{
				$(self).trigger(self.events.PAUSE);
				$(self.htmlElement.media).jPlayer("pause");
				setTimeout(function() {
					$(self).trigger(self.events.SLIDER_SLIDE_STOP);
					if(self.options.audioPlayerPaused == false)
					{
						$(self.htmlElement.media).jPlayer("play");
					}
				}, 200);
			},
			timeupdate : function(event) {
                jplayerCurrTime = event.jPlayer.status.currentTime;
                if(audioInterval)
				{
					clearInterval(audioInterval);
				}
				if(audioTimeOut)
				{
					clearTimeout(audioTimeOut);
				}	
				$(self).trigger(self.events.TIME_UPDATED);
				var ptime = event.jPlayer.status.currentTime;
				var ctime = self.options.currentTime;
				
				if(ctime!=0)
				{
					audioIncrTime = ((ptime - ctime) / ctime)/1000 + audioIncrTime;
					
					if(audioIncrTime<0)
					{
						audioIncrTime = audioIncrTime*(-1);
					}
					
				}
				
				
				self.options.currentTime = event.jPlayer.status.currentTime;
				self.options.duration = event.jPlayer.status.duration;
                /*
                 * event.jPlayer.status.currentPercentAbsolute returns 0 sometimes in iOS. Thus value of progressbar will not update in this case if we use event.jPlayer.status.currentPercentAbsolute. Hence, using percentage value of jp=playbar to update slider value.
                 */
                
                var jpPlayBar = $(self.element).find('[class=jp-play-bar]');
                var sliderValue = ($(jpPlayBar).width()/$(jpPlayBar).parent().width()) * 100;
                if(sliderValue || sliderValue == 0){
                    $(self.members.progressBarSlider).slider("value", sliderValue);
                }
				
				currentTimeAudio = event.jPlayer.status.currentPercentAbsolute;
				
				audioInterval = setInterval(function(){
					self.options.currentTime += audioIncrTime;
					$(self).trigger(self.events.TIME_UPDATED);
				},1);
				audioTimeOut = setTimeout(function(){
					clearInterval(audioInterval);
				},100);
				//$(self).trigger(self.events.TIME_UPDATED);
			},
			volumechange : function(event) {
				if (false) {
					$(self.members.volumeBarSlider).slider("value", 0);
				} else {
                    if(event.jPlayer.status.volume == 0){
                        $(self.element).find('[class=jp-unmute]').css('opacity',0.4);
                    }
                    else{
                        $(self.element).find('[class=jp-unmute]').css('opacity',1);
                    }
					$(self.members.volumeBarSlider).slider("value", event.jPlayer.status.volume * 100);
					self.members.volumeSliderValue = event.jPlayer.status.volume*100; // to retain the state of the volume bar when page changes
				}
			},
			ended : function() {
				self.options.currentTime = 0;
				clearInterval(audioInterval);
				clearTimeout(audioTimeOut);
				audioIncrTime= 0;
				$(self.members.progressBarSlider).slider('value', 0);
				$(self.htmlElement.media).jPlayer('stop');
				$(self.element).parent().find('[class=jp-stop]').parent().addClass('ui-disabled');
				$(self.members.progressBarSlider).addClass('ui-disabled');
				$(self.element).find('[class=jp-progress]').addClass('ui-disabled');
                $(self.element).find('#jp-pause-parent').css('display','none');
                $(self).trigger(self.events.AUDIO_ENDED);
			},
			pause : function(event) {
			},
			swfPath : "js/widgets/audioPlayer/component",
			supplied : "oga,mp3",
			volume : (self.members.volumeSliderValue/100),
		}).bind($.jPlayer.event.play, function() {// pause other instances of player when current one play
			$(self.htmlElement.media).jPlayer("pauseOthers");
		});
	},
	
	toTimeFormat: function(ms) {
		sec_numb    = parseInt(ms);
	    var hours   = Math.floor(sec_numb / 3600);
	    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
	    var seconds = sec_numb - (hours * 3600) - (minutes * 60);
	
	    if (hours   < 10) {hours   = "0"+hours;}
	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}
	    if(hours > 0) {
	    	var time    = hours+':'+minutes+':'+seconds;
	    } else {
	    	var time    = minutes+':'+seconds;
	    }
	    return time;

	},

	reset : function() {
		$(this.htmlElement.media).jPlayer("destroy");

	},
	showPanel : function() {
		$(this.element).show();
	},

	hidePanel : function() {
		$(this.element).hide();
	},
	
	/**
	 * This functions returns the current seek time
 	 * @param none
 	 * @return {Number} currentTime
	 */
	getCurrentPosition : function() {
		//var currentTime = $(this.members.progressBarSlider).slider('value') * this.options.duration / 100;
		return this.options.currentTime;
	},

	/**
	 * This functions seeks the audio to specified position
 	 * @param {Number} fPosition
 	 * @return void
	 */
	seekToPosition : function(fPosition) {
		var nSeekPercent = fPosition / this.options.duration * 100;
		$(this.htmlElement.media).jPlayer("pause");
		$(this.htmlElement.media).jPlayer("playHead", nSeekPercent);
	},
	
	/**
	 * This functions pauses the currently playing audio.
 	 * @param none
 	 * @return void
	 */
	pause: function()
	{
		self.options.currentTime = jplayerCurrTime;
		audioIncrTime = 0.001;
		$(this.htmlElement.media).jPlayer("pause");
	},
	
	/**
	 * This functions clears the media and resets current time.
 	 * @param none
 	 * @return void
	 */
	resetCurrentTime: function()
	{
		this.options.currentTime = 0;
		$(this.members.progressBarSlider).slider("value", 0);
	},
	
	/**
	 * This functions sets whether audio player is paused or not. Setting this externally because pause state has to be determined by the button click of the player.
 	 * @param {Boolean} bPaused
	 */
	setAudioPlayerPaused: function(bPaused)
	{
		this.options.audioPlayerPaused = bPaused;
	}
});
