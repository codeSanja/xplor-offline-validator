/**
 * @author sumanta.mishra
 * Initialize when ready.
 * Invoke the content showroom role.
 * Set initial variables.
 */
var currentReaderVersion = "v1.47";
var click = { x: 0, y: 0 };
var _objQueryParams = {};
var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
var lastVisitedPage;
var isWinRT = false;
var WinRTPointerEventType = "";
var WinRTPointerEventType_Up = "";
var WinRTPointerType = "";

var nCSSLoadCounter = 0;
var nCSSLoadedCounter = 0;
var isLayoutAdded = false;
var isContentCSSLoaded = false;
var isAppInitialized = false;

var isToLoadReaderSettingsFile = true;

var _bUseOnDemandLoader = true;
//true will use onDemandLoader utility to load files asynchronously while false will load the files synchronously using locally written method
var preloaderPath = "";

var _iTaskNumber = 0;
var isNative;
var isNativePublisher;
var topNavigationBarHeight = 0;
//this variable contain the tasks in a sequential manner
var arrTaskSequence = [AppConst.PARSE_QUERY_STRING,
    AppConst.LOAD_EPUB_GLOABL_SETTINGS,
    AppConst.INITIALIZE_APP
];

var arrTasksBeforeInitialization = [
    AppConst.LOAD_APP_FEATURES,
    AppConst.LOAD_LOCALIZATION_DATA,
    AppConst.LOAD_CSS,
    AppConst.LOAD_TOC_XHTML,
    AppConst.LOAD_LAYOUT_HTML
];

$(function() {

    //console.log("strReaderPath: " + strReaderPath);
    if (strReaderPath != undefined) {
        preloaderPath = strReaderPath + "css/6TO12/images/main/Preloader_circles.gif";
        //console.log("preloaderPath: " + preloaderPath);
        $('<img/>').attr('src', preloaderPath);
    }
    //log( navigator.userAgent.match(/Touch/), "navigator.userAgent");
    if( window.navigator.msPointerEnabled && navigator.userAgent.match(/Touch/) != null )
    {
        if( window.MSPointerEvent )
        {
            WinRTPointerEventType = "MSPointerDown";
            WinRTPointerEventType_Up = "MSPointerUp";
        }
        else
        {
            WinRTPointerEventType = "pointerdown";
            WinRTPointerEventType_Up = "pointerup";
        }
        isWinRT = true;
    }

    //$($('head').find('[href*="jquery.mobile-1.0.1.min.css"]')[0]).remove();
    $($('head').find('[href*="jquery.mobile.simpledialog.css"]')[0]).remove();


    startTask();
});

/**
 * This function starts a given task (through _iTaskNumber)
 * @param none
 * @return void
 */
function startTask() {
    var strTask = arrTaskSequence[_iTaskNumber];
	
    switch(strTask) {
        case AppConst.PARSE_QUERY_STRING:
            parseQueryString();
            break;

        case AppConst.LOAD_EPUB_GLOABL_SETTINGS:
            loadEPubGlobalSettings();
            break;

        case AppConst.INITIALIZE_APP:
            if(GlobalModel.isLoginPopup) {
                $("#appPreloaderContainer").css("display","none");
                showDPerror();
            }
            else
            {
                storeUserDetails();
                //getUserRole();
                $("#mainShowroom").css("visibility", "visible");
                $("#mainShowroom").css("opacity", "0.3");
                $( "#mainShowroom" ).addClass( "shellFadeInEffect" );
                //console.log("$('.ui-loader'): ", $('.ui-loader'));
                $('.ui-loader').remove();
            }
            break;
    }
}

/**
 * This function is called at the end of a task
 * @param none
 * @return void
 */
function taskComplete() {
    _iTaskNumber++;
	
    //if all tasks are not complete, start the next task;
    if (_iTaskNumber < arrTaskSequence.length) {
        startTask()
    }
}

function parseQueryString() {
    var parts = window.location.search.substr(1).split("&");
    for (var i = 0; i < parts.length; i++) {
        if(parts[i] != ""){
            var queryParamAndValue = parts[i].split("=");
            var parameter = "";
            //value at index 0 represents the query parameter and at index 1 represents the value for the parameter;
            if(queryParamAndValue.length > 2){
                parameter = parts[i].substring(parseInt(parts[i].indexOf("=")) + 1, parts[i].length);
                _objQueryParams[queryParamAndValue[0].toLowerCase()] = parameter;
            }
            else{
                parameter = queryParamAndValue[1];
                _objQueryParams[queryParamAndValue[0].toLowerCase()] = queryParamAndValue[1];
            }

        }
    }
    if(_objQueryParams.booktitle)
        EPubConfig.bookTitle = _objQueryParams.booktitle;



    if (_objQueryParams.printpage){
        EPubConfig.Print_isAvailable = true;
    }

    if (_objQueryParams.grade) {
        if (AppConst["GRADE_" + _objQueryParams.grade.toUpperCase()] == null) {
            alert("Invalid Grade. Kindly enter correct grade")
            throw new Error("Invalid Grade");
        }

        //ensuring that value of grade is in required case;
        _objQueryParams.grade = AppConst["GRADE_" + _objQueryParams.grade.toUpperCase()];

    }
	
    taskComplete();
}



/**
 * This function loads the Global Settings file available in ePub
 * @param none
 * @return void
 */
function loadEPubGlobalSettings() {

    var url = getPathManager().getEPubGlobalSettingsPath();
    var isIE8or9;
	var ver = getInternetExplorerVersion();
	if (ver > -1) {
		if (ver <= 9.0) {
			isIE8or9 = true;
		}
	}
    var processData = function(objConfigSettings) {
        //mapping properties in Global Settings file to properties in Config.js
        for (var i = 0; i < readerPropertyList.length; i++) {
            var objPropertyInfo = readerPropertyList[i];

            //check if data exists or not. If it doesn't, check if it was mandatory. If data is mandatory, and property does not exists, throw an error;
            if (objConfigSettings[objPropertyInfo.propertyName] != null) {
                EPubConfig[objPropertyInfo.propertyName] = objConfigSettings[objPropertyInfo.propertyName];
            } else if (objPropertyInfo.isMandatory) {
                throw new Error("Mandatory Configuration Property (" + objPropertyInfo.propertyName + ") is missing, null or undefined in Global Settings File")
            }
        }

        ServiceManager.searchService = EPubConfig.Search_Service_URL;

        if(ismobile)
        {
            EPubConfig.KeyboardAccessibility_isAvailable = false;
        }
        if(_objQueryParams.viewtype)
            EPubConfig.Page_Navigation_Type = AppConst[_objQueryParams.viewtype.toUpperCase()];

        if (_objQueryParams.theme) {
            EPubConfig.Theme = _objQueryParams.theme;
        }

        if (_objQueryParams.componenttype){
            EPubConfig.Component_type = _objQueryParams.componenttype.toUpperCase();
        }

        if(EPubConfig.DPLive != undefined)
        {
            if(EPubConfig.DPLive == false)
            {
                GlobalModel.DPLive = EPubConfig.DPLive;
            }
        }

        if (EPubConfig.Title) {
            $('title').html(EPubConfig.Title);
        }
        if(EPubConfig.HTML5_audio){
            EPubConfig.AudioPlayer_isAvailable = true;
        }
        else
        {
            EPubConfig.AudioPlayer_isAvailable = false;
        }

        if(_objQueryParams.grade)
            EPubConfig.ReaderType = _objQueryParams.grade;
            
        if( _objQueryParams.addcomments )
        {
            GlobalModel.AddComment = true;
        }
        
        if(ismobile || isIE8or9)
        {
            EPubConfig.Print_isAvailable = false;
        }

        //Setting the Default HELP URL if help type is Internal
        if(EPubConfig.Help_Type == "Internal"){
            if(EPubConfig.ReaderType.toUpperCase() == "6TO12"){
                EPubConfig.Help_URL = getPathManager().appendReaderPath( 'content/pdf/eReader_G6-12_JobAid_08.12.14.pdf' );
            }
            else{
                EPubConfig.Help_URL = getPathManager().appendReaderPath( 'content/pdf/eReader_G2-5_JobAid_08.12.14.pdf' );
            }
        }
        if(EPubConfig.Volume == ""){
            EPubConfig.Volume = -1;
        }
		
		if(EPubConfig.ReaderType.toUpperCase() != "6TO12")
		{
			EPubConfig.AnnotationSharing_isAvailable = false;
		}
		if(EPubConfig.ReaderType.toUpperCase() == "6TO12")
		{
			topNavigationBarHeight = 50;
		}

	    if( EPubConfig.Notes_isAvailable == false )
	    {
	    	EPubConfig.AnnotationSharing_isAvailable = false;
	    }
	    
	    if( ( ServiceManager.getUserRole() == AppConst.USERTYPE_STUDENT ) && ( EPubConfig.Component_type == "TE" ) )
	    {
	    	EPubConfig.AnnotationSharing_isAvailable = false;
	    }
        configSettingsParseCompleteHandler();

        //taskComplete();
    }
    
    try {
        GlobalModel.ePubGlobalSettings = objConfig;
        //processData(objConfig["Settings"])
		
		if (_objQueryParams.tshare == undefined && isToLoadReaderSettingsFile == true) {
			$.getJSON(getPathManager().getReaderSettingsFilePath(), function(objData){
				EPubConfig.AnnotationSharing_isAvailable = objData.AnnotationSharing_isAvailable;
				processData(objConfig["Settings"]);
			}).error(function(){
				processData(objConfig["Settings"]);
			});
		}
		else
		{
			if(_objQueryParams.tshare != undefined)
			{
				if(_objQueryParams.tshare == "true")
				{
					EPubConfig.AnnotationSharing_isAvailable = true;
				}
				else
				{
					EPubConfig.AnnotationSharing_isAvailable = false;
				}
			}
			
			processData(objConfig["Settings"]);
		}
    } catch(e) {
    	
        $.getJSON(url, function(objData) {
            GlobalModel.ePubGlobalSettings = objData;

            var objConfigSettings = objData["Settings"];

            //LOADS settings.txt FILE FOR CONFIGURATION SETTINGS OF READER
			if (_objQueryParams.tshare == undefined && isToLoadReaderSettingsFile == true) {
				$.getJSON(getPathManager().getReaderSettingsFilePath(), function(objData){
					EPubConfig.AnnotationSharing_isAvailable = objData.AnnotationSharing_isAvailable;
					processData(objConfigSettings);
				}).error(function(){
					processData(objConfigSettings);
				});
			}
			else
			{
				//EPubConfig.AnnotationSharing_isAvailable = (_objQueryParams.tshare != undefined) ? _objQueryParams.tshare : true;
				if(_objQueryParams.tshare != undefined)
				{
					if(_objQueryParams.tshare == "true")
					{
						EPubConfig.AnnotationSharing_isAvailable = true;
					}
					else
					{
						EPubConfig.AnnotationSharing_isAvailable = false;
					}
				}
				
				processData(objConfigSettings);
			}
        }).error(function() {
            alert("Unable to load book")
            console.error("Global Settings file not found")
        });
    }
}

function configSettingsParseCompleteHandler(){
    //ensuring that all properties are in required format;
    //areAllPropertiesInRequiredFormat();
    ServiceManager.DPLive = ServiceManager.isDPLive()//GlobalModel.DPLive,
    setPageViewType();

    loadContentCSSFiles();
    loadThemeCSS();
    loadLayoutXHTMLFile();
    loadLocalizationData();
    loadTocXHTMLFile();
    loadApplicationAndCoreJSFiles();
}


function setPageViewType() {
    var _isDefaultViewVisible = true;

    if (!EPubConfig.SinglePageView_isAvailable && !EPubConfig.ScrollPageView_isAvailable && !EPubConfig.DoublePageView_isAvailable) {
        // do nothing
    } else {
        _isDefaultViewVisible = checkifDefaultViewisVisible();
        if (_isDefaultViewVisible == false) {
            if (EPubConfig.SinglePageView_isAvailable) {
                EPubConfig.Page_Navigation_Type = AppConst.SINGLE_PAGE;
            } else if (EPubConfig.ScrollPageView_isAvailable) {
                EPubConfig.Page_Navigation_Type = AppConst.SCROLL_VIEW;
            } else if (EPubConfig.DoublePageView_isAvailable) {
                EPubConfig.Page_Navigation_Type = AppConst.DOUBLE_PAGE;
            }
        }
    }

    switch(EPubConfig.Page_Navigation_Type) {
        case AppConst.SCROLL_VIEW:
            EPubConfig.pageView = AppConst.SCROLL_VIEW_CAMEL_CASING;
            //	EPubConfig.swipeNavEnabled = false;
            break;

        case AppConst.SINGLE_PAGE:
            EPubConfig.pageView = AppConst.SINGLE_PAGE_CAMEL_CASING;
            //EPubConfig.swipeNavEnabled = true;
            break;

        case AppConst.DOUBLE_PAGE:
            EPubConfig.pageView = AppConst.DOUBLE_PAGE_CAMEL_CASING;
            //EPubConfig.swipeNavEnabled = true;
            break;

        default:
            throw new Error("Value of config property Page_Navigation_Type is not either " + AppConst.SCROLL_VIEW + " or " + AppConst.SINGLE_PAGE + " or " + AppConst.DOUBLE_PAGE)
            break;
    }
}


function loadContentCSSFiles(){
    var objThis = this;
    var url = getPathManager().getEPubCSSPath(EPubConfig.cssFileNames[nCSSLoadCounter]);
    //console.log("main::::",url)
    var hasPageLevelCss = false;

    $("<iframe/>").attr("src", url);

    nCSSLoadCounter++;
    if (nCSSLoadCounter < EPubConfig.cssFileNames.length) {
        loadContentCSSFiles();
    }
}

/**
 * This function loads the css of application based on set/selected theme
 * @param none
 * @return void
 */
function loadThemeCSS() {
    var url = ""
    var strHoverURL = "";
    /* */

    if (EPubConfig.Theme.toLowerCase() == "default" || EPubConfig.Theme.toLowerCase() == "6to12" || EPubConfig.Theme.toLowerCase() == "2to5" || EPubConfig.Theme.toLowerCase() == "prekto1") {
        EPubConfig.Theme = EPubConfig.ReaderType;
        url = getPathManager().appendReaderPath("css/" + EPubConfig.Theme + "/mg.theme.css");
        strHoverURL = getPathManager().appendReaderPath("css/" + EPubConfig.Theme + "/mg.theme.hover.css");
    }


    $.ajax({
        type : "GET",
        url : url,
        processData : false,
        dataType:"text",
        contentType : "plain/text",

        success : function(data) {
            appendCSSContent(data);
            preInitializationTaskCompleteHandler(AppConst.LOAD_CSS);
            loadThemeHoverCSS(strHoverURL);

        },

        error : function() {
            console.error("Error in loading requested theme.")
        }
    });
}

/**
 * This function loads the hover CSS of application based on set/selected theme if user is using desktop environment else it calls the task complete function.
 * @param none
 * @return void
 */
function loadThemeHoverCSS(strHoverURL) {
    if (ismobile == null) {
        var url = strHoverURL;
        //getPathManager().appendReaderPath("css/" + EPubConfig.Theme + "/mg.theme.hover.css");
        $.ajax({
            type : "GET",
            url : url,
            processData : false,
            dataType:"text",
            contentType : "plain/text",
            success : function(data) {
                appendCSSContent(data);
                //taskComplete();
            },
            error : function() {
                //in case hover states are not available, build can still run...
                //taskComplete();
            }
        })
    } else {
        //taskComplete();
    }
}

/**
 * This function adds the loaded css to the application
 * @param none
 * @return void
 */
function appendCSSContent(strCSS) {

    objReg = /url\(css/g;
    strCSS = strCSS.replace(objReg, "url(" + strReaderPath + "css");

    objReg = /url\(\"/g;
    strCSS = strCSS.replace(objReg, "url(\"" + strReaderPath);

    objReg = /url\(\'/g;
    strCSS = strCSS.replace(objReg, "url('" + strReaderPath);

    var scrScript = '<style type="text/css">' + strCSS + "</style>";
    $('head').append(scrScript);
}

function loadLayoutXHTMLFile()
{
    var type = "GET";
    var url = getPathManager().getLayoutFilePath();
    //console.log("loading layout html");
    $.ajax({
        url: url,
        type: type,
        dataType: "html",
        // Complete callback (responseText is used internally)
        complete: function(jqXHR, status, responseText){
            //console.log("** load complete of layout xhtml");
            // Store the response as specified by the jqXHR object
            responseText = jqXHR.responseText;
            // If successful, inject the HTML into all the matched elements
            if (jqXHR.isResolved()) {
                // #4825: Get the actual response in case
                // a dataFilter is present in ajaxSettings
                jqXHR.done(function(r){
                    responseText = r;
                });
                responseText = getPathManager().replaceMediaURLs(responseText);
                //console.log(responseText);

                objReg = /url\(\"/g;
                responseText = responseText.replace(objReg, "url(\"" + strReaderPath);

                objReg = /url\(\'/g;
                responseText = responseText.replace(objReg, "url('" + strReaderPath);
                // See if a selector was specified

                GlobalModel.layoutHTMLData = responseText;
                preInitializationTaskCompleteHandler(AppConst.LOAD_LAYOUT_HTML);
            }
        }
    });
}


/**
 * This function loads the localized data based on the configuration parameter
 * @param none
 * @return void
 */
function loadLocalizationData() {
    EPubConfig.LocalizationLanguage = EPubConfig.LocalizationLanguage.toLowerCase();
    EPubConfig.defaultLocalizationLanguage = EPubConfig.defaultLocalizationLanguage.toLowerCase();
    //alert(EPubConfig.ReaderType);
    var strReaderType = EPubConfig.ReaderType;
    //alert(strReaderType);

    var strLocalizationFilePath = getPathManager().appendReaderPath('content/localization/' + strReaderType + "/"+ EPubConfig.LocalizationLanguage + '.txt');

    $.getJSON(strLocalizationFilePath, function(objData) {

        GlobalModel.localizationData = objData;
		if(EPubConfig.StandardsTitle != undefined)
		{
			GlobalModel.localizationData.COMMON_CORE_STANDARDS = EPubConfig.StandardsTitle;
		}
		preInitializationTaskCompleteHandler(AppConst.LOAD_LOCALIZATION_DATA);
    }).error(function(e) {

        //in case set localization language which failed to load is not the default localization language, then load the default default localization language.
        if (EPubConfig.LocalizationLanguage != EPubConfig.defaultLocalizationLanguage) {
            console.log("Unable to locate the required loacalization data. Loading default Localization data")
            EPubConfig.LocalizationLanguage = EPubConfig.defaultLocalizationLanguage;
            loadLocalizationData();
        } else {
            console.error("Localization file(s) missing")
        }
    });
}

function loadTocXHTMLFile()
{
    var strURL = getPathManager().getTOCPath();
    //console.log("loading TOC xhtml");
    $.ajax({
        type:"GET",
        url:strURL,
        processData: false,
        contentType: "plain/text",
        success: function(data)
        {
            //console.log("** load complete of TOC xhtml");
            try
            {
                GlobalModel.tocXHTMLData = data;
                preInitializationTaskCompleteHandler(AppConst.LOAD_TOC_XHTML);
            }
            catch(e)
            {
                console.error(e.message)
            }
        },
        error: function()
        {
            console.error("TOC ERROR: TOC failed to load.")
        }
    })
}

/**
 * This function loads the main application files and theme/widget specific files.
 * @param none
 * @return void
 */
function loadApplicationAndCoreJSFiles() {
    var arrFilesToLoad = [];
    var strExt = ".js";

    var coreFilesLoadCompleteHandler = function(){
        loadAppFeatures();
    }

    try {

        if(strReaderPath != "")
        {
            {
                LayoutConfig = {};

                LayoutConfig.READER_TYPE_6TO12 = {CORE: ["js/widgets/core_min"]};
                LayoutConfig.READER_TYPE_2TO5 = {CORE: ["js/widgets2TO5/core_min"]};
                LayoutConfig.READER_TYPE_PREKTO1 = {CORE: ["js/widgetsPREKTO1/core_min"]};
            }
        }

        if (LayoutConfig["READER_TYPE_" + EPubConfig.ReaderType] == null) {
            throw new Error("Invalid Reader Type");
        }
        //adding to list the widget specific files
        var arrCoreFiles = LayoutConfig["READER_TYPE_" + EPubConfig.ReaderType].CORE;
        //console.log("Main.js: 212: loadApplicationAndCoreJSFiles start");
        var nTotalFileLoadCounter = 0;
        var strFilePath = "";
        var fileLoadFailed = false;
        for ( i = 0; i < arrCoreFiles.length; i++) {
            strFilePath = getPathManager().appendReaderPath(arrCoreFiles[i] +  strExt);

            arrFilesToLoad.push(strFilePath);
            var scriptTag = document.createElement('script');
            scriptTag.onload = function(){

                nTotalFileLoadCounter++;
                if(nTotalFileLoadCounter == arrCoreFiles.length)
                {
                    if(fileLoadFailed)
                    {
                        alert("Unable to load some Javascript files.");
                    }
                    else
                    {
                        coreFilesLoadCompleteHandler();
                    }

                }
            };
            scriptTag.onerror = function(){
                fileLoadFailed =  true;
                nTotalFileLoadCounter++;
                if(nTotalFileLoadCounter == arrCoreFiles.length)
                {
                    alert("Unable to load some Javascript files.");
                }
            };
            scriptTag.setAttribute('src', strFilePath);
            document.head.appendChild(scriptTag);
        }
    } catch(e) {
        console.error(e.message);
    }
}




/**
 * This function loads the JS files of required features at runtime.
 * @param none
 * @return void
 */
function loadAppFeatures() {
    createRequiredFeaturesList();

    if (EPubConfig.RequiredApplicationComponents == null) {
        EPubConfig.RequiredApplicationComponents = [];
    }

    preInitializationTaskCompleteHandler(AppConst.LOAD_APP_FEATURES);

}

function checkifDefaultViewisVisible() {
    if (EPubConfig.Page_Navigation_Type == AppConst.SINGLE_PAGE) {
        if (EPubConfig.SinglePageView_isAvailable) {
            return true;
        } else {
            return false;
        }
    } else if (EPubConfig.Page_Navigation_Type == AppConst.SCROLL_VIEW) {
        if (EPubConfig.ScrollPageView_isAvailable) {
            return true;
        } else {
            return false;
        }
    } else if (EPubConfig.Page_Navigation_Type == AppConst.DOUBLE_PAGE) {
        if (EPubConfig.DoublePageView_isAvailable) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * This function creates required features list based on settings by HMH
 * @param none
 * @return void
 */
function createRequiredFeaturesList() {
    var strFeature = "";

    //if annotations are set to false, override isAvailable property of all annotation tools and set them to false;
    //if (EPubConfig.Markup_Tool_isAvailable
    if (EPubConfig.Markup_Tool_isAvailable == false) {
        for (strFeature in EPubConfig.AnnoToolNames) {
            var strIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strFeature];

            if (strIsAvailableProperty) {
                EPubConfig[strIsAvailableProperty] = false;
            }
        }
    } else {
        var iFalse = 0;
        var iTotal = 0;
        for (strFeature in EPubConfig.AnnoToolNames) {

            var strIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strFeature];
            if (strIsAvailableProperty) {
                iTotal++;
                if (EPubConfig[strIsAvailableProperty] == false) {
                    iFalse++;
                }
            }
        }

        if (iFalse == iTotal) {
            EPubConfig.Markup_Tool_isAvailable = false;
        }
    }

    //adding required tools to RequiredApplicationComponents property for further processing;
    EPubConfig.RequiredApplicationComponents = (EPubConfig.RequiredApplicationComponents == null) ? [] : EPubConfig.RequiredApplicationComponents;

    //creating tabbed panel required list
    for (strPanelType in EPubConfig.TabbedPanels) {
        var objPanelSubPanels = EPubConfig.TabbedPanels[strPanelType];
        var isPanelAvailable = false;

        for (strFeature in objPanelSubPanels) {
            var strIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strFeature];

            //if any of the sub-panels is available, make the master panel available
            if (EPubConfig[strIsAvailableProperty] == true) {
                isPanelAvailable = true;
                break;
            }
        }

        var strPanelsIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strPanelType];
        EPubConfig[strPanelsIsAvailableProperty] = isPanelAvailable;
    }

    for (strFeature in EPubConfig.Feature_isAvaliblePropertyMapping) {
        var strIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strFeature];

        if (EPubConfig[strIsAvailableProperty] == true) {
            EPubConfig.RequiredApplicationComponents.push(strFeature);
        }
    }
}


function preInitializationTaskCompleteHandler(strTaskName)
{
    var iIndex = arrTasksBeforeInitialization.indexOf(strTaskName);
    if(iIndex > -1)
    {
        arrTasksBeforeInitialization.splice(iIndex, 1);
    }
    if(arrTasksBeforeInitialization.indexOf(AppConst.LOAD_CSS) == -1 && arrTasksBeforeInitialization.indexOf(AppConst.LOAD_LAYOUT_HTML) == -1)
    {
        if(isLayoutAdded == false)
        {
            $("#mainShowroom").append(GlobalModel.layoutHTMLData);
            isLayoutAdded = true;

            MainController.updateBrandingBar();
        }
    }
    //console.log(strTaskName, iIndex, arrTasksBeforeInitialization);
    if(arrTasksBeforeInitialization.length == 0)
    {
        console.log("initialization start------------------------------------");
        taskComplete();
        ServiceManager.initializeAnnotationServiceAPI();

    }
}

/**
 * This method check if all isAvailable properties hold a boolean value. If not, it throws an error.
 * @param	none
 * @return	void
 */
function areAllPropertiesInRequiredFormat() {
    var arrInvalidProperties = [];

    for (var i = 0; i < readerPropertyList.length; i++) {
        var objPropertyInfo = readerPropertyList[i];
        if (EPubConfig[objPropertyInfo.propertyName]) {
            switch(objPropertyInfo.type) {
                case "integer":
                    if(isInteger(EPubConfig[objPropertyInfo.propertyName]) == false)
                        arrInvalidProperties.push({
                            propertyName : objPropertyInfo.propertyName,
                            type : objPropertyInfo.type
                        });
                    break;

                case "number":
                    if(isNumber(EPubConfig[objPropertyInfo.propertyName]) == false)
                        arrInvalidProperties.push({
                            propertyName : objPropertyInfo.propertyName,
                            type : objPropertyInfo.type
                        });
                    break;

                case "string":
                    if((typeof EPubConfig[objPropertyInfo.propertyName] === "string") == false)
                        arrInvalidProperties.push({
                            propertyName : objPropertyInfo.propertyName,
                            type : objPropertyInfo.type
                        });
                    break;

                case "array":
                    if(isArray(EPubConfig[objPropertyInfo.propertyName]) == false)
                        arrInvalidProperties.push({
                            propertyName : objPropertyInfo.propertyName,
                            type : objPropertyInfo.type
                        });
                    break;

                case "boolean":
                    if((typeof EPubConfig[objPropertyInfo.propertyName] === "boolean") == false)
                        arrInvalidProperties.push({
                            propertyName : objPropertyInfo.propertyName,
                            type : objPropertyInfo.type
                        });
            }
        }
    }

    if (arrInvalidProperties.length > 0) {
        var strMessage = "";
        for (i = 0; i < arrInvalidProperties.length; i++) {
            var aOrAn = (arrInvalidProperties[i].type == "integer" || arrInvalidProperties[i].type == 'array') ? "an " : "a ";
            aOrAn = (arrInvalidProperties[i].type == "boolean") ? "" : aOrAn;

            strMessage += (i + 1) + ". " + arrInvalidProperties[i].propertyName + " is not " + aOrAn  + arrInvalidProperties[i].type + "\n"
        }

        var strMessage1 = (arrInvalidProperties.length == 1) ? "property" : "properties"

        throw new Error("Data type of following " + strMessage1 + " is incorrect\n\n" + strMessage)
    }
}

function initAppFlow()
{
    //return;
    AppConst.ORIENTATION = "landscape";

    OperatorManager.initializeAllOperators($("#mainShowroom"));

    var strOrientFunc = "onresize";
    var userAgent = navigator.userAgent;
    if (ismobile) {//if running on device.
        var strOrientFunc = "onorientationchange";
    }
    isAppInitialized = true;
    window[strOrientFunc] = function() {
        click = { x: 0, y: 0 };
        /*if(document.activeElement)
         document.activeElement.blur();*/
        updateShowroom("1");

        //adding a timeout to ensure that resize or orientation change related changes take effect.
        setTimeout(function() {
            updateShowroom("2");
        }, 500);
    };

    //$.keepalive.configure( { interval : 840000, url: pingUrl, method: "GET" } );
    var keepAliveErrorHandler = function()
    {
        //$.keepalive.stop();
    }
    try {
        $.keepalive.configure({
            interval: 840000,
            url: EPubConfig.DP_KEEP_LIVE_URL,
            method: "GET",
            errorCallback: keepAliveErrorHandler
        });
    } catch(e) {
        console.warn("WARNING: Error in Keep alive call request!");

    }
    updateShowroom();

    HotkeyManager.init();
}

function updateShowroom(i) {
    if ($(window).height() != window.innerHeight) {
        height = $(window).height() - ($(window).height()-window.innerHeight);
    }
    else {
        height = $(window).height();
    }
    width = $(window).width();
    /*

     $("body").css("width", width);
     $("body").css("height", height);

     $("#pg").css({"width": width,"height": height, "margin-top": "0px", "top": "0px"});

     $("#mainShowroom").css("width", width);
     $("#mainShowroom").css("height", height);*/
}

function showDPerror(){
    $("body").append('<div id="loginAlertPanel" class="deleteConfirmationPanel" style="padding-top:24px;padding-bottom: 21px;"><div class="warningIcon"></div><div id="loginAlertTxt" class="dltConfirmationTxt" ></div><div id="loginAlertOKBtn" data-role="buttonComp" class="dltConfirmationSuccess deletePrimaryButton" ></div></div>');
    $("#loginAlertOKBtn").html(GlobalModel.localizationData["ALERT_OK_BUTTON"]);
    $("#loginAlertTxt").html(GlobalModel.localizationData["ERROR_LOGIN"]);
    $("#loginAlertOKBtn").unbind('click').bind('click',function(){
        PopupManager.removePopup();
        window.location = "/";
        return;
    });
    setTimeout(function() {
        PopupManager.addPopup($("#loginAlertPanel"), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        });
    }, 0);
}

function storeUserDetails()
{
	var Authn = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL215Lmhydy5jb20iLCJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlhdCI6MTQwMjI5MDk1MSwic3ViIjoiY25cdTAwM2RKV09MVFoyLHVpZFx1MDAzZEpXT0xUWjIsdW5pcXVlSWRlbnRpZmllclx1MDAzZDMyNkE3NUE2MjM2NzQxNDk2NzlGRDZCNDlDMjU2NzZFLG9cdTAwM2QwMDIwNTE2OSxkY1x1MDAzZDAwMjA1MTU3LHN0XHUwMDNkR0EsY1x1MDAzZE4vQSIsImh0dHA6Ly93d3cuaW1zZ2xvYmFsLm9yZy9pbXNwdXJsL2xpcy92MS92b2NhYi9wZXJzb24iOlsiSW5zdHJ1Y3RvciJdLCJQbGF0Zm9ybUlkIjoiSE1PRiIsImV4cCI6MTQwMjI5NDU1MX0.YWurpOwln7dX1WYnF27JiNhrd_cE3eGtrmbbpX5sjjQ";
	var userInfo = decodeBase64(Authn);//$.cookie('Authn')
	var objReg = /uniqueIdentifier[\s\S]*?(,)/gi;
	var arrUDet = userInfo.match(objReg);
	//console.log("arrUDet", arrUDet);
	objReg = /,/gi;
	var uid = arrUDet[0].replace(objReg, "").split("\\u003d")[1];	
	GlobalModel.uid = uid;
	initAppFlow();
}

function getInternetExplorerVersion() {

	var rv = -1;
	if (navigator.appName == 'Microsoft Internet Explorer') {

		var ua = navigator.userAgent;

		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

		if (re.exec(ua) != null)

			rv = parseFloat(RegExp.$1);
	}

	return rv;

}