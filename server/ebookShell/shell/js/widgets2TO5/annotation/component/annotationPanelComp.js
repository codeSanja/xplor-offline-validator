/**
 * @author lakshay.ahuja
 * This class handles the functionality of stickynotepanel and stickynote
 */
var strList = "";
var strTOCList = "";
var level = 0;
var curriculumXml = "";
var isPopupLayoutChanged = false;
var isPopupFirstTime = true;
var CompModel = {
    xPos:0,
    yPos:0
};

var objToc = $.widget("magic.annotationPanelComp", $.magic.tabViewComp, {
    _strDefaultFontFamily : "Arial",
    _nDefaultFontSize : "3",
    options : {},
    events : {
        STICKY_TEXT_FORMAT_CHANGE : 'stickyTextformatchange'

    },
    member : {
        notePanelScroll : null,
        isContainerScrollBool : false,
        sortBy : "pageASC",
        bPlaceCaretAtEnd : "false",
        contentWidth : 820,
    },
    _create : function() {

        //adding this check because these panels are not handled the way other tab panels are handled
        if (EPubConfig.QAPanel_isAvailable == true || EPubConfig.Notes_isAvailable == true) {
            $("#NotesPanel").css("display", "block");
        }

        var objThis = this;
        $("#bookContentContainer2").bind('scrollstop', function() {
            if (objThis.member.isContainerScrollBool == true) {
                $(objThis).trigger('openStickyNotePanel');
            }

        });
        $("#bookContentContainer").bind('scrollstop', function() {
            if (objThis.member.isContainerScrollBool == true) {
                $(objThis).trigger('openStickyNotePanel');
            }

        });
    },

    _init : function() {
        $.magic.tabViewComp.prototype._init.call(this);
        var objThis = this;

        $(window).bind('orientationchange', function() {
            if (navigator.userAgent.match(/(android)/i) && ($("#stickyNoteContainer").css("display") == "block")) {
                isPopupFirstTime = true;
            }

        });
        var ddOptiosClicked = false;
        if ($(objThis.element).attr("id") == "NotesPanel") {
            objThis.member.notePanelScroll = new iScroll('addedStickyNotesList', {
                scrollbarClass : 'myScrollbar'
            });
            $(window).resize(function() {
                objThis.updateViewSize();
            });

            /*--------------------------------------------Left panel open event ---------------------------------------------------- */
	        $(objThis.element).unbind("panelOpened").bind("panelOpened", function() {
	        	
	        	//Open Annotation Tab/My Question Tab depending on visible Tab 
	        	if ($(objThis.element).find("#annotationMyQuestionsTab")[0].className != "leftPanelTab") {
	        		
	        		$($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
	        		$(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
	            	$(objThis.element).find('[id=externalURLLink]').css('display', 'none');
	            	$(objThis).trigger('addDataToQuestionAnswerSectionPanel');
	        	}
	        	else
	        	{
	        		$($(objThis.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");
	        		objThis.onPanelOpen();	
	        	}
	        });
			/*------------------------------------------------------------------------------------------------------------------------ */

            /*--------------------------------------------Left panel close event ---------------------------------------------------- */
	        $(objThis.element).unbind("panelClosed").bind("panelClosed", function() {
	            objThis.onPanelClose();
	        });
	        /*------------------------------------------------------------------------------------------------------------------------ */
	       
	       $( document ).bind( "stickyNotesDataLoadComplete", function (){
               if ( $( objThis.element ).find("#annotationMyQuestionsTab")[ 0 ].className == "leftPanelTab") 
               {
                   objThis.onPanelOpen();
               }
               
           });
        }

        $(objThis.element).unbind('click').bind('click', function(e) {
            if (ddOptiosClicked == false) {
                $($(objThis.element).find("#dd-options")[0]).css("display", "none");
            } else {
                ddOptiosClicked = false;
            }
        });

        $($(objThis.element).find("#dd-clickable")).unbind('click').bind("click", function() {
            ddOptiosClicked = true;
            if ($($(objThis.element).find("#dd-options")[0]).css("display") == "none") {
                $($(objThis.element).find("#dd-options")[0]).css("display", "block");
                $($(objThis.element).find("#dd-options li")).unbind("click").bind("click", function(e) {
                    if ( typeof (e.currentTarget.innerText) != "undefined")
                        $($(objThis.element).find("#dd-selected")).html(e.currentTarget.innerText);
                    else
                        $($(objThis.element).find("#dd-selected")).html(e.currentTarget.textContent);
                    $($(objThis.element).find("#dd-options")[0]).css("display", "none");
                    objThis.member.sortBy = $($(e.currentTarget).find("a")).attr("value");

                    //deleting and recreating data in annotations panel
                    objThis.onPanelOpen();
                    var firstElementId = $($('.panelData')[0]).attr('id');
                    objThis.member.notePanelScroll.scrollToElement("#" + firstElementId, 0);
                });
            } else {
                $($(objThis.element).find("#dd-options")[0]).css("display", "none");
            }
        });
        var objMyNoteBookBtn = $(objThis.element).find("#externalURLLink")[0];
        if (EPubConfig.My_Notebook_isAvailable) {
            if (EPubConfig.My_Notebook_isAvailable == true) {
                $($(objThis.element).find("#externalURLLink")[0]).unbind('click').bind("click", function() {
                    window.open(EPubConfig.MY_NOTEBOOK_PATH, '_blank');
                });
            } else {
                $($(objThis.element).find("#externalURLLink")[0]).css("visibility", "hidden");
            }
        } else {
            $($(objThis.element).find("#externalURLLink")[0]).css("visibility", "hidden");
        }
        //setting the default markup color
        if ($("#markupcolor")[0] == null)
            $("head").append("<style id='markupcolor' type='text/css'>::-moz-selection { background: " + EPubConfig.Markup_default_Color + " !important;} ::selection{ background: " + EPubConfig.Markup_default_Color + " !important;}</style>")
            
    },
    
    _setOption : function() {

    },

    /**
     * This function updates the height of sticky note panel list & refresh the iscroll
     * return void
     */
    updateViewSize : function() {
        var objThis = this;
        setTimeout(function() {
            var headerHeight = ($("#header").css('display') == 'block') ? $("#header").height() : 1;
            var stickynoteTopHeight = parseInt($("#StickyNotesSectionPanelTop").height());
            $("#addedStickyNotesList").height($($.find("[id=NotesSectionPanel]")).height() - (stickynoteTopHeight + headerHeight + 10));
            objThis.member.notePanelScroll.refresh();
            if (ismobile == null) {
                //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), objThis.member.notePanelScroll, "addedStickyNotesList");
            }
        }, 200);

    },
    onhighlightAnnotationBtnClick : function(highlightObj, matchFound) {

        var objThis = this;
        var defaultcolor = highlightObj.defaultcolor;
        if (!ismobile) {
            PopupManager.removePopup();
        } else {
            $("#annotation-bar").css('display', 'none');
        }
        var currentHighlight = $($("#innerScrollContainer")[0]).find('[type=highlighterComp]')[highlightObj.numberofHighlights];

        //This will open the stickynote container panel

        objThis.openHighlightPanel(highlightObj, highlightObj.highlightContainer, highlightObj.rangeData, highlightObj.numberofHighlights, defaultcolor, matchFound);

    },

    setCaretPositionVar : function(boolVal) {
        this.member.bPlaceCaretAtEnd = boolVal;

    },

    openHighlightPanel : function(highlightObj, highlightcontainerRef, rangeData, numberofHighlights, defaultcolor, matchFound) {
        var objThis = this;
        PopupManager.removePopup();
		setTimeout(function() {
            objThis.openHighlightContainerPanel(highlightObj, highlightcontainerRef.element, rangeData, numberofHighlights, defaultcolor, matchFound);
        }, 0)
    },

    openHighlightContainerPanel : function(highlightObj, objPopupPanel, rangeData, numberofHighlights, defaultcolor, matchFound) {

        var objThis = this;
        var toCenter = false;
        var panelRef = $(objPopupPanel).find('[type=color]');
        var defaultnum = defaultcolor.replace('selectioncolor', '').replace("_underline", '');
        $(panelRef[defaultnum - 1]).removeClass('colorButton');
        $(panelRef[defaultnum - 1]).addClass('colorButtonSelected');

        var toppos = highlightObj.topPos + 20;

        //toppos = toppos * GlobalModel.currentScalePercent;
        //toppos = rangeData.commonAncestorContainer.offsetTop * GlobalModel.currentScalePercent;
        //console.log(toppos,rangeData);
        var leftpos = highlightObj.leftPos + 20;
        //console.log("1",leftpos);
        //console.log("2",leftpos);
        if ($(window).height() <= toppos + 110) {
            toppos = toppos - 10 - $(objPopupPanel).height();
        }
        if (leftpos <= 0) {
            leftpos = rangeData.getBoundingClientRect().left;
        } else if (leftpos + $(objPopupPanel).width() >= $(window).width()) {
            leftpos = $(window).width() - $("annotationPanel").width() - $(objPopupPanel).width() - 15;
        }
        if (toppos < 0) {

            toCenter = true;
        }
        PopupManager.addPopup($(objPopupPanel), objThis, {
            isModal : true,
            isCentered : toCenter,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        });

        $(objPopupPanel[0]).css("display", "none");

        if (toppos >= 0) {
            $(objPopupPanel).css('top', toppos);
            /*  for ff, we may need, X GlobalModel.currentScalePercent*/
            $(objPopupPanel).css('left', leftpos);
        }
        //        var rangeData2 = rangy.createRange();
        //        rangeData2 = rangeData.cloneRange();
        if (matchFound == 1) {
            
            if (navigator.userAgent.match(/(android)/i)) 
            {
            	var startRange = document.createRange();
	            startRange.setStart(rangeData.startContainer, rangeData.startOffset);
	            var $start = $("<span/>");
	            startRange.insertNode($start[0]);
	            $start.remove();
            }
            
            $(objPopupPanel).find('[type=saveBtn]').removeClass('ui-disabled');
            rangy.init();
            var cssClassApplier = rangy.createCssClassApplier("selection" + numberofHighlights);
            var cssClassAppliercolor = rangy.createCssClassApplier(defaultcolor);
            window.getSelection().addRange(rangeData);
            cssClassApplier.toggleSelection();
            cssClassAppliercolor.toggleSelection();
            var count = 0;
            while ($(".selection" + numberofHighlights).length == 0) {
                if (count > 100) {
                    break;
                }
                cssClassApplier.toggleSelection();
                cssClassAppliercolor.toggleSelection();
                count++;
            }
            if (window.getSelection) {
                if (window.getSelection().empty) {// Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {// Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) {// IE?
                document.selection.empty();
            }

        }

        $(objPopupPanel[0]).css("display", "block");
        PageNavigationManager.addSwipeListner($($(objThis.element).parent().parent().parent()).find('[id="bookContentContainer2"]'), $("#bookContentContainer").data('scrollviewcomp'));

    },

    onPanelClose : function() {
        this.updateViewSize();
    },

    /**
     * @description This method sets the carat index to end of text in a contenteditable div
     * @param {Object} HTML element (contenteditable div)
     * @return void
     */
    placeCaretAtEnd : function(el) {
        if (!ismobile) {
            el.focus();
        }

        if ( typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if ( typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    },

    switchSavetoNote : function(savetonoteChk, savetoNoteBookButtonRef) {

        if (savetonoteChk == 1) {
            $(savetoNoteBookButtonRef).addClass('savetoNoteBookBtnChk');
            $(savetoNoteBookButtonRef).removeClass('savetoNoteBookBtn');

            return 0;
        } else {
            $(savetoNoteBookButtonRef).removeClass('savetoNoteBookBtnChk');
            $(savetoNoteBookButtonRef).addClass('savetoNoteBookBtn');

            return 1;
        }

    },
    
    /**
     * This function is called from MG.PanelViewOperator. It creates the list that needs to be added in the bookmark section Panel.
     * Triggers an event 'addDataToBookmarkSectionPanel', it is handled in MG.BookmarkOperator
     */
    onPanelOpen : function() {
        var objThis = this;
        var todaysDate = getTodaysDate();
    	var yesterdaysDate = getYesterdaysDate();
    	var strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';
    	
        objThis.annotationArrSort(objThis.member.sortBy);
        var annotationData = "";
        if( GlobalModel.annotations.length == 0 )
        {
            $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "block");
            $(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'none');
            $("#addedStickyNotes").html("");
            return;
        }
        else
        {
            $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
            $(objThis.element).find('[id=annotationSortBy]').css('display', 'block');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'block');
            $("#addedStickyNotes").html( strListPart_2 );
        }
        
        setTimeout( function ()
        {
        for (var i = 0; i < GlobalModel.annotations.length; i++) 
        {
            var annotationID = GlobalModel.annotations[i].id;
            if (GlobalModel.annotations[i]){
                var dayData = "";
                if (GlobalModel.annotations[i].date == yesterdaysDate) {
                    dayData = GlobalModel.localizationData["DATE_YESTERDAY"];
                } else if (GlobalModel.annotations[i].date == todaysDate) {
                    dayData = GlobalModel.localizationData["DATE_TODAY"];
                } else {
                    dayData = GlobalModel.annotations[i].date;
                }

                var iPageNumber = GlobalModel.annotations[i].pageNumber;
                if (GlobalModel.annotations[i].type == "highlight") 
                {
                    var titleStr = GlobalModel.annotations[i].title;
                    var color = GlobalModel.annotations[i].selectioncolor.replace('_underline', '');
                    annotationData += "<div id='highlightData_" + annotationID + "' page='" + iPageNumber + "' class='panelData stickynoteData' type='highlightData' style='text-shadow: none;'><div class='annotationData'><span id='stickynoteDataDayData' class='dayData'>" + dayData + "</span><span class='panelPageNumber'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + iPageNumber + "</span><div class='highlightDataIcon " + color + "_icon'></div><span id='stickynoteDataTitle' class='dataTitle'>" + $(titleStr).text() + "</span><span id='stickynoteDataTitle_detail' class='dataTitle' style='display:none;'><span>" + $(titleStr).text() + "</span></span></div><div id='editDeleteAnnotation'></div><div id='expandCollapseNoteList_" + annotationID + "' class='expandCollapseNoteList showDetail' type='expandCollapseNoteList' >" + GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] + "</div></div>";
                } 
                else 
                {
                    var regex = /(<([^>]+)>)/ig;
                    var titleStr = GlobalModel.annotations[i].title;

                    //if only images are present in a note, display alt text (preffered) or src if alt text is not available;
                    if ($.trim(titleStr.replace(regex, "")) == "") {
                        titleStr = "";

                        var strTemp = "<div>" + GlobalModel.annotations[i].title + "</div>";

                        var objTemp = $(strTemp).find("img");
                        for (var imgCounter = 0; imgCounter < objTemp.length; imgCounter++) {
                            if ($(objTemp[imgCounter]).attr("alt") && $(objTemp[imgCounter]).attr("alt") != "")
                                titleStr += $(objTemp[imgCounter]).attr("alt") + ", ";
                            else
                                titleStr += $(objTemp[imgCounter]).attr("src") + ", ";
                        }

                        titleStr = titleStr.substr(0, titleStr.length - 2);
                    }

                    annotationData += "<div id='stickynoteData_" + annotationID + "' noteRef='" + annotationID + "' page='" + iPageNumber + "'  class='panelData stickynoteData' type='stickynoteData' style='text-shadow: none;'><div class='annotationData'><span id='stickynoteDataDayData' class='dayData'>" + dayData + "</span><span class='panelPageNumber'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + iPageNumber + "</span><div class='stickynoteDataIcon'></div><span id='stickynoteDataTitle' class='dataTitle'>" + titleStr.replace(regex, "") + "</span><span id='stickynoteDataTitle_detail' class='dataTitle' style='display:none;'>" + GlobalModel.annotations[i].title + "</span></div><div id='editDeleteAnnotation'></div><div id='expandCollapseNoteList_" + annotationID + "' class='expandCollapseNoteList showDetail' type='expandCollapseNoteList' >" + GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] + "</div></div>";
        		}
        	}
        }
        $(objThis).trigger('addDataToStickyNoteSectionPanel', annotationData);
       // if (annotationData == "") 
       // {
      //      $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "block");
      //      $(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
      //      $(objThis.element).find('[id=externalURLLink]').css('display', 'none');
      //  } 
       // else 
      //  {
      //      $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
       //     $(objThis.element).find('[id=annotationSortBy]').css('display', 'block');
       //     $(objThis.element).find('[id=externalURLLink]').css('display', 'block');
       // }
            
        objThis.updateViewSize();
        }, 500 );
    },
    annotationArrSort : function(option) {
        switch(option) {
            case "dateASC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = new Date(a.timeMilliseconds);
                    var d = new Date(b.timeMilliseconds);
                    return c - d;
                });
                break;

            case "dateDSC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = new Date(a.timeMilliseconds);
                    var d = new Date(b.timeMilliseconds);
                    return d - c;
                });
                break;

            case "pageASC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = GlobalModel.pageBrkValueArr.indexOf(a.pageNumber);
                    var d = GlobalModel.pageBrkValueArr.indexOf(b.pageNumber);
                    return c - d;
                });
                break;

            case "pageDSC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = GlobalModel.pageBrkValueArr.indexOf(a.pageNumber);
                    var d = GlobalModel.pageBrkValueArr.indexOf(b.pageNumber);
                    return d - c;
                });
                break;
        }
    },

    destroy : function() {
        $.widget.prototype.destroy.call(this);
    }
});
