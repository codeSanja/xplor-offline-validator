/**
 * @author anand.yadav
 */
(function($, undefined){
    $.widget("magic.stickyNoteComp", $.magic.buttonComp, {

        _init: function(){
			//$('head').append("<style>.noteselection{background:" + EPubConfig.Markup_default_Color + ";cursor:pointer}</style>");
        },


        options: {
            objSuper: null
        },
        
        getText: function(){
            var txt = $(this.element).find('#stickyNoteData').val();
            return txt;
        },
        
        setText: function(strText){
            $(this.element).find('#stickyNoteData').html(strText);
        },
        removeIt: function(){
        
        },
        
        getPanelViewComp: function(){
            return $('#stickyNotePanel');
        },
        
        /*
         * This function is called from stickynoteoperator, when a new stickynote is saved.
         * stickynoteTxtAreaRefvalue is the text of textarea.
         * numberofStickynotes is the current number of stickynotes
         * totalStickynotes is the total number of stickynotes i.e. existing stickynotes + deleted notes
         */
        saveNewStickyNoteData: function(stickynoteTxtAreaRefvalue, totalStickynotes){
            // To add data in stickynotesectionpanel (Left side panel)
            
			var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
            var stickynoteTitle = stickynoteTxtAreaRefvalue;
            var arrObj = new Array();
            arrObj.title = stickynoteTitle;
            arrObj.date = dateOfCreation;
            arrObj.id = totalStickynotes;
			arrObj.type = "stickynote";
            arrObj.timeMilliseconds = d;
            arrObj.top = CompModel.yPos;
			return arrObj;
        },
        
        modifyStickyNoteData: function(arrObj, savetoNote){
            // To modify data in stickynotesectionpanel (Left side panel)
            var totalStickynotes = GlobalModel.annotations.length;
			var stickyNoteModifiedId = arrObj.id;
			var stickynoteTxtAreaRefvalue = arrObj.title;
			var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
            var stickynoteTitle = stickynoteTxtAreaRefvalue;
            for (var i = 0; i < totalStickynotes; i++) {
				if (GlobalModel.annotations[i]){
                    if (GlobalModel.annotations[i].type == "stickynote") {
                        if (GlobalModel.annotations[i].id == parseInt(stickyNoteModifiedId)) {
                            GlobalModel.annotations[i].title = stickynoteTxtAreaRefvalue;
                            GlobalModel.annotations[i].savetonote = savetoNote;
                            GlobalModel.annotations[i].date = dateOfCreation;
                            GlobalModel.annotations[i].timeMilliseconds = d;
                            GlobalModel.annotations[i].tagsLists = arrObj.tagsLists;
                        }
                    }
				}
                
                
            }
            
            var arrObj = new Object();
            arrObj.title = stickynoteTitle;
            var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
            arrObj.pageNumber = iPageIndex;
            arrObj.date = dateOfCreation;
            arrObj.id = totalStickynotes;
			arrObj.type = "stickynote";
            arrObj.timeMilliseconds = d;
            return arrObj;
        }
    });
})(jQuery);
