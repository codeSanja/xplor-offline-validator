function NotePopup()
{
	var noteContainer = null;
	var self = this;
	var _componentsJSON = { "stickynoteTextArea" : { "event":"click,paste", "type" : "textarea" }, 
	                        "closeButton" : { "event":"click", "type" : "button" }, 
	                        "saveBtn" : { "event":"click", "type" : "button" }, 
				            "deleteBtn" : { "event":"click", "type" : "button" },
				            "savetoNoteBookButton" : { "event":"click", "type" : "checkbox" },
						    "tagInputBox" : { "event":"paste,keyup,change", "type" : "textinput" }, 
						    "tagAddBtn" : { "event":"click", "type" : "button" } };
	var _textArea = null;
	var _tempTagListArray = [];
	var _tagListArray = [];
	var _isNewNote = true; // must be set only in showPopup function
	var _currentNoteIcon = null; // must be set only in showPopup function
	var _isAlertPopupOpenedFromThisClass = false;
	var scrollviewcompRef = null;
	
	this.addListeners = function ()
	{
		var container = noteContainer;
		scrollviewcompRef = $( "#bookContentContainer" ).data( 'scrollviewcomp' );
		if ( !( $.browser.msie && $.browser.version == 10 ) && !isWinRT && $.browser.version != 11 )
        {
        	var eventName = "input propertychange";
        }
        else
        {
        	var eventName = "keyup";
        }
        
        _textArea = container.find( '[type = "stickynoteTextArea"]' );
        _textArea.bind( eventName, self.textAreaTextChangeHandler );
        _textArea.bind( "keydown" , function ( event ) { if(!(event.ctrlKey && event.which == 88)){event.stopPropagation();}} );
        noteContainer.find(".tagInputBox").bind( eventName, self.tagInputBoxchangeHandler );
        noteContainer.find(".tagInputBox").bind('keydown', 'tab', function(evt){
         	noteContainer.find(".tagInputBox").blur();
			HotkeyManager.tabKeyHandler();
			return false;
		});
        for( var i in _componentsJSON )
        {
        	var item = _componentsJSON[ i ];
        	var eventArr = item.event.split( "," );
        	var selector = "";
        	
        	for( var j = 0; j < eventArr.length; j++ )
        	{
        		if( item.type == "textinput" )
        		{
        			selector = "." + i;
        		}
        		else
        		{
        			selector = "'[type = " + i + " ]'";
        		}
        		container.find( selector ).unbind( eventArr[ j ] ).bind( eventArr[ j ], self[ i + eventArr[ j ] + "Handler" ] );
        	}
        }
        
        $( scrollviewcompRef ).bind( "alertokclick", function ()
        {
        	if( !_isAlertPopupOpenedFromThisClass )
        	{
        		return;
        	}
        	_isAlertPopupOpenedFromThisClass = false;
        	
        	$( self ).trigger( "adjustPopupForAndroid" );
        	
        	PopupManager.addPopup( $( "#stickyNoteContainer" ), _currentNoteIcon, 
        	{
		        isModal : true,
		        hasPointer : true,
		        offsetScale : GlobalModel.currentScalePercent,
		        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
		        isTapLayoutDisabled : true
			} );
			
		} );
	};
	
	this.showPopup = function ( isNew, noteIconID, isOpenNoteForEdit )
	{
	    var noteObj = null;
		_isNewNote = isNew;
		_currentNoteIcon = $( "#" + noteIconID );
		this.disableEnableComponents ( "all", "disable" );
		this.setSaveToNotebookCheckboxState( false );
		this.hideTagSection( true );
		if( !EPubConfig.My_Notebook_isAvailable )
		{
			$( ".savetonotebook" ).css( "display" , "none" );
			noteContainer.find( '[type = savetoNoteBookButton]' ).css( "display" , "none" );
		}
		
		if( !_isNewNote )
		{
			populateObjectInPopup = function ( obj )
			{
				self.setSaveToNotebookCheckboxState( !parseInt( obj.savetonote ) );// 0 means checkbox is selected 1 means it is not
				self.hideTagSection( parseInt( obj.savetonote ) );
				_textArea.html ( obj.title );
				if(obj.tagsLists != undefined)
				{
					for( var i = 0; i < obj.tagsLists.length; i++ )
					{
						self.addTag( obj.tagsLists[ i ] );
					}
				}
			};
			self.disableEnableComponents( "savetoNoteBookButton", "enable" );
			var id = _currentNoteIcon.attr( "id" );
			noteObj = self.findNoteObjectInGlobalModel( id.replace( "btnStickyNote_", "" ) );
			populateObjectInPopup( noteObj );
			this.disableEnableComponents ( "deleteBtn", "enable" );
			_textArea.attr( "contenteditable" , false );
		}
		
		$( scrollviewcompRef ).trigger( 'closeOpenedPanel' );
		
		if(!( $(_currentNoteIcon).offset().top > 0 && $(_currentNoteIcon).offset().top < ( $(window).height() - 20 * GlobalModel.currentScalePercent ) && $( _currentNoteIcon).offset().left < ( $(window).width() - 30 ) ) )
    	{
            scrollviewcompRef.resetZoom();
            if ( EPubConfig.pageView.toUpperCase() == 'SCROLL' ) 
			{
	            if ( $(_currentNoteIcon).offset().top > $(window).height() - 20 * GlobalModel.currentScalePercent )
	            {
					var scrollBy = $(_currentNoteIcon).offset().top + $("#bookContentContainer2").scrollTop();
	            	$("#bookContentContainer2").scrollTop( scrollBy - $(window).height()/2 );	
	            }
	        }
        }
        
		setTimeout( function () 
		{
			PopupManager.addPopup( $( "#stickyNoteContainer" ), _currentNoteIcon, {
		        isModal : true,
		        hasPointer : true,
		        offsetScale : GlobalModel.currentScalePercent,
		        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
		        isTapLayoutDisabled : true
			} );
			
			if ( !ismobile && (isNew || isOpenNoteForEdit)) 
			{
	            _textArea.attr( "contenteditable" , true );
	            _textArea.focus();
	            self.placeCaretAtEnd(_textArea[0]);
	        }
			else
			{
				if( !ismobile )
		        {
		         $( "#pg" ).focus();
		        }
			}
		}, 0 ); 
		
	};
	
	this.findNoteObjectInGlobalModel = function ( id )
	{
		for( var i = 0; i < GlobalModel.annotations.length; i++ )
		{
			if( GlobalModel.annotations[ i ].id == id && GlobalModel.annotations[ i ].type == "stickynote" )
			{
				return GlobalModel.annotations[ i ];
			}
		}
	};
	
	this.disableEnableComponents = function ( componentType, operation  )
	{
		applyOperationOnComponent = function ( component )
		{
			if( operation == "disable" )
			{
				component.attr( "disabled", true );
				component.addClass( "ui-disabled" ).attr( "aria-disabled", true );
			}
			else
			{
				component.attr( "disabled", false );
				component.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
			}
		};
		
		disableEnableBasedOnType = function ( type, selector, operation )
		{
			switch( type )
			{
				case "button":
					applyOperationOnComponent( noteContainer.find( selector ) );			
				break;
				
				case "textarea":
					noteContainer.find( selector ).html( "" );
				break;
				
				case "textinput":
					noteContainer.find( selector ).val( "" );
				break;
				
				case "checkbox":
					applyOperationOnComponent( noteContainer.find( selector ) );
				break;
			}
		};
		
		if( componentType != "" && componentType != "all" )
		{
			var arr = componentType.split( "," );
			for ( var i = 0; i < arr.length; i++ )
			{
				if( _componentsJSON[ arr[ i ] ].type == "textinput" )
        		{
        			var selector = "." + arr[ i ];
        		}
        		else
        		{
        			var selector = "'[type = " + arr[ i ] + " ]'";
        		}
				disableEnableBasedOnType( _componentsJSON[ arr[ i ] ].type, selector, operation );
			}
		}
		else if( componentType == "all" )
		{
			for ( var j in _componentsJSON )
			{
				if( j == "closeButton" )
					continue;

				if( _componentsJSON[ j ].type == "textinput" )
        		{
        			var selector = "." + j;
        		}
        		else
        		{
        			var selector = "'[type = " + j + " ]'";
        		}
				disableEnableBasedOnType( _componentsJSON[ j ].type, selector, operation );
			}
		}
	};
	
	this.setSaveToNotebookCheckboxState = function ( check /* Boolean */ )
	{
		if( !check  )
		{
			noteContainer.find( '[type = savetoNoteBookButton]' ).removeClass( 'savetoNoteBookBtnChk' ).addClass( 'savetoNoteBookBtn' );
		}
		else
		{
			noteContainer.find( '[type = savetoNoteBookButton]' ).removeClass( 'savetoNoteBookBtn' ).addClass( 'savetoNoteBookBtnChk' );
		}
	},
	
	this.hideTagSection = function ( bool )
	{
		if( bool )
		{
			noteContainer.find( '.stickyNoteTags' ).css( "display", "none" );
			noteContainer.find( '.tagBorder' ).css( "display", "none" );
			_tempTagListArray = [];
			noteContainer.find( "#tagList .tagOuter" ).remove();
		}
		else
		{
			noteContainer.find( '.stickyNoteTags' ).css( "display", "block" );
			noteContainer.find( '.tagBorder' ).css( "display", "block" );
		}
	};
	
	this.closeButtonclickHandler = function ()
	{
		PopupManager.removePopup();
		if( _isNewNote )
		{
			self.deleteBtnclickHandler();
		}
	};
	
	this.saveBtnclickHandler = function ()
	{
		// TO DO handling if on closing of popup keyboard does not close in ipad
		try
		{
			if (! ( $.browser.msie && $.browser.version == 10 ) && !isWinRT && $.browser.version != 11 ) 
			{
			  self.modifyContent($(_textArea)[0]);
			}
			var sticknoteText = _textArea.text();
			if ( isInteger( EPubConfig.maxCharactersInNotes ) == false )
			{
			    throw new Error( "Value of maxCharactersInNotes is not an integer" );
			}
			 
			if ( EPubConfig.maxCharactersInNotes < 1 )
			{
			    throw new Error( "Max character limit in notes is set to less than one" );
			}
             
            if( sticknoteText.length > EPubConfig.maxCharactersInNotes )    
            {
            	self.showAlertPopup( GlobalModel.localizationData["STICKY_NOTE_LIMIT"].replace("$1", EPubConfig.maxCharactersInNotes) );
            } 
            else
            {
            	var obj = {};
				obj.tagsLists = _tempTagListArray.slice( 0 );
				obj.title = _textArea.html();

            	if( _isNewNote )
            	{
					$( self ).trigger( "saveNewNote", [ obj ] );   		
            	}
            	else
            	{
            		var id = $( _currentNoteIcon ).attr( "id" );
            		obj.id = id.replace( "btnStickyNote_", "" );
            		$( self ).trigger( "updateExistingNote", [ obj ] );
            	}
            	PopupManager.removePopup();
            }      
		}
		catch ( error )
		{
		    PopupManager.removePopup();
			console.log( error.message );
		}
	};
	
	this.tagInputBoxchangeHandler = function ()
	{
	    var stateOfButton = self.checkIfButtonIsToBeEnabled ( "tagAddBtn" );
        self.disableEnableComponents( "tagAddBtn", stateOfButton );
	};
	
	this.showAlertPopup = function ( message )
	{
		_textArea.blur();
        PopupManager.removePopup();
        if ( ismobile )
        {
        	document.onselectionchange = null;
            $("#annotation-bar").css("display","none");
            $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
            $("#inputToRemove").focus();
            $("#inputToRemove").blur();
            $("#inputToRemove").remove();   
        }
        $( "#alertTxt" ).html( message );
        PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        } );
        
        _isAlertPopupOpenedFromThisClass = true;
	};
	
	this.deleteBtnclickHandler = function ()
	{
		if( _isNewNote )
		{
			self.deleteNewNote();
		}
		else
		{
			$( self ).trigger( "showDeleteConfirmationPopup", [ _currentNoteIcon.attr( "id" ) ] ); 
		}
		
	};
	
	this.deleteNewNote = function ()
	{
		$( ".noteselection_" + GlobalModel.totalStickyNotes ).removeClass( "noteselection" )
															 .removeClass( "noteselection_" + GlobalModel.totalStickyNotes );
																 
		if ( ismobile )
		{
			scrollviewcompRef.options.rangedata = "";
		}
        else
        {
        	$( scrollviewcompRef ).trigger( "select_text_end" );
        }
        
        $( "#btnStickyNote" ).remove();
	};
	
	/* Text area */
	this.stickynoteTextAreapasteHandler = function ()
	{
		// TO DO
		// There are some other things also in StickyNoteOperator for paste event. Not sure of what these are. Will check later and see
		self.textAreaTextChangeHandler();
	};
	
	this.textAreaTextChangeHandler = function ()
	{
		var anchorArr = $( _textArea ).find( '[ type = custom ]' );
        for( var i = 0; i < anchorArr.length; i++ )
        {
        	var strToMatch = $( anchorArr[i] ).attr( "href" );
        	if($( anchorArr[i] ).text().indexOf("http") == -1)
        	{
        		strToMatch = $( anchorArr[i] ).attr( "href" ).replace( "http://","" );
        	}
        	if( $( anchorArr[i] ).text() != strToMatch )
        	{
        		$( anchorArr[i] ).remove();
       	 	}
       	}
		if ( $.trim( _textArea.text() ) != "" || _textArea.html().toString().indexOf( "<img" ) > -1) 
		{
			self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "enable" );
		}
		else
		{
			self.disableEnableComponents( "saveBtn,savetoNoteBookButton", "disable" );
		}
		
	};
	
	this.stickynoteTextAreaclickHandler = function ()
	{
		var contentEditable = _textArea.attr( "contenteditable" );
		_textArea.focus();
		_textArea.attr( "contenteditable" , true );
		$(".focusglow").removeClass("focusglow");
		HotkeyManager.currentActiveElementIndex = 1;
	};
	
	
	/* Tag */
	this.savetoNoteBookButtonclickHandler = function ()
	{
		self.setSaveToNotebookCheckboxState( !noteContainer.find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) );
		var ischeckBoxChecked = noteContainer.find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' );
		
		if( !ischeckBoxChecked )
		{
			self.hideTagSection ( true );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
		}
		else
		{
			self.hideTagSection ( false );
		}
		self.disableEnableComponents( "saveBtn", "enable" );
		PopupManager.positionPopup();
	};
	
	this.tagInputBoxpasteHandler = function ()
	{	   
	    setTimeout(function () {
	        noteContainer.find(".tagInputBox").trigger("keyup");
	    }, 100);
	};
	
	this.tagInputBoxkeyupHandler = function ( event )
	{
		var stateOfButton = self.checkIfButtonIsToBeEnabled ( "tagAddBtn" );
		var tagStr = $( this ).val();
		self.disableEnableComponents( "tagAddBtn", stateOfButton );
		if( stateOfButton == "enable" )
		{                                    
			$(noteContainer.find( '[id = tagInputBox]' )).autocomplete({
                source : _tagListArray
            });
			if( event.keyCode == "13" )
			{
				stateOfButton = self.checkIfButtonIsToBeEnabled( "saveBtn" );
				self.disableEnableComponents( "saveBtn", stateOfButton );
				self.tagAddBtnclickHandler();
			}
		}
	};
	
	this.removeScriptTag = function( textContent ) 
	{
	    textContent = textContent.replace( /<script(.*?)>/gim, "" );
	    textContent = textContent.replace( /<\/script(.*?)>/gim, "" );
	    return textContent;
	};
	
	/**
	 * This function will modify text part from content. It will convert url written by user into clickable links
	 */
	this.modifyContent = function ( el )
	{
	
	    var arrContentEditableDivChildren = [];
	    for ( var startIndex = 0, endIndex = el.childNodes.length; startIndex < endIndex; startIndex++ ) 
	    {
	        var item = el.childNodes[ startIndex ];
	        
	        //if node is of type text
	        if ( item.nodeType == 3 ) 
	        {
	            var strModifiedText = this.modifyContentforRegExp( item.data );
	            arrContentEditableDivChildren.push( strModifiedText );
	        }
	        else 
	        {
	            if ( $(item).html() != "" ) {
	                var elTemp = this.modifyContent( $(item)[0] );
	                
	                //ensuring all links (including copied from other websites) open in new tab
	                if ( item.nodeName == "A" )
	                {
	                	elTemp[0].target = "_blank";
	                }
	                
	                arrContentEditableDivChildren.push( elTemp );
	            }
	            else {
	                arrContentEditableDivChildren.push( item );
	            }
	        }    
	    }
	    
	    $(el).html( "" );
	    for ( var i = 0; i < arrContentEditableDivChildren.length; i++ ) 
	    {
	        $(el).append( arrContentEditableDivChildren[i] );
	    }
	    
	    return $(el);
	};
	
	/**
	 * This function will modify content to find regular Expression
	 */
	this.modifyContentforRegExp = function( StickyContent )
	{
	    StickyContent = StickyContent.replace(/<script(.*?)>/gim, "");
	    StickyContent = StickyContent.replace(/<\/script(.*?)>/gim, "");
	    StickyContent = StickyContent.replace(/\&nbsp\;/gim, " ");
	    StickyContent = StickyContent.replace(/<a([^<>]+)>/gim, "");
	    StickyContent = StickyContent.replace(/<\/a>/gim, "");
	    StickyContent = StickyContent.replace(/([^\/\'])(http\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/([^\/\'])(https\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/([^\/])(www\.[^ \,<>\.]+.\.[^ \,<>]+)/gim, "$1<a href='http:\/\/$2' target='_blank' type='custom' style='cursor:pointer'>$2</a>");
	    StickyContent = StickyContent.replace(/^(www\.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='http:\/\/$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	    StickyContent = StickyContent.replace(/^(http\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	    StickyContent = StickyContent.replace(/^(https\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' type='custom' style='cursor:pointer'>$1</a>");
	
	    return StickyContent;
	};
	
	this.checkIfButtonIsToBeEnabled = function ( type )
	{
		switch ( type )
		{
			case "tagAddBtn" :
				if( $.trim( noteContainer.find( ".tagInputBox" ).val() ) == "" )
				{
					return "disable";
				}
				else
				{
					return "enable";
				}
			break;
			
			case "saveBtn" : 
				if( $.trim( noteContainer.find( '[type = "stickynoteTextArea"]' ).text() ) == "" )
				{
					return "disable";
				}
				else
				{
					return "enable";
				}
			break;
			
			case "deleteBtn" :
			break;
		}
	};
	
	this.tagAddBtnclickHandler = function ()
	{
		var tagInputBoxRef = noteContainer.find( '[id = "tagInputBox"]' );
		$(tagInputBoxRef).val( self.removeScriptTag( $( tagInputBoxRef ).val() ) );
		var tagStr = $.trim( tagInputBoxRef.val() );
		$(".ui-autocomplete").css("display", "none");
		if( _tempTagListArray.length == EPubConfig.Tags_Per_Note )
		{
			self.showAlertPopup( GlobalModel.localizationData[ "MAXIMUM_TAG_ADDED" ] );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
			return;
		}
		else if( $.inArray( tagStr, _tempTagListArray ) != -1 )
		{
			self.showAlertPopup( GlobalModel.localizationData[ "SAME_TAG_ADDED" ] );
			self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
			return;
		}
		self.addTag( tagStr );
		stateOfButton = self.checkIfButtonIsToBeEnabled( "saveBtn" );
		self.disableEnableComponents( "saveBtn", stateOfButton );
	};
	
	this.addTag = function ( tagStr )
	{
		_tempTagListArray.push( tagStr );
		if( $.inArray( tagStr, _tagListArray ) == -1 )
		{
			_tagListArray.push( tagStr );
		}
		_tempTagListArray.sort();
		// TO DO add item to auto complete list
		 var tagDiv = "<div class='tagOuter'><div class='tag'>" + 
		 			  tagStr + "</div><span class='tagTweek'><div id='tag' data-role='buttonComp'" + 
		 			  " class='tagClsBtn' type='closeButton'></div></span></div>";
         noteContainer.find( '[id = tagList]' ).append( tagDiv );
         self.disableEnableComponents( "tagAddBtn,tagInputBox", "disable" );
		 if( noteContainer.hasClass( 'noteContainerLayoutchanged' ) )
		 {
		 	// do nothing
		 }
		 else
		 {
		 	if( noteContainer.css("display") == "block" )
		 	{
		 	   PopupManager.positionPopup(); 
		 	}
		 }
		 
		 /* Close button click of the newly added tag */
		 noteContainer.find( ".tagClsBtn" ).last().bind( "click", function ()
		 {
		 	var tagText = $( this ).parent().parent().find( ".tag" ).text();
		 	_tempTagListArray.splice( $.inArray( tagText, _tempTagListArray ), 1 );
		 	$( this ).parent().parent().remove();
		 	var stateOfButton = self.checkIfButtonIsToBeEnabled( "saveBtn" );
		 	self.disableEnableComponents( "saveBtn", stateOfButton );
		 	self.disableEnableComponents( "savetoNoteBookButton", "enable" );
		 	PopupManager.positionPopup();
		 } );
	};
	
	this.createTagListArray = function ()
	{
		for( var i = 0; i < GlobalModel.annotations.length; i++ )
		{
			if( GlobalModel.annotations[ i ].type == "stickynote" )
			{
				for( var j = 0; j < GlobalModel.annotations[ i ].tagsLists.length; j++ )
				{
					if( $.inArray( GlobalModel.annotations[ i ].tagsLists[j] , _tagListArray ) == -1 )
						_tagListArray.push(GlobalModel.annotations[ i ].tagsLists[j]);
				}
			}
		}	
	};	
	
	/**
     * @description	This method sets the carat index to end of text in a contenteditable div
     * @param {Object} HTML element (contenteditable div)
     * @return void
     */
    this.placeCaretAtEnd = function(el) {
        if ( typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") 
        {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } 
        else if ( typeof document.body.createTextRange != "undefined") 
        {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    };
	noteContainer = $( "#stickyNoteContainer" );
	self.addListeners ();
	self.createTagListArray();		
}