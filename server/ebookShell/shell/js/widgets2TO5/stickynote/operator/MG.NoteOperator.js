MG.NoteOperator = function()
{
	this.notePopup = null;
	this.scrollViewRef = null;
	this.contentWidth = 820;
	this.currentNoteID = null;
	this.deleteFromPanel = false;
	this.noteToEdit = -1;
	this.androidLandscapeHeight = 768;
	this.isOpenNoteForEdit = false;
};

MG.NoteOperator.prototype = new MG.BaseOperator();
	
MG.NoteOperator.prototype.attachComponent = function( objComp )
{
	if ( objComp == null )
	{
		return;
	}

    var _self = this, _type = objComp.element.attr( AppConst.ATTR_TYPE ).toString();
    
    switch ( _type )
    {
    	case AppConst.SCROLL_VIEW_CONTAINER:
	    	if ( navigator.userAgent.match(/(android)/i) ) 
	    	{
	            setTimeout( function ()
	            { 
	            	_self.androidLandscapeHeight = ( window.height > window.width ) ? window.width : window.height;
	            }, 1000 );
	            
	        }
	        
	        if ( _self.notePopup == null )
            {
            	 _self.notePopup = new NotePopup();
            }
	        _self.addListeners();
    		_self.scrollViewRef = objComp;
    		$( objComp ).bind( objComp.events.PAGE_RENDER_COMPLETE, function () 
    		{
                _self.scrollViewRenderCompleteHandler( objComp );
            } );
            
    	break;
    	
    	/* Notes click on Annotation bar */
    	case AppConst.NOTES_ANNOTATION_BTN:
    		if( isWinRT )
            {
				var eventListener = function( evt )
				{
				    _self.noteAnnoIconClickHandler( evt );
				};
			    var target = document.getElementById( "notes-anno" );
			    target.addEventListener( WinRTPointerEventType, eventListener );
            }
            else
            {
            	$( objComp.element ).bind( "click tap", function ( event ){
            		_self.noteAnnoIconClickHandler( event  );
            	}  );
            }
    	break;
    	
    	/* Sticky note popup */
    	case AppConst.STICKYNOTE_CONTAINER:
    		this.notePopup = new NotePopup ();
    	break;
    	
    	/* Sticky note/highlight left panel */
    	case AppConst.NOTE_SECTION_PANEL:
    	
	    	var addedNotes = $( objComp.element ).find( '[id = addedStickyNotes ]' )[0];

	    	$( objComp ).bind( 'addDataToStickyNoteSectionPanel' , function ( e , stickyNotesData )
	    	{
	        	$( addedNotes ).html( stickyNotesData );
	            /*-----------------------------------------Left Panel annotation edit event ---------------------------------------------- */
	            $( objComp.element ).find( '.annotationData' ).unbind( 'click' ).bind( 'click' , function ( e )
	            {
	            	//Edit Sticky Note
	            	switch ( $( this ).parent().attr( 'type' ) ) 
	            	{
                            case "highlightData" :
                            
	                                var currentListId = $( this ).parent().attr( 'id' ).replace( 'highlightData_' , '' );
	                                $( $("#bookContentContainer" ).data( 'scrollviewcomp' ) ).trigger( 'closeOpenedPanel' );
	                                $( objComp.element ).trigger( 'editHighlightData' , currentListId );
                                
                                break;
                                
                            case "stickynoteData" :
	                                var currentListId = $( this ).parent().attr( 'id' ).replace( 'stickynoteData_' , '' );
	                                _self.editNote( currentListId , $( this ).parent().attr( 'page' ) );
	                                
                                break;
                                
                            default:
					}
	            });
	            
	           /*------------------------------------Left Panel annotation Expand/Collapse event ----------------------------------------- */
	            $( objComp.element ).find( '.expandCollapseNoteList' ).unbind( 'click' ).bind( 'click' , function ( e )
	            {
	            	//Expand/collapse Sticky Note	
	            	if ( $( this ).hasClass( 'showDetail' ) ) 
	            	{
	            		$( objComp.element ).find( '.expandCollapseNoteList:not(.showDetail)' ).trigger( "click" );
	            		$( this ).html( GlobalModel.localizationData["STICKY_NOTE_PANEL_HIDE_DETAILS_BUTTON"] );
                        $( this ).removeClass( 'showDetail' );
                        $( this ).parent().find( "#stickynoteDataTitle" ).hide();
                        $( this ).parent().find( "#stickynoteDataTitle_detail" ).show();
                        $( this ).parent().find( '.dataTitle' ).css ( {
                            'height': 'auto',
                            'overflow': 'auto',
                            'white-space':'normal'
                        } );
                        
                        $( this ).parent().find( "#editDeleteAnnotation" ).append( '<div class="editDeleteAnnotaionPanelContainer"><div class="annotaionEditDiv"></div><div class="annotaionDeleteDiv"></div></div>' );
	            		
	            		$( objComp.element ).find( '.annotaionEditDiv' ).unbind( 'click' ).bind( 'click' , function ( e ) 
	            		{
			            	//Edit Sticky Note
			            	_self.isOpenNoteForEdit = true;
			            	$( this ).parent().parent().parent().find( '.annotationData' ).trigger( "click" , true );
			            });
			            /*------------------------------------------------------------------------------------------------------------------------ */
			           
			           /*-----------------------------------------Left Panel annotation delete event ---------------------------------------------- */
			           $( objComp.element ).find( '.annotaionDeleteDiv' ).unbind( 'click' ).bind( 'click' , function ( e ) 
			           {
			            	//Delete Sticky Note
			            	switch ( $( this ).parent().parent().parent().attr( 'type' ) ) 
			            	{
		                            case "highlightData":
		                            
			                                var currentListId = $( this ).parent().parent().parent().attr( 'id' ).replace( 'highlightData_' , '' );
			                                _self.deleteFromPanel = true;
			                                $( objComp.element ).trigger( 'deleteHighlightData' , currentListId );
			                                
		                                break;
		                            case "stickynoteData":
		                            
			                                var currentListId = $( this ).parent().parent().parent().attr( 'id' ).replace( 'stickynoteData_' , '' );
			                                _self.deleteFromPanel = true;
			                                _self.openDeleteConfirmPanel( "btnStickyNote_" + currentListId );
			                                
		                                break;
		                            default:
							}	
			            });
			            /*------------------------------------------------------------------------------------------------------------------------ */
	            	}
	            	else
	            	{
	            		$( this ).parent().find( "#stickynoteDataTitle" ).show();
                        $( this ).parent().find( "#stickynoteDataTitle_detail" ).hide();
                        $( this ).parent().find( '.editDeleteAnnotaionPanelContainer' ).remove();
                        $( this ).html( GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] );
                        $( this ).addClass( 'showDetail' );
                        $( this ).parent().find( '[class=dataTitle]' ).css( {
                            'height': '18px',
                            'overflow': 'hidden',
                            'white-space':'nowrap'
                        } );
	            	}

	            	$(objComp.element).data( 'annotationPanelComp' ).updateViewSize();
	            });
	            /*------------------------------------------------------------------------------------------------------------------------ */
	           
	      	});
	      	
	      	var notesTab = $( objComp.element ).find( '[ id = "annotationMyNotesTab" ]' );
	      	var qaTab = $( objComp.element ).find( '[ id = "annotationMyQuestionsTab" ]' );
	      	/*-----------------------------------------Left Panel NotesTab click event ---------------------------------------------- */
	      	notesTab.unbind( 'click' ).bind( 'click' , function ( ) 
	      	{
	      		if( notesTab.hasClass( 'leftPanelTabActive' ) )
	      		{
	      			return;
	      		}
                //annotationAllTab
                notesTab.addClass( 'leftPanelTabActive' ).removeClass( 'leftPanelTab' );
                qaTab.addClass('leftPanelTab').removeClass('leftPanelTabActive');
                
                $(objComp.element).find('[id=addedStickyNotesList]').css('display', 'block');
                $(objComp.element).find('[id=addedQuestionsList]').css('display', 'none');
                $($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");
                if ($.trim( $(addedNotes).html() ) == "") 
                {
                    $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "block");
                    $(objComp.element).find('[id=annotationSortBy]').css('display', 'none');
                } 
                else 
                {
                    $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "none");
                    $(objComp.element).find('[id=annotationSortBy]').css('display', 'block');
                    if (EPubConfig.My_Notebook_isAvailable)
	                {
	                    $(objComp.element).find('[id=externalURLLink]').css('display', 'block');
	                }
	                
                }
                $(objComp.element).data( 'annotationPanelComp' ).onPanelOpen();
                $(objComp.element).data( 'annotationPanelComp' ).updateViewSize();
            });
	      /*------------------------------------------------------------------------------------------------------------------------ */
	     	
    	break;
    }
};
	
/*-------------------------------------------------------------- Handlers start--------------------------------------------------------------------------*/

MG.NoteOperator.prototype.addListeners = function ()
{
	var self = this;
	$( window ).bind( 'resize', function() 
	{
        if ( navigator.userAgent.match(/(android)/i) && ( $( "#stickyNoteContainer" ).css( "display" ) == "block" ) ) 
        {
            self.adjustPopupOnResize();
        }
    } );
    
    $( window ).bind( 'orientationchange', function() 
    {
    	document.onselectionchange = null;
        $("#annotation-bar").css("display","none");
        $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
        $("#inputToRemove").focus();
        $("#inputToRemove").blur();
        $("#inputToRemove").remove();
        
    } );
    
    self.bindEventsOfPopup();
};

MG.NoteOperator.prototype.scrollViewRenderCompleteHandler = function( objComp )
{
	var self = this;
    var _arrPageComps = $( objComp.element ).find( '[ data-role = "pagecomp" ]' );
    var _objPageRole = null;
    var index = 0;
    
    for ( var i = 0; i < _arrPageComps.length; i++ ) 
    {
        _objPageRole = $( _arrPageComps[ i ] ).data( "pagecomp" );

        $( _objPageRole ).bind( "createHighlightOnPage", function( e ) 
        {
            for( var i = 0; i < GlobalModel.annotations.length; i++ )
            {
            	if ( GlobalModel.annotations[ i ].type == 'stickynote' ) 
            	{
	            	index = self.scrollViewRef.member.curPageElementsPgBrk.indexOf( GlobalModel.annotations[ i ].pageNumber );
	            	
	            	if ( EPubConfig.pageView == 'doublePage' ) 
	            	{
	                    if ( GlobalModel.currentPageIndex == index || index == GlobalModel.currentPageIndex + 1 ) 
	                    {
	                        self.createNoteHighlight( GlobalModel.annotations[ i ].id, GlobalModel.annotations[ i ].range );
	                    }
	                }
	            	else if( GlobalModel.currentPageIndex == index )
	            	{
	            		self.createNoteHighlight( GlobalModel.annotations[ i ].id, GlobalModel.annotations[ i ].range );
	            	}
	           	}
            }
        } );
        
        $( _objPageRole ).bind( "addEventOnStickyNoteIcon", function ()
        {
            self.addEventOnIcon();
        } );
	}
};

MG.NoteOperator.prototype.createNoteHighlight = function ( strNoteSelectionID, objRange )
{
	var objThis = this;
    var rangedata;
    setTimeout( function() {

        var Obj = $.xml2json( objRange );
        var startC = $( '[id = "' + Obj.startContainer + '"]' ).find( '#mainContent' )[ 0 ];
        var startO = parseInt( Obj.startOffset );
        var endO = parseInt( Obj.endOffset );

        var savedSel = {
            start : startO,
            end : endO
        };

        objThis.addEventOnHighlightAndIcon( strNoteSelectionID );
        
        try {
            rangedata = objThis.restoreSelection( $( startC )[ 0 ], savedSel );
            rangy.init();
            var cssClassApplier = rangy.createCssClassApplier( "noteselection_" + strNoteSelectionID );
            var cssClassAppliercolor = rangy.createCssClassApplier( "noteselection" );
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( rangedata );
            if ( $( startC ).find( ".noteselection_" + strNoteSelectionID )[ 0 ] == undefined ) {
                cssClassApplier.toggleSelection();
                cssClassAppliercolor.toggleSelection();
            }
            $( ".noteselection_" + strNoteSelectionID ).attr( "note-id", strNoteSelectionID );
        } catch(e) {
				console.log( e.message );
        }

    }, 0);
};

MG.NoteOperator.prototype.addEventOnHighlightAndIcon = function ( id )
{
	var self = this;
	setTimeout( function() {
		$( ".noteselection_" + id ).unbind( 'click' ).bind( 'click', function( e ) 
	    {
	    	var element = $( $( e.target )[ 0 ] );
	
	    	if( ( element.closest( "[type = glossterm]" ).length > 0 ) || ( element.closest( "[type = footnote]" ).length > 0 ) ||
	    	  ( element.closest( "[type = internal-link]" ).length > 0 ) || ( element.closest( "[data-ajax = false]" ).length > 0 ) )
	    	{
	    		return;
	    	}
	    	if( GlobalModel.hasDPError( "notes" ) )
	    	{
	    		return;
	    	}
	        e.stopPropagation();
	        // TO DO implement click for icon also
	        $( "#btnStickyNote_" + id ).trigger ( "click" );
		} );
	}, 100);
	self.addEventOnIcon();
	
	if ( self.noteToEdit != -1 && self.noteToEdit == id) 
    {	    	
    	if( $( "#btnStickyNote_" + self.noteToEdit ).length > 0 )
    	{
    		$( "#btnStickyNote_" + self.noteToEdit ).trigger( "click" );
    		self.noteToEdit = -1;
    	}
    }
	
    window.getSelection().removeAllRanges();
};

MG.NoteOperator.prototype.addEventOnIcon = function ()
{
    var self = this;
    /* Icon Click Handler */
	$( ".btnStickyNoteHotspot" ).unbind( 'click' ).bind( 'click', function( evt ) 
	{
		self.notePopup.showPopup( false, $( evt.target ).attr( "id" ), self.isOpenNoteForEdit );
		if ( navigator.userAgent.match(/(android)/i) ) 
        {
            self.adjustPopupOnResize();
        }
		self.isOpenNoteForEdit = false;
	} );
};

MG.NoteOperator.prototype.restoreSelection = function( containerEl, savedSel ) 
{
    var charIndex = 0, range = document.createRange();
    range.setStart( containerEl, 0 );
    range.collapse( true );
    var nodeStack = [ containerEl ], node, foundStart = false, stop = false;
    while ( !stop && ( node = nodeStack.pop() ) ) 
    {
        if ( node.nodeType == 3 ) 
        {
            var nextCharIndex = charIndex + node.length;

            if ( !foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex ) 
            {
                range.setStart( node, savedSel.start - charIndex );
                foundStart = true;

            }
            if ( foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex ) 
            {
                range.setEnd( node, savedSel.end - charIndex );
                stop = true;
            }
            charIndex = nextCharIndex;
        } 
        else 
        {
            var i = node.childNodes.length;
            while ( i-- ) 
            {
                nodeStack.push( node.childNodes[ i ] );
            }
        }
    }

    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange( range );
    return range;
};

MG.NoteOperator.prototype.noteAnnoIconClickHandler = function( event )
{
	var self = this;
	
	addNoteHighlight = function ()
	{
		if ( self.scrollViewRef.options.rangedata ) 
        {
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( self.scrollViewRef.options.rangedata );
        }
        rangy.init();
        rangy.createCssClassApplier( "noteselection_" + GlobalModel.totalStickyNotes ).toggleSelection();
        rangy.createCssClassApplier( "noteselection" ).toggleSelection();	
	};
	
	if( GlobalModel.hasDPError( "notes" ) )
	{
		return;
	}

	var iPageNum = self.scrollViewRef.options.pageIndexForAnnotation;
	var _arrStickyNotesOnPage = $( "#PageContainer_" + iPageNum ).find( '.btnStickyNoteHotspot' );

    if ( _arrStickyNotesOnPage.length < EPubConfig.Notes_Per_Page || EPubConfig.Notes_Per_Page < 1 ) 
    {    
    	var offset = $( event.currentTarget ).offset();
    	var arrPos = new Object();
    	var lastAddedIcon = null;
    	
        arrPos.top = this.scrollViewRef.options.Icontop;
        if ( arrPos.top < 0 ) 
        {
           arrPos.top = event.pageY;
        }
        arrPos.left = this.scrollViewRef.options.Iconleft;
        arrPos.contentWidth = this.scrollViewRef.options.contentWidth;
        this.setElementPosition( offset, arrPos );
        addNoteHighlight();
        var newComponentDiv = '<div id="btnStickyNote" type="stickyNoteComp" style="position:absolute;"' + 
        					  ' class="btnStickyNoteHotspot" moveable="true" authorable="true" ' + 
        					  ' data-options={"expandedWidth":"51","expandedHeight":"50","width":"43","height":"41"}>' + '</div>';
    	var pageNum = this.scrollViewRef.options.pageIndexForAnnotation;
		$( "#bookContentContainer2" ).find( '[id = "PageContainer_' + pageNum + '"]' ).append( newComponentDiv );
		lastAddedIcon = $( "#btnStickyNote" );
		lastAddedIcon.css( "top", CompModel.yPos );
		lastAddedIcon.css( "left", CompModel.xPos );
		
		var isContainerScroll = self.adjustPageBeforeNoteAdd( lastAddedIcon, lastAddedIcon.css( 'left' ), lastAddedIcon.css( 'top' ) );
		if ( isContainerScroll == true ) 
		{
            objThis.member.isContainerScrollBool = true;
        } 
        else 
        {
            objThis.member.isContainerScrollBool = false;
            
            if ( ismobile )
            {
            	document.onselectionchange = null;
                $("#annotation-bar").css("display","none");
                $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
                $("#inputToRemove").focus();
                $("#inputToRemove").blur();
                $("#inputToRemove").remove();   
            }
            setTimeout( function () { 
            	if ( navigator.userAgent.match(/(android)/i) ) 
		        {
		            self.adjustPopupOnResize();
		        }
            	self.notePopup.showPopup( true, lastAddedIcon.attr( "id" ) );
            	 }, 0 );
        }
    }
    else
    {
    	PopupManager.removePopup();
    	window.getSelection().removeAllRanges();
    	$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
        $("#inputToRemove").focus();
        $("#inputToRemove").blur();
        $("#inputToRemove").remove();
    	$( "#alertTxt" ).html( GlobalModel.localizationData[ "MAX_NOTE_ANNOTATION_ALLOWED_PER_PAGE" ] );
        PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        } );
    }		
	// to do
	//add icon, position it, add click event on it, show popup
};

MG.NoteOperator.prototype.bindEventsOfPopup = function ()
{
	var self = this;
	$( self.notePopup ).bind( "showDeleteConfirmationPopup", function ( event, currentNoteID )
	{
		self.openDeleteConfirmPanel( currentNoteID );
	} );
	
	$( self.notePopup ).bind( "adjustPopupForAndroid", function ( event )
	{
		if ( navigator.userAgent.match(/(android)/i) ) 
        {
            self.adjustPopupOnResize();
        }
	} );
	
	/* Save new note. This event is dispatched from NotePopup */
	$( self.notePopup ).bind( "saveNewNote", function ( event, data )
	{
		self.createNewNote( data );
		GlobalModel.totalStickyNotes++;
	} );
	
	/* Modify already existing note. This event is dispatched from NotePopup */
	$( self.notePopup ).bind( "updateExistingNote", function ( event, data )
	{
		self.modifyNote( data );
	} );
	
	
	$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationSuccess" ).bind( "click", function ( event )
	{
		var len = GlobalModel.annotations.length;
		var id = self.currentNoteID;
		var annotation = null;
		
		id = id.replace( "btnStickyNote_", "" );
		if( $( "#" + self.currentNoteID ).length > 0 )
		{
			$( ".noteselection_" + id ).removeClass( "noteselection" ).removeClass( "noteselection_" + GlobalModel.totalStickyNotes );
			$( "#" + self.currentNoteID ).remove();
		}
		
		PopupManager.removePopup();
		for( var i = 0; i < len; i++ )
		{
		    annotation = GlobalModel.annotations[ i ];
			if( id == annotation.id && annotation.type == "stickynote" )
			{
			    if ( ( annotation.annotationID != undefined ) &&
			       ( annotation.annotationID != 0 ) ) 
                {
				    ServiceManager.Annotations.remove( annotation.annotationID, 3 );
				}
				GlobalModel.annotations.splice( i, 1 );
				break;
			}
		}
		if( self.deleteFromPanel == true )
		{
			$( $("#bookContentContainer" ).data( 'scrollviewcomp' ) ).trigger( 'closeOpenedPanel' );
		}
		self.deleteFromPanel = false;
	} );
	
	$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationFailure" ).bind( "click", function ( event )
	{
		PopupManager.removePopup();
		
		if( $( "#" + self.currentNoteID ).length > 0 && self.deleteFromPanel != true )
		{
			if ( navigator.userAgent.match(/(android)/i) ) 
	        {
	            self.adjustPopupOnResize();
	        }
			PopupManager.addPopup( $( "#stickyNoteContainer" ), $( "#" + self.currentNoteID ), {
		        isModal : true,
		        hasPointer : true,
		        offsetScale : GlobalModel.currentScalePercent,
		        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
		        isTapLayoutDisabled : true
			} );
		}
		self.deleteFromPanel = false;
	} );
};

MG.NoteOperator.prototype.editNote = function ( noteId , pageNum )
{
	var self = this;
	var scrollviewCompRefnce = $( "#bookContentContainer" ).data( 'scrollviewcomp' );
	var pageIndex = scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf( String( pageNum ) );
	var isNoteVisible = false;
	if ( EPubConfig.pageView == 'doublePage' )
	{
        isNoteVisible = ( pageIndex == GlobalModel.currentPageIndex || pageIndex == GlobalModel.currentPageIndex + 1 ) ? true : false;
    }
    else
    {
        isNoteVisible = ( pageIndex == GlobalModel.currentPageIndex ) ? true : false;
	}

    if ( isNoteVisible ) 
    {
    	self.noteToEdit = -1;
    	$( "#btnStickyNote_" + noteId ).trigger( "click" );
    }
    else
    {
    	self.noteToEdit = noteId;
    	scrollviewCompRefnce.showPageAtIndex( GlobalModel.pageBrkValueArr.indexOf( String ( pageNum ) ) );
    }
	
};

MG.NoteOperator.prototype.modifyNote = function ( data )
{
	var self = this;
	var obj = {};
	var objData = {};
	obj = self.findNoteObjectInGlobalModel( data.id );
	objData.stickyContent = obj.title = data.title;
    objData.savetonote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    obj.date = self.getDate();
    obj.timeMilliseconds = new Date();
    objData.tagsLists = obj.tagsLists = data.tagsLists;
    objData.contentID = obj.id;
    objData.title = "";
    objData.pagenum = objData.objectID = obj.pageNumber;
    objData.rangeObject = obj.range;
    objData.annotation_id = obj.annotationID;

    if ( ( objData.annotation_id != undefined ) && ( objData.annotation_id != 0 ) ) 
    {
        ServiceManager.Annotations.update( 3, objData );
    }
};

MG.NoteOperator.prototype.findNoteObjectInGlobalModel = function ( id )
{
	for( var i = 0; i < GlobalModel.annotations.length; i++ )
	{
		if( GlobalModel.annotations[ i ].id == id && ( GlobalModel.annotations[ i ].type == "stickynote" ) )
		{
			return GlobalModel.annotations[ i ];
		}
	}
};

MG.NoteOperator.prototype.createNewNote = function ( data )
{
	var self = this;
	var obj = {};
	var objData = {};
	var newIcon = $( "#btnStickyNote" );
	objData.pagenum = obj.pageNumber = self.scrollViewRef.options.pageIndexForAnnotation;
	objData.tagsLists = obj.tagsLists = data.tagsLists;
	objData.savetoNote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
	objData.rangeObject = obj.range = self.scrollViewRef.options.rangeObject;
	objData.icontop = obj.icontop = parseInt( newIcon.css( "top" ) );
	objData.iconleft = obj.iconleft = parseInt( newIcon.css( "left" ) );
	objData.title = "";
	obj.date = self.getDate();
	obj.type = "stickynote";
    obj.timeMilliseconds = new Date();
    objData.id = obj.id = ( GlobalModel.totalStickyNotes ).toString();
    objData.stickyContent = obj.title = data.title;
		
    GlobalModel.annotations[ GlobalModel.annotations.length ] = obj;

	newIcon.attr( 'id', 'btnStickyNote_' + GlobalModel.totalStickyNotes );
	newIcon.attr( 'associated-page-number', obj.pageNumber );
	
	self.addEventOnHighlightAndIcon ( objData.id );
	
	/* Send call to Service Manager to save the data on Server */
    ServiceManager.Annotations.create( objData, 3, function( data ) 
    {
        for ( var i = 0; i < GlobalModel.annotations.length; i++ ) 
        {
            if ( ( objData.id == GlobalModel.annotations[ i ].id ) && ( GlobalModel.annotations[ i ].type == "stickynote" ) ) 
            {
                if( data.length == undefined )
				{
					GlobalModel.annotations[ i ].annotationID = data.annotation_id;
				}   
				else
				{
					GlobalModel.annotations[ i ].annotationID = data[0].annotation_id;
				}                                                                         
                // TO DO update class of note icon and icon as well  i.e. noteselection_annotationID. Bind event also
                break;
            }
        }
    } );
};

MG.NoteOperator.prototype.getDate = function ()
{
	var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var dateOfCreation = ( ( '' + month ).length < 2 ? '0' : '' ) + month + '/' + ( ( '' + day ).length < 2 ? '0' : '' ) + 
    					 day + '/' + year.toString().substr( 2, 2 );
    return dateOfCreation;
};

MG.NoteOperator.prototype.openDeleteConfirmPanel = function ( currentNoteID )
{
	var self = this;
	self.currentNoteID = currentNoteID;
	
	PopupManager.removePopup();
	$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationTxt" ).html( GlobalModel.localizationData[ "STICKY_NOTE_DELETE_CONFIRMATION_TEXT_NOTEBOOK" ] );
	PopupManager.addPopup( $( "#dltConfirmPanel.noteDeleteConfimPanel" ), null, {
        isModal : true,
        isTapLayoutDisabled : true,
        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
        isCentered : true

    } );
};


/*---------------------------------------------------------- Handlers end ------------------------------------------------------------------------------*/

MG.NoteOperator.prototype.setElementPosition = function( offset, arrPos )
{
    this.contentWidth = arrPos.contentWidth;
    var scrollTopElement = $( "#bookContentContainer" );
    var offsetTopParent = 0;
    var pageBrkValue = this.scrollViewRef.options.pageIndexForAnnotation;
    
    CompModel.xPos = $( "#innerScrollContainer" ).width() - AppConst.NOTE_MARGIN_FROM_RIGHT;
    if ( EPubConfig.pageView == 'doublePage' || EPubConfig.pageView == 'singlePage' ) 
    {
        CompModel.xPos = arrPos.contentWidth - AppConst.NOTE_MARGIN_FROM_RIGHT;
        if( EPubConfig.ReaderType.toUpperCase() == '6TO12' )
        {
        	offsetTopParent = 50;
        }
        
        CompModel.yPos = ( arrPos.top / GlobalModel.currentScalePercent ) + ( $( scrollTopElement ).scrollTop() / GlobalModel.currentScalePercent ) - 
        				 offsetTopParent / GlobalModel.currentScalePercent;
    } 
    else 
    {       
        offsetTopParent = $( "#bookContentContainer2" ).find( '[id = "PageContainer_' + pageBrkValue + '"]' ).offset().top / GlobalModel.currentScalePercent;
        CompModel.yPos = ( arrPos.top / GlobalModel.currentScalePercent ) + 
        				 ( $( scrollTopElement ).scrollTop() / GlobalModel.currentScalePercent ) - offsetTopParent;
    }
    
    var arrStickyNotePositionsInCurrentPage = [];
    var sortedTopPos = [];
    var objStickyNotesInCurrentPage = $( '[id = "PageContainer_' + pageBrkValue + '"]' ).find( '[type = "stickyNoteComp"]' );
    var elementHeight = $( objStickyNotesInCurrentPage[ 0 ] ).height();
    
    if ( objStickyNotesInCurrentPage && objStickyNotesInCurrentPage.length > 0 ) 
    {
        for ( var iCount = 0; iCount < objStickyNotesInCurrentPage.length; iCount++ ) 
        {
            arrStickyNotePositionsInCurrentPage.push( parseInt( $( objStickyNotesInCurrentPage[ iCount ] ).css( "top" ) ) );
        }

        sortedTopPos = ( arrStickyNotePositionsInCurrentPage ).sort( function( a, b ) 
        {
            return a - b;
        } );
    }
	
	var bFirstTime = true;
    for ( var i = 0; i < sortedTopPos.length; i++ ) 
    {
        if ( CompModel.yPos  <= ( sortedTopPos[ i ] + elementHeight + 2 ) ) 
        {
        	if ( bFirstTime == true ) 
        	{
                if ( (sortedTopPos[i] - CompModel.yPos ) > ( elementHeight + 2 ) ) 
                {
                    CompModel.yPos = CompModel.yPos;
                    break;
                }
                bFirstTime = false;
            }
            
            if ( sortedTopPos[ i + 1 ] != undefined ) 
            {
                if ( ( sortedTopPos[ i + 1 ] - sortedTopPos[ i ] ) > 2 * elementHeight ) 
                {
                    CompModel.yPos = sortedTopPos[ i ] + elementHeight + 2;
                    break;
                }
            } 
            else 
            {
                CompModel.yPos = sortedTopPos[ i ] + elementHeight + 2;
            }
        }
    }   
    CompModel.yPos = parseInt( CompModel.yPos ); 
};

MG.NoteOperator.prototype.adjustPageBeforeNoteAdd = function( launcher, xPos, yPos ) 
{
    var isContainerScroll = false;
    var container = null;
    if ( EPubConfig.pageView != 'doublePage' ) 
    {
        yPos = parseInt( yPos );
        var stickyNotes = $( $( "#innerScrollContainer" )[ 0 ] ).find( '[type = stickyNoteComp]' )[ 0 ];
        var scrollby = $( launcher ).offset().top + $( "#bookContentContainer2" ).scrollTop() - 
        			   $( window ).height() + $( "#header" ).height() + $( stickyNotes ).height();
        
        var isScroll = $( launcher ).offset().top - $( window ).height();
        var isContainerScroll = false;
        // if stickynote is note in the visible screen area (i.e isScroll < 0), and scrollby value is greater than 0, the screen will shift upwards.
        if ( $( launcher ).offset().top < 0) 
        {
            var curScrollTopVal = $( "#bookContentContainer2" ).scrollTop();
            if ( EPubConfig.pageView == 'singlePage' )
            {
            	container = $( "#bookContentContainer" );
            }
            else
            {
            	container = $( "#bookContentContainer2" );
            }
            container.scrollTop( curScrollTopVal + $( launcher ).offset().top );
            isContainerScroll = true;
        } 
        else 
        {
            if ( scrollby > 0 && ( isScroll > 0 ) ) 
            {
            	container.scrollTop( scrollby );
                isContainerScroll = true;
            } 
            else 
            {
                isContainerScroll = false;
            }
        }
    }
    return isContainerScroll;
};

MG.NoteOperator.prototype.adjustPopupOnResize = function() 
{
		var self = this;
        if ( ( window.height < window.width ) && ( window.innerHeight < (this.androidLandscapeHeight - 100) ) &&
         ( document.activeElement.id == "stickynoteTextArea" || document.activeElement.id == "tagInputBox" ) ) 
        {
            isPopupLayoutChanged = true;
            var launcher = PopupManager.objLauncherHotspot;
            var launcherOffsetLeft = $(launcher).offset().left;
            var launcherOffsetTop = $(launcher).offset().top;
            var bkcontainerOffset = $("#bookContentContainer").offset().left;
            var stickNoteContainerHeight = window.innerHeight - 5;
            var stickyNoteContainerWidth = launcherOffsetLeft - bkcontainerOffset - 15;
           
            $("#stickyNoteContainer").height( stickNoteContainerHeight );
            $("#stickyNoteContainer").css("max-width", ( parseInt($("#bookContentContainer").width()) - 10 ));
            $("#stickyNoteContainer").css("max-height", stickNoteContainerHeight );
            $("#stickyNoteContainer").css("width", stickyNoteContainerWidth );
            $("#stickyNoteContainer").addClass('noteContainerLayoutchanged');
            $("#stickynoteTextArea").addClass('stickynoteTextAreaLayoutchanged');
            $("#stickynoteTextArea").css('width', stickyNoteContainerWidth - 200 );
            $("#stickynoteDeleteBtn").addClass('stickynoteBtnLayoutchanged');
            $("#stickynoteSaveBtn").addClass('stickynoteBtnLayoutchanged');
            $("#stickyNoteContainer .savetonotebook").addClass('savetonotebookLayoutchanged');
            $(".stickyNoteTags").attr('type', 'stickyNoteTagsLayoutChanged');
            $(".stickyNoteTags").css('width', $("#stickynoteTextArea").width());
            $('.tagBorder').addClass('displayNone');
            $(".tagList").attr('type', 'tagListLayoutChanged');
            
            $(document).on('touchmove', function(e) {
                if (isPopupLayoutChanged) {
                    e.preventDefault();
                }
            });
            
            if( $(".stickyNoteTags").css( "display" ) != "block" )
            {
            	$("#stickynoteTextArea").css( 'height', ( stickNoteContainerHeight - 50 - 20 - 10 ) + "px" );
            }
            else
            {
            	$("#stickynoteTextArea").css( 'height', ( stickNoteContainerHeight - 30 - 20 - 10 - $(".stickyNoteTags").height() ) + "px" );
            }
            
            setTimeout(function() {
                $(window).scrollTop(launcherOffsetTop - (window.innerHeight / 2));
                $("#stickyNoteContainer").css('position', 'fixed');
                $("#stickyNoteContainer").css('top', 0);
                $("#stickyNoteContainer").css('left', bkcontainerOffset);
                PopupManager.positionArrow(true);
            }, 300);
        } else {
        	$("#stickyNoteContainer").css("max-height", "initial" );
            $("#stickyNoteContainer").css('position', 'absolute');
            
            isPopupLayoutChanged = false;
            if(window.height < window.width){
                $("#stickynoteTextArea").css('height', '176px');
                $("#stickyNoteContainer").height("auto");
            }
            else{
                if(isPopupFirstTime){
                    $("#stickynoteTextArea").css('height', '176px');
                    $("#stickyNoteContainer").height("auto");
                    isPopupFirstTime = false;
                }
            }
            $("#stickynoteTextArea").css('width', '304px');
            $("#stickyNoteContainer").removeClass('noteContainerLayoutchanged');
            $("#stickynoteTextArea").removeClass('stickynoteTextAreaLayoutchanged').removeClass('noteAreaShrinkLayoutchanged');
            $("#stickynoteDeleteBtn").removeClass('stickynoteBtnLayoutchanged');
            $("#stickynoteSaveBtn").removeClass('stickynoteBtnLayoutchanged');
            $(".savetonotebook").removeClass('savetonotebookLayoutchanged');
            $(".stickyNoteTags").attr('type', 'stickyNoteTags');
            $("#stickyNoteContainer").css("width", '345px');
            $(".stickyNoteTags").css('width', "345px");
            $('.tagBorder').removeClass('displayNone');
            $(".tagList").attr('type', 'tagList');
            PopupManager.positionPopup(true);
            setTimeout(function() {
                $(window).scrollTop(0);
            }, 300);
        }
};

