/**
 * @author: lakshay.ahuja
 */
(function($, undefined) {
    $.widget("magic.tableInteractivity", $.magic.magicwidget, {

        options : {
            userResponse : [],
            interactivityType : "tableInteractivity",
            parentgrpId : null,
            xmlContent : null,
            currentResponse : []
        },

        member : {
            questionArray : [],
            questionArrData : null,
            questionInnercontent : null
        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            this.options.parentgrpId = questionManagerRef.member.lastGrpXMLLoaded;
            if (GlobalModel.questionPanelAnswers[this.options.parentgrpId] == null)
                GlobalModel.questionPanelAnswers[this.options.parentgrpId] = {};
        },

        _create : function() {
        },

        setQAPanelInnercontentLayout : function(strQAContent, questionInnerContent) {
            var objThis = this;
            var numberOfHeading;
            $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelMultipleChoiceOptions");
            if (strQAContent.itemBody.div == undefined || strQAContent.itemBody.div.p == undefined) {
                numberOfHeading = 0;
                $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                $(questionInnerContent).find("#questionPanelSectionContent").html('');
            } else if (strQAContent.itemBody.div.p.length == undefined && typeof strQAContent.itemBody.div.p == "object") {
                numberOfHeading = 1;
            } else {
                numberOfHeading = strQAContent.itemBody.div.p.length;
            }
            for (var k = 0; k < numberOfHeading; k++) {
                if (numberOfHeading == 1) {
                    switch(strQAContent.itemBody.div.p.label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div.p.span);
                            $(questionInnerContent).find("#questionPanelSectionContent").html('');
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div.p.span);
                            $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                            break;
                    }
                } else {
                    switch(strQAContent.itemBody.div.p[k].label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div.p[k].span);
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div.p[k].span);
                            break;
                    }
                }
            }
            if (strQAContent.itemBody.p) {
                $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.p);
            } else {
                $(questionInnerContent).find('[id=theQuestionAsked]').html('');
            }
            $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptionsText").addClass("questionPanelOptions");
            $(questionInnerContent).find('[id=questionImage]').css("display", "none");
            $(questionInnerContent).find('[id=questionPanelOptions]').html(strQAContent.itemBody.blockquote);
            $(questionInnerContent).find('[id=questionPanelOptions]').find('table').attr('id', 'QAPanelTable');
            $(questionInnerContent).find('[id=questionPanelOptions]').find('tr:even').addClass('evenBlocks');
            $(questionInnerContent).find('textentryinteraction').replaceWith('<input class="qaPanelTableInput" type="text"  maxlength="25"/>');

            /*******************************
             * setting CSS
             */

            $(questionInnerContent).find("#questionPanelQuestionContent").css('width', '30%');
            //var tableParentWidth = $("#questionPanelOptions").outerWidth();
            $(questionInnerContent).find("#QAPanelTable").css('width','65%');
            var tableQAPanelWidth = $(questionInnerContent).find("#QAPanelTable").width();
            var max = -1;
            $(questionInnerContent).find("#QAPanelTable").find('tr').each(function() {
                var tdLength = $(this).find('td').length;
                //console.log(tdLength);
                max = tdLength > max ? tdLength : max;
                //console.log(max);
            });
            //console.log("Max is: ", max);
            var tableDataLength = $("#QAPanelTable").find('td').length;
            $(questionInnerContent).find("#QAPanelTable").find('td').css('max-width', ((tableQAPanelWidth - max * 4) / (max)));
            var QAPanelinputWidth = $("#QAPanelTable").find('.qaPanelTableInput').outerWidth();
            //console.log("QAPanelinput: ",QAPanelinput,$("#QAPanelTable").find('.qaPanelTableInput'));
            $(questionInnerContent).find("#QAPanelTable").find('td').css('width', QAPanelinputWidth);
            var paddingLeft = (((tableQAPanelWidth - max * 4) / (max)) - QAPanelinputWidth)/2;
            if (paddingLeft < 12)
                $(questionInnerContent).find("#QAPanelTable").find('td').css('padding-left', paddingLeft);

            $(questionInnerContent).find("#QAPanelTable").find('td').each(function() {
                var tdRef = this;
                if($(this).find('input').length > 0){
                    $(tdRef).css('padding-left',paddingLeft);
                }
            });
            
            var tableRowLength = $("#QAPanelTable").find('tr').length;
            $(questionInnerContent).find("#QAPanelTable").find('tr').height((190 / tableRowLength));
            //#QAPanelTable td
            //qaPanelTableInput
            //console.log(tableParentWidth, tableDataLength, tableParentWidth / tableDataLength);
            //console.log($("#questionPanelOptions").outerWidth());

            //evenBlocks
        },

        populatePrintPanelForInteractivity : function(strQAContent, printableInnerContent,arrObj) {
            var objThis = this;
            objThis.setQAPanelInnercontentLayout(strQAContent, printableInnerContent);
           $(printableInnerContent).find('[id=questionPanelOptions]').find('table').css("width","700px")
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').attr('cellpadding', '4');
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').attr('cellspacing', '1');
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').css("left", "12px");
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').css("margin-right", "25px");
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').css("bottom", "15px");
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').css("position", "relative");
            $(printableInnerContent).find('input').attr('disabled', 'disabled');
            $(printableInnerContent).find('input').css('width', '220px');
            // console.log( $(printableInnerContent).find("#QAPanelTable")[0]);
            // var tableRowLength = $(printableInnerContent).find("#QAPanelTable").find('tr').length;
            // $(printableInnerContent).find("#QAPanelTable").find('tr').height((190 / tableRowLength))
            // $(printableInnerContent).find("#questionPanelQuestionContent").css('width', '30%');
            // $(printableInnerContent).find("#questionPanelOptions").css('width', '60%');
            // var tableParentWidth = $("#questionPanelOptions").outerWidth();
            // //console.log("tableParentWidthtableParentWidthtableParentWidth :  ", tableParentWidth);
            // $(printableInnerContent).find("#QAPanelTable").width(tableParentWidth);
            //console.log( $(printableInnerContent).find("#QAPanelTable"));
            var max = -1;
            var tableDataLength = $("#QAPanelTable").find('td').length;
            var tblWidth = $(printableInnerContent).find('[id=questionPanelOptions]').find('table').width();
            var tdsInaRows = $($("#QAPanelTable").children('tbody').children('tr')[1]).children('td').length;
            var maxWidth = tblWidth/tdsInaRows; 
            $(printableInnerContent).find('[id=questionPanelOptions]').find('table').find('td').css('max-width',maxWidth)
            //$(printableInnerContent).find("#QAPanelTable").find('td').css('max-width', (tableParentWidth / tableDataLength));
            var elemId = arrObj.interid;
            var parentGrpIdforPrint = arrObj.interParentid;
            //console.log(elemId + "    " + parentGrpIdforPrint);
            var objData = GlobalModel.questionPanelAnswers[parentGrpIdforPrint][elemId];
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");
                //console.log("objThis.options.userResponse: ",objThis.options.userResponse,objThis.options.userResponse.length);
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    //console.log($(printableInnerContent).find('.qaPanelTableInput')[i]);
                    var fillUpResponse = $($(printableInnerContent).find('.qaPanelTableInput')[i]).val(objThis.options.userResponse[i]);
                }
                var flag = -1;
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    if (objThis.options.userResponse[i] != "") {
                        flag = 0;
                        break;
                    }
                }
            }
        },

        populateQAPanelInnerContent : function(strQAContent, questionInnerContent) {
            var objThis = this;
            try {
                objThis.member.questionInnercontent = questionInnerContent;
                objThis.options.xmlContent = strQAContent;
                objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                objThis.setSavedUserResponse();
            } catch(e) {

            }
        },

        saveCurrentResponse : function() {
            this.setUserResponse();
        },

        setUserResponse : function() {
            var objThis = this;
            objThis.options.userResponse = [];
            var numberOfQuestions = ($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput').length);
            for (var i = 0; i < numberOfQuestions; i++) {
                var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val());
                objThis.options.userResponse.push(fillUpResponse);
            }

            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');

        },

        setSavedUserResponse : function() {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];

            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];

            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");

                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val($.trim(objThis.options.userResponse[i])));
                }
                var flag = -1;
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    if ($.trim(objThis.options.userResponse[i]) != "") {
                        flag = 0;
                        break;
                    }
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (flag != -1) {
                    //var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                } else {
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
            }
        },

        saveCurrentResponseToServices : function() {
            var objThis = this;
            var arrObj = new Object();
            objThis.options.userResponse = [];
            var numberOfQuestions = ($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput').length);
            for (var i = 0; i < numberOfQuestions; i++) {

                var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val());
                objThis.options.userResponse.push(fillUpResponse);
            }
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            arrObj.id = questionsGrp[indexofQues];
            var reponseString = "";
            for (var i = 0; i < objThis.options.userResponse.length; i++) {
                reponseString = reponseString + objThis.options.userResponse[i];
                if (i != objThis.options.userResponse.length - 1) {
                    reponseString = reponseString + ",";
                }
            }

            var strFormattedQuestion = $(objThis.member.questionInnercontent.element).find('[id=questionPanelQuestion]').html();
            var strBodyText = "<qa><question>" + strFormattedQuestion + "</question><response>" + reponseString + "</response></qa>";

            arrObj.response = strBodyText;
            arrObj.interactivityType = objThis.options.interactivityType;
            arrObj.pageNumber = $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]').html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
            arrObj.creationDate = getTodaysDate();
            arrObj.parentGrpId = questionManagerRef.member.lastGrpXMLLoaded;

            if ($("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk')) {
                arrObj.savetonote = 1;
            } else {
                arrObj.savetonote = 0;
            }
            var tempflag = -1;
            for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                var gModelIdParent = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                if (gModelIdParent == arrObj.parentGrpId) {
                    tempflag = 1;
                    break;
                }
            }

            var flag = -1;
            for (var i = 0; i < objThis.options.userResponse.length; i++) {
                if ($.trim(objThis.options.userResponse[i]) != "") {
                    flag = 0;
                    break;
                }
            }
            var indexElem = $(objThis.element).parent().children().index($(objThis.element));
            if (flag != -1) {
                arrObj.flag_id = 1;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
            } else {
                arrObj.flag_id = 0;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
            }
            if (GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id] == null) {
                //save data
                ServiceManager.Annotations.create(arrObj, 9, function(data){
                	GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].annotation_id = data.annotation_id;
                });
            } else {

                //update data
                ServiceManager.Annotations.update(9, arrObj);
                arrObj.annotation_id = GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id].annotation_id;
            }
            GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] = arrObj;
        },

        displayHideCorrectResponse : function(bVal) {
            var objThis = this;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = [];
            var correctResponseLength = objThis.options.xmlContent.responseDeclaration.correctResponse.value.length;
            if (bVal == 1) {
                for (var i = 0; i < correctResponseLength; i++) {
                    objThis.options.currentResponse[i] = $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val();
                    correctResponse[i] = objThis.options.xmlContent.responseDeclaration.correctResponse.value[i];
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val(correctResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).attr('disabled', 'disabled');
                }
            } else {
                for (var i = 0; i < correctResponseLength; i++) {
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val(objThis.options.currentResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).removeAttr('disabled');
                }
            }
        }
    });
})(jQuery);
