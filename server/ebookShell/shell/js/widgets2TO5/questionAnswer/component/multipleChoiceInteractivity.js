/**
 * @author lakshay.ahuja
 */
(function($, undefined) {
    $.widget("magic.multipleChoiceInteractivity", $.magic.magicwidget, {

        options : {
            userResponse : null,
            interactivityType : "multipleChoiceInteractivity",
            xmlContent : null,
            parentgrpId : null
        },

        member : {
            questionArray : [],
            questionArrData : null,
            questionInnercontent : null,
            numberofResponses : 0
        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            this.options.parentgrpId = questionManagerRef.member.lastGrpXMLLoaded;
            if (GlobalModel.questionPanelAnswers[this.options.parentgrpId] == null)
                GlobalModel.questionPanelAnswers[this.options.parentgrpId] = {};
        },

        _create : function() {
        },

        setQAPanelInnercontentLayout : function(strQAContent, questionInnerContent) {
            var objThis = this;
            var numberOfHeading;
            if (strQAContent.itemBody.div[0] == undefined || strQAContent.itemBody.div[0].p == undefined) {
                numberOfHeading = 0;
                $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                $(questionInnerContent).find("#questionPanelSectionContent").html('');
            } else if (strQAContent.itemBody.div[0].p.length == undefined && typeof strQAContent.itemBody.div[0].p == "object") {
                numberOfHeading = 1;
            } else {
                numberOfHeading = strQAContent.itemBody.div[0].p.length;
            }
            for (var k = 0; k < numberOfHeading; k++) {
                if (numberOfHeading == 1) {
                    switch(strQAContent.itemBody.div[0].p.label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div[0].p.span);
                            $(questionInnerContent).find("#questionPanelSectionContent").html('');
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div[0].p.span);
                            $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                            break;
                    }
                } else {
                    switch(strQAContent.itemBody.div[0].p[k].label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div[0].p[k].span);
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div[0].p[k].span);
                            break;
                    }
                }
            }
            if (strQAContent.itemBody.choiceInteraction.prompt) {
                $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.choiceInteraction.prompt);
            } else {
                $(questionInnerContent).find('[id=theQuestionAsked]').html('');
            }
            
            $(questionInnerContent).find("#questionPanelQuestionContent").css('width', 'auto');
            //$(questionInnerContent.element).find('[id=theQuestionAsked]').html(strQAContent.itemBody.choiceInteraction.prompt);
            $(questionInnerContent).find('[id=QAfreeTextImage]').remove();
            $(questionInnerContent).find('[id=questionPanelOptions]').removeClass("questionPanelOptionsText").addClass("questionPanelOptions").addClass("questionPanelMultipleChoiceOptions");
            $(questionInnerContent).find('[id=questionImage]').css("display", "block");
            var isImgAvailable = false;
			if(strQAContent.itemBody.div[1])
			{
				if(strQAContent.itemBody.div[1].div)
				{
					if(strQAContent.itemBody.div[1].div.object)
					{
						var imgSrc = getPathManager().getQAPanelQuestionImagePath(strQAContent.itemBody.div[1].div.object.data);

            			$($(questionInnerContent).find('[id=questionImage]')[0]).find('[id=qaInnerImage]').attr("src", imgSrc);
						isImgAvailable = true;
					}
				}
			}
            if(isImgAvailable == false)
			{
				$($(questionInnerContent).find('[id=questionImage]')[0]).css('display', 'none');
			}
			else
			{
				$($(questionInnerContent).find('[id=questionImage]')[0]).css('display', 'block');
			}
			var numberOfChoices = strQAContent.itemBody.choiceInteraction.simpleChoice.length;
            if (numberOfChoices == undefined && typeof strQAContent.itemBody.choiceInteraction.simpleChoice == "Object") {
                numberOfChoices = 1;
            }
            var optionList = "";
            for (var i = 0; i < numberOfChoices; i++) {
                if (numberOfChoices == 1) {
                    optionList += "<div class='leftFloat marginB15' identifier='" + strQAContent.itemBody.choiceInteraction.simpleChoice.identifier + "'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice + "</div><div type='showHideAnswer' class='incorrectAnswer'><div id='IncorrectIcon'></div></div></div>";
                } else {
                    optionList += "<div id='multipleChoiceOption" + (i + 1) + "' class='leftFloat marginB15' identifier='" + strQAContent.itemBody.choiceInteraction.simpleChoice[i].identifier + "'><div id='chkBoxBtn' class='choiceUnselected leftFloat'></div><div id='option' class='leftFloat choiceCopy'>" + strQAContent.itemBody.choiceInteraction.simpleChoice[i] + "</div><div type='showHideAnswer' class='incorrectAnswer'><div id='IncorrectIcon'></div></div></div>";
                }
            }
            $(questionInnerContent).find('[id=questionPanelOptions]').html(optionList);
        },

        populatePrintPanelForInteractivity : function(strQAContent, printableInnerContent,arrObj) {
            var objThis = this;
            objThis.setQAPanelInnercontentLayout(strQAContent, printableInnerContent);

            var elemId = arrObj.interid;
            var parentGrpIdforPrint = arrObj.interParentid;
            var objData = GlobalModel.questionPanelAnswers[parentGrpIdforPrint][elemId];
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");

                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    var iIndex = parseInt(objThis.options.userResponse[i]);

                    $($(printableInnerContent).find('[id=chkBoxBtn]')[iIndex]).removeClass("choiceUnselected").addClass("choiceSelected");
                    $($(printableInnerContent).find('[id=option]')[iIndex]).removeClass("choiceCopy").addClass("choiceCopySelected");
                }
            }
        },

        populateQAPanelInnerContent : function(strQAContent, questionInnerContent) {
            var objThis = this;
            try {
                objThis.member.questionInnercontent = questionInnerContent;
                objThis.options.xmlContent = strQAContent;
                objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                objThis.setSavedUserResponse();
                $(questionInnerContent.element).find('[id=chkBoxBtn]').unbind('click').bind('click', function() {
                    objThis.choiceClickHandler(this);
                });
            } catch(e) {

            }
        },
        choiceClickHandler : function(selectedRef) {
            var objThis = this;

            if ($(selectedRef).hasClass("choiceUnselected")) {
                $(selectedRef).removeClass("choiceUnselected").addClass("choiceSelected");
                $($(selectedRef).parent()).find('#option').removeClass("choiceCopy").addClass("choiceCopySelected");
                var indexElem = ($(selectedRef).parent().parent().children().index($(selectedRef).parent()));
                if (!objThis.options.userResponse)
                    objThis.options.userResponse = new Array();
                objThis.options.userResponse.push(indexElem);
                objThis.member.numberofResponses++;
            } else {
                $(selectedRef).removeClass("choiceSelected").addClass("choiceUnselected");
                $($(selectedRef).parent()).find('#option').removeClass("choiceCopySelected").addClass("choiceCopy");
                var indexElem = ($(selectedRef).parent().parent().children().index($(selectedRef).parent()));
                var indexToDelete = objThis.options.userResponse.indexOf(indexElem);
                objThis.options.userResponse.splice(indexToDelete, 1)
                objThis.member.numberofResponses--;
            }
        },
        saveCurrentResponse : function() {
            this.setUserResponse();
        },

        setUserResponse : function() {
            var objThis = this;

            if (objThis.member.questionInnercontent == null)
                return;

            //objThis.options.userResponse = $(objThis.member.questionInnercontent.element).find('.choiceCopySelected');
            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');

        },
        setSavedUserResponse : function() {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];
            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];

            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");

                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    var iIndex = parseInt(objThis.options.userResponse[i]);

                    $($(objThis.member.questionInnercontent.element).find('[id=chkBoxBtn]')[iIndex]).removeClass("choiceUnselected").addClass("choiceSelected");
                    $($(objThis.member.questionInnercontent.element).find('[id=option]')[iIndex]).removeClass("choiceCopy").addClass("choiceCopySelected");
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (objThis.options.userResponse != null && objThis.options.userResponse != -1 && objThis.options.userResponse != "") {

                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                } else {
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
            }
        },

        saveCurrentResponseToServices : function() {
            var objThis = this;

            if (objThis.member.questionInnercontent == null)
                return;

            var arrObj = new Object();
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            arrObj.id = questionsGrp[indexofQues];
            var reponseString = "";
            if (objThis.options.userResponse != null) {
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    reponseString = reponseString + objThis.options.userResponse[i];
                    if (i != objThis.options.userResponse.length - 1) {
                        reponseString = reponseString + ",";
                    }
                }

                var strFormattedQuestion = $(objThis.member.questionInnercontent.element).find('[id=questionPanelQuestion]').html();
                strFormattedQuestion = strFormattedQuestion.replace("choiceSelected", "choiceUnselected");
                strFormattedQuestion = strFormattedQuestion.replace("choiceCopySelected", "choiceCopy");

                var strBodyText = "<qa><question>" + strFormattedQuestion + "</question><response>" + reponseString + "</response></qa>";

                arrObj.response = strBodyText;
                arrObj.interactivityType = objThis.options.interactivityType;
                arrObj.pageNumber = $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]').html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
                arrObj.creationDate = getTodaysDate();
                arrObj.parentGrpId = questionManagerRef.member.lastGrpXMLLoaded;

                if ($("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk')) {
                    arrObj.style = 1;
                } else {
                    arrObj.style = 0;
                }
                //var curParentId = ($("#"+arrObj.id).parent().attr('interactiongroupid'));
                var flag = -1;
                //GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj)
                for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                    var gModelIdParent = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                    if (gModelIdParent == arrObj.parentGrpId) {
                        flag = 1;
                        break;
                    }
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (objThis.options.userResponse != null && objThis.options.userResponse != -1 && objThis.options.userResponse != "") {
                    arrObj.flag_id = 1;
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                   /* if (flag == -1) {
                        var pushItem = true;
                        for (var n = 0; n < GlobalModel.questionPanelAnswersForAnnoPanel.length; n++) {
                            if (GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId == arrObj.parentGrpId) {
                                pushItem = false;
                            }
                        }
                        if (pushItem == true) {
                            GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj);
                        }
                    }*/
                } else {
                    arrObj.flag_id = 0;
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
                if (GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id] == null) {
                    //save data
	                ServiceManager.Annotations.create(arrObj, 9, function(data){
	                	GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].annotation_id = data.annotation_id;
	                });
                } else {
                    //update data
                    ServiceManager.Annotations.update(9, arrObj);
                    arrObj.annotation_id = GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id].annotation_id;
                }
                GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] = arrObj;

            }
        },
        displayHideCorrectResponse : function(bVal) {
            var objThis = this;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = [];
            var correctResponseLength = objThis.options.xmlContent.responseDeclaration.correctResponse.value.length;
            if (bVal == 1) {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'visible')
                for (var i = 0; i < correctResponseLength; i++) {
                    correctResponse[i] = objThis.options.xmlContent.responseDeclaration.correctResponse.value[i];
                    $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse[i] + ']').find('[type=showHideAnswer]').removeClass('incorrectAnswer').addClass('correctAnswer');
                    $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse[i] + ']').find('[type=showHideAnswer]').html('<div id="correctIcon"></div>');
                }
            } else {
                $(objThis.member.questionInnercontent.element).find('[type=showHideAnswer]').css('visibility', 'hidden')
                for (var i = 0; i < correctResponseLength; i++) {
                    correctResponse[i] = objThis.options.xmlContent.responseDeclaration.correctResponse.value[i];
                    $(objThis.member.questionInnercontent.element).find('[identifier=' + correctResponse[i] + ']').find('[type=showHideAnswer]').addClass('incorrectAnswer').removeClass('correctAnswer');
                }
            }

        }
    });
})(jQuery);
