/**
 * @author harpreet.saluja
 */
$.widget("magic.languageComp", $.magic.buttonComp, {
	
	_create : function() {
	},
	
	_init : function() {
		$.magic.buttonComp.prototype._init.call(this);
		this.setLanguageButton()
	},
	
	setLanguageButton: function()
	{
		var objThis = this;
		
		var strLang = "";
		var strLangAttr = "";
		
		switch(EPubConfig.LocalizationLanguage)
		{
			case "en":
				strLang = GlobalModel.localizationData["SIDE_LANG_ES"];
				strLangAttr = "es";
				break;
			
			case "es":
				strLang = GlobalModel.localizationData["SIDE_LANG_EN"];
				strLangAttr = "en";
				break;
		}
		
		$(this.element).attr("language", strLangAttr);
		$(this.element).find(".navigationLabel").html(strLang);
		
		//if build is launched directly, disable the language change button;
		if(strReaderPath == "")
		{
			objThis.disable();
			return;
		}
		
		$(this.element).unbind("click").bind("click", function()
		{
			var nTimeout = 0;
			if(GlobalModel.panelViewOperator.arrOpenablePanels)
			{
				for(var i = 0; i < GlobalModel.panelViewOperator.arrOpenablePanels.length; i++)
				{
					 var objItem = $("#" + GlobalModel.panelViewOperator.arrOpenablePanels[i].options.panelViewComp);
					 if($(objItem).css('display') == 'block')
					 {
					 	nTimeout = 500;
					 	break;
					 }
				}
			}
			var objScComp = $($.find('[data-role=scrollviewcomp]')[0]).data('scrollviewcomp');
			$(objScComp).trigger('closeOpenedPanel');
			setTimeout(function(e){
				objThis.openBookInSelectedLanguage();
				nTimeout = 0;
			}, nTimeout);
		})
	},
	
	openBookInSelectedLanguage: function()
	{
		var strLang = $(this.element).attr("language").toUpperCase();
		var strLanguagePath = EPubConfig["Localize_" + strLang + "_URL"];
		
		var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
		strBuildPath = strLanguagePath + "?page=" + GlobalModel.pageBrkValueArr[iPageIndex] + "&grade=" + EPubConfig.ReaderType;
		window.open(strBuildPath, "_blank");
	}
});
