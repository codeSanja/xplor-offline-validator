/**
 * @author Lakshay Ahuja
 */
var audioSyncClass = "-epub-media-overlay-active";
MG.MediaPlayerOperator = function() {
	this.audioPlayerPanelRef = "";
	this.objAudioLauncher = "";
	this.readAloudSpans = null;
	this.objSMILInfo = null;
	this.objSlowSMILInfo = null;
	this.objNormalSMILInfo = null;
	this.currentAudioComp = null;
	this.iTimeoutValue = -1;
	this.isAudioPlayerPaused = false;
	this.audioPlayerStatus = "";
	this.playerUser = null;
	this.pageIndexOfCurrentPageAudio = -1;
	this.prevTime = 0;
	this.prevSmilInfo = null;
	this.prevHiligtSpan = null;
	this.readAloudBlock = null;
	this.defaultPageAudio = "leftPageAudio";
	this.pageLevelAudioPath = "";
	this.clickAllowed = true;
	this.onState = null;
	this.offState = null;
	this.isSeeked = null;
	this.playRightPageAudio = false;
	this.hasLeftPageIDPFAudio = false;
	this.hasRightPageIDPFAudio = false;
}

MG.MediaPlayerOperator.prototype = new MG.BaseOperator();

/**
 * This function will handle all the events related to bookmark components.
 *
 */
MG.MediaPlayerOperator.prototype.attachComponent = function(objComp) {
	if (objComp == null)
		return;
	
	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
	var objThis = this;
	switch (strType) {

		case AppConst.AUDIO_LAUNCHER_BTN:
			objThis.objAudioLauncher = objComp;
			
			$(objComp).bind("click", function() {
				$(objThis.objAudioLauncher.element).addClass("ui-disabled");
    			$(objThis.objAudioLauncher.element).data('buttonComp').disable();
				objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'hidingQAPAnel');
				if($(objThis.objAudioLauncher.element).hasClass('audioLauncherBtn-on'))
				{
					objThis.closeAudioPanel(true);
					clearInterval( objThis.timeOut );
					clearInterval(audioInterval);
					clearTimeout(audioTimeOut);
					audioIncrTime= 0;
				}
				else
				{
					var strCurrentPageName = objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getCurrentPageName');
					objThis.playRightPageAudio = false;
					objThis.hasLeftPageIDPFAudio = false;
					objThis.hasRightPageIDPFAudio = false;
					var arrPageNames = strCurrentPageName.split(",");
					//if global settings exists, check if current page has page-level audio available so that the audio icon in annotations panel could be enabled or disabled accordingly.
					if(GlobalModel.ePubGlobalSettings)
					{
			            if (GlobalModel.ePubGlobalSettings.Page) {
			                var arrPages = GlobalModel.ePubGlobalSettings.Page;
			                
			                for (var i = 0; i < arrPages.length; i++) {
			                    var objItem = arrPages[i];
			                    if (arrPageNames[0].indexOf(objItem.PageName) > -1 && objItem.mediaOverlayPath) {
			                       objThis.hasLeftPageIDPFAudio = true;
			                    }
			                    else if(arrPageNames[1])
			                    {
			                    	if (arrPageNames[1].indexOf(objItem.PageName) > -1 && objItem.mediaOverlayPath) {
				                        objThis.playRightPageAudio = true;
				                        objThis.hasRightPageIDPFAudio = true;
				                    }
			                    }
			                }
			            }
					}
					if(objThis.hasLeftPageIDPFAudio)
					{
						objThis.playMediaOverlayAudio(false);
					}
					else if(objThis.hasRightPageIDPFAudio)
					{
						objThis.playMediaOverlayAudio(true);
					}
					else
					{
						objThis.playRightPageAudio = false;
						objThis.clearSelection();
						objThis.launchAudioPanel(objComp);
					}
				}

				/*
				if(objComp.options.selected == true){
				//$(objThis.audioPlayerPanelRef.element.parent()).css('display','none');
				$("#jquery_jplayer_1").jPlayer("stop");
				//                $("#jquery_jplayer_1").jPlayer("clearMedia");
				}
				else{
				objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL,'setContent');
				$(objThis.audioPlayerPanelRef.element.parent()).css('display','block');
				}
				*/
				//console.log(objComp);
			});
			break;

		case AppConst.AUDIO_PLAYER_PANEL:
			objThis.audioPlayerPanelRef = objComp;
            $(objThis.audioPlayerPanelRef.element).find('[class=jp-unmute]').parent().css('display','none');
            $(objThis.audioPlayerPanelRef.element).parent().css('width','375px');
            $(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width','375px');
          

		    /////////////////////////////////////////
		    // main visibility API function 
		    // check if current tab is active or not
            var vis = (function () {
                var stateKey,
                    eventKey,
                    keys = {
                        hidden: "visibilitychange",
                        webkitHidden: "webkitvisibilitychange",
                        mozHidden: "mozvisibilitychange",
                        msHidden: "msvisibilitychange"
                    };
                for (stateKey in keys) {
                    if (stateKey in document) {
                        eventKey = keys[stateKey];
                        break;
                    }
                }
                return function (c) {
                    if (c) document.addEventListener(eventKey, c);
                    return !document[stateKey];
                }
            })();


            function pauseAudioPlayer(evt) {
                if ($(".players").css("display") == "block" && ($(objComp.element).find('[class="jp-pause"]').css("display") == "block" || $(objComp.element).find('[class="jp-pause"]').css("display") == "inline")) {
                    $(objComp.element).find('[class="jp-pause"]').trigger("click");
                }
            }

		    /////////////////////////////////////////
		    // check if current tab is active or not
            vis(function () {
               
                if (vis()) {

                    // the setTimeout() is used due to a delay 
                    // before the tab gains focus again, very important!
                    //setTimeout(function () {

                    // resume() code goes here	


                    //}, 300);

                } else {

                    // pause() code goes here	
                    pauseAudioPlayer();
                }
            });


		    /////////////////////////////////////////
		    // check if browser window has focus		
            var notIE = (document.documentMode === undefined),
                isChromium = window.chrome;

            if (notIE && !isChromium) {
                
                // checks for Firefox and other  NON IE Chrome versions
                //$(window).on("focusin", function () {

                    //setTimeout(function () {

                    // resume() code goes here


                    //}, 300);

                //}).on("focusout", function () {

                    // pause() code goes here
                    //pauseAudioPlayer();

                //});

                //window.onpageshow = window.onpagehide = window.onfocus = window.onblur = pauseAudioPlayer;

            } else {
                
                // checks for IE and Chromium versions
                if (window.addEventListener) {

                    // bind focus event
                    window.addEventListener("focus", function (event) {

                        //setTimeout(function () {

                        // resume() code goes here


                        //}, 300);

                    }, false);

                    // bind blur event
                    window.addEventListener("blur", function (event) {

                        // pause() code goes
                        pauseAudioPlayer();

                    }, false);

                } else {
                  
                    // bind focus event
                    window.attachEvent("focus", function (event) {

                        //setTimeout(function () {

                        // resume() code goes here


                        //}, 300);

                    });

                    // bind focus event
                    window.attachEvent("blur", function (event) {

                        // pause() code goes here
                        pauseAudioPlayer();

                    });
                }
            }

            


			if(navigator.userAgent.match(/(android)/i))
			{
				var hitArea = $('<div style="position:absolute;width:30px;height:30px;left:-7px;top:-5px; background-color:yellow; opacity:0.0;"/>');
				var scrubber = $($($(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]')).find('[id=progressBarSlider]')).find('a')[0];
				$(scrubber).append(hitArea);
			}
			var fnPlay = function() {
				
				$(objThis.audioPlayerPanelRef.element).find('.jp-stop').parent().removeClass('ui-disabled');
				$(objThis.audioPlayerPanelRef.element).find('#jp-pause-parent').css('display', "block");
				$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").removeClass("ui-disabled");
				$(objThis.audioPlayerPanelRef.element).find(".jp-progress").removeClass("ui-disabled");
				
				if (objThis.isAudioPlayerPaused == true) {
					if(objThis.playerUser == "AUDIO_HOTSPOT")
					{
						objThis.resumeReadAloud();
					}
					objThis.isAudioPlayerPaused = false;
					objThis.audioPlayerStatus = "PLAYING";
					objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
				} else {
					
					if(objThis.playerUser == "AUDIO_HOTSPOT")
					{
						//objThis.highlightronizer(objThis);
						//objThis.iTimeoutValue = setInterval(objThis.highlightSynchronizer, 0.1, objThis);
					}
				}
				
				objThis.audioPlayerStatus = "PLAYING";
				
				if(objThis.currentAudioComp && ($(objThis.currentAudioComp).hasClass(objThis.offState) || $(objThis.currentAudioComp).hasClass(objThis.onState)))
				{
					if ($(objThis.audioPlayerPanelRef.element).find('.jp-play').css('display') == "none") {
						$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
					} else {
						$(objThis.currentAudioComp).addClass(objThis.onState).removeClass(objThis.offState);

					}
				}
			}
			
			var fnStop = function() {
				objThis.stopReadAloud();

				//resetting Info;
				objThis.audioPlayerStatus = "STOPPED";
				objThis.isAudioPlayerPaused = false;
				objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
				$(objThis.audioPlayerPanelRef.element).find('#jp-pause-parent').css('display', "none")
				$(this).parent().addClass('ui-disabled');
				$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").addClass('ui-disabled');
				$(objThis.audioPlayerPanelRef.element).find(".jp-progress").addClass('ui-disabled');
				
				if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
					$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
			}
			
			var fnPause = function() {

				if ($(objThis.audioPlayerPanelRef.element).find('.jp-play').css('display') == "none") {
					$(objThis.audioPlayerPanelRef.element).find('#jp-pause-parent').css('display', "none")
					objThis.isAudioPlayerPaused = true;
					objThis.audioPlayerStatus = "PAUSED";
					objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', true);
				} else {
					objThis.audioPlayerStatus = "PLAYING";
					objThis.isAudioPlayerPaused = false;
					objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
					objThis.resumeReadAloud();
					if(objThis.currentAudioComp)
						$(objThis.currentAudioComp).addClass(objThis.onState).removeClass(objThis.offState);
				}
			}
			
			var fnSetPageAudio = function(e) {
				//deselecting previous default button
				$("#" + objThis.defaultPageAudio).removeClass(objThis.defaultPageAudio + "-selected").addClass(objThis.defaultPageAudio);
				$("#" + objThis.defaultPageAudio).css("pointer-events", "auto");
				
				//setting currently selected button as default
				objThis.defaultPageAudio = e.currentTarget.id;
				
				//selecting previous default button
				$("#" + objThis.defaultPageAudio).removeClass(objThis.defaultPageAudio).addClass(objThis.defaultPageAudio + "-selected");
				$("#" + objThis.defaultPageAudio).css("pointer-events", "none");
				
				//enable required audio controls
				$(objThis.audioPlayerPanelRef.element).find('[class=jp-stop]').removeClass("ui-disabled");
				$(objThis.audioPlayerPanelRef.element).find('[class=jp-stop]').parent().removeClass("ui-disabled");
				
				$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").removeClass("ui-disabled");
				$(objThis.audioPlayerPanelRef.element).find(".jp-progress").removeClass("ui-disabled");
				
				$(objThis.audioPlayerPanelRef.element).find('[class=jp-pause]').css("display", "block");
				$(objThis.audioPlayerPanelRef.element).find('[class=jp-pause]').parent().css("display", "block");
				
				//play new audio
				var strAudioPath = objThis.getPageLevelAudioPathToPlay()
				objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setContent', strAudioPath, 0);
			}
			
			if(ismobile)
			{
				$(objComp.element).find('[class=jp-play]').bind('click', fnPlay);
				$(objComp.element).find('[id="jp-play-parent"]').bind('click', function() {
					$(this).find('[class=jp-play]').trigger('click');
				});
				
				$(objComp.element).find('[class=jp-stop]').bind('click', fnStop);
				$(objComp.element).find('[class=jp-stop]').parent().bind('click', function() {
					$(this).find('[class=jp-stop]').trigger('click');
				});
				
				$(objComp.element).find('[class="jp-pause"]').bind('click', fnPause);
				$(objComp.element).find('[id="jp-pause-parent"]').bind('click', function() {
					$(this).find('[class=jp-pause]').trigger('click');
				});
				
				$("#leftPageAudio").bind('click', fnSetPageAudio);
				$("#rightPageAudio").bind('click', fnSetPageAudio);
			}
			else
			{
				$(objComp.element).find('[class=jp-play]').unbind('click').bind('click', fnPlay);
				$(objComp.element).find('[id="jp-play-parent"]').unbind('click').bind('click', function() {
					$(this).find('[class=jp-play]').trigger('click');
				});
				
				
				$(objComp.element).find('[class=jp-stop]').unbind('click').bind('click', fnStop);
				$(objComp.element).find('[class=jp-stop]').parent().unbind('click').bind('click', function() {
					$(this).find('[class=jp-stop]').trigger('click');
				});
			
				$(objComp.element).find('[class="jp-pause"]').unbind('click').bind('click', fnPause);
				$(objComp.element).find('[id="jp-pause-parent"]').unbind('click').bind('click', function() {
					$(this).find('[class=jp-pause]').trigger('click');
				});
				
				$("#leftPageAudio").unbind('click').bind('click', fnSetPageAudio);
				$("#rightPageAudio").unbind('click').bind('click', fnSetPageAudio);
			}
			
			$("#" + objThis.defaultPageAudio).removeClass(objThis.defaultPageAudio).addClass(objThis.defaultPageAudio + "-selected");
			$("#" + objThis.defaultPageAudio).css("pointer-events", "none");


			$(objComp.element).find('[class=outerPanelClsBtn]').click(function() {
				$(objThis.objAudioLauncher.element).trigger("click");
			});
			
			$(objComp.element).find('[id=jp-volume-container]').bind('mouseenter', function() {
				$(objThis.audioPlayerPanelRef.element.find('[id=jp-volume-container]')).css('display', 'block');
			});
			
			$(objComp.element).find('[class=jp-unmute]').parent().bind('click mouseenter', function() {
				$(objThis.audioPlayerPanelRef.element.find('[id=jp-volume-container]')).css('display', 'block');
			});

			$(objComp.element).find('[id=jp-volume-container]').bind('mouseleave', function() {
				$(objThis.audioPlayerPanelRef.element.find('[id=jp-volume-container]')).css('display', 'none');
			});
			
			$(objComp.element).find('[class=jp-unmute]').parent().bind('mouseleave', function() {
				$(objThis.audioPlayerPanelRef.element.find('[id=jp-volume-container]')).css('display', 'none');
			});
			
			//Ensuring that the value of Text_Highlight is either ON or OFF
			//Error Handling::: Converting the Text_Highlight value to upper case; appending double-quotes to ensure that the value is string so that it could be converted to upper case
			EPubConfig.Text_Highlight = (EPubConfig.Text_Highlight + "").toUpperCase();
			
			//Error Handling::: If the value of Text_Highlighting is not ON or true, turning it to OFF state. If value is true, setting the text highlight to ON state
			if(EPubConfig.Text_Highlight == "TRUE") {
				EPubConfig.Text_Highlight = "ON";
			}
			else if(EPubConfig.Text_Highlight != "ON") {
				EPubConfig.Text_Highlight = "OFF";
			}
			
			//override read aloud if enabled in background;
			$(objComp.element).find('[id="manualReadAloudEnabler"]').unbind("click").bind("click", function() {
				if(objThis.playerUser == "PAGE_LEVEL_AUDIO_PLAYER") {
                                                  return;
                                        }
                                        //add/remove read-aloud settings from required read-aloud blocks
				if(EPubConfig.Text_Highlight == 'OFF')
				{
					EPubConfig.Text_Highlight = 'ON';
					
					$("#manualReadAloudEnabler").addClass('read-aloud-enabled');
					$("#manualReadAloudEnabler").removeClass('read-aloud-disabled');
				}
				else
				{
					EPubConfig.Text_Highlight = 'OFF';
					
					$("#manualReadAloudEnabler").removeClass('read-aloud-enabled');
					$("#manualReadAloudEnabler").addClass('read-aloud-disabled');
					if(objThis.prevHiligtSpan)
					{
						objThis.prevHiligtSpan.removeClass(audioSyncClass); 
						objThis.prevHiligtSpan.find("*").removeClass(audioSyncClass);
						objThis.prevHiligtSpan = null;
					}

				}
				
				objThis.highlightSynchronizer(objThis);
			});
			
			$(objComp.element).find('[id="slowerAudioBtn"]').unbind("click").bind("click", function() {
				objThis.objSMILInfo = null;
				objThis.prevSmilInfo = null;
				if ($("#slowerAudio").hasClass('jp-slower-disabled')) {
					$("#slowerAudio").removeClass('jp-slower-disabled');
					$("#slowerAudio").addClass('jp-slower-enabled');
					objThis.objSMILInfo = objThis.objSlowSMILInfo;
				} else {
					$("#slowerAudio").removeClass('jp-slower-enabled');
					$("#slowerAudio").addClass('jp-slower-disabled');
					objThis.objSMILInfo = objThis.objNormalSMILInfo;
				}
				objThis.stopReadAloud();
				objThis.playReadAloudAudio($(objThis.readAloudSpans[0]).attr("id"));
			});

			//close audio player;
			$('[class="audioPlayerCloseBtn"]').unbind("click").bind("click", function() {
				$(".audioPlayerCloseBtn").addClass("ui-disabled");
				objThis.closeAudioPanel();
				setTimeout(function() {
					$(".audioPlayerCloseBtn").removeClass("ui-disabled");
				}, 200);
				clearInterval(audioInterval);
				clearTimeout(audioTimeOut);
				audioIncrTime= 0;
			});


			$(objComp).unbind($(objComp)[0].events.SLIDER_SLIDE_STOP).bind($(objComp)[0].events.SLIDER_SLIDE_STOP, objThis, objThis.audioPlayerEventsHandler);
			$(objComp).unbind($(objComp)[0].events.PROGRESS_BAR_SLIDING).bind($(objComp)[0].events.PROGRESS_BAR_SLIDING, objThis, objThis.audioPlayerEventsHandler);
			$(objComp).unbind($(objComp)[0].events.AUDIO_LOAD_ERROR).bind($(objComp)[0].events.AUDIO_LOAD_ERROR, objThis, objThis.onAudioUnavailableError);
			$(objComp).unbind($(objComp)[0].events.AUDIO_ENDED).bind($(objComp)[0].events.AUDIO_ENDED, objThis, objThis.audioPlayerEventsHandler);
			$(objComp).unbind($(objComp)[0].events.TIME_UPDATED).bind($(objComp)[0].events.TIME_UPDATED, objThis, objThis.audioPlayerEventsHandler);
			break;

		case AppConst.SCROLL_VIEW_CONTAINER:
			$(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function() {
				objThis.scrollViewRenderCompleteHandler(objComp);
			});
			
			$(objComp).bind(objComp.events.PAGE_INDEX_CHANGE, function() {
				
				if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING || EPubConfig.pageView == AppConst.SINGLE_PAGE_CAMEL_CASING)
					objThis.scrollViewRenderPageIndexChange(true, true, false);
				else
					objThis.scrollViewRenderPageIndexChange(true);
				
				/*if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING) 
				{
					$(objThis.objAudioLauncher.element).attr("class","audioLauncherBtn-off ui-disabled");
				}*/
			});
						
			break;

		default:
			//console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
			break;
	}

	return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}


MG.MediaPlayerOperator.prototype.audioPlayerEventsHandler = function(e) {
	var objThis = e.data;
	
	switch(e.type) {
		case "SLIDER_SLIDE_STOP": 
		case "PROGRESS_BAR_SLIDING":
		//if audio player was launched by audio hotspot, 
			if (objThis.playerUser == "AUDIO_HOTSPOT") {
				objThis.sliderSlideEnd(e);
			}
			objThis.prevSmilInfo = null;
			
			if(navigator.userAgent.match(/(android)/i))
			{
				if(isSeeked)
				{
					clearTimeout(isSeeked);
					isSeeked = null;
				}
				if(e.type == "PROGRESS_BAR_SLIDING")
				{
					isSeeked = setTimeout(function(){
						var scrubber = $($($(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]')).find('[id=progressBarSlider]')).find('a')[0];
						$(scrubber).trigger('mouseup');	
					},1000);	
				}
			}
			
			break;
			
		case "AUDIO_ENDED":
			if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
				$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
			
			clearInterval( objThis.timeOut );
			objThis.audioPlayerStatus = "STOPPED";
			var strPageName = objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getCurrentPageName');
			var arrPageNames = strPageName.split(",");
			objThis.stopReadAloud();
			
			if(objThis.hasLeftPageIDPFAudio && objThis.hasRightPageIDPFAudio)
			{
				if(objThis.playRightPageAudio == true)
				{
					objThis.playRightPageAudio = false;
					objThis.playMediaOverlayAudio(true);
					
				}
				else
				{
					objThis.playRightPageAudio = true;
					objPageContainer = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]);
					objThis.objSMILInfo = $(objPageContainer).data().pagecomp.getOverlaySMILInfo();
				
					if(objThis.objSMILInfo == null)
					{
						$("#alertTxt").html(GlobalModel.localizationData["SMIL_ERROR"]);
						setTimeout(function(){
							PopupManager.addPopup($("#alertPopUpPanel"), null, {
								        isModal: true,
								        isCentered: true,
										popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
										isTapLayoutDisabled: true
								    });
						},0);
						return;
					}						
					objThis.readAloudBlock = $(objPageContainer);
					var arrSpans = $(objThis.readAloudBlock).find("span");
					objThis.readAloudSpans = [];
					for(var l = 0; l < arrSpans.length; l++)
					{
						if(objThis.objSMILInfo[$(arrSpans[l]).attr("id")] != undefined)
						{
							objThis.readAloudSpans.push(arrSpans[l]);
						}
						
					}
					if(objThis.readAloudSpans.length == 0)
					{
						var audioSrc;
							for(span in objThis.objSMILInfo){
								audioSrc = objThis.objSMILInfo[span].src;
								break;
						}
						objThis.pageLevelAudioPath = getPathManager().getSMILAudioPath(audioSrc)
						objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER";	
						objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setMediaSource', objThis.pageLevelAudioPath);
						return;
					}
					var objInfo = objThis.objSMILInfo[objThis.readAloudSpans[0].id];
					var strAudioPath = getPathManager().getSMILAudioPath(objInfo.src);
					objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setMediaSource', strAudioPath);
				}			
			}
			break;
		
		case "TIME_UPDATED": 
			if (objThis.playerUser == "AUDIO_HOTSPOT" || objThis.playerUser == "IDPF_AUDIO") {
				objThis.highlightSynchronizer(objThis);
			}
	}
}


MG.MediaPlayerOperator.prototype.scrollViewRenderCompleteHandler = function(objComp) {
	var objThis = this;

	var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
	var objPageRole = null;
	for (var i = 0; i < arrPageComps.length; i++) {
		objPageRole = $(arrPageComps[i]).data("pagecomp");

		$(objPageRole).bind(objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function(e) {
			objThis.initializeAudioItemClick(e.currentTarget);
		});
	}
}

MG.MediaPlayerOperator.prototype.initializeAudioItemClick = function(objComp) {
	var objThis = this;
	
	$('.players').unbind('mousedown').bind('mousedown', function(e) {
        e.preventDefault();
    });
	var objScrollViewComp = objThis.getCompArray(AppConst.SCROLL_VIEW_CONTAINER)[0];
	
	//simulate page index change event
	if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING || EPubConfig.pageView == AppConst.SINGLE_PAGE_CAMEL_CASING)
		objThis.scrollViewRenderPageIndexChange(true, null, false);
	
	
	//Handling Page level audio hotspots
	objThis.setPageLevelAudioHotspot(objComp, "pageLevelAudioHotSpot");
	objThis.setPageLevelAudioHotspot(objComp, "pageLevelNoIconAudioHotSpot");
	
	
	//Handling Read-aloud hotspots
	var arrPageComps = $(objComp.element).find('[type=audioHotSpot]');
	for (var i = 0; i < arrPageComps.length; i++) {
		var strMethod = $(arrPageComps[i]).attr("data-role");
		try {
			$(arrPageComps[i])[strMethod]();
		} catch (err) {
			//console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);
		}
		
		var objComponent = $(arrPageComps[i]).data(strMethod);
		
		//if read-aloud audio hot-spots are available; objComp is not null even if audio hot-spots are unavailable, therefore checking for events property.
		if(objComponent.events)
		{
			if($(arrPageComps[i]).hasClass('audiohotspotpos') || $(arrPageComps[i]).hasClass('audioHotspot') || $(arrPageComps[i]).hasClass('audioHotspotContent')){
                    if($(arrPageComps[i]).attr('off-state') == undefined){
                        $(arrPageComps[i]).attr('off-state','audioHotspotContent');
                    }
                    if($(arrPageComps[i]).attr('on-state') == undefined){
                         $(arrPageComps[i]).attr('on-state','playAudioContent');
                    }
                   
                }
                
			$(arrPageComps[i]).unbind(objComponent.events.CLICK).bind(objComponent.events.CLICK, function(e) {
				e.stopPropagation();
				objThis.clearSelection();
				objThis.stopReadAloud();

				objThis.prevTime = 0;
				objThis.prevSmilInfo = null;
				objThis.playerUser = "AUDIO_HOTSPOT";
				
				var strReadAloudBlock = $(this).attr('read-aloud-block');
				var objPageContainer = $(objComp.element).closest('[type="PageContainer"]')[0];
				
				objThis.pageIndexOfCurrentPageAudio = parseInt($(objPageContainer).attr("pagenum"));
	            
				//objThis.readAloudBlock = $("#" + strReadAloudBlock);
				//var arrSpans = $("#" + strReadAloudBlock).find("span");
				objThis.readAloudBlock = $(objPageContainer).find('[id="' + strReadAloudBlock + '"]');
                var arrSpans = $(objThis.readAloudBlock).find("span");
                
				objThis.readAloudSpans = [];
				objThis.objNormalSMILInfo = $(objPageContainer).data().pagecomp.getSMILInfo();
				objThis.objSlowSMILInfo = $(objPageContainer).data().pagecomp.getSlowSMILInfo();
				if($("#slowerAudio").css("display") == "block")
				{
					if ($("#slowerAudio").hasClass('jp-slower-disabled'))
						objThis.objSMILInfo = objThis.objNormalSMILInfo;
					else
						objThis.objSMILInfo = objThis.objSlowSMILInfo;
				}
				else
					objThis.objSMILInfo = objThis.objNormalSMILInfo;
									
				for(var l = 0; l < arrSpans.length; l++)
				{
				    //console.log(objThis.objSMILInfo[$(arrSpans[l]).attr("id")]);
					if(objThis.objSMILInfo[$(arrSpans[l]).attr("id")] != undefined)
					{
						objThis.readAloudSpans.push(arrSpans[l]);
					}
					
				}
				if(objThis.audioPlayerPanelRef)
				{
					//$(objThis.audioPlayerPanelRef.element.parent()).css('display', 'block');
					if (ismobile)
					{
						$("#volumeContainerList").css("display", "none");
					}
					else
					{
						$("#volumeContainerList").css("display", "block");
					}
					$(".players").stop(true, true).slideDown(500, "easeOutBounce", function()
					{
						//adding because in one case player did not open properly;
						$(".players").css("height", "40px")
					});
					
					$(document).trigger( "repositionLearnosityPanelUp" );
					$("#bookContentContainer").data('scrollviewcomp').updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent);
				}
				
								
				$('#jp-pause-parent').css('display', "block")
	
				//resetting Info;
				objThis.isAudioPlayerPaused = false;
				objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
				
				if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
				{
					$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
				}
				
				/*if(objThis.currentAudioComp == this)
				{
					if($(".players").find("audio")[0].src != "")
					{
						objThis.audioPlayerStatus = "PLAYING";
						
						$(objThis.audioPlayerPanelRef.element).find(".jp-stop").trigger('click');
						objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'resetCurrentTime');
						$(objThis.audioPlayerPanelRef.element).find(".jp-play").trigger("click");
						
						$(objThis.currentAudioComp).addClass(objThis.onState).removeClass(objThis.offState);
						//objThis.highlightSynchronizer(objThis);
					}
					
					return;
				}*/
				
				objThis.currentAudioComp = this;
				
				objThis.onState = $(this).attr('on-state');
				objThis.offState = $(this).attr('off-state');
				objThis.hideLeftRightPageAudioButtons();
				$("#manualReadAloudEnabler").css("display","block");
				//set the CSS of read-aloud's manual override button
				if(EPubConfig.Text_Highlight == 'ON')
				{
					$("#manualReadAloudEnabler").addClass('read-aloud-enabled');
					$("#manualReadAloudEnabler").removeClass('read-aloud-disabled');
					if(ismobile == null)
						$("#ismobileDisabledSeparator").css("display","block");
				}
				else
				{
					$("#manualReadAloudEnabler").removeClass('read-aloud-enabled');
					$("#manualReadAloudEnabler").addClass('read-aloud-disabled');
				}
				
				//enabling events on read-aloud's manual override button
				$("#manualReadAloudEnabler").css('pointer-events', 'auto');
				
				objThis.playReadAloudAudio($(objThis.readAloudSpans[0]).attr("id"));
				//$(objThis.currentAudioComp).removeClass().addClass(objThis.onState);
			});
		}
	}
}

MG.MediaPlayerOperator.prototype.setPageLevelAudioHotspot = function(objComp, strType)
{
	var objThis = this;
	
	var arrPageLevelAudioComps = $(objComp.element).find('[type="' + strType + '"]');
	for (var i = 0; i < arrPageLevelAudioComps.length; i++) {
		var strMethod = $(arrPageLevelAudioComps[i]).attr("data-role");
		try {
			$(arrPageLevelAudioComps[i])[strMethod]();
		} catch (err) {
			//console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);
		}
		
		var objComponent = $(arrPageLevelAudioComps[i]).data(strMethod);
		
		//if page level audio hot-spots are available; objComp is not null even if audio hot-spots are unavailable, therefore checking for events property.
		if(objComponent.events)
		{
			if($(arrPageLevelAudioComps[i]).hasClass('audiohotspotpos') || $(arrPageLevelAudioComps[i]).hasClass('audioHotspot') || $(arrPageLevelAudioComps[i]).hasClass('audioHotspotContent')){
                    if($(arrPageLevelAudioComps[i]).attr('off-state') == undefined){
                        $(arrPageLevelAudioComps[i]).attr('off-state','audioHotspotContent');
                    }
                    if($(arrPageLevelAudioComps[i]).attr('on-state') == undefined){
                         $(arrPageLevelAudioComps[i]).attr('on-state','playAudioContent');
                    }
                   
                }
			$(arrPageLevelAudioComps[i]).unbind(objComponent.events.CLICK).bind(objComponent.events.CLICK, function(e) {
				objThis.stopReadAloud();
				objThis.clearSelection();
				//handling very frequent click events
				if(objThis.clickAllowed == false)
					return;
				
				objThis.clickAllowed = false;
				setTimeout(function()
				{
					objThis.clickAllowed = true;
				}, 200);
				
				
				
				//closing audio player if this button is clicked again
				if(objThis.currentAudioComp == this)
				{
					if($(objThis.currentAudioComp).hasClass(objThis.onState) || 
						($(objThis.currentAudioComp).hasClass("playAudio_NoIcon") && (objThis.audioPlayerStatus == "PLAYING" || objThis.audioPlayerStatus == "PAUSED")))
					{
						$(objThis.objAudioLauncher.element).trigger("click");
						return;
					}
				}
				
				objThis.hideLeftRightPageAudioButtons();
				//deselecting previously playing component
				if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
					$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
				
				objThis.currentAudioComp = this;
				
				objThis.onState = $(this).attr('on-state');
				objThis.offState = $(this).attr('off-state');
				
				objThis.pageLevelAudioPath = getPathManager().getEPubPageLevelAudioPath($(this).attr("src"));
				
				if(objThis.playerUser == null)
				{
					//setting player user value here so that the audio launcher click handler does not call playPageLevelAudio with it's own audio. 
					objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER";
					$(objThis.objAudioLauncher.element).trigger("click");
					
					if(objThis.audioPlayerStatus != "")
					{
						$(objThis.audioPlayerPanelRef.element).find(".jp-stop").trigger('click');
						objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'resetCurrentTime');
					}
					
					objThis.playPageLevelAudio();
				}
				else
				{
					if(objThis.audioPlayerStatus != "")
					{
						$(objThis.audioPlayerPanelRef.element).find(".jp-stop").trigger('click');
						objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'resetCurrentTime');
					}
					
					objThis.playPageLevelAudio();
				}
				//$(objThis.currentAudioComp).removeClass().addClass(objThis.onState);
			});
		}
	}
}

/**
 * This method displays an error message on screen if audio cannot be played
 */
MG.MediaPlayerOperator.prototype.onAudioUnavailableError = function(e) {
	
	var objThis = e.data;
	
	//close audio player it is is open (as it can be opened by any other audio button before current audio button is clicked)
	if (objThis.objAudioLauncher.options.selected == true) {
		$(objThis.objAudioLauncher.element).trigger("click")
	}

	$("#alertTxt").html(GlobalModel.localizationData["AUDIO_FILE_UNAVAILABLE"]);
	setTimeout(function() {
		PopupManager.addPopup($("#alertPopUpPanel"), null, {
			isModal : true,
			isCentered : true,
			popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
			isTapLayoutDisabled : true
		});
	}, 0);
}


MG.MediaPlayerOperator.prototype.highlightSynchronizer = function(objThis)
{
	if(objThis.playerUser == "PAGE_LEVEL_AUDIO_PLAYER")
	{
		objThis.stopReadAloud();
		return;
	}
	
	var fCurrentPosition = objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'getCurrentPosition');
	var strSpanID;
	var objInfo;
	
	if($(objThis.audioPlayerPanelRef.element).find(".jp-stop").parent().hasClass("ui-disabled"))
	{
		clearInterval(objThis.iTimeoutValue);
		objThis.prevTime = 0;
		objThis.prevSmilInfo = null;
		for (strSpanID in objThis.objSMILInfo) {
			//var objParentSpan = $("#" + strSpanID);
			var objParentSpan = $(objThis.readAloudBlock.find('[id="' + strSpanID + '"]'));
			var objChildrenSpans = objParentSpan.find("*");
			
			objParentSpan.removeClass(audioSyncClass);
			objChildrenSpans.removeClass(audioSyncClass);
		}
		
		if(objThis.currentAudioComp)
			$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
		
		return;
	}
	
	if(fCurrentPosition > 0)
	{
		if(objThis.prevSmilInfo)
		{
			if (fCurrentPosition < objThis.prevSmilInfo.clipEnd) {
				return;
			}
		}
		for (strSpanID in objThis.objSMILInfo) {
			//ensuring children to highlight belongs to current read-aloud block as children from other blocks may have same beginning and ending time...
			if(objThis.readAloudBlock.find('[id="' + strSpanID + '"]').length > 0)
			{
				objInfo = objThis.objSMILInfo[strSpanID];
				//console.log(objThis);
				var objParentSpan = $(objThis.readAloudBlock.find('[id="' + strSpanID + '"]'));
				var objChildrenSpans = objParentSpan.find("*");
				//console.log(strSpanID , (fCurrentPosition >= objInfo.clipBegin && fCurrentPosition < objInfo.clipEnd && EPubConfig.Text_Highlight == "ON"));
				var condition = fCurrentPosition >= objInfo.clipBegin && fCurrentPosition < objInfo.clipEnd && EPubConfig.Text_Highlight == "ON";
				
				if (condition) {
					if(objThis.prevHiligtSpan)
					{
						objThis.prevHiligtSpan.removeClass(audioSyncClass); 
						objThis.prevHiligtSpan.find("*").removeClass(audioSyncClass);
						objThis.prevHiligtSpan = null;
					}
					objThis.prevSmilInfo = objInfo;
					objParentSpan.addClass(audioSyncClass);
					objChildrenSpans.addClass(audioSyncClass);
					objThis.prevHiligtSpan = objParentSpan;
					break;
				}
			}
		}
	}
	
	objThis.prevTime = fCurrentPosition;
}

MG.MediaPlayerOperator.prototype.sliderSlideEnd = function(e) {
	var objThis = (e == null) ? this : e.data;
	var fCurrentPosition = objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'getCurrentPosition');
	
	if(objThis.prevTime == 0 && fCurrentPosition == 0)
	{
		for (strSpanID in objThis.objSMILInfo) {
			//ensuring children to highlight belongs to current read-aloud block as children from other blocks may have same beginning and ending time...
			if(objThis.readAloudBlock.find('[id="' + strSpanID + '"]').length > 0)
			{
				objInfo = objThis.objSMILInfo[strSpanID];
		
				if (fCurrentPosition >= objInfo.clipBegin && fCurrentPosition < objInfo.clipEnd) {
					if (EPubConfig.Text_Highlight == "ON") {
						var objParentSpan = objThis.readAloudBlock.find('[id="' + strSpanID + '"]');;
						var objChildrenSpans = objParentSpan.find("*");
						
						objParentSpan.addClass(audioSyncClass);
						objChildrenSpans.addClass(audioSyncClass);
						break;
					}
				}
			}
		}
	}
	
	//clearInterval(objThis.iTimeoutValue);
	//objThis.iTimeoutValue = setInterval(objThis.highlightSynchronizer, 0.1, objThis);
}

MG.MediaPlayerOperator.prototype.resumeReadAloud = function() {
	//this.iTimeoutValue = setInterval(this.highlightSynchronizer, 0.1, this);
}

MG.MediaPlayerOperator.prototype.stopReadAloud = function() {
	var objThis = this;
	
	clearInterval(objThis.iTimeoutValue);
	objThis.prevSmilInfo = null;
	objThis.iTimeoutValue = 0;
	$('.'+audioSyncClass).removeClass(audioSyncClass);
	/*
	if(objThis.objSMILInfo)
	{
		for (strSpanID in objThis.objSMILInfo) {
			var objParentSpan = $("#" + strSpanID);
			var objChildrenSpans = objParentSpan.find("*");
			objParentSpan.removeClass("-epub-media-overlay-active");
			objChildrenSpans.removeClass("-epub-media-overlay-active");
		}
	}
	*/
}

MG.MediaPlayerOperator.prototype.playReadAloudAudio = function(strSpanID) {
	var objThis = this;
	
	clearInterval(objThis.iTimeoutValue);
	objThis.iTimeoutValue = 0;
	
	var objInfo = objThis.objSMILInfo[strSpanID];

	if (objInfo) {
		
		var strAudioPath = getPathManager().getSMILAudioPath(objInfo.src)

		if(objThis.audioPlayerStatus != "")
		{
			$(objThis.audioPlayerPanelRef.element).find(".jp-stop").trigger('click');
			objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'resetCurrentTime');
		}
		
		$(objThis.currentAudioComp).addClass(objThis.onState).removeClass(objThis.offState);
		
		$(objThis.audioPlayerPanelRef.element).find('[class=jp-stop]').parent().removeClass('ui-disabled');
		$(objThis.audioPlayerPanelRef.element).find('[class=jp-play]').parent().removeClass('ui-disabled');
		$(objThis.audioPlayerPanelRef.element).find('[class=jp-pause]').parent().removeClass('ui-disabled');
		$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").removeClass('ui-disabled');

		$(objThis.audioPlayerPanelRef.element).find('[class=jp-pause]').parent().css('display','block');
		$(objThis.audioPlayerPanelRef.element).find("#jp-progress").removeClass("ui-disabled");
		
		//objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'stopPlaying');
		
		objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setContent', strAudioPath, objInfo.clipBegin);
		objThis.audioPlayerStatus = "PLAYING";
		//objThis.highlightSynchronizer(objThis);
		//objThis.iTimeoutValue = setInterval(objThis.highlightSynchronizer, 0.1, objThis);
	}
}

MG.MediaPlayerOperator.prototype.launchAudioPanel = function(objComp) {
	var objThis = this;
	$("#leftPageAudio").css("display" ,"none");
	$("#rightPageAudio").css("display" ,"none");
	if (objComp.options.selected == true) {

		if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
			$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
		
		objThis.isAudioPlayerPaused = false;
		objThis.audioPlayerStatus = "";
		objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
		objThis.playerUser = null;
		objThis.currentAudioComp = null;
		objThis.onState = null;
		objThis.offState = null;
		objThis.stopReadAloud();
		objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'stopPlaying');
		//$(objThis.audioPlayerPanelRef.element.parent()).css('display', 'none');
		$(".players").stop(true, true).slideUp(500,"easeInOutExpo");
		
		//enabling events on read-aloud's manual override button
		$("#manualReadAloudEnabler").css('pointer-events', 'auto');
		
		objThis.pageIndexOfCurrentPageAudio = -1;
        
        setTimeout(function()
        {
            objThis.scrollViewRenderPageIndexChange(false, null, false);
        }, 1)
	} else {

		//$(objThis.audioPlayerPanelRef.element.parent()).css('display', 'block');
		//objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'displayAudioPanel');
		//$(".players").slideDown(500, "easeOutBounce");
		
		if (ismobile == null) {
			$(objThis.audioPlayerPanelRef.element).find('[class=jp-unmute]').parent().css('display', 'block');
			$(objThis.audioPlayerPanelRef.element).find('[id=ismobileDisabledSeparator]').css('display', 'block');
			var curPageBrkVal = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue');
			if (EPubConfig.Read_Aloud_isAvailable && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + curPageBrkVal].dualAudio) {
				$(objThis.audioPlayerPanelRef.element).parent().css('width', '452px');
				$(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width', '452px');
				$(".players").css("width", "452px");
				$("#slowerAudioBtn").css("display", "block");
			} else {
				$(objThis.audioPlayerPanelRef.element).parent().css('width', '414px');
				$(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width', '414px');
				$(".players").css("width", "414px");
				$("#slowerAudioBtn").css("display", "none");
			}
		}
		
		//if player is launched directly by clicking the audio icon in left panel
		if(objThis.playerUser == null)
		{
			objThis.pageLevelAudioPath = objThis.getPageLevelAudioPathToPlay();
			objThis.playPageLevelAudio();
		}
		
		
		$(objThis.audioPlayerPanelRef.element).find('#jp-pause-parent').css('display', "block");
		$(objThis.audioPlayerPanelRef.element).find(".jp-stop").parent().removeClass("ui-disabled");
		$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").removeClass("ui-disabled");
		$(objThis.audioPlayerPanelRef.element).find(".jp-progress").removeClass("ui-disabled");
	}
}

MG.MediaPlayerOperator.prototype.playPageLevelAudio = function()
{
	var objThis = this;
	//objThis.manageLeftRightPageAudioButtons();
	
	$(".players").stop(true, true).slideDown(500, "easeOutBounce", function()
	{
		//adding because in one case player did not open properly;
		$(".players").css("height", "40px")
	});
	$(document).trigger( "repositionLearnosityPanelUp" );
	$("#manualReadAloudEnabler").removeClass("read-aloud-enabled").addClass('read-aloud-disabled');
	$("#manualReadAloudEnabler").css("display","none");
	$("#ismobileDisabledSeparator").css("display","none");
	$("#slowerAudioBtn").css("display", "none");
	if(ismobile == null)
	{
		$(objThis.audioPlayerPanelRef.element).parent().css('width','354px');
    	$(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width','354px');
		$(".players").css("width","354px");
	}
	else
	{
		$(objThis.audioPlayerPanelRef.element).parent().css('width','320px');
    	$(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width','320px');
		$(".players").css("width","320px");
		$("#progressSeprator").css("display","none");
	}
	
	objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'hidingQAPAnel');
	
	//disabling events on read-aloud's manual override button
	$("#manualReadAloudEnabler").css('pointer-events', 'none');
	
	if(objThis.currentAudioComp && !($(objThis.currentAudioComp).hasClass(objThis.onState)))
	{
		$(objThis.currentAudioComp).removeClass(objThis.offState).addClass(objThis.onState);
	}
	
	//play audio
	objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setContent', objThis.pageLevelAudioPath, 0);
	objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER";
	
	objThis.pageIndexOfCurrentPageAudio = GlobalModel.currentPageIndex;
	objThis.isAudioPlayerPaused = false;
	objThis.audioPlayerStatus = "PLAYING";
	objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
	
	$(objThis.audioPlayerPanelRef.element).find('#jp-pause-parent').css('display', "block");
	$(objThis.audioPlayerPanelRef.element).find(".jp-stop").parent().removeClass("ui-disabled");
	$(objThis.audioPlayerPanelRef.element).find("#progressBarSlider").removeClass("ui-disabled");
	$(objThis.audioPlayerPanelRef.element).find(".jp-progress").removeClass("ui-disabled");
	$("#bookContentContainer").data('scrollviewcomp').updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent);
}

MG.MediaPlayerOperator.prototype.hideLeftRightPageAudioButtons = function()
{
	var objThis = this;
	$("#leftPageAudio").css("display" ,"none");
	$("#rightPageAudio").css("display" ,"none");
	$("#leftRightSeparator").css("display" ,"none");
	
	$("#manualReadAloudEnabler").css("right", "23px");
	if (ismobile == null) {
		var curPageBrkVal = $("#bookContentContainer").data('scrollviewcomp').options.pageIndexForAnnotation;
		if (EPubConfig.Read_Aloud_isAvailable && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + curPageBrkVal].dualAudio) {
			$(".players").css("width", "452px");
			$("#jp_interface_1").css("width", "452px");
			$("#slowerAudioBtn").css("display", "block");
			$("#slowerAudioBtn").css("left", "338px");
			//$("#slowerAudioBtn").css("top", "-48px");
		} else {
			$(".players").css("width", "414px");
			$("#jp_interface_1").css("width", "414px");
			$("#slowerAudioBtn").css("display", "none");
			$("#manualReadAloudEnabler").css("display","block");
			$("#ismobileDisabledSeparator").css("display","block");
		}
	} else {
		var curPageBrkVal = $("#bookContentContainer").data('scrollviewcomp').options.pageIndexForAnnotation;
		if (EPubConfig.Read_Aloud_isAvailable && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + curPageBrkVal].dualAudio) {
			$(".players").css("width", "413px");
			$("#jp_interface_1").css("width", "413px");
			$("#progressSeprator").css("display", "block");
			$("#ismobileDisabledSeparator").css("display", "none");
			$("#slowerAudioBtn").css("display", "block");
			$("#slowerAudioBtn").css("left", "298px");
			//$("#slowerAudioBtn").css("top", "-7px");
		} else {
			$(".players").css("width", "375px");
			$("#jp_interface_1").css("width", "375px");
			$("#progressSeprator").css("display", "block");
			$("#ismobileDisabledSeparator").css("display", "none");
			$("#slowerAudioBtn").css("display", "none");
		}
	}
}

MG.MediaPlayerOperator.prototype.manageLeftRightPageAudioButtons = function()
{
	if(EPubConfig.pageView != AppConst.DOUBLE_PAGE_CAMEL_CASING)
	{
		this.hideLeftRightPageAudioButtons();
	}
	else if(objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER" || objThis.playerUser == null)
	{
		$("#leftPageAudio").removeClass("ui-disabled");
		$("#rightPageAudio").removeClass("ui-disabled");
		
		$("#leftPageAudio").css("display" ,"block");
		$("#rightPageAudio").css("display" ,"block");
		$("#leftRightSeparator").css("display" ,"block");
		
		$("#manualReadAloudEnabler").css("right", "204px");
		if(ismobile == null)
		{
			$(".players").css("width", "595px");
			$("#jp_interface_1").css("width", "595px");
		}
		else
		{
			$("#leftPageAudio").css("left", "358px");
			$("#rightPageAudio").css("left", "445px");
			
			$(".players").css("width", "556px");
			$("#jp_interface_1").css("width", "556px");
		}
	}
}

/**
 * This function handles the page change index. In this method, if page changes, and current page does not have an audio associated, it disables the audio button in the side panel.
 * If audio player is open and/or playing the audio, the audio stops playing and the player closes. 
 * 
 * @param {Object} objComp ScrollViewComp
 * @return void
 */
MG.MediaPlayerOperator.prototype.scrollViewRenderPageIndexChange = function(bTriggerAudioPanelClickEvent, byPassPageCheck, bManageButtons)
{
	var objThis = this;
	
	//bTriggerAudioPanelClickEvent is false only when panel is closing. Ensuring that there is no change in the layout of panel when panel is closing;
	//if(bManageButtons == true || bManageButtons == null)
        //objThis.manageLeftRightPageAudioButtons();
	
	if(objThis.pageIndexOfCurrentPageAudio != GlobalModel.currentPageIndex|| byPassPageCheck)
	{
		var bEnableAudioIcon = false;
		var strCurrentPageName = objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getCurrentPageName');
		
		var arrPageNames = strCurrentPageName.split(",");
		
		//change css of read-aloud icon if being used
		if(this.currentAudioComp && $(this.currentAudioComp).hasClass(this.onState))
		{
			$(this.currentAudioComp).removeClass(this.onState).addClass(this.offState);
			this.currentAudioComp = null;
		}
		
		//close the player if it is open is is required to close from here.
	 	if(bTriggerAudioPanelClickEvent)
	 	{
	 		clearInterval(audioInterval);
			clearTimeout(audioTimeOut);
			audioIncrTime= 0;
			objThis.closeAudioPanel();
	 		//$(objThis.objAudioLauncher.element).trigger("click");
	 		
	 	}
	}
   
}

MG.MediaPlayerOperator.prototype.getPageLevelAudioPathToPlay = function()
{
	var fnDisablePageAudioButtons = function()
	{
		$("#leftPageAudio").removeClass("leftPageAudio-selected").addClass("leftPageAudio");
		$("#rightPageAudio").removeClass("rightPageAudio-selected").addClass("rightPageAudio");
		
		//disabling left and right page audio buttons;
		$("#leftPageAudio").addClass("ui-disabled");
		$("#rightPageAudio").addClass("ui-disabled");
	}
	
	var strPageName = this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getCurrentPageName');
	
	if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING )
	{
		var objThis = this;
		var fnFetchPageName = function(iPageIndex, iFallbackPageIndex) {
			var strTempPageName = "";
			var arrPageNames = strPageName.split(",");
			
			//if there is a page at given page index and has an associated page level audio, play it else disable buttons and play audio for fall back page index
			//(as audio button will be enabled only if there is at least one page with page level audio)
			if(arrPageNames[iPageIndex] != "" && objThis.hasPageAudio(arrPageNames[iPageIndex]) && objThis.playRightPageAudio == false)
			{
				strTempPageName = arrPageNames[iPageIndex];
				objThis.playRightPageAudio = true;
				if(objThis.hasPageAudio(arrPageNames[iFallbackPageIndex]) == false)
					fnDisablePageAudioButtons();
			}
			else {
				objThis.playRightPageAudio = false;
				fnDisablePageAudioButtons();
				strTempPageName = arrPageNames[iFallbackPageIndex];
			}
			return strTempPageName;
		}
		
		//setting default button selected
		$("#" + this.defaultPageAudio).removeClass(this.defaultPageAudio).addClass(this.defaultPageAudio + "-selected");
		
		if(this.defaultPageAudio == "leftPageAudio")
		{
			strPageName = fnFetchPageName(0, 1);
		}
		else
		{
			strPageName = fnFetchPageName(1, 0);
		}
	}
	else
	{
		//disabling left and right page audio buttons for scroll and single page views;
		fnDisablePageAudioButtons();
	}
	
	return getPathManager().getEPubAudioPath(strPageName);
}

MG.MediaPlayerOperator.prototype.hasPageAudio = function(strPageName)
{
	var bHasAudio = false;
	if (GlobalModel.ePubGlobalSettings.Page) {
        var arrPages = GlobalModel.ePubGlobalSettings.Page;
        
        for (var i = 0; i < arrPages.length; i++) {
            var objItem = arrPages[i];
            
            if (strPageName == objItem.PageName) {
                //bHasAudio = objItem.HasPageLevelAudio;
                if(objItem.mediaOverlayPath)
                {
                	bHasAudio = true;
                }
                break;
            }
        }
    }
    
    return bHasAudio;
}

MG.MediaPlayerOperator.prototype.clearSelection = function()
{
	if (window.getSelection) {
        if (window.getSelection().empty) {// Chrome
            window.getSelection().empty();
        }
        else 
            if (window.getSelection().removeAllRanges) {// Firefox
                window.getSelection().removeAllRanges();
            }
    }
    else {
        if (document.selection) {// IE?
            document.selection.empty();
        }
    }
}

MG.MediaPlayerOperator.prototype.closeAudioPanel = function(isChangeState)
{
	var objThis = this;
	if(objThis.currentAudioComp && $(objThis.currentAudioComp).hasClass(objThis.onState))
			$(objThis.currentAudioComp).removeClass(objThis.onState).addClass(objThis.offState);
	
	if(!isChangeState)
	{
		objThis.objAudioLauncher.options.selected = false;
		$(objThis.objAudioLauncher.element).removeClass('audioLauncherBtn-on').addClass('audioLauncherBtn-off');
	}
	objThis.isAudioPlayerPaused = false;
	objThis.audioPlayerStatus = "";
	objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'setAudioPlayerPaused', false);
	objThis.playerUser = null;
	objThis.currentAudioComp = null;
	objThis.onState = null;
	objThis.offState = null;
	objThis.stopReadAloud();
	objThis.doFunctionCall(AppConst.AUDIO_PLAYER_PANEL, 'stopPlaying');
	//$(objThis.audioPlayerPanelRef.element.parent()).css('display', 'none');
	
	if($(".players").css("display") == "block")
	{
		$(".players").stop(true, true).slideUp(500,"easeInOutExpo",function(){
			if(isChangeState)
			{
				$(objThis.objAudioLauncher.element).removeClass("ui-disabled");
				$(objThis.objAudioLauncher.element).data('buttonComp').enable();
			}
		});
	}
	//enabling events on read-aloud's manual override button
	$("#manualReadAloudEnabler").css('pointer-events', 'auto');
	
	objThis.pageIndexOfCurrentPageAudio = -1;
    
    setTimeout(function()
    {
        objThis.scrollViewRenderPageIndexChange(false, null, false);
    }, 1)	
    $(document).trigger( "repositionLearnosityPanelDown" );
}
MG.MediaPlayerOperator.prototype.playMediaOverlayAudio = function(isRightPageAudio)
{
	var objThis = this;
	if(ismobile == null)
	{
		var playerWidth = "414px";
		$("#ismobileDisabledSeparator").css("display","block");
	}
	else
	{
		var playerWidth = "380px";
		$("#progressSeprator").css("display","none");
		$("#ismobileDisabledSeparator").css("display","none");
	}
	var objPageContainer;
	if(isRightPageAudio)
		objPageContainer = $($('[type=PageContainer]')[GlobalModel.currentPageIndex + 1]);
	else
		objPageContainer = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]);
	objThis.pageIndexOfCurrentPageAudio = parseInt($(objPageContainer).attr("pagenum"));
	objThis.objSMILInfo = $(objPageContainer).data().pagecomp.getOverlaySMILInfo();
	
	if(objThis.objSMILInfo == null)
	{
		$("#alertTxt").html(GlobalModel.localizationData["SMIL_ERROR"]);
		setTimeout(function(){
			PopupManager.addPopup($("#alertPopUpPanel"), null, {
				        isModal: true,
				        isCentered: true,
						popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
						isTapLayoutDisabled: true
				    });
		},0);
		return;
	}						
	objThis.readAloudBlock = $(objPageContainer);
	var arrSpans = $(objThis.readAloudBlock).find("span");
	objThis.readAloudSpans = [];
	for(var l = 0; l < arrSpans.length; l++)
	{
		if(objThis.objSMILInfo[$(arrSpans[l]).attr("id")] != undefined)
		{
			objThis.readAloudSpans.push(arrSpans[l]);
		}
		
	}

	if(EPubConfig.MediaActiveClass && EPubConfig.MediaActiveClass != "")
	{
		objThis.playerUser = "IDPF_AUDIO";
		$("#manualReadAloudEnabler ").css("display", "block");
		$("#ismobileDisabledSeparator").css("display","block");
		if(EPubConfig.Text_Highlight == 'ON')
		{
			$("#manualReadAloudEnabler").addClass('read-aloud-enabled');
			$("#manualReadAloudEnabler").removeClass('read-aloud-disabled');
		}
		else
		{
			$("#manualReadAloudEnabler").removeClass('read-aloud-enabled');
			$("#manualReadAloudEnabler").addClass('read-aloud-disabled');
		}
	}
	else
	{
		$("#manualReadAloudEnabler ").css("display", "none");
		$("#ismobileDisabledSeparator").css("display","none");
		if(ismobile == null)
		{
			var playerWidth = "354px";
		}
		else
		{
			var playerWidth = "320px";
			$("#progressSeprator").css("display","none");
		}
		objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER";	
	}
	$("#leftPageAudio").css("display" ,"none");
	$("#rightPageAudio").css("display" ,"none");
	$("#leftRightSeparator").css("display" ,"none");
	
	if(ismobile)
	{
		$("#volumeContainerList").css("display", "none");
	}
	else
	{
		$("#volumeContainerList").css("display", "block");
	}
	$("#slowerAudioBtn").css("display", "none");
	$(objThis.audioPlayerPanelRef.element).parent().css('width',playerWidth);
	$(objThis.audioPlayerPanelRef.element).find('[id=jp_interface_1]').css('width',playerWidth);
	$(".players").css("width",playerWidth);
	
	objThis.stopReadAloud();
	objThis.clearSelection();
	if(objThis.readAloudSpans.length == 0)
	{
		var audioSrc;
			for(span in objThis.objSMILInfo){
				audioSrc = objThis.objSMILInfo[span].src;
				break;
		}
		objThis.pageLevelAudioPath = getPathManager().getSMILAudioPath(audioSrc)
		objThis.playerUser = "PAGE_LEVEL_AUDIO_PLAYER";	
		objThis.playPageLevelAudio();
		return;
	}
	if($(".players").css("display") != "block")
	{
		$(".players").stop(true, true).slideDown(500, "easeOutBounce", function()
		{
			//adding because in one case player did not open properly;
			$(".players").css("height", "40px");
			$(objThis.objAudioLauncher.element).removeClass("ui-disabled");
			$(objThis.objAudioLauncher.element).data('buttonComp').enable();
		});
		$(document).trigger( "repositionLearnosityPanelUp" );
	}
	
	objThis.playReadAloudAudio(objThis.readAloudSpans[0].id);
	
	$("#bookContentContainer").data('scrollviewcomp').updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent);
}