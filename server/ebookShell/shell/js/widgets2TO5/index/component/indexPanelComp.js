/**
 * Zoom wrapper is a container for zoom-able content. It is used to display the
 * contents of the ebook
 */
(function($, undefined) {
    $.widget("magic.indexPanelComp", $.magic.tabViewComp, {
        isPageLoaded : false,
        //style="text-align: left; overflow: hidden; width:100%;height:100%;"
        options : {
            id : null,
            mediaSrc : "",
            oggMediaSrc : "",
            isMediaReady : false
        },

        events : {},

        member : //member variables
        {
            _objPathManager : null,
            indexData : null,
            indexPanelIscroll : "",
            iSelectableCharacterCode : -1,
            setCharacterPosition : false,
            numberDataObj : []
        },
        _create : function() {
            $.extend(this.options, this.element.jqmData('options'));

        },
        _init : function() {
            $.magic.tabViewComp.prototype._init.call(this);
            var objThis = this
            //this.member._objPathManager = getPathManager();
            var scrollViewCompRef = $("#bookContentContainer").data('scrollviewcomp');
            /*$(scrollViewCompRef).bind(scrollViewCompRef.events.PAGE_RENDER_COMPLETE,function(){
             objThis.loadindexContent();
             });*/

            $(window).resize(function() {
                objThis.updateViewSize();
            });

            $(objThis.element).unbind("tabDisplayChanged panelOpened").bind("tabDisplayChanged panelOpened", function() {
                if (objThis.member.indexData == null) {
                    objThis.loadindexContent();
                } else {
                    if (document.getElementById('indexLeftPanelListParent') == null) {
                        objThis.indexLoadComplete(objThis.member.indexData);
                    }
                    else {
						$("#indexPanel").find('[hotkeyindex]').attr("hotkeyindex", "-1");
						var arrHotKey = $("#tocMainPanel").find('[hotkeyindex]').not('#tocGotoInputBox');
						var hotIndex = findLastHotKeyIndex( arrHotKey );
						setHotkeyIndex( $("#indexPanel").find('[hotkeyindex]') , hotIndex );
						HotkeyManager.tocPanelHandler();
					}
                }
            });
            $(objThis.element).unbind("tabDisplayReset").bind("tabDisplayReset", function() {
				$("#indexPanel").find('[hotkeyindex]').attr("hotkeyindex", "-1");
			});
        },

        displayErrorMessage : function() {
            $(".index-leftPanel").parent().css("background", "none")
            $(".index-leftPanel").parent().html('<div style="margin-top:15px;">' + GlobalModel.localizationData["INDEX_NOT_AVAILABLE"] + '</div>');

            $(".index-leftPanel").css("display", "none");
            $(".index-rightPanel").css("display", "none");

            this.member.indexPanelIscroll = null;

            //unbind the click event on all the alphabets
            for (var j = 65; j < 91; j++) {
                var objComp = $("#char_" + j)[0];
                if (objComp)
                    $(objComp).unbind("click");
            }
            //unbind the click event on all the Numbers
            for (var j = 48; j < 58; j++) {
                var objComp = $("#char_" + j)[0];
                if (objComp)
                    $(objComp).unbind("click");
            }
        },

        /**
         * This function will populate the index panel with alphabets from A to Z.
         */
        populateIndexPanel : function() {
            var self = this;

            //index error handling
            if (objThis.member.indexData == "") {
                throw new Error("INDEX: file empty");
            } else if (objThis.member.indexData.letters == "") {
                throw new Error("INDEX: letters list empty");
            }

            var alphabetList = "<div id='indexLeftPanelListParent'>";
            var arrHotKey = $("#tocMainPanel").find('[hotkeyindex]').not('#tocGotoInputBox');
			var hotIndex = findLastHotKeyIndex( arrHotKey );
            for (var i = 65; i < 91; i++) {

                var strCharInUpperCase = String.fromCharCode(i);
                var strCharInLowerCase = strCharInUpperCase.toLowerCase();
                if (objThis.member.indexData.letters[strCharInLowerCase] != undefined || objThis.member.indexData.letters[strCharInUpperCase] != undefined) {
                    if (objThis.member.indexData.letters[strCharInLowerCase].ln != "")
                    {
                        alphabetList += "<div data-role= 'buttonComp' class='index-leftPanelList' hotkeygroupid='TOCMenu' hotkeyindex='"+hotIndex+"' id='char_" + i + "'>" + strCharInUpperCase + "</div>";
                    	hotIndex++;
                    }
                    else
                        alphabetList += "<div data-role= 'buttonComp' class='index-leftPanelList ui-disabled' id='char_" + i + "'>" + strCharInUpperCase + "</div>";
                } else {
                    alphabetList += "<div data-role= 'buttonComp' class='index-leftPanelList ui-disabled' id='char_" + i + "'>" + strCharInUpperCase + "</div>";
                }
            }
            alphabetList += "</div>";
            //insert the alphabets in the left panel
            $(self.element).find("#leftPanel").html(alphabetList);

            $('.index-leftPanelList').each(function() {
                $(this)['buttonComp']();
                if ($(this).hasClass('ui-disabled')) {
                    $(this).data('buttonComp').disable();
                }
            });
            var numberList = "<div id='indexLeftPanelListParent'> <span class='index-leftPanelNumberText leftFloat'>Numbers</span>";
            var numberLen = objThis.member.indexData.numbers.num.length;
            if (numberLen == undefined)
                numberLen = 1;

            for (var i = 48; i < 58; i++) {
                var included = false;
                for ( j = 0; j < numberLen; j++) {
                    var strChar = String.fromCharCode(i);
                    if (numberLen == 1) {
                        if (objThis.member.indexData.numbers.num.txt == strChar && objThis.member.indexData.numbers.num.ln != "") {
                            included = true;
                            numberList += "<div data-role='buttonComp' class='index-leftPanelNumberList' hotkeygroupid='TOCMenu' hotkeyindex='"+hotIndex+"'  id='char_" + i + "'><b>" + strChar + "</b></div>";
                            objThis.member.numberDataObj.push(objThis.member.indexData.numbers.num)
                            hotIndex++;
                        }
                    } else {
                        if (objThis.member.indexData.numbers.num[j].txt == strChar && objThis.member.indexData.numbers.num[j].ln != "") {
                            included = true;
                            numberList += "<div data-role='buttonComp' class='index-leftPanelNumberList' hotkeygroupid='TOCMenu' hotkeyindex='"+hotIndex+"' id='char_" + i + "'><b>" + strChar + "</b></div>";
                            objThis.member.numberDataObj.push(objThis.member.indexData.numbers.num[j])
                            hotIndex++;
                        }
                    }
                }

                if (!included) {
                    numberList += "<div data-role='buttonComp' class='index-leftPanelNumberList ui-disabled' id='char_" + i + "'><b>" + strChar + "</b></div>";
                }
            }
            numberList += "</div>";
            //insert the alphabets in the left panel
            $(self.element).find("#leftPanelNumber").html(numberList);

            $('.index-leftPanelNumberList').each(function() {
                $(this)['buttonComp']();
                if ($(this).hasClass('ui-disabled')) {
                    $(this).data('buttonComp').disable();
                }
            });

            //bind the click event to all the alphabets
            for (var j = 65; j < 91; j++) {
                var objComp = $("#char_" + j)[0];
                $(objComp).bind("click", function(e) {
                    if ($(this).hasClass('ui-disabled') == false) {
                        self.member.iSelectableCharacterCode = parseInt(this.id.split("_")[1])
                        self.indexClickHandler(this);
                        e.stopPropagation();
                    }
                });
            }

            //bind the click event to all the alphabets
            for (var j = 48; j < 58; j++) {
                var objComp = $("#char_" + j)[0];
                $(objComp).bind("click", function(e) {
                    if ($(this).hasClass('ui-disabled') == false) {
                        self.member.iSelectableCharacterCode = parseInt(this.id.split("_")[1])
                        self.indexClickHandler(this);
                        e.stopPropagation();
                    }

                });
            }
        },

        /**
         * This function is called when an alphabet in the index panel is clicked.
         * @param {Object} alphabetRef: The alphabet which is clicked
         */
        indexClickHandler : function(alphabetRef) {
            var self = this;
            $($($(self.element).find("#leftPanel")).find('[class="index-leftPanelList index-leftPanelListSelected"]')).removeClass('index-leftPanelListSelected');
            $($($(self.element).find("#leftPanelNumber")).find('[class="index-leftPanelNumberList index-leftPanelListNumberSelected"]')).removeClass('index-leftPanelListNumberSelected');
            //$($($(self.element).find("#leftPanel")).find('[class=index-leftPanelList]')).css('color','white');
            if ($.isNumeric($(alphabetRef).text())) {
                $(alphabetRef).addClass('index-leftPanelListNumberSelected');
            } else {
                $(alphabetRef).addClass('index-leftPanelListSelected');
            }
            //$(alphabetRef).css('color','#383636');
            var strLetter = $(alphabetRef).text();
            $(".indexListHide").css("display", "none");
            $($("#indexList_" + strLetter)[0]).css("display", "block");
            self.member.indexPanelIscroll.refresh();
            this.setIndexContent(strLetter);
        },

        /**
         * This will show the related words with the clicked letter
         * @param {Object} strLetter - The clicked Letter
         */
        setIndexContent : function(strLetter) {
            var objThis = this;
            var strLetter = strLetter.toUpperCase();
            if (objThis.member.indexPanelIscroll)
                objThis.member.indexPanelIscroll.scrollToElement("#indexList_" + strLetter, 0);
        },
        /**
         * This function loads the content of index.xml in an object through ajax call.
         */
        loadindexContent : function() {
            //content/en/ePubPOC/OPS/glossary.xml
            var objThis = this;
            var objPathManager = getPathManager();
            var fileName = EPubConfig.indexXMLPath;
            var strUri = objPathManager.getIndexPath(fileName);

            var type = "GET";
            //var url = "content/en/Journeys_2013_GK_L15_9780544009370/OEBPS/Page001.html";
            // var url = this.member._objPathManager.getEPubGlossaryPath();
            var url = strUri;

            if (EPubConfig.Index_isAvailable) {
                // If file type is JSON
                if (url.indexOf('.json') != -1) {
                    $.getJSON(url, function(objData) {
                        if (objData.index) {
                            objData = objData.index;
                        }
                        objThis.indexLoadComplete(objData);
                    });
                }
                // If file type is xml
                else if (url.indexOf('.xml') != -1) {
                    $.ajax({
                        type : "GET",
                        url : url,
                        success : function(data) {
                            var objData = $.xml2json(data);
                            objThis.indexLoadComplete(data);
                        },
                        error : function() {
                            objThis.displayErrorMessage();
                        }
                    });
                }

                //objThis.updateViewSize();
            }
        },

        indexLoadComplete : function(objData) {
            var objThis = this;

            var indexData = objData;
            objThis.member.indexData = indexData;

            /*var strXML;

             if (window.ActiveXObject) {
             var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
             xmlObject.async = false;
             strXML = objData;
             }
             else {
             strXML = new XMLSerializer().serializeToString(objData);
             }
             indexData = $.xml2json(strXML);
             objThis.member.indexData = indexData;*/

            try {
                objThis.populateIndexPanel(indexData);
                var indexList = objThis.createIndexList(indexData);
                objThis.createNumIndexList(indexData, indexList);
                setTimeout(function() {
                    objThis.scrollIndexElementToTop();
                }, 200);
            } catch (e) {
                console.log(e.message)
                objThis.displayErrorMessage();
            }
        },

        createIndexList : function(indexData) {
            var objThis = this;
            var indexList = "";

            var arrRejectedCharacters = [];

            for (var i = 65; i <= 90; i++) {
                var strCharacterInUpperCase = String.fromCharCode(i);
                var strCharacterInLowerCase = strCharacterInUpperCase.toLowerCase();

                if (objThis.member.indexData.letters[strCharacterInLowerCase] != undefined || objThis.member.indexData.letters[strCharacterInUpperCase] != undefined) {
                    var characterToUse = (objThis.member.indexData.letters[strCharacterInLowerCase]) ? strCharacterInLowerCase : strCharacterInUpperCase;
                    var objCharacterInfo = objThis.member.indexData.letters[characterToUse];
                    // This list starts the new Alphabet
                    indexList += "<div id='indexList_" + strCharacterInUpperCase + "' class='leftFloat indexListHide' style='width: 100%;'><div class='alphabetIndexed leftFloat'>" + strCharacterInUpperCase + "</div>";

                    if (objCharacterInfo.ln == "" || objCharacterInfo.ln == null) {
                        //throw new Error("INDEX: No word(s) defined for character '" + strCharacterInUpperCase + "'")
                    } else {
                        if (objThis.member.iSelectableCharacterCode == -1) {
                            objThis.member.iSelectableCharacterCode = i;
                        }
                    }

                    if (objCharacterInfo.ln != undefined) {
                        var objLnLength;
                        if (objCharacterInfo.ln.length == undefined) {
                            if ( typeof (objCharacterInfo.ln) == "object") {
                                objLnLength = 1;
                            }
                        } else {
                            objLnLength = objCharacterInfo.ln.length;
                        }
                    }

                    for (var p = 0; p < objLnLength; p++) {
                        var strLetterWordLength;
                        if (objLnLength == 1) {
                            strLetterWordLength = objCharacterInfo.ln.word.length;
                            if (strLetterWordLength == undefined) {
                                if ( typeof (objCharacterInfo.ln.word) == "object") {
                                    strLetterWordLength = 1;
                                }
                            }
                        } else {
                            strLetterWordLength = objCharacterInfo.ln[p].word.length;
                            if (strLetterWordLength == undefined) {
                                if ( typeof (objCharacterInfo.ln[p].word) == "object") {
                                    strLetterWordLength = 1;
                                }
                            }
                        }
                        var iNumWordsAddedPerCharacter = 0;
                        for (var j = 0; j < strLetterWordLength; j++) {
                            var indexWord = "";
                            if (strLetterWordLength == 1) {
                                if (objLnLength == 1) {
                                    indexWord = objCharacterInfo.ln.word.term;
                                } else {
                                    indexWord = objCharacterInfo.ln[p].word.term;
                                }
                            } else {
                                if (objLnLength == 1) {
                                    indexWord = objCharacterInfo.ln.word[j].term;
                                } else {
                                    indexWord = objCharacterInfo.ln[p].word[j].term;
                                }
                            }
                            if (indexWord && indexWord.trim != "") {
                                // This div includes new WORD
								indexList += "<div id='elem_" + characterToUse + "_" + j + "' class='indexItems leftFloat clear' style='cursor: pointer; width:100%;'><div class='indexWord'><div class='leftFloat indexHeading' style='clear:left;font-weight: bold;line-height: 24px;'>" + indexWord + ":";
                                if (strLetterWordLength == 1) {
                                    if (objLnLength == 1) {
                                        addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln.word);
                                    } else {
                                        addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln[p].word);
                                    }
                                } else {
                                    if (objLnLength == 1) {
                                        addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln.word[j]);
                                    } else {
                                        addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln[p].word[j]);
                                    }
                                }
                                indexList += addedPageIndexList + "</div></div>";
                                var subWrdRefLength = "";
                                var lnRef;
                                if (objLnLength == 1) {
                                    lnRef = objCharacterInfo.ln;
                                } else {
                                    lnRef = objCharacterInfo.ln[p];
                                }
                                if (strLetterWordLength == 1) {
                                    if (lnRef.word.word == undefined) {
                                        subWrdRefLength = 0
                                    } else {
                                        subWrdRefLength = lnRef.word.word.length;
                                    }
                                } else {
                                    if (lnRef.word[j].word == undefined) {
                                        subWrdRefLength = 0
                                    } else {
                                        subWrdRefLength = lnRef.word[j].word.length;
                                    }

                                }
                                if (subWrdRefLength == undefined) {
                                    if (strLetterWordLength == 1) {
                                        if ( typeof (lnRef.word.word) == "object") {
                                            subWrdRefLength = 1;
                                        }
                                    } else {
                                        if ( typeof (lnRef.word[j].word) == "object") {
                                            subWrdRefLength = 1;
                                        }
                                    }
                                }
                                //var subWrdRefLength = objCharacterInfo.ln.word[j].page.length;
                                var iNumberOfSubWordReferenceAdded = 0;
                                for (var k = 0; k < subWrdRefLength; k++) {
                                    var strSubWord = "";
                                    if (subWrdRefLength == 1) {
                                        if (strLetterWordLength == 1) {
                                            strSubWord = lnRef.word.word.subterm;
                                        } else {
                                            strSubWord = lnRef.word[j].word.subterm;
                                        }
                                    } else {
                                        if (strLetterWordLength == 1) {
                                            strSubWord = lnRef.word.word[k].subterm;
                                        } else {
                                            strSubWord = lnRef.word[j].word[k].subterm;
                                        }
                                    }
                                    // var pageNumber = objCharacterInfo.ln.word[j].page[k].reference;

                                    //if page number value is not null or blank
                                    if (strSubWord && strSubWord.trim() != "") {
                                        iNumberOfSubWordReferenceAdded++;
                                        var marginBtm = 0;
                                        /*
                                        if (k == subWrdRefLength - 1) {
                                        marginBtm = 13;
                                        }
                                        else {
                                        marginBtm = 0;
                                        }
                                        */
                                        // This includes the subwords
                                        indexList += "<div class='leftFloat clear subWordContainer' style='width:100%;'><div id='pagenumber_" + characterToUse + "_" + k + "' class='pageNumber indexItemsList' style='margin-bottom:" + marginBtm + "px'>" + strSubWord + ":&nbsp";
                                    } else {
                                        throw new Error("INDEX: Sub-term not defined for word '" + indexWord + "'");
                                    }

                                    var addedPagesubWordList = "";
                                    if (subWrdRefLength == 1) {
                                        if (strLetterWordLength == 1) {
                                            addedPagesubWordList = objThis.addPageReference(lnRef.word.word);
                                        } else {
                                            addedPagesubWordList = objThis.addPageReference(lnRef.word[j].word);
                                        }

                                    } else {
                                        if (strLetterWordLength == 1) {
                                            addedPagesubWordList = objThis.addPageReference(lnRef.word.word[k]);
                                        } else {
                                            addedPagesubWordList = objThis.addPageReference(lnRef.word[j].word[k]);
                                        }

                                    }
                                    indexList += addedPagesubWordList + "</div></div>";
                                }

                                //if no page references have been added, remove the reference of entire word
                                /*
                                 if (iNumberOfSubWordReferenceAdded == 0) {
                                 indexList = indexList.replace("<div id='elem_" + characterToUse + "_" + j + "' style='cursor: pointer;'><div class='indexWord'><div class='strLetter indexItemsList' style='clear:left;font-weight: bold;line-height: 35px;'>" + indexWord + "</div></div>", "");
                                 }
                                 else {
                                 iNumWordsAddedPerCharacter++;
                                 indexList += "</div>";
                                 }
                                 */
                                indexList += "</div>";
                            }
                        }
                        /*
                         if (iNumWordsAddedPerCharacter == 0) {
                         arrRejectedCharacters.push(i);
                         indexList = indexList.replace("<div id='indexList_" + strCharacterInUpperCase + "'><div class='alphabetIndexed'>" + characterToUse + "</div>", "");
                         }
                         else {
                         if (objThis.member.iSelectableCharacterCode == -1) {
                         objThis.member.iSelectableCharacterCode = i;
                         }

                         indexList += "</div>";
                         }
                         */
                    }
                    indexList += "</div>";
                }

            }
            return indexList;
        },

        createNumIndexList : function(indexData, indexList) {
            var objThis = this;
            var arrRejectedCharacters = [];

            for (var i = 48; i <= 58; i++) {
                var strCharacter = String.fromCharCode(i);
                for ( l = 0; l < objThis.member.numberDataObj.length; l++) {
                    if (objThis.member.numberDataObj[l].txt == strCharacter) {

                        var objCharacterInfo = objThis.member.numberDataObj[l];
                        // This list starts the new Number
                        indexList += "<div id='indexList_" + strCharacter + "' class='leftFloat indexListHide' style='width: 100%; '><div class='alphabetIndexed leftFloat'>" + strCharacter + "</div>";

                        if (objCharacterInfo.ln == "" || objCharacterInfo.ln == null) {
                            // throw new Error("INDEX: No word(s) defined for character '" + strCharacter + "'")
                        } else {
                            if (objThis.member.iSelectableCharacterCode == -1) {
                                objThis.member.iSelectableCharacterCode = i;
                            }
                        }
                        if (objCharacterInfo.ln != undefined) {
                            var objLnLength;
                            if (objCharacterInfo.ln.length == undefined) {
                                if ( typeof (objCharacterInfo.ln) == "object") {
                                    objLnLength = 1;
                                }
                            } else {
                                objLnLength = objCharacterInfo.ln.length;
                            }
                        }

                        for (var p = 0; p < objLnLength; p++) {
                            var strLetterWordLength;
                            if (objLnLength == 1) {
                                strLetterWordLength = objCharacterInfo.ln.word.length;
                                if (strLetterWordLength == undefined) {
                                    if ( typeof (objCharacterInfo.ln.word) == "object") {
                                        strLetterWordLength = 1;
                                    }
                                }
                            } else {
                                strLetterWordLength = objCharacterInfo.ln[p].word.length;
                                if (strLetterWordLength == undefined) {
                                    if ( typeof (objCharacterInfo.ln[p].word) == "object") {
                                        strLetterWordLength = 1;
                                    }
                                }
                            }
                            var iNumWordsAddedPerCharacter = 0;
                            for (var j = 0; j < strLetterWordLength; j++) {
                                var indexWord = "";
                                if (strLetterWordLength == 1) {
                                    if (objLnLength == 1) {
                                        indexWord = objCharacterInfo.ln.word.term;
                                    } else {
                                        indexWord = objCharacterInfo.ln[p].word.term;
                                    }
                                } else {
                                    if (objLnLength == 1) {
                                        indexWord = objCharacterInfo.ln.word[j].term;
                                    } else {
                                        indexWord = objCharacterInfo.ln[p].word[j].term;
                                    }
                                }
                                if (indexWord && indexWord.trim != "") {
                                    // This div includes new WORD
									indexList += "<div id='elem_" + strCharacter + "_" + j + "' class='indexItems leftFloat clear' style='cursor: pointer; width:100%;'><div class='indexWord'><div class='leftFloat indexHeading' style='clear:left;font-weight: bold;line-height: 24px;'>" + indexWord + ":";
                                    if (strLetterWordLength == 1) {
                                        if (objLnLength == 1) {
                                            addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln.word);
                                        } else {
                                            addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln[p].word);
                                        }
                                    } else {
                                        if (objLnLength == 1) {
                                            addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln.word[j]);
                                        } else {
                                            addedPageIndexList = objThis.addPageReference(objCharacterInfo.ln[p].word[j]);
                                        }
                                    }
                                    indexList += addedPageIndexList + "</div></div>";
                                    var subWrdRefLength = "";
                                    var lnRef;
                                    if (objLnLength == 1) {
                                        lnRef = objCharacterInfo.ln;
                                    } else {
                                        lnRef = objCharacterInfo.ln[p];
                                    }
                                    if (strLetterWordLength == 1) {
                                        if (lnRef.word.word == undefined) {
                                            subWrdRefLength = 0
                                        } else {
                                            subWrdRefLength = lnRef.word.word.length;
                                        }
                                    } else {
                                        if (lnRef.word[j].word == undefined) {
                                            subWrdRefLength = 0
                                        } else {
                                            subWrdRefLength = lnRef.word[j].word.length;
                                        }

                                    }
                                    if (subWrdRefLength == undefined) {
                                        if (strLetterWordLength == 1) {
                                            if ( typeof (lnRef.word.word) == "object") {
                                                subWrdRefLength = 1;
                                            }
                                        } else {
                                            if ( typeof (lnRef.word[j].word) == "object") {
                                                subWrdRefLength = 1;
                                            }
                                        }
                                    }
                                    //var subWrdRefLength = objCharacterInfo.ln.word[j].page.length;
                                    var iNumberOfSubWordReferenceAdded = 0;
                                    for (var k = 0; k < subWrdRefLength; k++) {
                                        var strSubWord = "";
                                        if (subWrdRefLength == 1) {
                                            if (strLetterWordLength == 1) {
                                                strSubWord = lnRef.word.word.subterm;
                                            } else {
                                                strSubWord = lnRef.word[j].word.subterm;
                                            }
                                        } else {
                                            if (strLetterWordLength == 1) {
                                                strSubWord = lnRef.word.word[k].subterm;
                                            } else {
                                                strSubWord = lnRef.word[j].word[k].subterm;
                                            }
                                        }
                                        // var pageNumber = objCharacterInfo.ln.word[j].page[k].reference;

                                        //if page number value is not null or blank
                                        if (strSubWord && strSubWord.trim() != "") {
                                            iNumberOfSubWordReferenceAdded++;
                                            var marginBtm = 0;
                                            /*
                                            if (k == subWrdRefLength - 1) {
                                            marginBtm = 13;
                                            }
                                            else {
                                            marginBtm = 0;
                                            }
                                            */
                                            // This includes the subwords
                                            indexList += "<div class='leftFloat clear subWordContainer' style='width:100%;'><div id='pagenumber_" + strCharacter + "_" + k + "' class='pageNumber indexItemsList' style='margin-bottom:" + marginBtm + "px'>" + strSubWord + ":&nbsp";
                                        } else {
                                            throw new Error("INDEX: Sub-term not defined for word '" + indexWord + "'");
                                        }

                                        var addedPagesubWordList = "";
                                        if (subWrdRefLength == 1) {
                                            if (strLetterWordLength == 1) {
                                                addedPagesubWordList = objThis.addPageReference(lnRef.word.word);
                                            } else {
                                                addedPagesubWordList = objThis.addPageReference(lnRef.word[j].word);
                                            }

                                        } else {
                                            if (strLetterWordLength == 1) {
                                                addedPagesubWordList = objThis.addPageReference(lnRef.word.word[k]);
                                            } else {
                                                addedPagesubWordList = objThis.addPageReference(lnRef.word[j].word[k]);
                                            }

                                        }
                                        indexList += addedPagesubWordList + "</div></div>";
                                    }

                                    //if no page references have been added, remove the reference of entire word
                                    /*
                                     if (iNumberOfSubWordReferenceAdded == 0) {
                                     indexList = indexList.replace("<div id='elem_" + characterToUse + "_" + j + "' style='cursor: pointer;'><div class='indexWord'><div class='strLetter indexItemsList' style='clear:left;font-weight: bold;line-height: 35px;'>" + indexWord + "</div></div>", "");
                                     }
                                     else {
                                     iNumWordsAddedPerCharacter++;
                                     indexList += "</div>";
                                     }
                                     */
                                    indexList += "</div>";
                                }
                            }
                            /*
                             if (iNumWordsAddedPerCharacter == 0) {
                             arrRejectedCharacters.push(i);
                             indexList = indexList.replace("<div id='indexList_" + strCharacterInUpperCase + "'><div class='alphabetIndexed'>" + characterToUse + "</div>", "");
                             }
                             else {
                             if (objThis.member.iSelectableCharacterCode == -1) {
                             objThis.member.iSelectableCharacterCode = i;
                             }

                             indexList += "</div>";
                             }
                             */
                        }
                        indexList += "</div>";
                    }
                }
            }
            $("#addedIndex").html(indexList);
            objThis.member.indexPanelIscroll = new iScroll('addedIndexParent', {
                scrollbarClass : 'myScrollbar'
            });
            for (var iCounter = 0; iCounter < arrRejectedCharacters.length; iCounter++) {
                var iChar = arrRejectedCharacters[iCounter]
                //$("#char_" + iChar).addClass("ui-disabled");
            }
            var chartoshow = "";

            for (var i = 65; i <= 90; i++) {
                var strCharacterInUpperCase = String.fromCharCode(i);
                var strCharacterInLowerCase = strCharacterInUpperCase.toLowerCase();
                if (objThis.member.indexData.letters[strCharacterInLowerCase] != undefined || objThis.member.indexData.letters[strCharacterInUpperCase] != undefined) {
                    var characterToUse = (objThis.member.indexData.letters[strCharacterInLowerCase]) ? strCharacterInLowerCase : strCharacterInUpperCase;
                    if (objThis.member.indexData.letters[characterToUse].ln != "") {
                        var objCharacterInfo = objThis.member.indexData.letters[characterToUse];
                        chartoshow = strCharacterInUpperCase;
                        break;
                    }
                }
            }
            if (chartoshow == "") {
                for (var i = 0; i < objThis.member.numberDataObj.length; i++) {
                    if (objThis.member.numberDataObj[i].ln != "") {
                        chartoshow = objThis.member.numberDataObj[i].txt
                        break;
                    }
                }

            }
            objThis.wordClickHandler();
            $(".indexListHide").css("display", "none");
            $($("#indexList_"+chartoshow)[0]).css("display", "block");
            objThis.member.indexPanelIscroll.refresh();
            setTimeout(function() {
                objThis.updateViewSize();
            }, 300);
        },

        addPageReference : function(XMLRefPt) {

            var strText = (XMLRefPt.term) ? "term '" + XMLRefPt.term : "sub-term '" + XMLRefPt.subterm;
            strText = strText + "'";

            if (XMLRefPt.page == null)
                throw new Error("INDEX: Page reference(s) for " + strText + " not defined.")

            var pageRefLength = XMLRefPt.page.length;
            var addedPageIndexList = "";
            if ( typeof (XMLRefPt) == "object" && pageRefLength == undefined) {
                pageRefLength = 1;
            }

            for (var i = 0; i < pageRefLength; i++) {
                var pageNumber;
                if (pageRefLength == 1) {
                    pageNumber = XMLRefPt.page.reference;
                } else {
                    pageNumber = XMLRefPt.page[i].reference;
                }
                addedPageIndexList += "<div class='pageIndexParent'>";
                if (pageNumber)
                    addedPageIndexList += "<div class='addPageIndex'  pageNumber='" + pageNumber + "'  >" + pageNumber;
                else {
                    throw new Error("INDEX: Page reference for " + strText + " not defined.")
                }

                addedPageIndexList += "</div>";
                if (i != (pageRefLength - 1)) {
                    addedPageIndexList += "<span class='commaAfterPageNumber' style='cursor: default !important; text-decoration: none !important;'>,</span>";
                }
                 addedPageIndexList += "</div>";
            }
            return addedPageIndexList;
        },

        /**
         * This function handles when a word pagenumber from the list is clicked.
         * @param1 {Object} Object of data-role an element is binded with
         * @return void
         */
        wordClickHandler : function() {
            var objThis = this;
            $('.addPageIndex').unbind('click').bind('click', function() {
                $(objThis.element).trigger("loadPageThorughIndexPanel", $(this).attr("pageNumber"));
            });
        },
        /**
         * This function is used to refresh iscroll and update the panel view
         */
        updateViewSize : function() {
            var objThis = this;
            setTimeout(function() {
                var headerHeight = ($("#header").css('display') == 'block') ? $("#header").height() : 1;
                $("#addedIndexElementlist").height($("#tocMainPanel").height() - (192 + headerHeight));
                if (objThis.member.indexPanelIscroll)
                    objThis.member.indexPanelIscroll.refresh();

                if (ismobile == null && objThis.member.indexPanelIscroll) {
                    //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                    iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), objThis.member.indexPanelIscroll, "addedIndexElementlist");

                }

            }, 100);

        },

        /**
         * This function is called everytime the index panel is opened.
         */
        scrollIndexElementToTop : function() {
            var self = this;
            setTimeout(function() {
                var headerHeight = ($("#header").css('display') == 'block') ? $("#header").height() : 1;
                $("#addedIndexElementlist").height($("#tocMainPanel").height() - (192 + headerHeight));

                if (self.member.indexPanelIscroll)
                    self.member.indexPanelIscroll.refresh();

                /* $($($(self.element).find("#leftPanel")).find('[class="index-leftPanelList index-leftPanelListSelected"]')).removeClass('index-leftPanelListSelected');
                 $($($(self.element).find("#leftPanelNumber")).find('[class="index-leftPanelNumberList index-leftPanelListNumberSelected"]')).removeClass('index-leftPanelListNumberSelected');*/

                if (self.member.iSelectableCharacterCode > -1) {

                    //scrolling to top of Character's index list
                    if (self.member.indexPanelIscroll) {
                        var objComp = $("#indexList_" + String.fromCharCode(self.member.iSelectableCharacterCode)).find('[class="alphabetIndexed leftFloat"]');
                        self.member.indexPanelIscroll.scrollToElement($(objComp)[0], 0);
                    }
                    //making select-able character selected
                    if (self.member.iSelectableCharacterCode < 60) {
                        $("#char_" + self.member.iSelectableCharacterCode).addClass("index-leftPanelListNumberSelected");
                    } else {
                        $("#char_" + self.member.iSelectableCharacterCode).addClass("index-leftPanelListSelected");
                    }

                }
                if (ismobile == null && self.member.indexPanelIscroll) {
                    //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
                    iScrollClickHandler($(self.element).find("[class=myScrollbarV]"), self.member.indexPanelIscroll, "addedIndexElementlist");

                }
            }, 1);
        }
    })
})(jQuery);
