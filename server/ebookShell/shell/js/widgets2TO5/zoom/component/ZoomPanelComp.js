/**
 * @author abhishek.shakya
 */
( function( $, undefined ) {
$.widget( "magic.zoomPanelComp", $.magic.magicwidget, 
{
	options: 
	{
		strList:"",
		level:0,
		curriculumXml :""
	},
		////////////////////////NOTEPANEL
	_getPanel: function()
	{
		return  this.getVar('objPanel');
	},
	_create: function()
	{
		
	},
	_init: function()
	{
        $.magic.magicwidget.prototype._init.call(this);
		self = this;
		
		self.updatePosition(self.element);
		$(window).bind('resize', function(){
			self.updatePosition();
		});
		
		self.hideZoomButtons();
		self.hidePageViewButtons();
	},
	
	hideZoomButtons: function()
	{
		if (ismobile || EPubConfig.Zoom_isAvailable == false) {
            $(".zoomTxtParent").css('display', 'none');
            $(".zoomSliderParent").css('display', 'none');
            $(this.element).css({"top":"0px"})
            $("#zoomBtnPanel").css({"margin-top":"-4px","height":"81px !important"})
            $("#zoomPanelArrow").css({"margin-top":"0px"})
        }
	},
	
	hidePageViewButtons : function()
	{
		var _width = 0, _defaultView = "", _singlePageIconWidth = 0, _scrollPageIconWidth = 0, _doublePageIconWidth = 0;
		
		_defaultView = EPubConfig.Page_Navigation_Type;
		
		// set visibility
		if( !EPubConfig.SinglePageView_isAvailable && ( _defaultView != AppConst.SINGLE_PAGE ) )
		{
			$( this.element ).find('[type="singlePageLauncher"]').css( "display", "none" );
		}
		else
		{
			_singlePageIconWidth = $( this.element ).find('[type="singlePageLauncher"]').width();
		}
		
		if( !EPubConfig.ScrollPageView_isAvailable && ( _defaultView != AppConst.SCROLL_VIEW ) )
		{
			$( this.element ).find('[type="scrollPageLauncher"]').css( "display", "none" );
		}
		else
		{
			_scrollPageIconWidth = $( this.element ).find('[type="scrollPageLauncher"]').width();
		}
		
		if( !EPubConfig.DoublePageView_isAvailable && ( _defaultView != AppConst.DOUBLE_PAGE ) )
		{
			$( this.element ).find('[type="doublePageLauncher"]').css( "display", "none" );
		}
		else
		{
			_doublePageIconWidth = $( this.element ).find('[type="doublePageLauncher"]').width();
		}
		
		_width = _singlePageIconWidth + _scrollPageIconWidth + _doublePageIconWidth;
		$( this.element ).find(".viewSwitchBtnParent").css( "width", _width);
	},
	
	onPanelOpen: function() {
		//this.hideZoomButtons();
	},
	
	onPanelClose: function() {
		 
	},
	
	updatePosition:function(){
       $(this.element).parent().css('top', $(zoomBtn).offset().top);
       $(this.element).parent().css('left' , $($(zoomBtn).parent().find('[id="zoomBtn"]')).width());
       $(this.element).css('top', 0);
	}
	
})
})( jQuery );
