/**
 * @author sachin.manchanda
 * Class handles the search panel
 */
$.widget( "magic.searchPanelComp", $.magic.tabViewComp, 
{
	_create: function()
	{
		$.extend(this.options, this.element.jqmData('options'));
	},
	_init: function()
	{
		var self = this;
		$.magic.tabViewComp.prototype._init.call(this);	
	},
	destroy: function()
	{
		$.widget.prototype.destroy.call( this );		
	}
		
});
