var strList="";
var level=0;
var curriculumXml ="";
var myScroll;
var bFirstTime = true;
var currentNavId="nav1";
function treeItemClick(e)
{
	tocItemId = e;

	if (tocItemId) {
		//OSSProductSpec.interactionId = tocItemId;
		launchNewInteraction(tocItemId);
	}
}
var TreeUIHelper = function() {

    return {

    linkList: [

    ],
    eventManager: {

    },

    events : {
        UPDATE_TREE_VIEW_SIZE : "UPDATE_TREE_VIEW_SIZE"
    },

    addCustomEvent: function(strEventType, objFunction){
        if(this.eventManager[strEventType])
        {
            this.eventManager[strEventType].push(objFunction);
        }
        else
        {
            this.eventManager[strEventType] = [];
            this.eventManager[strEventType].push(objFunction);
        }
    },

    dispatchCustomEvent: function(strEventType, objData){
        if(this.eventManager[strEventType])
        {
            for(var i = 0; i < this.eventManager[strEventType].length; i++)
            {
                this.eventManager[strEventType][i](objData);
            }
        }
    },


    init: function(objhidden,objInteraction, bAllPagesInBookAreHidden)
	{
		var objThis = this;
		this.renderTree(objhidden, objInteraction, bAllPagesInBookAreHidden);
		
		$(document).bind("updateHotkeyIndex", function(e){
			objThis.updateHotKeyIndex();
		});
	},
	
 /**
   * Set TOC View on page scroll
   * @pageid ID of page genereated on page scroll
   * @return null
   */

	
	setTOCView: function(pageid) {
		var id = "#" + pageid;
try
{

		//searching for nav id to set instead of nav1
		if (($.find("[id=" + pageid + "]") != '') && ($.find("[id=" + pageid + "]") != undefined)) {
			
			//removing highlighted state from all depth 4 (page) nodes
			$(".treeview").find(".depth4-nodeContentCont").replaceClass("depth4-nodeContentContToggle", "depth4-nodeContentCont").end();
			
			var objParent = $(id).parent();
			
			//if current node is depth 4 node, set it's toggle state;
			if($(objParent).hasClass("depth4"))
			{
				$(objParent).find('[id=' + pageid + ']').find(".depth4-nodeContentCont").swapClass("depth4-nodeContentCont", "depth4-nodeContentContToggle").end();
			}
			
			if($(objParent).hasClass("treeview") == false)
			{
				$(id).swapClass("expandable", "collapsable");
			}
			
			while (true) {
				if ($(objParent).hasClass("treeview")) {
					break;
				}
				else
				{
					var arrToggableNode = $(objParent).find(".viewlevel-0");
					
					if(arrToggableNode[0])
					{
						objPreviouslyToggledNode = arrToggableNode[0];
						$($(objPreviouslyToggledNode).parent()).swapClass("expandable", "collapsable");
					}
				}
				objParent.find(">li").show();
				objParent.find("#arrowIcon").show();
				objParent.css("display", "block");
				objParent = objParent.parent();
				objParent.find(">.nodeContentCont").swapClass("depth1-nodeContentCont", "depth1-nodeContentContToggle").end();
				objParent.find(">.nodeContentCont").swapClass("depth2-nodeContentCont", "depth2-nodeContentContToggle").end();
				objParent.find(">.hitarea").swapClass("expandable-hitarea", "collapsable-hitarea").end();
				
				
			}

			$(id).parent().siblings().find(".nodeContentCont>.nodeContent>.depth1-nodeIcon").swapClass("depth1-nodeIcon", "depth1-nodeIconToggle").end();

			$(id).parent().parent().find(">.nodeContentCont>.nodeContent>.depth1-nodeIcon").swapClass("depth1-nodeIcon", "depth1-nodeIconToggle").end();

			$(id).find(">.nodeContentCont>.nodeContent>.depth1-nodeIcon").swapClass("depth1-nodeIcon", "depth1-nodeIconToggle").end();
		}
}
catch(err)
{
	//console.log("err");
}

	},

	swapClass: function(c1, c2) {
	var c1Elements = this.filter('.' + c1);
	this.filter('.' + c2).removeClass(c2).addClass(c1);
	c1Elements.removeClass(c1).addClass(c2);
	return this;
	},
	
	
	/**
	 * @public
	 * Creating list items for tree
	 * @param None
	 * @return None
	 */
	
	to_ul : function(branches, depth, viewLevel) 
	{
		if(branches != undefined)
		{
			var strLabel = "";
			var nLinkIndex = 0;
			var preDepth = 0;
			var ispreDepth = false;
			depth += 1;
			if(level>0)
			{
				strList = strList + '<ul style="display:none;clear:both;" class="depth'+depth+'">';
			}
			if (bFirstTime == true) 
			{
				bFirstTime = false;
			}
			else if(branches.li != undefined)
			{
				branches = branches.li;
			}
			if(branches.length == undefined)
			{
				var branch = branches;
				
				if(branch.id ==  null || branch.id.trim() == "")
					throw new Error("TOC Malformed: Branch ID missing");
				     
				if (branch.li) 
				{   
					if(branch.a.type != "hidden")
					{
						strLabel = (branch.a != undefined)?branch.a.text : "";
						nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
						
	//					strList = strList + '<li style="overflow:hidden;" class="expandable depth'+depth+'" id=""  type="" >';
						strList = strList + '<li title="'+getTextWithoutTags(strLabel)+'" style="overflow:hidden;" hotkeygroupid="TOCMenu" class="expandable depth'+depth+'" id="'+branch.id+'" type="" linkIndex="'+ nLinkIndex + '"  levelId= "'+ depth + '" >';
						//strList = strList + '<div class="depth'+depth+'-divider"></div>';
						strList = strList + '<div class="depth'+depth+'-nodeContentCont nodeContentCont viewlevel-' + viewLevel + '">';
						strList = strList + '<div class="nodeContent">';
						strList = strList + '<div class="firstnode depth'+depth+'-nodeIcon" id=""  type=""></div>';
						
						
						//this.storePageLink(branch.a);
						if(depth==4)
							strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,275,'depth' + depth + '-nodeText') + '</div>';
						else
							strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,255,'depth' + depth + '-nodeText') + '</div>';
						strList = strList + '</div>';
	//					strList = strList + '<div id="overlayDiv" class="depth'+depth+'" style="opacity:0.5; position:relative; left:0px; right:0px;  height:45px;"></div>';
						strList = strList + '</div>';
							strList = strList + '<div class="hitarea expandable-hitarea" id="arrowIcon">';
						strList = strList + '</div>';
						
						level=level+1;
					}
					this.to_ul(branch, depth, (viewLevel + 1));
				}
				else
				{
					//depth = 4;
					strLabel = (branch.a != undefined)?branch.a.text : "";
					nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
					if(branch.a.type != "hidden")
					{	
	//					strList = strList + '<li style="overflow:hidden;" id="" class="leaf depth'+depth+'" isActiveLink="true" linkIndex="'+ nLinkIndex + '">';
						strList = strList + '<li title="'+getTextWithoutTags(strLabel)+'" style="overflow:hidden;" hotkeygroupid="TOCMenu" class="leaf depth'+depth+'" isActiveLink="true" linkIndex="'+ nLinkIndex + '" id="'+branch.id+'" levelId= "'+ depth + '">';
						//strList = strList + '<div class="depth'+depth+'-divider"></div>';
						strList = strList + '<div class="depth'+depth+'-nodeContentCont">';
						strList = strList + '<div class="nodeContent">';
	
	
						strList = strList + '<div class="firstnode depth'+depth+'-nodeIcon" id=""  type=""></div>';
	
						//strLabel = (branch.a != undefined)?branch.a.text : "";
						
						//this.storePageLink(branch.a);
						strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,275,'depth' + depth + '-nodeText') + '</div>';
						strList = strList + '</div>';
	//					strList = strList + '<div id="overlayDiv" class="depth'+depth+'" style="opacity:0.5; position:relative; left:0px; right:0px;  height:45px;"   onclick="treeItemClick()"></div>';
						strList = strList + '</div>'; 
					}
					if(branch.a.type != "hidden")
						strList = strList +'</li>';
					level=0;
				}
			}
			else
			{
				
				for (var i=0; i<branches.length; i++) 
				{
					var branch = branches[i]; 
					
					if(branch.id ==  null || branch.id.trim() == "")
						throw new Error("TOC Malformed: Branch ID missing");
					
					//identify actual depth (starting from page towards unit)
					//var iBranchLevels = this.identifyMaxDepth(branch, 1);
					//depth = 4 - iBranchLevels + 1;
					
					if (branch.li) 
					{
						if(ispreDepth)
						{
							ispreDepth = false;
							depth=preDepth;		
						}
						if(branch.a.type != "hidden")
						{
							strLabel = (branch.a != undefined)?branch.a.text : "";
							nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
							
	//						strList = strList + '<li style="overflow:hidden;" class="expandable depth'+depth+'" id=""  type="" >';
							strList = strList + '<li title="'+getTextWithoutTags(strLabel)+'" style="overflow:hidden;" hotkeygroupid="TOCMenu" class="expandable depth'+depth+'" id="'+branch.id+'"  type=""  linkIndex="'+ nLinkIndex + '"  levelId= "'+ depth + '">';
	
							//strList = strList + '<div class="depth'+depth+'-divider"></div>';
	
							strList = strList + '<div class="depth'+depth+'-nodeContentCont nodeContentCont viewlevel-' + viewLevel + '">';
							strList = strList + '<div class="nodeContent">';
	
							strList = strList + '<div class="firstnode depth'+depth+'-nodeIcon" id=""  type=""></div>';
							
							//this.storePageLink(branch.a);
							if(depth==4)
								strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,275,'depth' + depth + '-nodeText') + '</div>';
							else
								strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,255,'depth' + depth + '-nodeText') + '</div>';
							strList = strList + '</div>';
	//						strList = strList + '<div id="overlayDiv" class="depth'+depth+'" style="opacity:0.5; position:relative; left:0px; right:0px;  height:45px;"></div>';
							strList = strList + '</div>';
								strList = strList + '<div class="hitarea expandable-hitarea" id="arrowIcon">';
							strList = strList + '</div>';
	
							level=level+1;
						}						
						this.to_ul(branch,depth, (viewLevel + 1));
					}
					else
					{
						if(depth>1)
						{
							if(!ispreDepth)
							{
								preDepth = depth;
								ispreDepth = true;
							//	depth=4;
							}
						}
						strLabel = (branch.a != undefined)?branch.a.text : "";
						nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
						if(branch.a.type != "hidden")
						{
	//						strList = strList + '<li style="overflow:hidden;" id=""  type=""  class="leaf depth'+depth+'" isActiveLink="true" linkIndex="'+ nLinkIndex + '">';
							strList = strList + '<li title="'+getTextWithoutTags(strLabel)+'" style="overflow:hidden;" hotkeygroupid="TOCMenu" type=""  class="leaf depth'+depth+'" isActiveLink="true" linkIndex="'+ nLinkIndex + '" id="'+branch.id+'" levelId= "'+ depth + '">';
							//strList = strList + '<div class="depth'+depth+'-divider"></div>';
	
							strList = strList + '<div class="depth'+depth+'-nodeContentCont">';
							strList = strList + '<div class="nodeContent">';
                            strList = strList + '<div class="firstnode depth' + depth + '-nodeIcon" id=""  type=""></div>';
							
							if(depth==4)
								strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,275,'depth' + depth + '-nodeText') + '</div>';
							else
								strList = strList + '<div class="depth' + depth + '-nodeText" id="" type="">' + getTextWithEllipsis(2,strLabel,255,'depth' + depth + '-nodeText') + '</div>';
							strList = strList + '</div>';
	//						strList = strList + '<div id="overlayDiv" class="depth'+depth+'" style="opacity:0.5; position:relative; left:0px; right:0px;  height:45px;"   onclick="treeItemClick()"></div>';
							strList = strList + '</div>';
							//strList = strList + '<div class="hitarea1 expandable-hitarea1">';
							//strList = strList + '</div>';
						}
						
					}
					if(branch.a.type != "hidden")
						strList = strList +'</li>';
					level=0;
				}    
			}
			strList =strList + '</ul>';
			
			return;
		}
		
		throw new Error("TOC empty")
	},
	
	/**
	   * This function calculates the number of levels a branch is divided into.
	   * @param (Object) branch
	   * @param (int) Starting depth
	   * @return (int) number of levels
	   */
	identifyMaxDepth : function(branch, iDepth) {
		if (branch.li == null) {
			return iDepth;
		} else {
			iDepth++;

			if (branch.li.length == null) {
				if (branch.li.li) {
					iDepth = this.identifyMaxDepth(branch.li, iDepth);
					return iDepth;
				} else {
					return iDepth;
				}
			} else {
				for (var i = 0; i < branch.li.length; i++) {
					var objSubBranch = branch.li[i];
					if (objSubBranch.li) {
						iDepth = this.identifyMaxDepth(objSubBranch, iDepth);
						return iDepth;
					} else {
						return iDepth;
					}
				}
			}
		}
	},
	
	/**
	 * @public
	 * Rendering tree menu
	 * @param None
	 * @return None
	 */
	renderTree: function(objHiddenTree,objTree, bAllPagesInBookAreHidden) 
	{  
		var objThis = this;
		var depth = 0;
		this.pageCount(objHiddenTree.li,depth, depth);
		
		if(bAllPagesInBookAreHidden == false)
		{
			this.to_ul(objTree,depth, depth);
		}
		
		$("#curTree").html('<ul class="treeview" id="tree">'+strList+'</ul>');
		
		var onToggle = function(divId)
		{
            objThis.dispatchCustomEvent(objThis.events.UPDATE_TREE_VIEW_SIZE, divId);
		};
		
		$("#tree").treeview({
			collapsed: true,
			animated: "medium",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "",
			toggle: onToggle
		});
		objThis.updateHotKeyIndex();
	},
	
	/**
	 * @public
	 * Calculating the page count
	 * @param None
	 * @return None
	 */
	
	pageCount : function(branches, depth, viewLevel) 
	{
		if(branches != undefined)
		{
			var nLinkIndex = 0;
			
			if (bFirstTime == true) 
			{
				bFirstTime = false;
			}
			else if(branches.li != undefined)
			{
				branches = branches.li;
			}
			
			if(branches.length == undefined)
			{
				var branch = branches;
				     
				if (branch.li) 
				{   
					nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
					level=level+1;
					this.pageCount(branch, depth, (viewLevel + 1));
				}
				else
				{
					nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
					level=0;
				}
			}
			else
			{
				
				for (var i=0; i<branches.length; i++) 
				{
					var branch = branches[i]; 
					
					//identify actual depth (starting from page towards unit)
					var iBranchLevels = this.identifyMaxDepth(branch, 1);
					depth = 4 - iBranchLevels + 1;
					
					if (branch.li) 
					{
						nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
						level=level+1;
						this.pageCount(branch,depth, (viewLevel + 1));
					}
					else
					{
						nLinkIndex = this.storePageLinkAndGetIndex(branch.a);
					}
					level=0;
				}    
			}
		}
	},
	
	storePageLinkAndGetIndex: function(strData)
	{
		var nIndex = -1;
		if(strData != undefined)
		{
			//if href is blank, throw an error;
			if(strData.href == null || strData.href.trim() == "")
				throw new Error("TOC Malformed: Page reference is blank")
			
			strUrl = strData.href.split("#")[0];
			if(this.linkList.indexOf(strUrl) == -1)
			{
				nIndex = this.linkList.length;
				this.linkList.push(strUrl);
				
			}
			else
			{
				nIndex = this.linkList.indexOf(strUrl);
			}
			 
		}
		return nIndex;
	},
	
	
	updateHotKeyIndex : function() {
		var strList = $(".treeview").children();
		var arrHotKey = $("#tocMainPanel").find('[hotkeyindex]').not('[levelid]');
		var hotId = findLastHotKeyIndex( arrHotKey );
		$("#tocGotoInputBox").attr("hotkeyindex", hotId );
		if (EPubConfig.ReaderType.toLowerCase() == '2to5')
		{
			hotId++;
			$("#tocGotoBtn").attr("hotkeyindex", hotId );
		}
		hotId++;
		for(var i=0;i<$(strList).length;i++)
		{
			$($(strList)[i]).attr("hotkeyindex",hotId);
			hotId++
			
			var strChildren = $($($(strList)[i]).find("ul")[0]).children()
			for(var k=0;k<$(strChildren).length;k++)
			{
				if($($(strList)[i]).hasClass("collapsable"))
				{
					$($(strChildren)[k]).attr("hotkeyindex",hotId);
					hotId++;
				}
				else
					$($(strChildren)[k]).attr("hotkeyindex","");
				
				var strChildren1 = $($($(strChildren)[k]).find("ul")[0]).children()
				for(var j=0;j<$(strChildren1).length;j++)
				{
					if($($(strChildren)[k]).find('#arrowIcon').hasClass('collapsable-hitarea'))
					{
						$($(strChildren1)[j]).attr("hotkeyindex",hotId);
						hotId++;
					}
					else
						$($(strChildren1)[j]).attr("hotkeyindex","");
					
					var strChildren2 = $($($(strChildren1)[j]).find("ul")[0]).children()
					for(var l=0;l<$(strChildren2).length;l++)
					{
						if($($(strChildren1)[j]).hasClass("collapsable"))
						{
							$($(strChildren2)[l]).attr("hotkeyindex",hotId);
							hotId++;
						}
						else
							$($(strChildren2)[l]).attr("hotkeyindex","");
						
					}
				}
			}
			
		}
		setTimeout(function(){
			HotkeyManager.findAndSetFocus("#"+currentNavId);	
		},100);
		
	},
	
	collapseAll : function() {
		$("#tree").collapseAll();
	}
	
}}