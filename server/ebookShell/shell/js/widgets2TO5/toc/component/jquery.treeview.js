var isAnimation = false;
;(function($) {

	$.extend($.fn, {
		
		swapClass : function(c1, c2) {
			var c1Elements = this.filter('.' + c1);
			this.filter('.' + c2).removeClass(c2).addClass(c1);
			c1Elements.removeClass(c1).addClass(c2);
			return this;
		},
		replaceClass : function(c1, c2) {
			return this.filter('.' + c1).removeClass(c1).addClass(c2).end();
		},
		hoverClass : function(className) {
			className = className || "hover";
			return this.hover(function() {
				$(this).addClass(className);
			}, function() {
				$(this).removeClass(className);
			});
		},
		heightToggle : function(animated, callback, scrollFunction) {/* Adding one more parameter-'scrollFunction', to refresh scroll on tree.*/
			animated ? this.animate({
				height : "toggle"
			}, animated, function() {
				if (scrollFunction)
					scrollFunction.call();
				if (callback)
					callback.call();
			}) : this.each(function() {
				//jQuery(this)[ jQuery(this).is(":hidden") ? "show" : "hide" ]();
				if (jQuery(this).css('display') == 'none') {
					jQuery(jQuery(this).parent()).find(">.hitarea").removeClass(CLASSES.expandableHitarea)
					.addClass(CLASSES.collapsableHitarea);
					jQuery(this).end();
					jQuery(this).find('li').show();	
					isAnimation = true;
					jQuery(this).slideDown('500',function(){isAnimation = false;});
					jQuery(this).find('[id="arrowIcon"]').show();
				} else {
					jQuery(jQuery(this).parent()).find(">.hitarea").removeClass(CLASSES.collapsableHitarea)
					.addClass(CLASSES.expandableHitarea);
					jQuery(this).end();
					isAnimation = true;
					jQuery(this).slideUp('500',function(){isAnimation = false;});
					jQuery(this).find('li').hide();	
					jQuery(this).find('[id="arrowIcon"]').hide();		
				}
				if (callback)
					callback.apply(this, arguments);
			});
		},
		heightHide : function(animated, callback) {
			if (animated) {
				this.animate({
					height : "hide"
				}, animated, callback);
			} else {
				this.hide();
				if (callback)
					this.each(callback);
			}
		},
		prepareBranches : function(settings) {
			if (!settings.prerendered) {
				// mark last tree items
				this.filter(":last-child:not(ul)").addClass(CLASSES.last);
				// collapse whole tree, or only those marked as closed, anyway except those marked as open
				this.filter((settings.collapsed ? "" : "." + CLASSES.closed) + ":not(." + CLASSES.open + ")").find(">ul").hide();
			}
			// return all items with sublists
			return this.filter(":has(>ul)");
		},
		applyClasses : function(settings, toggler) {
			this.filter(":has(>ul):not(:has(>a))").find(">span").click(function(event) {
				toggler.apply($(this).next());
			}).add($("a", this)).hoverClass();

			if (!settings.prerendered) {
				// handle closed ones first
				this.filter(":has(>ul:hidden)").addClass(CLASSES.expandable).replaceClass(CLASSES.last, CLASSES.lastExpandable);

				// handle open ones
				this.not(":has(>ul:hidden)").addClass(CLASSES.collapsable).replaceClass(CLASSES.last, CLASSES.lastCollapsable);

				// create hitarea
				this.prepend("<div class=\"" + CLASSES.hitarea + "\"/>").find("div." + CLASSES.hitarea).each(function() {
					var classes = "";
					$.each($(this).parent().attr("class").split(" "), function() {
						classes += this + "-hitarea ";
					});
					$(this).addClass(classes);
				});
			}

			// apply event to hitarea
			this.find("div." + CLASSES.hitarea).click(toggler);
			this.find("div." + "nodeContentCont").click(toggler);
		},
		
		/**
		   * This function collapses all the nodes in TOC
		   * @param none
		   * @return void	
		   */
		collapseAll: function()
		{
			$("#curTree")
				//set hit areas
				.find(".hitarea")
					.removeClass(CLASSES.lastCollapsableHitarea)
					.addClass(CLASSES.lastExpandableHitarea )
					
					.removeClass(CLASSES.collapsableHitarea)
					.addClass(CLASSES.expandableHitarea)
				.end()
				
				.removeClass(CLASSES.collapsable)
				.addClass(CLASSES.expandable)
				
				// find child lists and hide them
				.find("ul")
					.css("display", "none")
				.end();
			
			//reset all nodes on all depths
			for(var i = 1; i <= 4; i++)
			{
				$("#curTree")
					.find(".depth" + i)
						.removeClass(CLASSES.collapsable)
						.addClass(CLASSES.expandable)
					.end()
					
					.find(".depth" + i + "-nodeContentContToggle")
						.removeClass("depth" + i + "-nodeContentContToggle")
						.addClass("depth" + i + "-nodeContentCont")
					.end()
			}
			
			//specifically set ul with class treeview to display block because all ul in curtree has been set to display none.	
			$(".treeview").css("display","block");
			
			//now since all nodes are collapsed, setting objPreviouslyToggledNode to null so that node toggling works fine when panel is opened
			objPreviouslyToggledNode = null;
		},
			
		treeview : function(settings) {
			settings = $.extend({
				cookieId : "treeview"
			}, settings);

			if (settings.add) {
				return this.trigger("add", [settings.add]);
			}

			if (settings.toggle) {
				var callback = settings.toggle;
				settings.toggle = function() {
					return callback.apply($(this).parent()[0], arguments);
				};
			}

			// factory for treecontroller
			function treeController(tree, control) {
				// factory for click handlers
				function handler(filter) {
					return function() {
						// reuse toggle event handler, applying the elements to toggle
						// start searching for all hitareas
						toggler.apply($("div." + CLASSES.hitarea, tree).filter(function() {
							// for plain toggle, no filter is provided, otherwise we need to check the parent element
							return filter ? $(this).parent("." + filter).length : true;
						}));
						return false;
					};
				}

				// click on first element to collapse tree
				$("a:eq(0)", control).click(handler(CLASSES.collapsable));
				// click on second to expand tree
				$("a:eq(1)", control).click(handler(CLASSES.expandable));
				// click on third to toggle tree
				$("a:eq(2)", control).click(handler());
			}

			function refreshScroll() {
				if (myScroll != undefined && myScroll != 'undefined' && myScroll != null && myScroll != "")
					myScroll.refresh();
			}
			
			var ob = this;
			
			
			
			// handle toggle event
			function toggler() {
				if(isAnimation)
					return;
				//identify if current node is top level container node or not;
				var isCurrentlyClickedNodeLevel1Node = $(this).hasClass("viewlevel-0");
				
				var fnToggle = function(objThis)
				{
					$(objThis)
					.parent()
					// swap classes for hitarea
					.find(">.nodeContentCont")
					.swapClass( CLASSES.depth2nodeContentCont, CLASSES.depth2nodeContentContT )
					.swapClass( CLASSES.depth1nodeContentCont, CLASSES.depth1nodeContentContT )
					.end()

					.find(">.nodeContentCont>.nodeContent>.firstnode")
					.swapClass( CLASSES.depth1nodeIcon, CLASSES.depth1nodeIconT )
					.end()

					.find(">.hitarea")
						.swapClass( CLASSES.collapsableHitarea, CLASSES.expandableHitarea )
						.swapClass( CLASSES.lastCollapsableHitarea, CLASSES.lastExpandableHitarea )

					.end()
					// swap classes for parent li
					.swapClass( CLASSES.collapsable, CLASSES.expandable )
					.swapClass( CLASSES.depth1nodeIcon, CLASSES.depth1nodeIconT )
					.swapClass( CLASSES.lastCollapsable, CLASSES.lastExpandable )
					// find child lists
					.find( ">ul" )
					// toggle them
					.heightToggle( false, settings.toggle, refreshScroll );
					
					if ( settings.unique ) {
						$(objThis).parent()
							.siblings()
	
						.find(">.nodeContentCont")
						.replaceClass( CLASSES.depth2nodeContentCont, CLASSES.depth2nodeContentContT )
						.end()
	
	
						.find(">.nodeContentCont>.nodeContent>.firstnode")
						.replaceClass( CLASSES.depth1nodeIcon, CLASSES.depth1nodeIconT )
						.end()
	
	
						// swap classes for hitarea
						.find(">.hitarea")
							.replaceClass( CLASSES.collapsableHitarea, CLASSES.expandableHitarea )
							.replaceClass( CLASSES.lastCollapsableHitarea, CLASSES.lastExpandableHitarea )
						.end()
						.replaceClass( CLASSES.collapsable, CLASSES.expandable )
						.replaceClass( CLASSES.lastCollapsable, CLASSES.lastExpandable )


						.find( ">ul" )
						.heightHide( settings.animated, settings.toggle );
					}
				}
				
				//checking if it's somehow "this" object toggles content of view-level 0;
				if (!isCurrentlyClickedNodeLevel1Node) {
					var tempArr = $($(this).parent()[0].innerHTML);

					for (var i = 0; i < tempArr.length; i++) {
						var tempElement = tempArr[i];

						if ($(tempElement).hasClass("viewlevel-0")) {
							isCurrentlyClickedNodeLevel1Node = true;
							break;
						}
					}
				}
				
				//toggle previously opened top level container node if current node is top level node and is not the same as previously toggled node;
				if (objPreviouslyToggledNode && objPreviouslyToggledNode != this && isCurrentlyClickedNodeLevel1Node) {
					fnToggle(objPreviouslyToggledNode);
				}
				
				//toggle current node;
				fnToggle(this);

				//if current node is top level node, then set the value of previously toggled node;
				if (isCurrentlyClickedNodeLevel1Node) {
					//if current node is same as the previous node, set previous node to null
					if (objPreviouslyToggledNode == this) {
						objPreviouslyToggledNode = null;
					}
					//if current node is not the same as previous node, set previous node to current node
					else {
						objPreviouslyToggledNode = this;
					}
				}
				
				//call the callback function;
				settings.toggle($(this).parent().attr("id"));
			}

			function serialize() {
				function binary(arg) {
					return arg ? 1 : 0;
				}

				var data = [];
				branches.each(function(i, e) {
					data[i] = $(e).is(":has(>ul:visible)") ? 1 : 0;
				});
				$.cookie(settings.cookieId, data.join(""));
			}

			function deserialize() {
				var stored = $.cookie(settings.cookieId);
				if (stored) {
					var data = stored.split("");
					branches.each(function(i, e) {
						$(e).find(">ul")[ parseInt(data[i]) ? "show" : "hide" ]();
					});
				}
			}

			// add treeview class to activate styles
			this.addClass("treeview");

			// prepare branches and find all tree items with child lists
			var branches = this.find("li").prepareBranches(settings);

			switch(settings.persist) {
				case "cookie":
					var toggleCallback = settings.toggle;
					settings.toggle = function() {
						serialize();
						if (toggleCallback) {
							toggleCallback.apply(this, arguments);
						}
					};
					deserialize();
					break;
				case "location":
					var current = this.find("a").filter(function() {
						return this.href.toLowerCase() == location.href.toLowerCase();
					});
					if (current.length) {
						current.addClass("selected").parents("ul, li").add(current.next()).show();
					}
					break;
			}

			branches.applyClasses(settings, toggler);

			// if control option is set, create the treecontroller and show it
			if (settings.control) {
				treeController(this, settings.control);
				$(settings.control).show();
			}

			return this.bind("add", function(event, branches) {
				$(branches).prev().removeClass(CLASSES.last).removeClass(CLASSES.lastCollapsable).removeClass(CLASSES.lastExpandable).find(">.hitarea").removeClass(CLASSES.lastCollapsableHitarea).removeClass(CLASSES.lastExpandableHitarea);
				$(branches).find("li").andSelf().prepareBranches(settings).applyClasses(settings, toggler);
			});
		}
	});
	
	// classes used by the plugin
	// need to be styled via external stylesheet, see first example
	var CLASSES = $.fn.treeview.classes = {
		open : "open",
		closed : "closed",
		expandable : "expandable",
		depth1nodeIcon : "depth1-nodeIcon",
		depth1nodeIconT : "depth1-nodeIconToggle",
		expandableHitarea : "expandable-hitarea",
		lastExpandableHitarea : "lastExpandable-hitarea",
		depth2nodeContentContT : "depth2-nodeContentContToggle",
		depth2nodeContentCont : "depth2-nodeContentCont",
		depth1nodeContentContT : "depth1-nodeContentContToggle",
		depth1nodeContentCont : "depth1-nodeContentCont",
		depth3nodeContentContT : "depth3-nodeContentContToggle",
		depth3nodeContentCont : "depth3-nodeContentCont",
		collapsable : "collapsable",
		collapsableHitarea : "collapsable-hitarea",
		lastCollapsableHitarea : "lastCollapsable-hitarea",
		lastCollapsable : "lastCollapsable",
		lastExpandable : "lastExpandable",
		last : "last",
		hitarea : "hitarea"
	};

	// provide backwards compability
	$.fn.Treeview = $.fn.treeview;

})(jQuery); 

var objPreviouslyToggledNode = null;