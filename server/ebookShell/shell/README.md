# HTML5 Shell

To get this running locally, all we need is a web server program running in the root directory of this repository. 
Anything will do, even Apache. If you don't have a web server, we will get one from npm. Please follow these instructions:

### Prerequisites
  - You will need to have [Node.js] installed.

### Installation

Install the [http-server] module, globally:

```sh
$ npm install http-server -g
```

### Usage

Run the server from the command-line using the following command, using the filepath to the directory that contains your project's index.html file. For example:
```sh
$ cd html5_shell
$ http-server
```
The program defaults to port 8080. Hit [http://127.0.0.1:8080] in your favourite browser and you should be up and running.

[node.js]:http://www.nodejs.org/
[http-server]:https://www.npmjs.com/package/http-server

