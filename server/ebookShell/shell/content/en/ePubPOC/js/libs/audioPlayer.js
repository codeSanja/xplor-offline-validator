/**
 * @author lakshay.ahuja
 */
var GlobalVar_VolumeLevel = 1.0;
var ignore_timeupdate = false;
var currentTimeAudio = 0;
var audioInterval = null;
var audioTimeOut = null;
var audioIncrTime = 0.001;
var isSeeked = null;
var jplayerCurrTime = 0.000;

var seekStartTime;
var mediaElement;
var progressBarSlider;
var volumeBarSlider;
var currentTime;
var volumeSliderValue;
var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
	
	function _init() {
		var self = this;
		seekStartTime = 0;
		
		mediaElement = $('.jp-jplayer');
		progressBarSlider = $('[id=progressBarSlider]');
		volumeBarSlider = $('[id=volumeSlider]');
		
		$(".audiotrack").unbind("click").bind("click", function(e){
			e.stopPropagation();
			stopPlaying();
			reset();
			if(ismobile)
			{
				$("#volumeContainerList").css("display", "none");
				$('[id=jp_interface_1]').css('width','320px');
				$(".players").css("width","320px");
				$(".audioPanelSeparator").css("display","none");
			}
			var srcid = $(this).attr("data-src");
			var src = $("#"+srcid).attr("src");
		 	$(".players").css("display","block");
		 	setContent(src);
		 	
			
		});
		
		$(".audioPlayerCloseBtn").unbind("click").bind("click", function(e){
			stopPlaying();
			reset();
			$(".players").css("display","none");
		});
		
		$("#jp-stop-parent").unbind("click").bind("click", function(e){
			stopPlaying();
			
		});
		$("#jp-play-parent").unbind("click").bind("click", function(e){
			startPlaying();
			e.preventDefault();
		});
		$('[id=jp-volume-container]').bind('mouseenter', function() {
			$('[id=jp-volume-container]').css('display', 'block');
		});
		
		$('[class=jp-unmute]').parent().bind('click mouseenter', function() {
			$('[id=jp-volume-container]').css('display', 'block');
		});

		$('[id=jp-volume-container]').bind('mouseleave', function() {
			$('[id=jp-volume-container]').css('display', 'none');
		});
		
		$('[class=jp-unmute]').parent().bind('mouseleave', function() {
			$('[id=jp-volume-container]').css('display', 'none');
		});
		
		$(progressBarSlider).slider({
			//Config
			range : "min",
			min : 1,
			value : 0,
			max : 100,
			//animate: true,
			start : function(event, ui) {
			},

			//Slider Event
			slide : function(event, ui) {//When the slider is sliding
				$(mediaElement).jPlayer("pause");
			},

			stop : function(event, ui) {
				if ($(progressBarSlider).slider('value') == 100) {
					
					$(mediaElement).jPlayer("stop");
					
				} else {
					$(mediaElement).jPlayer("playHead", $(progressBarSlider).slider('value'));
					$(mediaElement).jPlayer("play");
					
				}
			},
		});

		$(volumeBarSlider).slider({
			orientation : "vertical",
			range : "min",
			min : 0,
			max : 100,
			value : 0,
			//animate: true,
			slide : function(event, ui) {
				$(mediaElement).jPlayer("volume", ui.value / 100);
                volumeSliderValue = ui.value;
			},
			stop : function(event, ui) {
			},
		});
		$($(".players").find("a")).removeAttr("href");
	}
	
	
	
	/**
	 * To set the media file associated with the audio hotspot.
	 * @param {Object} mediaSrc : the media file the player will play.
	 * @param {float/Number} fStartDuration: the duration from where the audio has to start
	 */
	function setContent(mediaSrc, fStartDuration) {
		var self = this;
		clearInterval(audioInterval);
		clearTimeout(audioTimeOut);
		audioIncrTime= 0;
		$(mediaElement).jPlayer("destroy");
		$(volumeBarSlider).slider('value', 100);
		if (mediaSrc == undefined) {
			var mediaSrc = GlobalModel.audioSrc;
		}
		
		var oggMediaSrc = mediaSrc.replace('mp3','ogg');

		var srcArr = mediaSrc.split("/");
		var src = srcArr[srcArr.length-1];
		// .mp3 format of audio files.
		$(mediaElement).jPlayer({
			ready : function() {
				console.log(mediaSrc);
				$(this).jPlayer("setMedia", {
					mp3 : mediaSrc,
					oga: oggMediaSrc
				}).jPlayer("play", fStartDuration); // auto play
			},

			play : function() {
				$('#jp-pause-parent').css('display','block');
			},
			
			error: function(e)
			{
				console.log(e.jPlayer.error.message)
			},
			
			progressbarclicked: function()
			{
				$(mediaElement).jPlayer("pause");
				
			},
			timeupdate : function(event) {
                
                jplayerCurrTime = event.jPlayer.status.currentTime;
                if(audioInterval)
				{
					clearInterval(audioInterval);
				}
				if(audioTimeOut)
				{
					clearTimeout(audioTimeOut);
				}	
				var ptime = event.jPlayer.status.currentTime;
				var ctime = currentTime;
				
				if(ctime!=0)
				{
					audioIncrTime = ((ptime - ctime) / ctime)/1000 + audioIncrTime;
					
					if(audioIncrTime<0)
					{
						audioIncrTime = audioIncrTime*(-1);
					}
					
				}
				
				
				currentTime = event.jPlayer.status.currentTime;
                /*
                 * event.jPlayer.status.currentPercentAbsolute returns 0 sometimes in iOS. Thus value of progressbar will not update in this case if we use event.jPlayer.status.currentPercentAbsolute. Hence, using percentage value of jp=playbar to update slider value.
                 */
                
                var jpPlayBar = $('[class=jp-play-bar]');
                var sliderValue = ($(jpPlayBar).width()/$(jpPlayBar).parent().width()) * 100;
                if(sliderValue || sliderValue == 0){
                    $(progressBarSlider).slider("value", sliderValue);
                }
				
				currentTimeAudio = event.jPlayer.status.currentPercentAbsolute;
				
				if(audioInterval)
				{
					clearInterval(audioInterval);
				}
				if(audioTimeOut)
				{
					clearTimeout(audioTimeOut);
				}	
				audioInterval = setInterval(function(){
					currentTime += audioIncrTime;
				},1);
				audioTimeOut = setTimeout(function(){
					clearInterval(audioInterval);
				},100);
			},
			volumechange : function(event) {
				if (false) {
					$(volumeBarSlider).slider("value", 0);
				} else {
                    if(event.jPlayer.status.volume == 0){
                        $('[class=jp-unmute]').css('opacity',0.4);
                    }
                    else{
                        $('[class=jp-unmute]').css('opacity',1);
                    }
					$(volumeBarSlider).slider("value", event.jPlayer.status.volume * 100);
					volumeSliderValue = event.jPlayer.status.volume*100; // to retain the state of the volume bar when page changes
				}
			},
			ended : function() {
				currentTime = 0;
				clearInterval(audioInterval);
				clearTimeout(audioTimeOut);
				audioIncrTime= 0;
				$(progressBarSlider).slider('value', 0);
				$(mediaElement).jPlayer('stop');
				//$('[class=jp-stop]').parent().addClass('ui-disabled');
				//$(progressBarSlider).addClass('ui-disabled');
				//$('[class=jp-progress]').addClass('ui-disabled');
                $('#jp-pause-parent').css('display','none');
			},
			pause : function(event) {
			},
			swfPath : "js/widgets/audioPlayer/component",
			supplied : "mp3,oga",
			volume : (100/100),
		}).bind($.jPlayer.event.play, function() {// pause other instances of player when current one play
			$(mediaElement).jPlayer("pauseOthers");
		});
	}
	


	/**
	 * This function is called when the play button is clicked. Called from mediaplayer operator.
	 */
	function startPlaying() {

		var self = this;
		// this variable is used to stop multiple calls of "canplaythrough" method of jplayer.
		/**
		 * If panel play button is clicked to play audio
		 * Starts here
		 */
		
		//$(progressBarSlider).removeClass('ui-disabled');
		//$('[id=jp-progress]').removeClass('ui-disabled');
		//$('[class=jp-stop]').parent().removeClass('ui-disabled');
		$(mediaElement).jPlayer("play");

		/**
		 * Ends here
		 */

	}

	/**
	 * This player is called when we have to stop the play event.
	 */
	function stopPlaying() {
		var self = this;
        $(progressBarSlider).slider('value', 0);
		$(mediaElement).jPlayer("stop");
		$(volumeBarSlider).slider('value', volumeSliderValue);
		
		currentTime = 0;
	}
    
	
	

	function reset() {
		$(mediaElement).jPlayer("clearMedia");
		$(mediaElement).jPlayer("destroy");

	}
	
	
	

	/**
	 * This functions seeks the audio to specified position
 	 * @param {Number} fPosition
 	 * @return void
	 */
	function seekToPosition(fPosition) {
		var nSeekPercent = fPosition / event.jPlayer.status.duration * 100;

		$(mediaElement).jPlayer("pause");
		$(mediaElement).jPlayer("playHead", nSeekPercent);
	}
	
	/**
	 * This functions pauses the currently playing audio.
 	 * @param none
 	 * @return void
	 */
	function pause()
	{
		currentTime = jplayerCurrTime;
		audioIncrTime = 0.001;
		$(mediaElement).jPlayer("pause");
	}
	
_init();