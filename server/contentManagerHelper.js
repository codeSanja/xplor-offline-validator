/**
 * Created by DugganJ on 14/10/2015.
 */
var fs = require('fs-extra');
var tarball = require('tarball-extract');
var q = require('q');
var path = require('path');
var untarFolderPath = path.join(__dirname, './untar');


function untar(thePath, folderName) {
    var defer = q.defer();
    if(!fs.existsSync(thePath)){
        throw "File does not exist: " + thePath;
    }

    var specUntarFolderPath = path.join(untarFolderPath,folderName);

    if (!fs.existsSync(specUntarFolderPath)){
        fs.mkdirSync(specUntarFolderPath);
    }

    tarball.extractTarball(thePath, specUntarFolderPath, function (err, result) {
        if (err) {
            return defer.reject(err);
        }
        defer.resolve(true);
    });
    return defer.promise;
};

module.exports.untar = untar;